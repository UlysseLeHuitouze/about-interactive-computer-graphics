# GPU-driven Models

## Introduction

Interactive computer graphics have come a long way from OpenGL 2.0- and direct-mode rendering back then. Modern graphic API such as Vulkan, Metal and DirectX12 offer substantially more possibilities at the cost of an exponential increase in rendering engine complexity. These possibilities include notably fully customizable shaders, render passes, etc, all that with the goal of moving as many things as possible on the GPU without having any application knownledge. Indeed, sending commands anytime with ordering constraints to a device as asynchronous as a GPU isn't exactly free and makes grouping/buffering/"batching" operations together before sending them the key to performance. However, there is something even better than "batching" and that is caching: One can easily imagine that having an "object" (whatever that means) already sitting on the GPU is several times better latency-wise than sending it through the PCI bus every time it's needed; The usual trade-off memory usage/computation time if you will. This fact is already well-established for textures, as having to send an entire texture to the GPU every time you want to draw a triangle would make GPUs unsuitable for graphics (at least for the shading part). What about models, i.e. vertex, texture and lightning information?

## What resources are expensive, GPU-wise, when doing graphics?

When phrased like that, the answer is GPU memory for sure. Another possible answer is warps, but unless you accidentally clone your scene's objects 1^6 times or turn on 64× supersampling, that shouldn't be your main concern.

Since textures are a fundamental part of computer graphics, those are not stored raw on the GPU but rather compressed and decompressed with hardware support. As for models, these are render engine-specific (though [some standards are progressively being adopted](https://www.khronos.org/gltf/)) so there is no hardware support whatsoever, and what's more, in your everyday render engine, models can just be that large to immediatly discard the thought of putting all of them in the GPU memory.

## Are your model too large to ever consider putting them in GPU-local memory?

If so, a common approach to reduce latency is to pass low-resolution versions of your models to the GPU at draw time and have the GPU then expand them somewhere in the pipeline (commonly called "tessellation" stage). That way, minimal information is passed from the CPU to the first stage of the graphic pipeline (commonly called "vertex" stage) on the GPU. The GPU can then deduced automatically the more detailed geometry from the rough model. See [Hair Simulation](https://github.com/clach/Realtime-Vulkan-Hair) for a concrete example.

Doing the above also has the benefits of being able to freely control the degree of model expansion, making level of detail rendering trivial to implement once the above is functional.

## Are your model relatively small, e.g. for a lightweight 3D software or a voxel engine cf. Minecraft

If it seems realistic to have all your models sitting in GPU-local memory, then it's most likely a good idea to do so and put them in GPU-local memory just like textures. What you have to pass to the GPU to draw what you want is now reduced to the model id. However, there is a catch here since there is no way to generate vertices out of thin air *traditionally*. As a matter of fact, the aforementioned "tessellation" stage is made for generating details out of a base geometry, and even if a model id happened to be a point in space, there is not much to "tessellate" from a single point.

There is, in fact, a stage in the GPU pipeline, commonly called the "geometry" stage, which is capable of creating vertices from scratch, but since it is hardly parallel everybody will tell you to stay away from it if you care about performance, for generating vertices at least (In GLSL geometry shaders, an instance of the shader (a thread) has to output a primitive, not just vertices, since vertex order is not guaranteed across instances, i.e. you have to generate a whole triangle in one thread, making the process 3 times slower than the "vertex" stage which is parallelized on the vertex level).

### Mesh Shaders

[Mesh shaders](https://developer.nvidia.com/blog/introduction-turing-mesh-shaders/), composed of an optional "task" stage and a "mesh" stage (mesh shaders aren't part of the "traditional" pipeline), were introduced specifically to solve the problem we are facing, that is generating vertices out of thin air. Visit the "Mesh-Shaders" folder for a quick example on how to render Minecraft-like models with Mesh shaders (constraints that define voxelized models, e.g. being axis-aligned, make culling easier at the cost of more complex and interesting shader code).

### Tessellation again

While tessellation is about amplifying existing geometry, there is a way to simulate what would become later "Mesh Shaders" with the "traditional" pipeline; a "tessellation hack" if you will.

After thinking hard about how to generate vertices on the GPU without geometry shaders, I came up with this hack that involves tessellation. But later on, I found out it was apparently a well-known approach, yet couldn't find anything on internet about it (generic interactive computer graphics moment) except this [video](https://www.youtube.com/watch?v=HH-9nfceXFw) which evasively mentions it. Visit the "Tessellation-Hack" folder for a quick example on how to render quads out of a single point sent to the "vertex" stage.