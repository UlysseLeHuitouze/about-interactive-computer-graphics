## Credits

Thanks to the authors of these libraries:

- [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/)
- [Vulkan Memory Allocator (VMA)](https://gpuopen.com/vulkan-memory-allocator/)
- [Volk](https://github.com/zeux/volk)
- [GLFW](https://www.glfw.org/)
- [STB Image](https://github.com/nothings/stb)
