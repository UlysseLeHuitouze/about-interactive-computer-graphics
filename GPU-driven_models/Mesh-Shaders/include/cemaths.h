#ifndef MINECRAFT_XEMATHS_H
#define MINECRAFT_XEMATHS_H


#include <stdint.h>

#if defined(__GNUC__) || defined(__GNUG__)
#else
    #error "Require GCC: https://gcc.gnu.org/onlinedocs/gcc/Vector-Extensions.html"
#endif

#define radians(deg) ((deg) * (3.14159265358979323846 / 180.0))

typedef int ivec2_t_g __attribute((vector_size(8)));
typedef float vec2_t_g __attribute((vector_size(8)));
typedef int ivec4_t_g __attribute((vector_size(16)));
typedef float vec4_t_g __attribute__((vector_size(16)));
typedef double dvec4_t_g __attribute__((vector_size(32)));

typedef union ivec2_t {
    ivec2_t_g gen;
    struct {
        int x;
        int y;
    };
} ivec2_t;

typedef union vec2_t {
    vec2_t_g gen;
    struct {
        float x;
        float y;
    };
} vec2_t;

typedef union ivec4_t {
    ivec4_t_g gen;
    struct {
        int x;
        int y;
        int z;
        int w;
    };
} ivec4_t;

typedef union vec4_t {
    vec4_t_g gen;
    struct {
        float x;
        float y;
        float z;
        float w;
    };
} vec4_t;

typedef union dvec4_t {
    dvec4_t_g gen;
    struct {
        double x;
        double y;
        double z;
        double w;
    };
} dvec4_t;

typedef union ivec3_t {
    ivec4_t_g gen;
    struct {
        int x;
        int y;
        int z;
    };
} ivec3_t;

typedef union vec3_t {
    vec4_t_g gen;
    struct {
        float x;
        float y;
        float z;
    };
} vec3_t;

typedef union dvec3_t {
    dvec4_t gen;
    struct {
        double x;
        double y;
        double z;
    };
} dvec3_t;

#define vec4(x, y, z, w) (vec4_t){x, y, z, w}
#define dvec4(x, y, z, w) (dvec4_t){x, y, z, w}


#define vec3(x, y, z) (vec3_t){x, y, z, 0.0f}
#define dvec3(x, y, z) (dvec3_t){x, y, z, 0.0}

#define vec2(x, y) (vec2_t){x, y}

#define ivec4(x, y, z, w) (ivec4_t){x, y, z, w}
#define ivec3(x, y, z) (ivec3_t){x, y, z, 0}

#define ivec2(x, y) (ivec2_t){x, y}


// matrices are column-major, i.e. mat4[3][1] retrieves the float at row #1 column #3.
typedef vec4_t mat4_t[4];
typedef dvec4_t dmat4_t[4];


#define mat4(n) { \
    {n, n, n, n}, \
    {n, n, n, n}, \
    {n, n, n, n}, \
    {n, n, n, n}  \
}

#define dmat4(n) mat4(n)


/**
 * Unsafe version of #perspective(x, y, a, b).<br>
 * Edits an already built perspective projection matrix which fov and/or aspect ratio are obsolete.<br>
 * Thus, only edits the two necessary slots; rest is assumed correct.
 * @see #perspective(x, y, a, b) to build a matrix from scratch.
 * @param src_dst the matrix to be edited
 * @param reverse_tan_half_fov 1 / tan(fov / 2)
 * @param reverse_aspect_ratio window_height / window_width
 * @note replaced by 2 full-fledged lines, thus needn't to put any comma afterwards and mustn't be used as assignation.
 *//*
#define nperspective(src_dst, reverse_tan_half_fov, reverse_aspect_ratio) \
    src_dst[0][0] = reverse_tan_half_fov * reverse_aspect_ratio;          \
    src_dst[1][1] = reverse_tan_half_fov;
*/

/**
 * Computes the cross product of <code>u</code> and <code>v</code>.
 * @param u first vector
 * @param v second vector
 * @return u ^ v
 */
vec3_t cross(vec3_t u, vec3_t v);

/**
 * Computes the dot product of <code>u</code> and <code>v</code>.
 * @param u first vector
 * @param v second vector
 * @return u . v
 */
float dot(vec4_t u, vec4_t v);

/**
 * Computes the normalized version of <code>u</code>.
 * @param u the vector to normalize
 * @return a copy of u normalized
 */
vec4_t normalize(vec4_t u);

/**
 * Completely overwrites a matrix to turn it into a perspective projection matrix for left-handed coordinate system.
 * @param dst the destination
 * @param fov the field of vision in degrees
 * @param width window's width, in pixels
 * @param height window's height, in pixels
 * @param z_near z_near
 * @param z_far z_far
 */
void perspective_lh(mat4_t dst, float fov, uint32_t width, uint32_t height, float z_near, float z_far);

/**
 * Unsafe version of #perspective that only writes necessary slots when either fov or window dimension changes.
 * Suited for both left-handed and right-handed coordinate systems.
 * @param dst the destination
 * @param reverse_tan_half_fov 1 / tan(0.5 * fov)
 * @param reverse_aspect_ratio window height / width ratio
 */
void nperspective(mat4_t dst, float reverse_tan_half_fov, float reverse_aspect_ratio);

/**
 * Completely overwrites a matrix to turn it into a perspective projection matrix for a right-handed coordinate system.
 * @param dst the destination
 * @param fov the field of vision in degrees
 * @param width window's width, in pixels
 * @param height window's height, in pixels
 * @param z_near z_near
 * @param z_far z_far
 */
void perspective_rh(mat4_t dst, float fov, uint32_t width, uint32_t height, float z_near, float z_far);


/**
 * Fills a matrix with the correct look_at build for left-handed coordinate system.
 * @param dst the destination matrix
 * @param cam_pos the eye position/the camera position
 * @param cam_look where the camera is looking (looking direction is <code>cam_look - cam_pos</code>)
 * @param up_dir the "up" axis, should always be y
 */
void look_at_lh(mat4_t dst, vec3_t cam_pos, vec3_t cam_look, vec3_t up_dir);

/**
 * Unsafe version of #look_at that
 * updates a view matrix created using #look_at. Only updates necessary slots, rest are ignore.
 * For left-handed coordinate systems only.
 * @param dst the destination matrix
 * @param eye the eye position (the looking direction is computed with <code>eye - cam_pos</code>)
 * @param cam_pos the camera center position
 * @param up_dir the "up" axis, should always be y
 */
void nlook_at_lh(mat4_t dst, vec3_t eye, vec3_t cam_pos, vec3_t up_dir);

/**
 * Fills a matrix with the correct look_at build for right-handed coordinate system.
 * @param dst the destination matrix
 * @param cam_pos the eye position/the camera position
 * @param cam_look where the camera is looking (looking direction is <code>cam_look - cam_pos</code>)
 * @param up_dir the "up" axis, should always be y
 */
void look_at_rh(mat4_t dst, vec3_t cam_pos, vec3_t cam_look, vec3_t up_dir);

/**
 * Unsafe version of #look_at that
 * updates a view matrix created using #look_at. Only updates necessary slots, rest are ignore.
 * For right-handed coordinate systems only.
 * @param dst the destination matrix
 * @param eye the eye position (the looking direction is computed with <code>eye - cam_pos</code>)
 * @param cam_pos the camera center position
 * @param up_dir the "up" axis, should always be y
 */
void nlook_at_rh(mat4_t dst, vec3_t eye, vec3_t cam_pos, vec3_t up_dir);

/**
 * Fills <code>dst</code> with the serialized version of <code>u</code>, starting from index 0 and up to index 63 max.
 * @param u the vector to be serialized
 * @param dst the string destination, should be allocated for 64 characters
 */
void vec3ta(vec3_t u, char *dst);
/**
 * Fills <code>dst</code> with the serialized version of <code>u</code>, starting from index 0 and up to index 99 max.
 * @param u the vector to be serialized
 * @param dst the string destination, should be allocated for 100 characters
 */
void vec4ta(vec4_t u, char *dst);
/**
 * Fills <code>dst</code> with the serialized version of <code>m</code>, starting from index 0 and up to index 399 max.
 * <code>m</code> is serialized row-major (like in usual maths) despite being stored column-major.
 * @param m the matrix to be serialized
 * @param dst the string destination, should be allocated for 400 characters
 */
void mat4ta(mat4_t m, char *dst);

/**
 * @param u the start of the interpolation range
 * @param v th end of the interpolation range
 * @param x the vector to interpolate
 * @return @code u × (1 - x) + v × x @endcode where × is the multiplication of vectors component-wise.
 */
vec3_t mix3(vec3_t u, vec3_t v, vec3_t x);
/**
 * @param u the start of the interpolation range
 * @param v th end of the interpolation range
 * @param x the vector to interpolate
 * @return @code u × (1 - x) + v × x @endcode where × is the multiplication of vectors component-wise.
 */
vec4_t mix4(vec4_t u, vec4_t v, vec4_t x);

#endif //MINECRAFT_XEMATHS_H