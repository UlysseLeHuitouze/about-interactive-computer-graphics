//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CARIG_H
#define CARIG_CARIG_H

// must respect this order for including header
/*
#include "camacro.h"
#include "caarray.h"
#include "camap.h"
#include "carand.h"
#include "castring.h"
#include "calogging.h"*/

#include <stddef.h>

#ifdef CARIG_IMPLEMENTATION
#   define CAARRAY_IMPLEMENTATION
#   define CAMAP_IMPLEMENTATION
#   define CARAND_IMPLEMENTATION
#   define CASTRING_IMPLEMENTATION
#   define CALOGGING_IMPLEMENTATION
#endif //CARIG_IMPLEMENTATION



//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CAMACRO_H
#define CARIG_CAMACRO_H



#define private_CONCAT2_bis2(A, B) A ## B
#define private_CONCAT2_bis(A, B) private_CONCAT2_bis2(A, B)
/**
 * Concatenates two raw texts.
 * @param A the first text to be concatenated
 * @param B the second text to be concatenated
 * @return A . B where '.' is the string concatenation
 */
#define CA_CONCAT2(A, B) private_CONCAT2_bis(A, B)


/**
 * Concatenates three raw texts.
 * @param A the first text to be concatenated
 * @param B the second text to be concatenated
 * @param C the third text to be concatenated
 * @return A . B . C where '.' is the string concatenation
 */
#define CA_CONCAT3(A, B, C) private_CONCAT2_bis( private_CONCAT2_bis(A, B), C )


/**
 * Concatenates four raw texts.
 * @param A the first text to be concatenated
 * @param B the second text to be concatenated
 * @param C the third text to be concatenated
 * @param D the fourth text to be concatenated
 * @return A . B . C . D where '.' is the string concatenation
 */
#define CA_CONCAT4(A, B, C, D) private_CONCAT2_bis(CA_CONCAT3(A, B, C), D)


/**
 * Creates a unique name based on a substring and a unique number.<br>
 * Typical usage:
 * @code
 *  #define __MY_MACRO_IMPL(COUNT, UNIQUE_SEED) ({
 *      int CA_UNIQUE_NAME(my_tmp, UNIQUE_SEED) = some_formula(COUNT);
 *      (MyDataStructure){
 *          .capacity = CA_UNIQUE_NAME(my_tmp, UNIQUE_SEED),
 *          .arr = malloc(sizeof(whatever) * CA_UNIQUE_NAME(my_tmp, UNIQUE_SEED));
 *      };
 *  })
 *
 *  #define MY_MACRO(COUNT) __MY_MACRO_IMPL(COUNT, __COUNTER__)
 * @endcode
 *
 * @param NAME the base name to use
 * @param ID the ID used for making a unique name
 * @return _ . NAME . ID where '.' is the string concatenation
 */
#define CA_UNIQUE_NAME(NAME, ID) CA_CONCAT3(_, NAME, ID)


#define private_STRINGIFY(A) #A
/**
 * Stringifies a macro expression.
 * @param A the expression to stringify
 */
#define STRINGIFY(A) private_STRINGIFY(A)


#define private_smalloc(size, SEED) ({              \
    void* CA_UNIQUE_NAME(mem, SEED) = malloc(size); \
    if (!CA_UNIQUE_NAME(mem, SEED)) {               \
        CA_ERROR("Failed malloc");                  \
        exit(1);                                    \
    }                                               \
    CA_UNIQUE_NAME(mem, SEED);                      \
})
/**
 * Safe version of malloc which crashes upon failure.
 * @param size[in] [size_t] the size of the allocation
 * @return [out][void*] always a valid pointer to the allocated memory
 */
#define smalloc(size) private_smalloc(size, __COUNTER__)


#define private_scalloc(el_count, el_size, SEED) ({              \
    void* CA_UNIQUE_NAME(mem, SEED) = calloc(el_count, el_size); \
    if (!CA_UNIQUE_NAME(mem, SEED)) {                            \
        CA_ERROR("Failed calloc");                               \
        exit(1);                                                 \
    }                                                            \
    CA_UNIQUE_NAME(mem, SEED);                                   \
})
/**
 * Safe version of calloc which crashes upon failure.
 * @param el_count[in] [size_t] the number of element to allocate
 * @param el_size[in] [size_t] the size of the one element
 * @return [out][void*] always a valid pointer to the allocated memory
 */
#define scalloc(el_count, el_size) private_scalloc(el_count, el_size, __COUNTER__)


#define private_srealloc(old_mem, new_size, SEED) ({              \
    void* CA_UNIQUE_NAME(mem, SEED) = realloc(old_mem, new_size); \
    if (!CA_UNIQUE_NAME(mem, SEED)) {                             \
        CA_ERROR("Failed realloc");                               \
        free(old_mem);                                            \
        exit(1);                                                  \
    }                                                             \
    CA_UNIQUE_NAME(mem, SEED);                                    \
})
/**
 * Safe version of realloc which automatically <code>old_mem</code> then crashes upon failure.
 * @param old_mem[in] [void*] a pointer to the memory to reallocate
 * @param new_size[in] [size_t] the size of the reallocation
 * @return [out][void*] always a valid pointer to the reallocated memory
 */
#define srealloc(old_mem, new_size) private_srealloc(old_mem, new_size, __COUNTER__)


#if DEBUG
/**
 * Gets expanded into smalloc (safe malloc which crashes upon failure) if DEBUG, into malloc otherwise.
 * @param size[in] [size_t] the size of the allocation
 * @return [out][void*] If DEBUG, always a pointer to the allocated memory, otherwise, NULL upon failure.
 */
#define nmalloc(size) smalloc(size)


/**
 * Gets expanded into scalloc (safe calloc which crashes upon failure) if DEBUG, into calloc otherwise.
 * @param el_count[in] [size_t] the number of element to allocate
 * @param el_size[in] [size_t] the size of the one element
 * @return [out][void*] If DEBUG, always a pointer to the allocated memory, otherwise, NULL upon failure.
 */
#define ncalloc(el_count, el_size) scalloc(el_count, el_size)


/**
 * Gets expanded into srealloc (safe realloc which crashes upon failure) if DEBUG, into realloc otherwise.
 * @param old_mem[in] [void*] a pointer to the memory to reallocate
 * @param new_size[in] [size_t] the size of the reallocation
 * @return [out][void*] If DEBUG, always a pointer to the reallocated memory, otherwise, NULL upon failure.
 */
#define nrealloc(old_mem, new_size) srealloc(old_mem, new_size)

#else
/**
 * Gets expanded into smalloc (safe malloc which crashes upon failure) if DEBUG, into malloc otherwise.
 * @param size[in] [size_t] the size of the allocation
 * @return [out][void*] If DEBUG, always a pointer to the allocated memory, otherwise, NULL upon failure.
 */
#define nmalloc(size) malloc(size)


/**
 * Gets expanded into scalloc (safe calloc which crashes upon failure) if DEBUG, into calloc otherwise.
 * @param el_count[in] [size_t] the number of element to allocate
 * @param el_size[in] [size_t] the size of the one element
 * @return [out][void*] If DEBUG, always a pointer to the allocated memory, otherwise, NULL upon failure.
 */
#define ncalloc(el_count, el_size) calloc(el_count, el_size)


/**
 * Gets expanded into srealloc (safe realloc which crashes upon failure) if DEBUG, into realloc otherwise.
 * @param old_mem[in] [void*] a pointer to the memory to reallocate
 * @param new_size[in] [size_t] the size of the reallocation
 * @return [out][void*] If DEBUG, always a pointer to the reallocated memory, otherwise, NULL upon failure.
 */
#define nrealloc(old_mem, new_size) realloc(old_mem, new_size)
#endif //DEBUG



#define private_MAX(A, B, SEED) ({                                                             \
    __auto_type CA_UNIQUE_NAME(a, SEED) = (A);                                                    \
    __auto_type CA_UNIQUE_NAME(b, SEED) = (B);                                                    \
    CA_UNIQUE_NAME(a, SEED) > CA_UNIQUE_NAME(b, SEED) ? CA_UNIQUE_NAME(a, SEED) : CA_UNIQUE_NAME(b, SEED); \
})
#define private_MIN(A, B, SEED) ({                                                             \
    __auto_type CA_UNIQUE_NAME(a, SEED) = (A);                                                    \
    __auto_type CA_UNIQUE_NAME(b, SEED) = (B);                                                    \
    CA_UNIQUE_NAME(a, SEED) < CA_UNIQUE_NAME(b, SEED) ? CA_UNIQUE_NAME(a, SEED) : CA_UNIQUE_NAME(b, SEED); \
})
#define MAX(A, B) private_MAX(A, B, __COUNTER__)
#define MIN(A, B) private_MIN(A, B, __COUNTER__)
#define CLAMP(A, B, X) ( MAX(A, MIN(B, X)) )

#endif //CARIG_CAMACRO_H



//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CAARRAY_H
#define CARIG_CAARRAY_H

#include <stdint.h>
#if __STDC_VERSION__ <= 201710
#   include <stdbool.h>
#endif


// To force inlining on pretty much everything, define CAARRAY_INLINE before including this header.


// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to arrays, which break down into four parts:
//
//
// # Array
//
// A simple array abstraction which stores length internally.
//
// Related to arrays in this file are:
//
// - struct array
// - macro ca_array_of :: type -> variadic list -> array
//
//
// # Unbound Array
//
// A growable array abstraction which offers similar behaviour to C++ vectors.
//
// Related to unbound arrays in this file are:
//
// - struct UnboundArray
// - macro ca_unbound_array_of :: type -> integral -> void* (size_t) -> void* (void*,size_t)) -> UnboundArray
// - macro ca_unbound_array_free :: UnboundArray -> void (void*) -> void
// - macro caarrget :: type -> UnboundArray* -> integral -> type instance
// - macro caarrset :: type -> UnboundArray* -> integral -> type instance -> void
// - macro caarrpush :: type -> UnboundArray* -> type instance -> void*
// - macro caarrpush_all :: type -> UnboundArray* -> type instance* -> integral -> void*
// - macro caarrpop :: type -> UnboundArray* -> type instance
//
//
// # Bound Ring buffer
//
// A fixed-size ring buffer abstraction which provides overwriting capabilities.
//
// Related to bound ring buffers in this file are:
//
// - struct BoundRingBuffer
// - macro ca_bound_ringbuffer_static_of :: type -> integral -> BoundRingBuffer
// - macro ca_bound_ringbuffer_dynamic_of :: type -> integral -> void* (size_t) -> BoundRingBuffer
// - macro ca_bound_ringbuffer_free :: BoundRingBuffer -> void (void*) -> void
// - macro cabuffisfull :: BoundRingBuffer* -> bool
// - macro cabuffoffer :: type -> BoundRingBuffer* -> type instance -> bool
// - macro cabuffoffer_unsafe :: type -> BoundRingBuffer* -> type instance -> void
// - macro cabuffoffer_overwrite :: type -> BoundRingBuffer* -> type instance -> void
// - macro cabuffpoll :: type -> BoundRingBuffer* -> type instance
// - macro cabuffpeek :: type -> BoundRingBuffer* -> type instance
// - func cabuffrst :: BoundRingBuffer* -> void
//
//
// # Unbound Ring buffer
//
// A ring buffer abstraction which provides overwriting and growing capabilities.
//
// Related to unbound ring buffers in this file are:
//
// - struct UnboundRingBuffer
// - macro ca_unbound_ringbuffer_of :: type -> integral -> void* (size_t) -> void* (void*,size_t) -> UnboundRingBuffer
// - macro ca_unbound_ringbuffer_free :: UnboundRingBuffer -> void (void*) -> void
// - macro cadbuffisfull :: UnboundRingBuffer* -> bool
// - macro cadbuffoffer :: type -> UnboundRingBuffer* -> type instance -> void*
// - macro cadbuffoffer_overwrite :: type -> UnboundRingBuffer* -> type instance -> void
// - macro cadbuffpoll :: type -> UnboundRingBuffer* -> type instance
// - macro cadbuffpeek :: type -> UnboundRingBuffer* -> type instance
// - func cadbuffrst :: UnboundRingBuffer* -> void
//
// ==================================================




/**
 * Builds a compile-time C array out of a compile-time variadic list.<br>
 * Expansion example:<br><br>
 * <code>ARRAY_OF(char*, "foo", "bar")</code> --&gt;<br><br>
 * <code>(const char*[]){"foo", "bar"}</code>
 * @param type the type of the array
 * @param ... the variadic list
 */
#define ARRAY_OF(type, ...) (const type[]){__VA_ARGS__}





/**
 * Represents a C array with its length cached.
 */
typedef struct array {
    /**
     * The underlying array of elements.
     */
    void *elements;
    /**
     * The length, i.e the number of allocated elements for <code>elements</code>, of this array.
     */
    uint32_t len;
} array;





/**
 * Builds an <code>array</code> structure out of a compile-time variadic list.<br>
 * Expansion example:<br><br>
 * <code>CARRAY_OF(char*, "foo", "bar")</code> --&gt;
 * @code
 *  (array){
 *      .elements = (const char*[]){"foo", "bar"},
 *      .len = 16 / 8
 *  }
 * @endcode
 * @param type the type of the array
 * @param ... the variadic list
 */
#define ca_array_of(type, ...) (array) {                  \
    .elements = (type[]){__VA_ARGS__},                    \
    .len = sizeof( (type[]){__VA_ARGS__} ) / sizeof(type) \
}











/**
 * Represents an array that can grow upon appending elements.
 * However, it cannot shrinks upon popping its elements, i.e. it won't ever free parts of its memory, even unused.
 *
 * <br><br><br>
 * For any valid UnboundArray <code>a</code>, the following properties hold:<br><br>
 *
 * <ol>
 *      <li><code>a.capacity &gt; 0</code></li>
 *      <li><code>forall i; a.count &lt;= i &lt; a.capacity -&gt; a.elements[i] = undefined</code></li>
 * </ol>
 *
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>While the implementation actually supports 0-sized array, it is strongly advised <b>not</b>
 *      to do so, as it would make the first allocation a joke, and life harder for the first reallocation afterwards.</li>
 *      <li>Elements outside the boundaries <code>[0, a.count[</code> should never be accessed manually.</li>
 * </ol>
 *
 * <h3>Performance note:</h3>
 *
 * <p>If the UnboundArray is used for a large structure A, e.g. larger than <code>sizeof(void*)</code>,
 * and memory scope won't cause you a headache, e.g. the array is instantiated in a function
 * and all structures are allocated in that function's stack scope, then it's recommended to make the UnboundArray an array
 * of A* rather than A to prevent unnecessary structure copy-pasting.</p>
 *
 * <p>Example:
 * @code
 *  void foo() {
 *      UnboundArray A_array = ca_unbound_array_of(A*, N, malloc, realloc);
 *      caarrpush(A*, A_array, ( &(A){...} ));
 *      ...
 *      bar(&A_array);
 *      ca_unbound_array_free(A_array, free);
 *  }
 *
 *  void bar(UnboundArray *A_array) {
 *      A *a_struct = caarrpop(A*, A_array);
 *      // read (only) from a_struct
 *      ...
 *  }
 * @endcode
 *
 * rather than
 *
 * @code
 *  void foo() {
 *      UnboundArray A_array = ca_unbound_array_of(A, N, malloc, realloc);
 *      caarrpush(A, A_array, ( (A){...} ));
 *      ...
 *      bar(&A_array);
 *      ca_unbound_array_free(A_array, free);
 *  }
 *
 *  void bar(UnboundArray *A_array) {
 *      A a_struct = caarrpop(A, A_array);
 *      // read (only) from a_struct
 *      ...
 *  }
 * @endcode
 *
 * </p>
 */
typedef struct UnboundArray {
    /**
     * The size of one element in <code>elements</code>.
     */
    size_t element_size;
    /**
     * The underlying array of elements.
     */
    void *elements;
    /**
     * A pointer to a function for reallocating memory. It must returns <code>NULL</code> when the reallocation failed,
     * and a non-NULL value when it succeeded.
     * @return[NULLABLE] a pointer to reallocated memory or <code>NULL</code> if it failed
     */
    void* (*reallocFuncPtr)(void*, size_t);
    /**
     * The capacity, i.e. the total space currently allocated for <code>elements</code>, of this array.
     */
    size_t capacity;
    /**
     * The count, i.e. the number of actually used (valid) elements in <code>elements</code>, of this array.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} UnboundArray;





/**
 * Creates an UnboundArray structure, allocating <code>elements</code> to hold <code>initial_capacity</code> elements.<br>
 * The user should NULL-check this array's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_unbound_array_free(unbound_array, free_func) when they are done with the array.<br>
 * Typical usage:
 * @code
 *  UnboundArray my_struct_array = ca_unbound_array_of(
 *      MyStruct, n, malloc, realloc
 *  );
 *  if ( !my_struct_array.elements ) {
 *      error("UnboundArray allocation failed");
 *      return MEM_ERROR;
 *  }
 * @endcode
 *
 * @param element_type the type of the elements the array will be holding
 * @param initial_capacity[in] [integral type] the initial number of elements the array will be able to hold
 * @param alloc_func a macro/function name for allocating <code>elements</code> at first.
 *                  (realloc is used internally afterwards)
 * @param realloc_func_ptr[in] [void* (void*, size_t)] a pointer to a valid realloc function (no macro!).
 *          Will be stored and used internally.
 *          Must return NULL upon failing to reallocate and a non-NULL pointer to memory upon success.
 * @return [UnboundArray] the resulting unbound array
 */
#define ca_unbound_array_of(element_type, initial_capacity, alloc_func, realloc_func_ptr) (UnboundArray){ \
    .element_size = sizeof(element_type),                                                                 \
    .elements = alloc_func(sizeof(element_type) * initial_capacity),                                      \
    .reallocFuncPtr = realloc_func_ptr,                                                                   \
    .capacity = initial_capacity                                                                          \
}


/**
 * Frees an UnboundArray's elements.
 * @param unbound_array[in] [UnboundArray] the array whose elements are to be freed
 * @param free_func[in] [void (void*)] a macro/function name for freeing memory.
 */
#define ca_unbound_array_free(unbound_array, free_func) free_func(unbound_array.elements)





/**
 * Gets the element at an index in <code>array</code>.<br>
 * Typical usage:
 * @code
 *  if ( DEBUG && !(0 &lt;= idx && idx &lt; my_struct_array.count) ) {
 *      error("Indexing array outside boundaries");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct foo = caarrget(MyStruct, &my_struct_array, idx);
 *  ...
 * @endcode
 *
 * To edit the actual element in <code>array</code> memory, consider referencing the result as follows:
 *
 * @code
 *  MyStruct *foo = &caarrget(MyStruct, &my_struct_array, idx);
 *  ... // edit foo
 * @endcode
 *
 * @pre <code>0 &lt;= index &lt; array-&gt;count</code>
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in] [UnboundArray*] the array to get an element from
 * @param index[in] [integral type] the index in <code>array-&gt;elements</code> to retrieve an element at
 * @return [element_type] the element in <code>array-&gt;elements</code> at <code>index</code>
 */
#define caarrget(element_type, array, index) ((element_type*)(array)->elements)[index]


/**
 * Sets the element in <code>array-&gt;elements</code> at index <code>index</code> to <code>element</code>.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && !(0 &lt;= idx && idx &lt; my_struct_array.count) ) {
 *      error("Indexing array outside boundaries");
 *      exit(CODE_NEED_FIX);
 *  }
 *  caarrset(MyStruct, &my_struct_array, idx, (MyStruct){...});
 *  ...
 * @endcode
 *
 * Note that referencing the result of <code>caarrget</code> and editing with that pointer
 * instead of calling this function might be more efficient for small changes, e.g.:
 * @code
 *  // only care to set dead to true
 *  caarrset(MyStruct, &my_struct_array, idx, ( (MyStruct){.dead = true} ));
 * @endcode
 * vs
 * @code
 *  MyStruct *ref = &caarrget(MyStruct, &my_struct_array, idx);
 *  // only care to set dead to true
 *  ref-&gt;dead = true;
 * @endcode
 *
 * @pre <code>0 &lt;= index &lt; array-&gt;count</code>
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in] [UnboundArray*] the array to be modified
 * @param index[in] [integral type] the index in <code>array-&gt;elements</code> to set <code>element</code> at
 */
#define caarrset(element_type, array, index, element) caarrget(element_type, array, index) = element


#define private_arRpusH(element_type, array, element, SEED) ({                                              \
    __auto_type CA_UNIQUE_NAME(arr, SEED) = (array);                                                        \
    void *CA_UNIQUE_NAME(mem, SEED) = NULL;                                                                 \
    if (CA_UNIQUE_NAME(arr, SEED)->count != CA_UNIQUE_NAME(arr, SEED)->capacity ||                          \
        !(CA_UNIQUE_NAME(mem, SEED) = private_groWunbounDarraY(CA_UNIQUE_NAME(arr, SEED))))                 \
        ((element_type*)CA_UNIQUE_NAME(arr, SEED)->elements)[CA_UNIQUE_NAME(arr, SEED)->count++] = element; \
                                                                                                            \
    CA_UNIQUE_NAME(mem, SEED);                                                                              \
})
/**
 * Pushes <code>element</code> at the end of <code>array</code>, possibly making <code>array</code> grow.<br>
 * Typical usage:
 * @code
 *  void *mem = caarrpush(MyStruct, &my_struct_array, ( (MyStruct){...} ));
 *  if (mem) {
 *      free(mem);
 *      error("Realloc failed. my_string_array may be corrupted");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in,out] [UnboundArray*] the array to append to
 * @param element[in] [element_type] the element to append at the end of <code>array</code>
 * @return [NULLABLE] [void*] the memory that must be freed in case reallocation failed.<br>
 *  It is advised to drop completely <code>array</code> when such a non-NULL pointer is returned.
 */
#define caarrpush(element_type, array, element) private_arRpusH(element_type, array, element, __COUNTER__)


#define private_arRpusH_alL(element_type, array, new_elements, element_count, SEED) ({ \
    __auto_type CA_UNIQUE_NAME(arr, SEED) = (array);                               \
    __auto_type CA_UNIQUE_NAME(count, SEED) = (element_count);                     \
    void *CA_UNIQUE_NAME(mem, SEED) = NULL;                                        \
    if (CA_UNIQUE_NAME(arr, SEED)->count != CA_UNIQUE_NAME(arr, SEED)->capacity ||                                     \
        !(CA_UNIQUE_NAME(mem, SEED) = private_groWunbounDarraYamounT(CA_UNIQUE_NAME(arr, SEED), element_count))) { \
        memcpy((element_type*)CA_UNIQUE_NAME(arr, SEED)->elements + CA_UNIQUE_NAME(arr, SEED)->count,                  \
                    (new_elements), CA_UNIQUE_NAME(count, SEED) * CA_UNIQUE_NAME(arr, SEED)->element_size);     \
        CA_UNIQUE_NAME(arr, SEED)->count += element_count;                                                                           \
    }                                                                               \
    CA_UNIQUE_NAME(mem, SEED);                                                     \
})
/**
 * Pushes <code>elements</code> at the end of <code>array</code>, possibly making <code>array</code> grow.<br>
 * Typical usage:
 * @code
 *  void *mem = caarrpush(MyStruct, &my_struct_array, ( (MyStruct[]){ (MyStruct){...},... } ), N);
 *  if (mem) {
 *      free(mem);
 *      error("Realloc failed. my_string_array may be corrupted");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in,out] [UnboundArray*] the array to append to
 * @param elements[in] [element_type*] the elements to append at the end of <code>array</code>
 * @param element_count[in] [integral] the number of elements in <code>elements</code> to append
 * @return [NULLABLE] [void*] the memory that must be freed in case reallocation failed.<br>
 *  It is advised to drop completely <code>array</code> when such a non-NULL pointer is returned.
 */
#define caarrpush_all(element_type, array, elements, element_count) \
private_arRpusH_alL(element_type, array, elements, element_count, __COUNTER__)


/**
 * Pops the last element of <code>array</code> and returns it.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_array.count == 0) {
 *      error("Trying to pop from an empty array. That should never happen");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct foo = caarrpop(MyStruct, &my_struct_array);
 *  ...
 * @endcode
 *
 * @pre <code>array-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>array-&gt;elements</code>
 * @param array[in,out] [UnboundArray*]
 *          the array to pop an element from. Must be non-empty, i.e. <code>array-&gt;count &gt; 0</code>.
 * @return [element_type] the popped element
 */
#define caarrpop(element_type, array) caarrget(element_type, array, --(array)->count)





/**
 * <b>Internal use only.</b><br>
 * Used in macro expansion.
 * Grows an UnboundArray by +50%.
 * @param array[in,out] the array to grow
 * @return[NULLABLE] the memory to be freed in case reallocation failed, NULL otherwise
 */
void* private_groWunbounDarraY(UnboundArray *array);


/**
 * <b>Internal use only.</b><br>
 * Used in macro expansion.
 * Grows an UnboundArray by max(+50%, +(new_el_count * array->element_size)).
 * @param array[in,out] the array to grow
 * @param new_el_count the minimum number of new elements the array will be able to hold after growing
 * @return[NULLABLE] the memory to be freed in case reallocation failed, NULL otherwise
 */
void* private_groWunbounDarraYamounT(UnboundArray *array, size_t new_el_count);











/**
 * Represents a ring buffer of fixed capacity.
 *
 * <br><br><br>
 * For any valid BoundRingBuffer <code>b</code>, the following properties hold:<br><br>
 *
 * <ol>
 *      <li><code>b.capacity_minus_one &gt;= 0</code></li>
 *      <li>@code forall i; 0 &lt;= i &lt; (b.capacity_minus_one - b.count) -&gt;
 *          b.elements[(b.head + i) % (b.capacity_minus_one + 1)] = undefined@endcode</li>
 * </ol>
 *
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>The implementation does <b>not</b> support 0-sized ring buffer.
 *          Instead, calling #ca_bound_ringbuffer_static_of(type, required_capacity) -&gt; BoundRingBuffer
 *          or #ca_bound_ringbuffer_dynamic_of(type, required_capacity, alloc_func) -&gt; BoundRingBuffer
 *          will force a non-null size.</li>
 *      <li>Elements should <b>never</b> be accessed manually.
 *          Do rely on the proposed methods such as #cabuffoffer(element_type, buffer, element) -&gt; bool and
 *          #cabuffpoll(element_type, buffer) instead.</li>
 * </ol>
 */
typedef struct BoundRingBuffer {
    /**
     * The underlying array of elements. Meant to be const.
     */
    void *elements;
    /**
     * The capacity, i.e. the number of elements <code>elements</code> is able to hold, minus one.<br>
     * Used to speed up modulo operation under the assumption that the capacity is a power of two. Meant to be const.
     */
    uint32_t capacity_minus_one;
    /**
     * The ring buffer's head, i.e. its writing end.
     * @invariant @code 0 &lt;= head &lt;= capacity_minus_one@endcode
     */
    uint32_t head;
    /**
     * The ring buffer's tail, i.e. its reading end.
     * @invariant @code 0 &lt;= tail &lt;= capacity_minus_one@endcode
     */
    uint32_t tail;
    /**
     * The ring buffer's element count, i.e. the number of elements "between" <code>tail</code> and <code>head</code>.
     * @invariant @code 0 &lt;= count &lt;= (capacity_minus_one + 1)@endcode
     */
    uint32_t count;
} BoundRingBuffer;





/**
 * Statically (array declaration) creates a ringbuffer
 * with as capacity <code>required_capacity</code> rounded up to the next power of two.
 * @param type the type of the elements the ringbuffer will be holding
 * @param required_capacity[in] [integral type] the minimum capacity of the ringbuffer.
 *              Will actually be round up to the next power of two internally.
 * @return [BoundRingBuffer] the resulting ringbuffer
 */
#define ca_bound_ringbuffer_static_of(type, required_capacity) {                                                   \
        .elements = (type[required_capacity < 2 ? 1 : 1 << ( 32 - __builtin_clz(required_capacity - 1) )]){},      \
        .capacity_minus_one = (required_capacity < 2 ? 1 : 1 << ( 32 - __builtin_clz(required_capacity - 1) )) - 1 \
}


#define private_bounDringbuffeRdynamiCoF(type, required_capacity, alloc_func, SEED) ({                           \
    int CA_UNIQUE_NAME(cap, SEED) = required_capacity < 2 ? 1 : 1 << ( 32 - __builtin_clz(required_capacity - 1) ); \
    (BoundRingBuffer) {                                                                                          \
        .elements = alloc_func(sizeof(type) * CA_UNIQUE_NAME(cap, SEED)),                                           \
        .capacity_minus_one = CA_UNIQUE_NAME(cap, SEED) - 1                                                         \
    };                                                                                                           \
})
/**
 * Creates a ring buffer with as capacity <code>required_capacity</code> rounded up to the next power of two.<br>
 * The user should NULL-check this ring buffer's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_ringbuffer_free(buffer, free_func) when they are done with the ringbuffer.<br>
 * Typical usage:
 * @code
 *  BoundRingBuffer my_struct_ringbuffer = ca_bound_ringbuffer_dynamic_of(
 *      MyStruct, A_BIG_NUMBER, malloc
 *  );
 *  if ( !my_struct_ringbuffer.elements ) {
 *      error("Ringbuffer allocation failed.");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param type the type of the elements the ringbuffer will be holding
 * @param required_capacity[in] [integral type] the minimum capacity of the ringbuffer.
 *              Will actually be round up to the next power of two internally.
 * @param alloc_func[in] [void* (size_t)] a macro/function name for allocating memory
 * @return [BoundRingBuffer] the resulting ringbuffer
 */
#define ca_bound_ringbuffer_dynamic_of(type, required_capacity, alloc_func) \
private_bounDringbuffeRdynamiCoF(type, required_capacity, alloc_func, __COUNTER__)


/**
 * Frees a ring buffer's elements. Should only be used for ring buffers allocated dynamically using
 * #ca_bound_ringbuffer_dynamic_of(type, required_capacity, alloc_func) -&gt; BoundRingBuffer.
 * @param buffer[in] [BoundRingBuffer] the ringbuffer to free
 * @param free_func[in] [void (void*)] a macro/function name for freeing memory
 */
#define ca_bound_ringbuffer_free(buffer, free_func) free_func(buffer.elements)





/**
 * Checks whether <code>buffer</code> is full or not.
 * @param buffer[in] [BoundRingBuffer*] the buffer to scan
 * @return whether the buffer is full or not
 */
#define cabuffisfull(buffer) ((buffer)->count == (buffer)->capacity_minus_one + 1)


#define private_bufFoffeR(element_type, buffer, element, SEED) ({ \
    bool CA_UNIQUE_NAME(is_full, SEED) = false;                        \
    if ( !cabuffisfull(buffer) ) {                                  \
        cabuffoffer_unsafe(element_type, buffer, element);          \
        CA_UNIQUE_NAME(is_full, SEED) = true;                          \
    }                                                               \
    CA_UNIQUE_NAME(is_full, SEED);                                     \
})
/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head.<br>
 * Typical usage:
 * @code
 *  if ( !cabuffoffer(MyStruct, &my_struct_buffer, ( (MyStruct){...} )) ) {
 *      // handle my_struct_buffer full
 *      ...
 *  }
 *  ...
 * @endcode
 *
 * For an unsafe version with 1 less branch, see #cabuffoffer_unsafe(element_type, buffer, element).
 *
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @return [bool] whether <code>buffer</code> had enough space for appending <code>element</code>
 * @see #cabuffoffer_unsafe(element_type, buffer, element)
 */
#define cabuffoffer(element_type, buffer, element) private_bufFoffeR(element_type, buffer, element, __COUNTER__)


/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head.<br>
 * Unsafe version of #cabuffoffer(element_type, buffer, element) -&gt; bool.<br>
 * Typical usage:
 * @code
 *  if ( cabuffisfull(&my_struct_buffer) ) {
 *      // handle my_struct_buffer full
 *      ...
 *  } else {
 *      cabuffoffer_unsafe(MyStruct, &my_struct_buffer, ( (MyStruct){...} ));
 *      ...
 *  }
 * @endcode
 *
 * @pre <code>¬cabuffisfull(buffer)</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @see #cabuffoffer(element_type, buffer, element) -&gt; bool
 */
#define cabuffoffer_unsafe(element_type, buffer, element)                 \
    ((element_type*)(buffer)->elements)[(buffer)->head] = element;        \
    (buffer)->head = ((buffer)->head + 1) & (buffer)->capacity_minus_one; \
    (buffer)->count++


#define private_bufFoffeRoverwritE(element_type, buffer, element, SEED) \
    ((element_type*)(buffer)->elements)[(buffer)->head] = element;        \
    (buffer)->head = ((buffer)->head + 1) & (buffer)->capacity_minus_one; \
    if (cabuffisfull(buffer))                                             \
        (buffer)->tail = (buffer)->head;                                  \
    else (buffer)->count++
/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head, possibly overwriting not-yet-polled elements.<br>
 * Variant of #cabuffoffer(element_type, buffer, element) -&gt; bool that allows overwriting.<br>
 *
 * @pre <code>¬cabuffisfull(buffer)</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @see #cabuffoffer(element_type, buffer, element) -&gt; bool
 */
#define cabuffoffer_overwrite(element_type, buffer, element) \
private_bufFoffeRoverwritE(element_type, buffer, element, __COUNTER__)


#define private_bufFpolL(element_type, buffer, SEED) ({                                      \
    element_type CA_UNIQUE_NAME(res, SEED) = ((element_type*)(buffer)->elements)[(buffer)->tail]; \
    (buffer)->tail = ((buffer)->tail + 1) & (buffer)->capacity_minus_one;                      \
    (buffer)->count--;                                                                         \
    CA_UNIQUE_NAME(res, SEED);                                                                    \
})
/**
 * Polls a ring buffer, i.e. reads the element at its tail then increments the latter.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_buffer.count == 0) {
 *      error("Tried to poll empty ring buffer. That should never happen.");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct a_struct = cabuffpoll(MyStruct, &my_struct_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to be polled
 * @return [element_type] the element at index <code>buffer-&gt;tail</code> of <code>buffer-&gt;elements</code>
 * @see BoundRingBuffer
 */
#define cabuffpoll(element_type, buffer) private_bufFpolL(element_type, buffer, __COUNTER__)


/**
 * Take a peek at a ring buffer, i.e. reads the element at its tail.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_buffer.count == 0) {
 *      error("Tried to poll empty ring buffer. That should never happen.");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct a_struct = cabuffpoll(MyStruct, &my_struct_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [BoundRingBuffer*] the buffer to peek from
 * @return [element_type] the element at index <code>buffer-&gt;tail</code> of <code>buffer-&gt;elements</code>
 * @see BoundRingBuffer
 */
#define cabuffpeek(element_type, buffer) ( ((element_type*)(buffer)->elements)[(buffer)->tail] )


/**
 * Resets a ring buffer, i.e. sets <code>buffer-&gt;count</code> to 0.
 * @param buffer[in,out] the buffer to be reset
 */
void cabuffrst(BoundRingBuffer *buffer);











/**
 * Represents a ring buffer of dynamic capacity.
 *
 * <br><br><br>
 * For any valid UnboundRingBuffer <code>b</code>, the following properties hold:<br><br>
 *
 * <ol>
 *      <li><code>b.capacity_minus_one &gt;= 0</code></li>
 *      <li>@code forall i; 0 &lt;= i &lt; (b.capacity_minus_one - b.count) →
 *          b.elements[(b.head + i) % (b.capacity_minus_one + 1)] = undefined@endcode</li>
 * </ol>
 *
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>The implementation does <b>not</b> support 0-sized ring buffer.
 *          Instead, calling #ca_unbound_ringbuffer__of(type, required_capacity) -&gt; UnboundRingBuffer
 *          will force a non-null size.</li>
 *      <li>Elements should <b>never</b> be accessed manually.
 *          Do rely on the proposed methods such as #cabuffoffer(element_type, buffer, element) -&gt; bool and
 *          #cabuffpoll(element_type, buffer) instead.</li>
 * </ol>
 */
typedef struct UnboundRingBuffer {
    /**
     * The underlying array of elements. Meant to be const.
     */
    void *elements;
    /**
     * The size of one element in <code>elements</code>.
     */
    size_t element_size;
    /**
     * A pointer to a realloc-like function for reallocating memory.
     * @return NULL upon reallocation failure, non-NULL memory otherwise
     */
    void* (*reallocFuncPtr)(void*, size_t);
    /**
     * The capacity, i.e. the number of elements <code>elements</code> is able to hold, minus one.<br>
     * Used to speed up modulo operation under the assumption that the capacity is a power of two. Meant to be const.
     */
    uint32_t capacity_minus_one;
    /**
     * The ring buffer's head, i.e. its writing end.
     * @invariant @code 0 &lt;= head &lt;= capacity_minus_one@endcode
     */
    uint32_t head;
    /**
     * The ring buffer's tail, i.e. its reading end.
     * @invariant @code 0 &lt;= tail &lt;= capacity_minus_one@endcode
     */
    uint32_t tail;
    /**
     * The ring buffer's element count, i.e. the number of elements "between" <code>tail</code> and <code>head</code>.
     * @invariant @code 0 &lt;= count &lt;= (capacity_minus_one + 1)@endcode
     */
    uint32_t count;
} UnboundRingBuffer;






#define private_unbounDringbuffeRoF(type, initial_capacity, alloc_func, realloc_func_ptr, SEED) ({             \
    int CA_UNIQUE_NAME(cap, SEED) = initial_capacity < 2 ? 1 : 1 << ( 32 - __builtin_clz(initial_capacity - 1) ); \
    (UnboundRingBuffer) {                                                                                      \
        .element_size = sizeof(type),                                                                          \
        .elements = alloc_func(sizeof(type) * CA_UNIQUE_NAME(cap, SEED)),                                         \
        .capacity_minus_one = CA_UNIQUE_NAME(cap, SEED) - 1,                                                      \
        .reallocFuncPtr = realloc_func_ptr                                                                     \
    };                                                                                                         \
})
/**
 * Creates a ringbuffer with as capacity <code>initial_capacity</code> rounded up to the next power of two.<br>
 * The user should NULL-check this ring buffer's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_unbound_ringbuffer_free(buffer, free_func) when they are done with the ringbuffer.<br>
 * Typical usage:
 * @code
 *  UnboundRingBuffer my_struct_ringbuffer = ca_unbound_ringbuffer_of(
 *      MyStruct, A_BIG_NUMBER, malloc, realloc
 *  );
 *  if ( !my_struct_ringbuffer.elements ) {
 *      error("Ringbuffer allocation failed.");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param type the type of the elements the ringbuffer will be holding
 * @param initial_capacity[in] [integral type] the initial capacity of the ringbuffer.
 *              Will actually be round up to the next power of two internally.
 * @param alloc_func[in] [void* (size_t)] a macro/function name for allocating memory
 * @param realloc_func[in] [void* (void*,size_t)] a pointer to a function for reallocating memory.
 *                  Must return non-NULL memory upon reallocation failure.
 * @return [UnboundRingBuffer] the resulting ringbuffer
 */
#define ca_unbound_ringbuffer_of(type, initial_capacity, alloc_func, realloc_func_ptr) \
private_unbounDringbuffeRoF(type, initial_capacity, alloc_func, realloc_func_ptr, __COUNTER__)


/**
 * Frees a ring buffer's elements. Should only be used for ring buffers allocated dynamically using
 * #ca_unbound_ringbuffer_of(type, initial_capacity, alloc_func, realloc_func_ptr) -&gt; UnboundRingBuffer.
 * @param buffer[in] [UnboundRingBuffer] the ringbuffer to free
 * @param free_func[in] [void (void*)] a macro/function name for freeing memory
 */
#define ca_unbound_ringbuffer_free(buffer, free_func) free_func(buffer.elements)





/**
 * Checks whether <code>buffer</code> is full or not.
 * @param buffer[in] [UnboundRingBuffer*] the buffer to scan
 * @return whether the buffer is full or not
 */
#define cadbuffisfull(buffer) cabuffisfull(buffer)


#define private_dbufFoffeR(element_type, buffer, element, SEED) ({                                             \
    const void* CA_UNIQUE_NAME(mem, SEED) = cadbuffisfull(buffer) ? private_groWunbounDringbuffeR(buffer) : NULL; \
    ((element_type*)(buffer)->elements)[(buffer)->head] = element;                                             \
    (buffer)->head = ((buffer)->head + 1) & (buffer)->capacity_minus_one;                                      \
    (buffer)->count++;                                                                                         \
    CA_UNIQUE_NAME(mem, SEED);                                                                                    \
})
/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head.<br>
 * Typical usage:
 * @code
 *  void* mem = cadbuffoffer(MyStruct, &my_struct_buffer, ( (MyStruct){...} ));
 *  if (mem) {
 *      error("Failed to reallocate UnboundRingBuffer");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [UnboundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @return [out][void*] if reallocation failed, the memory to be freed, NULL otherwise
 */
#define cadbuffoffer(element_type, buffer, element) private_dbufFoffeR(element_type, buffer, element, __COUNTER__)


/**
 * Offers <code>element</code> to <code>buffer</code>, i.e. sets the element at its head to <code>element</code>
 * then increments the said head, possibly overwriting not-yet-polled elements.<br>
 * Variant of #cadbuffoffer(element_type, buffer, element) -&gt; void* that allows overwriting.<br>
 *
 * @pre <code>¬cadbuffisfull(buffer)</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [UnboundRingBuffer*] the buffer to offer <code>element</code> to
 * @param element[in] [element_type] the element to append at the head of <code>buffer</code>
 * @see #cadbuffoffer(element_type, buffer, element) -&gt; void*
 */
#define cadbuffoffer_overwrite(element_type, buffer, element) cabuffoffer_overwrite(element_type, buffer, element)


/**
 * Polls a ring buffer, i.e. reads the element at its tail then increments the latter.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_buffer.count == 0) {
 *      error("Tried to poll empty ring buffer. That should never happen.");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct a_struct = cadbuffpoll(MyStruct, &my_struct_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in,out] [UnboundRingBuffer*] the buffer to be polled
 * @return [element_type] the element at index <code>buffer-&gt;tail</code> of <code>buffer-&gt;elements</code>
 * @see UnboundRingBuffer
 */
#define cadbuffpoll(element_type, buffer) cabuffpoll(element_type, buffer)


/**
 * Take a peek at a ring buffer, i.e. reads the element at its tail.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_struct_buffer.count == 0) {
 *      error("Tried to peek at empty ring buffer. That should never happen.");
 *      exit(CODE_NEED_FIX);
 *  }
 *  MyStruct a_struct = cadbuffpeek(MyStruct, &my_struct_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param element_type the type of the elements in <code>buffer-&gt;elements</code>
 * @param buffer[in] [UnboundRingBuffer*] the buffer to peek from
 * @return [element_type] the element at index <code>buffer-&gt;tail</code> of <code>buffer-&gt;elements</code>
 * @see UnboundRingBuffer
 */
#define cadbuffpeek(element_type, buffer) cabuffpeek(element_type, buffer)


/**
 * Resets a ring buffer, i.e. sets <code>buffer-&gt;count</code> to 0.
 * @param buffer[in,out] the buffer to be reset
 */
void cadbuffrst(UnboundRingBuffer *buffer);


/**
 * <b>Internal use only.</b><br>
 * Used in macro expansion.
 * Grows an UnboundRingBuffer by +100%.
 * @param buffer[in,out] the array to grow
 * @return[NULLABLE] the memory to be freed in case reallocation failed, NULL otherwise
 */
void* private_groWunbounDringbuffeR(UnboundRingBuffer *buffer);







// impl

#ifdef CAARRAY_IMPLEMENTATION


#include <string.h>

#define INLINE extern inline __attribute((always_inline))

#ifdef CAARRAY_INLINE
#define MAYBE_INLINE INLINE
#else
#define MAYBE_INLINE
#endif



void* private_groWunbounDarraY(UnboundArray *array) {
    size_t cap_factor = array->capacity >> 1;
    size_t new_cap = array->capacity + cap_factor + (!cap_factor);

    void *mem = array->reallocFuncPtr(array->elements, new_cap * array->element_size);
    if (mem == NULL)
        return array->elements;

    array->elements = mem;
    array->capacity = new_cap;
    return NULL;
}


void* private_groWunbounDarraYamounT(UnboundArray *array, size_t new_el_count) {
    size_t cap_factor = array->capacity >> 1;
    size_t cap1 = array->capacity + cap_factor + (!cap_factor);
    size_t cap2 = array->capacity + new_el_count;
    size_t newcap = MAX(cap1, cap2);

    void* mem = array->reallocFuncPtr(array->elements, newcap * sizeof(char));
    if (mem == NULL)
        return array->elements;

    array->elements = mem;
    array->capacity = newcap;
    return NULL;
}


void* private_groWunbounDringbuffeR(UnboundRingBuffer *buffer) {
    size_t cap = buffer->capacity_minus_one + 1;
    size_t new_cap = 2 * buffer->capacity_minus_one + 2;
    void *mem = buffer->reallocFuncPtr(buffer->elements, new_cap);
    if (mem == NULL)
        return buffer->elements;

    if (buffer->count != 0 && buffer->head <= buffer->tail)
        memcpy(
                (char*)mem + buffer->element_size * cap,
                mem,
                buffer->head
        );

    buffer->head += cap;
    buffer->elements = mem;
    buffer->capacity_minus_one = new_cap - 1;
    return NULL;
}


INLINE void cabuffrst(BoundRingBuffer *buffer) {
    buffer->count = 0;
}


INLINE void cadbuffrst(UnboundRingBuffer *buffer) {
    buffer->count = 0;
}

#endif //CAARRAY_IMPLEMENTATION

#endif //CARIG_CAARRAY_H




//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CAMAP_H
#define CARIG_CAMAP_H

#include <stdint.h>
#if __STDC_VERSION__ <= 201710
#   include <stdbool.h>
#endif


// To force inlining on pretty much everything, define CAMAP_INLINE before including this header.


// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to maps, which break down into five parts:
//
//
// # Hash Set
//
// A set abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case.
// Only a fixed-sized variant is implemented as of now.
//
// Related to hash sets in this file are:
//
// - struct BoundHashSet
// - macro ca_bound_hashset_static_of :: integral -> int (void*,void*) -> BoundHashSet
// - macro ca_bound_hashset_dynamic_of :: integral -> int (void*,void*) -> void* (size_t,size_t) -> BoundHashSet
// - macro ca_bound_hashset_free :: BoundHashSet -> void (void*) -> void
// - func cahsetput :: BoundHashSet* -> uint32_t -> void* -> bool
// - func cahsethas :: BoundHashSet* -> uint32_t -> void* -> bool
// - func cahsetrmv :: BoundHashSet* -> uint32_t -> void* -> bool
// - func cahsetrst :: BoundHashSet* -> void
// - func cahsetarr :: BoundHashSet* -> void** -> void
//
//
// # Int Set
//
// A set abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case,
// specifically tweaked for 32-bit integers.
// Only a fix-sized variant is implemented as of now.
//
// Related to int sets in this file are:
//
// - struct BoundIntSet
// - macro ca_bound_intset_static_of :: integral -> BoundIntSet
// - macro ca_bound_intset_dynamic_of :: integral -> void* (size_t,size_t) -> BoundIntSet
// - macro ca_bound_intset_free :: BoundIntSet -> void (void*) -> void
// - func caisetput :: BoundIntSet* -> uint32_t -> bool
// - func caisethas :: BoundIntSet* -> uint32_t -> bool
// - func caisetrmv :: BoundIntSet* -> uint32_t -> bool
// - func caisetrst :: BoundIntSet* -> void
// - func caisetarr :: BoundIntSet* -> uint32_t* -> void
//
//
// # Hash Map
//
// A map abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case
// for working with keys.
// Only a fix-sized variant is implemented as of now.
//
// Related to hash maps in this file are:
//
// - struct BoundHashMap
// - macro ca_bound_hashmap_static_of :: integral -> int (void*,void*) -> BoundHashMap
// - macro ca_bound_hashmap_dynamic_of :: integral -> int (void*,void*) -> void* (size_t,size_t) -> BoundHashMap
// - macro ca_bound_hashmap_free :: BoundHashMap -> void (void*) -> void
// - func cahmapput :: BoundHashMap* -> uint32_t -> void* -> void* -> void*
// - func cahmapget :: BoundHashMap* -> uint32_t -> void* -> void*
// - func cahmaprmv :: BoundHashMap* -> uint32_t -> void* -> void*
// - func cahmaprst :: BoundHashMap* -> void
// - func cahmaparr :: BoundHashMap* -> void** -> void** -> void
//
//
// # Int Map
//
// A map abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case
// for working with keys, specifically tweaked for 32-bit integer keys.
// Only a fix-sized variant is implemented as of now.
//
// Related to int maps in this file are:
//
// - struct BoundIntMap
// - macro ca_bound_intmap_static_of :: integral -> BoundIntMap
// - macro ca_bound_intmap_dynamic_of :: integral -> void* (size_t,size_t) -> BoundIntMap
// - macro ca_bound_intmap_free :: BoundIntMap -> void (void*) -> void
// - func caimapput :: BoundIntMap* -> uint32_t -> void* -> void*
// - func caimapget :: BoundIntMap* -> uint32_t -> void*
// - func caimaprmv :: BoundIntMap* -> uint32_t -> void*
// - func caimaprst :: BoundIntMap* -> void
// - func caimaparr :: BoundIntMap* -> uint32_t* -> void** -> void
//
//
// # Int-to-Int Map
//
// A map abstraction which uses hashing mechanisms to achieve O(1) best case trading off O(n) worst case
// for working with keys, specifically tweaked for 32-bit integer keys and values.
// Only a fix-sized variant is implemented as of now.
//
// Related to int-to-int maps in this file are:
//
// - struct BoundIntIntMap
// - macro ca_bound_intintmap_static_of :: integral -> BoundIntIntMap
// - macro ca_bound_intintmap_dynamic_of :: integral -> void* (size_t,size_t) -> BoundIntIntMap
// - macro ca_bound_intintmap_free :: BoundIntIntMap -> void (void*) -> void
// - func caiimapput :: BoundIntIntMap* -> uint32_t -> uint32_t -> uint32_t
// - func caiimapget :: BoundIntIntMap* -> uint32_t -> uint32_t
// - func caiimaprmv :: BoundIntIntMap* -> uint32_t -> uint32_t
// - func caiimaprst :: BoundIntIntMap* -> void
// - func caiimaparr :: BoundIntIntMap* -> uint32_t* -> uint32_t* -> void
//
// ==================================================




//
// All hash map/hash set related functions that require doing some hashing will ask the user to pass it as parameter
// instead of asking a pointer to a hash function at structure creation time.
// This is deliberate as storing a pointer to a hash function would restrain the hash integral type, possibly making
// the user having to write a 'lambda' for correct conversion.
// It also serves as a reminder for the user not to pass carelessly NULL values to those methods
// (unless the compare function supports NULL entries, which means extra checks, etc.).
//











/**
 * Represents a Hash Set of fixed size, i.e. with no growing capabilities (branching + rehashing required otherwise).
 * <br><br><br>
 * For any valid BoundHashSet <code>s</code>:<br><br>
 * Let <code>S</code> be the set theoretical representation of <code>s</code>.<br>
 * Let <code>s.has/1</code> and <code>s.rmv/1</code> be respectively the theoretical <code>cahsethas</code>
 * and the theoretical <code>cahsetrmv</code<, which both return whether
 *      <code>s</code> contains an element, given the element is valid.<br><br>
 * The following properties hold:
 *
 * <ol>
 *      <li><code>S ⊂ s.elements</code></li>
 *      <li><code>forall e ∈ S; e /= NULL</code></li>
 *      <li><code>forall e ∈ s.elements / S; e = NULL</code></li>
 *      <li><code>forall e; e ∈ S ↔ s.has e ^ e ∈ S ↔ s.rmv e</code></li>
 * </ol>
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>none</li>
 *      <li>the <code>element</code> parameter in a call to <code>cahsetput</code> <b>must never be NULL</b></li>
 *      <li>uphold prop #2 and <b>must always calloc</b> the <code>elements</code> array
 *          (see #HEAP_HASHSET_BOUND)</li>
 *      <li>the <code>element</code> parameter in a call to <code>cahsethas</code> or to <code>cahsetrmv</code>
 *          <b>should not be NULL</b> unless your <code>s.compareFunctionPtr</code> supports
 *          <code>NULL</code> entries</li>
 * </ol>
 */
typedef struct BoundHashSet {
    /**
     * The array of elements of this hash set. Meant to be const.
     */
    void **elements;
    /**
     * The function for comparing this hash set's elements.
     * It must returns zero when both parameters are equals, and a non-zero value otherwise, for the hash set to work.
     * @return 0 if both parameters are equals.
     */
    int (*compareFunctionPtr)(const void*, const void*);
    /**
     * The capacity, i.e. the length of <code>elements</code>, of this hash set. Meant to be const.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-NULL elements in <code>elements</code>, of this hash set.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} BoundHashSet;





/**
 * Statically (array declaration) creates a set of pointers(void*) able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this integer set will be holding
 * @param compare_func_ptr[in] [int (void*, void*)] a function for comparing two elements of this hash set
 * @return [BoundHashSet] the resulting 32-bit integer set
 */
#define ca_bound_hashset_static_of(element_count, compare_func_ptr) (BoundHashSet) { \
    .elements = (void*[2 * element_count + 1]){},                                 \
    .compareFunctionPtr = (int (*)(const void *, const void *))compare_func_ptr,                   \
    .capacity = 2 * element_count + 1                                                              \
}


/**
 * Creates a set of pointers (void*) able to hold <code>element_count</code> elements.<br>
 * The user should NULL-check this set's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_hashset_free(bound_hash_set, free_func) when they are done with the set.<br>
 * Typical usage:
 * @code
 *  BoundHashSet my_string_set = ca_bound_hashset_dynamic_of(
 *      A_BIG_NUMBER, strcmp, calloc
 *  );
 *  if ( !my_string_set.elements ) {
 *      error("BoundHashSet allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] [integral type] the maximum number of elements this hash set will be holding
 * @param compare_func_ptr[in] [int (void*, void*)] a pointer to a <code>strcmp</code>-like function for comparing
 *                              two <code>element_type</code> elements. Must return 0 upon equality.
 * @param calloc_func[in] [void* (size_t, size_t)] a macro/function name for allocating memory and zeroing it out.
 *                      Must follow stdlib's calloc signature, as well as returning NULL upon allocation failure.
 * @return [BoundHashSet] the resulting pointer set
 */
#define ca_bound_hashset_dynamic_of(element_count, compare_func_ptr, calloc_func) (BoundHashSet) { \
    .elements = calloc_func(2 * element_count + 1, sizeof(void*)),                                 \
    .compareFunctionPtr = (int (*)(const void *, const void *)) compare_func_ptr,                  \
    .capacity = 2 * element_count + 1                                                              \
}


/**
 * Frees a BoundHashSet allocated using
 * #ca_bound_hashset_dynamic_of(element_count, element_type, compare_func_ptr, calloc_func) -&gt; BoundHashSet
 * @param bound_int_set[in] [BoundHashSet] the set to be freed
 * @param free_func[in] [void (void*)] a macro/function name to free memory
 */
#define ca_bound_hashset_free(bound_hash_set, free_func) free_func(bound_hash_set.elements)





/**
 * Puts an element in <code>set</code>, ensuring that future calls to <code>cahsethas(element)</code> will return true
 * and that the next call to <code>cahsetrmv(element)</code> will return true.
 * @param element_type the type of the elements in <code>set</code>
 * @param set[in,out] [BoundHashSet*] the hash set to work on
 * @param element_hash[in] [integral type] the element hash
 * @param element[in] the element to put in <code>set</code>
 * @return [bool] whether <code>set</code> already contained <code>element</code> prior to the call
 */
bool cahsetput(BoundHashSet *set, uint32_t element_hash, void *element);


/**
 * Determines whether <code>set</code> contains <code>element</code> or not.
 * @param set[in] the hash set to work on
 * @param element_hash[in] the element hash
 * @param element[in] the element to check the presence in <code>set</code>
 * @return whether <code>set</code> does contain <code>element</code>
 */
bool cahsethas(BoundHashSet *set, uint32_t element_hash, const void *element);


/**
 * Determines whether <code>set</code> contains <code>element</code> or not,
 * then deletes <code>element</code> from <code>set</code>.
 * @param set[in,out] the hash set to work on
 * @param element_hash[in] the element hash
 * @param element[in] the element to delete in <code>set</code>
 * @return whether <code>set</code> contained <code>element</code> prior to its deletion
 */
bool cahsetrmv(BoundHashSet *set, uint32_t element_hash, const void *element);


/**
 * Resets a set, i.e. zeroes-out its <code>elements</code> array.
 * @param set[in,out] the hash set to work on
 */
void cahsetrst(BoundHashSet *set);


/**
 * Makes an array out of an hash set.
 * @param set[in] the hash set to copy into <code>dst</code>
 * @param dst[in,out] the destination array to copy elements into.
 *      Must be able to hold at least <code>set-&gt;count</code> elements.
 */
void cahsetarr(BoundHashSet *set, void **dst);











/**
 * Integral version of BoundHashSet.
 * See BoundHashSet documentation for general behaviour.
 * <br><br>
 * Implementation note:
 * We chose to use the integral 0 to represent the absence of value.
 * To still allow this useful value to the user, the actual integer stored in the map is incremented by one.
 * So for the user, the forbidden value has shift from 0 to UINT32_MAX.
 */
typedef struct BoundIntSet {
    /**
     * The array of elements of this integer set. Meant to be const.
     */
    uint32_t *elements;
    /**
     * The capacity, i.e. the length of <code>elements</code>, of this integer set. Meant to be const.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-NULL elements in <code>elements</code>, of this hash set.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} BoundIntSet;





/**
 * Statically (array declaration) creates a set of 32-bit integers able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this integer set will be holding
 * @return [BoundIntSet] the resulting 32-bit integer set
 */
#define ca_bound_intset_static_of(element_count) (BoundIntSet) { \
    .elements = (uint32_t[2 * element_count + 1]){},             \
    .capacity = 2 * element_count + 1                            \
}


/**
 * Creates a set of 32-bit integers able to hold <code>element_count</code> elements.<br>
 * The user should NULL-check this set's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_intset_free(bound_int_set, free_func) when they are done with the set.<br>
 * Typical usage:
 * @code
 *  BoundIntSet my_int_set = ca_bound_intset_dynamic_of(
 *      A_BIG_NUMBER, calloc
 *  );
 *  if ( !my_int_set.elements ) {
 *      error("BoundIntSet allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] [integral type] the maximum number of elements this integer set will be holding
 * @param calloc_func[in] [void* (size_t, size_t)] a macro/function name for allocating memory and zeroing it out.
 *                      Must follow stdlib's calloc signature, as well as returning NULL upon allocation failure.
 * @return [BoundIntSet] the resulting 32-bit integer set
 */
#define ca_bound_intset_dynamic_of(element_count, calloc_func) (BoundIntSet) { \
    .elements = calloc_func(2 * element_count + 1, sizeof(uint32_t)),          \
    .capacity = 2 * element_count + 1                                          \
}


/**
 * Frees a BoundIntSet allocated using #ca_bound_intset_dynamic_of(element_count, calloc_func) -&gt; BoundIntSet
 * @param bound_int_set[in] [BoundIntSet] the set to be freed
 * @param free_func[in] [void (void*)] a macro/function name to free memory
 */
#define ca_bound_intset_free(bound_int_set, free_func) ca_bound_hashset_free(bound_int_set, free_func)





/**
 * Puts an element in <code>set</code>, ensuring that future calls to <code>caisethas(element)</code> will return true
 * and that the next call to <code>caisetrmv(element)</code> will return true.
 * @param set[in,out] the hash set to work on
 * @param element[in] the element to put in <code>set</code>. Must be different than UINT32_MAX.
 * @return whether <code>set</code> already contained <code>element</code> prior to the call
 */
bool caisetput(BoundIntSet *set, uint32_t element);


/**
 * Determines whether <code>set</code> contains <code>element</code> or not.
 * @param set[in] the hash set to work on
 * @param element[in] the element to check the presence in <code>set</code>. Must be different than UINT32_MAX.
 * @return whether <code>set</code> does contain <code>element</code>
 */
bool caisethas(BoundIntSet *set, uint32_t element);


/**
 * Determines whether <code>set</code> contains <code>element</code> or not,
 * then deletes <code>element</code> from <code>set</code>.
 * @param set[in,out] the hash set to work on
 * @param element[in] the element to delete in <code>set</code>. Must be different than UINT32_MAX.
 * @return whether <code>set</code> contained <code>element</code> prior to its deletion
 */
bool caisetrmv(BoundIntSet *set, uint32_t element);


/**
 * Resets a set, i.e. zeroes-out its <code>elements</code> array.
 * @param set[in,out] the int set to work on
 */
void caisetrst(BoundIntSet *set);


/**
 * Makes an array out of a integral set.
 * @param set[in] the int set to copy into <code>dst</code>
 * @param dst[in,out] the destination array to copy elements into.
 *      Must be able to hold at least <code>set-&gt;count</code> integers.
 */
void caisetarr(BoundIntSet *set, uint32_t *dst);











/**
 * Represents a Hash Map of fixed size, i.e. with no growing capabilities (branching + rehashing required otherwise).
 * <br><br><br>
 * For any valid BoundHashMap <code>m</code>:<br><br>
 * Let <code>K</code> be <code>m</code>'s key set and <code>V</code> <code>m</code>'s value set.<br>
 * Let <code>m.get/1</code> and <code>m.rmv/1</code> be respectively the theoretical <code>cahmapget</code>
 * and the theoretical <code>cahmaprmv</code> which both return the value associated to a key
 * given the key the is valid.<br><br>
 * The following properties hold:
 *
 * <ol>
 *      <li><code>K ⊂ m.keys</code></li>
 *      <li><code>V ⊂ m.values</code></li>
 *      <li><code>forall k ∈ K; k /= NULL</code></li>
 *      <li><code>forall k ∈ m.keys / K; k = NULL</code></li>
 *      <li><code>forall v ∈ V; v /= NULL</code></li>
 *      <li><code>forall v ∈ m.values / V; v = NULL</code></li>
 *      <li><code>forall k; k ∈ K ↔ m.get k ∈ V ^ k ∈ K ↔ m.rmv k ∈ V</code></li>
 * </ol>
 *
 * And with these properties go the following remarks:
 *
 * <ol>
 *      <li>none</li>
 *      <li>none</li>
 *      <li>the <code>key</code> parameter in a call to <code>cahmapput</code> <b>must never be NULL</b></li>
 *      <li>uphold prop #3 and <b>must always calloc</b> the <code>keys</code> array
 *          (see #HEAP_HASHMAP_BOUND)</li>
 *      <li>the <code>value</code> parameter in a call to <code>cahmapput</code> <b>must never be NULL</b></li>
 *      <li>uphold prop #5 and <b>must always calloc</b> the <code>values</code> array
 *          (see #HEAP_HASHMAP_BOUND)</li>
 *      <li>the <code>key</code> parameter in a call to <code>cahmapget</code> or to <code>cahmaprmv</code>
 *          <b>should not be NULL</b> unless your <code>m.compareFunctionPtr</code> supports
 *          <code>NULL</code> entries</li>
 * </ol>
 *
 */
typedef struct BoundHashMap {
    /**
     * The array of keys of this hash map.
     */
    void **keys;
    /**
     * The array of values of this hash map.
     */
    void **values;
    /**
     * The function for comparing this hash map's keys.
     * It must returns zero when both parameters are equals, and a non-zero value otherwise, for the hash map to work.
     * @return 0 if both parameters are equals.
     */
    int (*compareFunctionPtr)(const void*, const void*);
    /**
     * The capacity, i.e. the length of both <code>keys</code> and <code>values</code>, of this hash map.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-NULL elements in <code>keys</code>, of this hash map.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} BoundHashMap;






/**
 * Statically (array declaration) creates a map of pointers (void*) keys and pointers (void*) values
 * able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this hash map will be holding
 * @param compare_func_ptr[in] [int (void*, void*)] a pointer to a <code>strcmp</code>-like function for comparing
 *                              two <code>key_type</code> keys. Must return 0 upon equality.
 * @return [BoundHashMap] the resulting hash map
 */
#define ca_bound_hashmap_static_of(element_count, compare_func_ptr) (BoundHashMap) { \
    .keys = (void*[2 * element_count + 1]){},                                        \
    .values = (void*[2 * element_count + 1]){},                                      \
    .compareFunctionPtr = (int (*)(const void *, const void *))compare_func_ptr,     \
    .capacity = 2 * element_count + 1                                                \
}


#define private_bounDhashmaPdynamiCoF(element_count, compare_func_ptr, calloc_func, SEED) ({               \
    size_t CA_UNIQUE_NAME(cap, SEED) = 2 * element_count + 1;                                                 \
    size_t CA_UNIQUE_NAME(key_size, SEED) = sizeof(void*) * CA_UNIQUE_NAME(cap, SEED);                           \
    void *CA_UNIQUE_NAME(mem, SEED) = calloc_func(1,                                                          \
                                    CA_UNIQUE_NAME(key_size, SEED) + sizeof(void*) * CA_UNIQUE_NAME(cap, SEED)); \
    (BoundHashMap) {                                                                                       \
        .keys = CA_UNIQUE_NAME(mem, SEED),                                                                    \
        .values = (void**)( (char*)CA_UNIQUE_NAME(mem, SEED) + CA_UNIQUE_NAME(key_size, SEED) ),                 \
        .compareFunctionPtr = (int (*)(const void *, const void *))compare_func_ptr,                       \
        .capacity = CA_UNIQUE_NAME(cap, SEED)                                                                 \
    };                                                                                                     \
})
/**
 * Creates a map of pointers (void*) keys and pointers (void*) values able to hold <code>element_count</code> elements.<br>
 * The user should NULL-check this map's keys afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_hashmap_free(bound_hash_map, free_func) when they are done with the map.<br>
 * Typical usage:
 * @code
 *  BoundHashMap my_string_to_struct_ptr_map = ca_bound_hashmap_dynamic_of(
 *      A_BIG_NUMBER, strcmp, calloc
 *  );
 *  if ( !my_string_to_struct_ptr_map.keys ) {
 *      error("BoundHashMap allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] [integral type] the maximum number of elements this hash map will be holding
 * @param compare_func_ptr[in] [int (void*, void*)] a pointer to a <code>strcmp</code>-like function for comparing
 *                              two <code>key_type</code> keys. Must return 0 upon equality.
 * @param calloc_func[in] [void* (size_t, size_t)] a macro/function name for allocating memory and zeroing it out.
 *                      Must follow stdlib's calloc signature, as well as returning NULL upon allocation failure.
 * @return [BoundHashMap] the resulting hash map
 */
#define ca_bound_hashmap_dynamic_of(element_count, compare_func_ptr, calloc_func) \
private_bounDhashmaPdynamiCoF(element_count, compare_func_ptr, calloc_func, __COUNTER__)


/**
 * Frees a BoundHashMap allocated using
 * #ca_bound_hashmap_dynamic_of(element_count, key_type, value_type, compare_func_ptr, calloc_func) -&gt; BoundHashMap
 * @param bound_hash_map[in] [BoundHashMap] the map to be freed
 * @param free_func[in] [void (void*)]a macro/function name to free memory
 */
#define ca_bound_hashmap_free(bound_hash_map, free_func) free_func(bound_hash_map.keys)





/**
 * Puts a key-value pair into <code>map</code>, ensuring that future calls to <code>cahmapget(key)</code> will return
 * <code>value</code> and that the next call to <code>cahmaprmv(key)</code> will return <code>value</code>.
 * @param map[in,out] the hash map to work on
 * @param key_hash[in] the key hash
 * @param key[in] the key to add to <code>map</code>
 * @param value[in] the value associated with <code>key</code>
 * @return[NULLABLE] the old value associated with <code>key</code>, if any
 */
void* cahmapput(BoundHashMap *map, uint32_t key_hash, void *key, void *value);


/**
 * Retrieves the value associated with <code>key</code> in this map.
 * @param map[in] the hash map to work on
 * @param key_hash[in] the key hash
 * @param key[in] the key to lookup for a key-value pair in <code>map</code>
 * @return[NULLABLE] NULL if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code>, in which case it is <b>not</b> NULL.
 */
void* cahmapget(BoundHashMap *map, uint32_t key_hash, void *key);


/**
 * Retrieves the value associated with <code>key</code> in this map, then remove that key-value pair from this map.
 * @param map[in,out] the hash map to work on
 * @param key_hash[in] the key hash
 * @param key[in] the key to lookup for a to-be-deleted key-value pair in <code>map</code>
 * @return[NULLABLE] NULL if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code> before the pair deletion, in which case it is <b>not</b> NULL.
 */
void* cahmaprmv(BoundHashMap *map, uint32_t key_hash, void *key);


/**
 * Resets a map, i.e. zeroes-out its <code>keys</code> and <code>values</code> arrays.
 * @param map[in,out] the hash map to work on
 */
void cahmaprst(BoundHashMap *map);


/**
 * Makes two arrays out of an hash map.
 * @param map[in] the hash map to copy into <code>key_dst</code> and <code>value_dst</code>
 * @param key_dst[in,out] the destination array to copy keys into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 * @param value_dst[in,out] the destination array to copy values into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 */
void cahmaparr(BoundHashMap *map, void **key_dst, void **value_dst);











/**
 * Integral-key version of BoundHashMap.
 * See BoundHashMap documentation for general behaviour.
 * <br><br>
 * Implementation note:
 * We chose to use the integral 0 to represent the absence of value.
 * To still allow this useful value to the user, the actual integer stored in the map is incremented by one.
 * So for the user, the forbidden value has shift from 0 to UINT32_MAX.
 */
typedef struct BoundIntMap {
    /**
     * The array of keys of this int map.
     */
    uint32_t *keys;
    /**
     * The array of values of this int map.
     */
    void **values;
    /**
     * The capacity, i.e. the length of both <code>keys</code> and <code>values</code>, of this int map.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-0 elements in <code>keys</code>, of this int map.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} BoundIntMap;





/**
 * Statically (array declaration) creates a map of 32-bit integer keys and pointers (void*) values
 * able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this int map will be holding
 * @return [BoundIntMap] the resulting int map
 */
#define ca_bound_intmap_static_of(element_count) (BoundIntMap) { \
    .keys = (uint32_t[2 * element_count + 1]){},                 \
    .values = (void*[2 * element_count + 1]){},                  \
    .capacity = 2 * element_count + 1                            \
}


#define private_bounDintmaPdynamiCoF(element_count, calloc_func, SEED) ({                                  \
    size_t CA_UNIQUE_NAME(cap, SEED) = 2 * element_count + 1;                                                 \
    size_t CA_UNIQUE_NAME(key_size, SEED) = sizeof(uint32_t) * CA_UNIQUE_NAME(cap, SEED);                        \
    void *CA_UNIQUE_NAME(mem, SEED) = calloc_func(1,                                                          \
                                    sizeof(void*) * CA_UNIQUE_NAME(cap, SEED) + CA_UNIQUE_NAME(key_size, SEED)); \
    (BoundIntMap) {                                                                                        \
        .keys = CA_UNIQUE_NAME(mem, SEED),                                                                    \
        .values = (void**)( (char*)CA_UNIQUE_NAME(mem, SEED) + CA_UNIQUE_NAME(key_size, SEED) ),                 \
        .capacity = CA_UNIQUE_NAME(cap, SEED)                                                                 \
    };                                                                                                     \
})
/**
 * Creates a map of 32-bit integer keys and pointers (void*) values able to hold <code>element_count</code> elements.<br>
 * The user should NULL-check this map's keys afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_intmap_free(bound_int_map, free_func) when they are done with the map.<br>
 * Typical usage:
 * @code
 *  BoundIntMap my_int_to_struct_ptr_map = ca_bound_intmap_dynamic_of(
 *      A_BIG_NUMBER, calloc
 *  );
 *  if ( !my_int_to_struct_ptr_map.keys ) {
 *      error("BoundIntMap allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] [integral type] the maximum number of elements this int map will be holding
 * @param calloc_func[in] [void* (size_t, size_t)] a macro/function name for allocating memory and zeroing it out.
 *                      Must follow stdlib's calloc signature, as well as returning NULL upon allocation failure.
 * @return [BoundIntMap] the resulting int map
 */
#define ca_bound_intmap_dynamic_of(element_count, calloc_func) \
private_bounDintmaPdynamiCoF(element_count, calloc_func, __COUNTER__)


/**
 * Frees a BoundIntMap allocated using
 * #ca_bound_intmap_dynamic_of(element_count, value_type, calloc_func) -&gt; BoundIntMap
 * @param bound_int_map[in] [BoundIntMap] the map to be freed
 * @param free_func[in] [void (void*)] a macro/function name to free memory
 */
#define ca_bound_intmap_free(bound_int_map, free_func) ca_bound_hashmap_free(bound_int_map, free_func)





/**
 * Puts a key-value pair into <code>map</code>, ensuring that future calls to <code>caimapget(key)</code> will return
 * <code>value</code> and that the next call to <code>caimaprmv(key)</code> will return <code>value</code>.
 * @param map[in,out] the int map to work on
 * @param key[in] the key to add to <code>map</code>. Must be different than UINT32_MAX.
 * @param value[in] the value associated with <code>key</code>
 * @return[NULLABLE] the old value associated with <code>key</code>, if any
 */
void* caimapput(BoundIntMap *map, uint32_t key, void *value);


/**
 * Retrieves the value associated with <code>key</code> in this map.
 * @param map[in] the int map to work on
 * @param key[in] the key to lookup for a key-value pair in <code>map</code>. Must be different than UINT32_MAX.
 * @return[NULLABLE] NULL if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code>, in which case it is <b>not</b> NULL.
 */
void* caimapget(BoundIntMap *map, uint32_t key);


/**
 * Retrieves the value associated with <code>key</code> in this map, then remove that key-value pair from this map.
 * @param map[in,out] the int map to work on
 * @param key[in] the key to lookup for a to-be-deleted key-value pair in <code>map</code>.
 *                  Must be different than UINT32_MAX.
 * @return[NULLABLE] NULL if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code> before the pair deletion, in which case it is <b>not</b> NULL.
 */
void* caimaprmv(BoundIntMap *map, uint32_t key);


/**
 * Resets a map, i.e. zeroes-out its <code>keys</code> and <code>values</code> arrays.
 * @param map[in,out] the int map to work on
 */
void caimaprst(BoundIntMap *map);


/**
 * Makes two arrays out of an int map.
 * @param map[in] the int map to copy into <code>key_dst</code> and <code>value_dst</code>
 * @param key_dst[in,out] the destination array to copy keys into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 * @param value_dst[in,out] the destination array to copy values into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 */
void caimaparr(BoundIntMap *map, uint32_t *key_dst, void **value_dst);











/**
 * Integral-value version of BoundIntMap.
 * See BoundHashMap documentation for general behaviour.
 * <br><br>
 * Implementation note:
 * We chose to use the integral 0 to represent the absence of value.
 * To still allow this useful value to the user, the actual integer stored in the map is incremented by one.
 * So for the user, the forbidden value has shift from 0 to UINT32_MAX.
 */
typedef struct BoundIntIntMap {
    /**
     * The array of keys of this int map.
     */
    uint32_t *keys;
    /**
     * The array of values of this int map.
     */
    uint32_t *values;
    /**
     * The capacity, i.e. the length of both <code>keys</code> and <code>values</code>, of this int map.
     */
    size_t capacity;
    /**
     * The length, i.e. the number of non-0 elements in <code>keys</code>, of this int map.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} BoundIntIntMap;





/**
 * Statically (array declaration) creates a map of 32-bit integer keys and values
 * able to hold <code>element_count</code> elements.
 * @param element_count[in] [integral type] the maximum number of elements this int-to-int map will be holding
 * @return [BoundIntIntMap] the resulting int-to-int map
 */
#define ca_bound_intintmap_static_of(element_count) (BoundIntIntMap) { \
    .keys = (uint32_t[2 * element_count + 1]){},                    \
    .values = (uint32_t[2 * element_count + 1]){},                  \
    .capacity = 2 * element_count + 1                               \
}


#define private_bounDintintmaPdynamiCoF(element_count, calloc_func, SEED) ({                      \
    size_t CA_UNIQUE_NAME(cap, SEED) = 2 * element_count + 1;                                        \
    uint32_t *CA_UNIQUE_NAME(mem, SEED) = calloc_func(2 * CA_UNIQUE_NAME(cap, SEED), sizeof(uint32_t)); \
    (BoundIntIntMap) {                                                                            \
        .keys = CA_UNIQUE_NAME(mem, SEED),                                                           \
        .values = CA_UNIQUE_NAME(mem, SEED) + CA_UNIQUE_NAME(cap, SEED),                                \
        .capacity = CA_UNIQUE_NAME(cap, SEED)                                                        \
    };                                                                                            \
})
/**
 * Creates a map of 32-bit integer keys and values able to hold <code>element_count</code> elements.<br>
 * The user should NULL-check this map's keys afterwards in case the allocation failed.<br>
 * The user should also use #ca_bound_intintmap_free(bound_int_map, free_func) when they are done with the map.<br>
 * Typical usage:
 * @code
 *  BoundIntMap my_int_to_int_map = ca_bound_intintmap_dynamic_of(
 *      A_BIG_NUMBER, calloc
 *  );
 *  if ( !my_int_to_int_map.keys ) {
 *      error("BoundIntIntMap allocation failed");
 *      return MEM_ERROR;
 *  }
 *  ...
 * @endcode
 *
 * @param element_count[in] [integral type] the maximum number of elements this int map will be holding
 * @param calloc_func[in] [void* (size_t, size_t)] a macro/function name for allocating memory and zeroing it out.
 *                      Must follow stdlib's calloc signature, as well as returning NULL upon allocation failure.
 * @return [BoundIntIntMap] the resulting int map
 */
#define ca_bound_intintmap_dynamic_of(element_count, calloc_func) \
private_bounDintintmaPdynamiCoF(element_count, calloc_func, __COUNTER__)


/**
 * Frees a BoundIntIntMap allocated using
 * #ca_bound_intintmap_dynamic_of(element_count, value_type, calloc_func) -&gt; BoundIntIntMap
 * @param bound_int_map[in] [BoundIntIntMap] the map to be freed
 * @param free_func[in] [void (void*)] a macro/function name to free memory
 */
#define ca_bound_intintmap_free(bound_int_map, free_func) ca_bound_hashmap_free(bound_int_map, free_func)





/**
 * Puts a key-value pair into <code>map</code>, ensuring that future calls to <code>caiimapget(key)</code> will return
 * <code>value</code> and that the next call to <code>caiimaprmv(key)</code> will return <code>value</code>.
 * @param map[in,out] the int-to-int map to work on
 * @param key[in] the key to add to <code>map</code>. Must be different than UINT32_MAX.
 * @param value[in] the value associated with <code>key</code>. Must be different than UINT32_MAX.
 * @return the old value associated with <code>key</code>, if any, -1 otherwise
 */
uint32_t caiimapput(BoundIntIntMap *map, uint32_t key, uint32_t value);


/**
 * Retrieves the value associated with <code>key</code> in this map.
 * @param map[in] the int-to-int map to work on
 * @param key[in] the key to lookup for a key-value pair in <code>map</code>. Must be different than UINT32_MAX.
 * @return -1 if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code>, in which case it is <b>not</b> -1.
 */
uint32_t caiimapget(BoundIntIntMap *map, uint32_t key);


/**
 * Retrieves the value associated with <code>key</code> in this map, then remove that key-value pair from this map.
 * @param map[in,out] the int-to-int map to work on
 * @param key[in] the key to lookup for a to-be-deleted key-value pair in <code>map</code>.
 *                  Must be different than UINT32_MAX.
 * @return -1 if <code>key</code> was not present in <code>map</code>, otherwise
 *  the value associated with <code>key</code> before the pair deletion, in which case it is <b>not</b> -1.
 */
uint32_t caiimaprmv(BoundIntIntMap *map, uint32_t key);


/**
 * Resets a map, i.e. zeroes-out its <code>keys</code> and <code>values</code> arrays.
 * @param map[in,out] the int-to-int map to work on
 */
void caiimaprst(BoundIntIntMap *map);


/**
 * Makes two arrays out of an int-to-int map.
 * @param map[in] the int-to-int map to copy into <code>key_dst</code> and <code>value_dst</code>
 * @param key_dst[in,out] the destination array to copy keys into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 * @param value_dst[in,out] the destination array to copy values into.
 *      Must be able to hold at least <code>map-&gt;count</code> elements.
 */
void caiimaparr(BoundIntIntMap *map, uint32_t *key_dst, uint32_t *value_dst);








// impl

#ifdef CAMAP_IMPLEMENTATION



#define INLINE extern inline __attribute((always_inline))

#ifdef CAMAP_INLINE
#define MAYBE_INLINE INLINE
#else
#define MAYBE_INLINE
#endif


// || Integral set & map


static uint32_t browse_integer(const uint32_t *elements, const uint32_t element_count, uint32_t element){
    uint32_t i = element % element_count;
    for (
            uint32_t el = elements[i];
            el && el != element;
            el = elements[i = (i + 1) % element_count]);
    return i;
}



// integral map


MAYBE_INLINE void* caimapput(BoundIntMap *map, uint32_t key, void *value) {
    key++;

    uint32_t i = browse_integer(map->keys, map->capacity, key);

    void *old = map->values[i];
    map->keys[i] = key;
    map->values[i] = value;
    map->count += old == NULL;
    return old;
}


INLINE void* caimapget(BoundIntMap *map, uint32_t key) {
    key++;

    uint32_t i = browse_integer(map->keys, map->capacity, key);

    return map->values[i];  // thanks to strong constraints, we bypass null-checking map->keys[i]. See BoundHashMap doc
}


MAYBE_INLINE void* caimaprmv(BoundIntMap *map, uint32_t key) {
    key++;

    uint32_t i = browse_integer(map->keys, map->capacity, key);

    void *old = map->values[i];
    map->keys[i] = 0;
    map->values[i] = NULL;
    map->count -= old != NULL;
    return old;
}


INLINE void caimaprst(BoundIntMap *map) {
    map->count = 0;
    for (int i = 0; i < map->capacity; i++) {
        map->keys[i] = 0;
        map->values[i] = NULL;
    }
}


void caimaparr(BoundIntMap *map, uint32_t *key_dst, void **value_dst) {
    int c = 0;
    for (int i = 0; i < map->capacity; i++) {
        uint32_t key = map->keys[i];
        if (key) {
            key_dst[c] = key - 1;
            value_dst[c++] = map->values[i];
        }
    }
}



// integral to integral map


MAYBE_INLINE uint32_t caiimapput(BoundIntIntMap *map, uint32_t key, uint32_t value) {
    key++;

    uint32_t i = browse_integer(map->keys, map->capacity, key);

    uint32_t old = map->values[i];
    map->keys[i] = key;
    map->values[i] = value + 1;
    map->count += old == 0;
    return old - 1;
}


INLINE uint32_t caiimapget(BoundIntIntMap *map, uint32_t key) {
    key++;

    uint32_t i = browse_integer(map->keys, map->capacity, key);

    // thanks to strong constraints, map->values[i] is well-defined. See BoundHashMap doc
    // so, we don't need to 0-check map->keys[i]
    return map->values[i] - 1;
}


MAYBE_INLINE uint32_t caiimaprmv(BoundIntIntMap *map, uint32_t key) {
    key++;

    uint32_t i = browse_integer(map->keys, map->capacity, key);

    uint32_t old = map->values[i];
    map->keys[i] = 0;
    map->values[i] = 0;
    map->count -= old != 0;
    return old - 1;
}


INLINE void caiimaprst(BoundIntIntMap *map) {
    map->count = 0;
    for (int i = 0; i < map->capacity; i++) {
        map->keys[i] = 0;
        map->values[i] = 0;
    }
}


void caiimaparr(BoundIntIntMap *map, uint32_t *restrict key_dst, uint32_t *restrict value_dst) {
    int c = 0;
    for (int i = 0; i < map->capacity; i++) {
        uint32_t key = map->keys[i];
        if (key) {
            key_dst[c] = key - 1;
            value_dst[c++] = map->values[i] - 1;
        }
    }
}




// integral set



INLINE bool caisetput(BoundIntSet *set, uint32_t element) {
    element++;

    uint32_t i = browse_integer(set->elements, set->capacity, element);

    bool had = set->elements[i];
    set->elements[i] = element;
    set->count += !had;
    return had;
}


INLINE bool caisethas(BoundIntSet *set, uint32_t element) {
    element++;

    uint32_t i = browse_integer(set->elements, set->capacity, element);
    return set->elements[i];
}


INLINE bool caisetrmv(BoundIntSet *set, uint32_t element) {
    element++;

    uint32_t i = browse_integer(set->elements, set->capacity, element);

    bool had = set->elements[i];
    set->elements[i] = 0;
    set->count -= had;
    return had;
}


INLINE void caisetrst(BoundIntSet *set) {
    set->count = 0;
    for (int i = 0; i < set->capacity; i++)
        set->elements[i] = 0;
}


void caisetarr(BoundIntSet *set, uint32_t *dst) {
    int c = 0;
    for (int i = 0; i < set->capacity; i++) {
        uint32_t el = set->elements[i];
        if (el)
            dst[c++] = el - 1;
    }
}





// || Generic set & map


static uint32_t browse(
        void **elements,
        const uint32_t element_count,
        const void *element,
        const uint32_t hash,
        int (*compareFunctionPtr)(const void*, const void*))
{
    uint32_t i = hash % element_count;
    for (
            void *el = elements[i];
            el != NULL && compareFunctionPtr(el, element) != 0;
            el = elements[i = (i + 1) % element_count]);
    return i;
}



// generic map



MAYBE_INLINE void* cahmapput(BoundHashMap *map, const uint32_t key_hash, void *key, void *value) {
    uint32_t i = browse(map->keys, map->capacity, key, key_hash, map->compareFunctionPtr);

    void *old = map->values[i];
    map->keys[i] = key;
    map->values[i] = value;
    map->count += old == NULL;
    return old;
}


INLINE void* cahmapget(BoundHashMap *map, const uint32_t key_hash, void *key) {
    uint32_t i = browse(map->keys, map->capacity, key, key_hash, map->compareFunctionPtr);

    return map->values[i];  // thanks to strong constraints, we bypass null-checking map->keys[i]. See BoundHashMap doc
}


MAYBE_INLINE void* cahmaprmv(BoundHashMap *map, const uint32_t key_hash, void *key) {
    uint32_t i = browse(map->keys, map->capacity, key, key_hash, map->compareFunctionPtr);

    void *old = map->values[i];
    map->keys[i] = NULL;
    map->values[i] = NULL;
    map->count -= old != NULL;
    return old;
}


INLINE void cahmaprst(BoundHashMap *map) {
    map->count = 0;
    for (int i = 0; i < map->capacity; i++)
        map->keys[i] = map->values[i] = NULL;
}


void cahmaparr(BoundHashMap *map, void **key_dst, void **value_dst) {
    int c = 0;
    for (int i = 0; i < map->capacity; i++) {
        void *key = map->keys[i];
        if (key) {
            key_dst[c] = key;
            value_dst[c++] = map->values[i];
        }
    }
}




// generic set



INLINE bool cahsetput(BoundHashSet *set, const uint32_t element_hash, void *element) {
    uint32_t i = browse(set->elements, set->capacity, element, element_hash, set->compareFunctionPtr);

    bool had = set->elements[i];
    set->elements[i] = element;
    set->count += !had;
    return had;
}


INLINE bool cahsethas(BoundHashSet *set, const uint32_t element_hash, const void *element) {
    uint32_t i = browse(set->elements, set->capacity, element, element_hash, set->compareFunctionPtr);
    return set->elements[i];
}


INLINE bool cahsetrmv(BoundHashSet *set, uint32_t element_hash, const void *element) {
    uint32_t i = browse(set->elements, set->capacity, element, element_hash, set->compareFunctionPtr);

    bool had = set->elements[i];
    set->elements[i] = NULL;
    set->count -= had;
    return had;
}


INLINE void cahsetrst(BoundHashSet *set) {
    set->count = 0;
    for (int i = 0; i < set->capacity; i++)
        set->elements[i] = NULL;
}


void cahsetarr(BoundHashSet *set, void **dst) {
    int c = 0;
    for (int i = 0; i < set->capacity; i++) {
        void *el = set->elements[i];
        if (el)
            dst[c++] = el;
    }
}




#endif //CAMAP_IMPLEMENTATION


#endif //CARIG_CAMAP_H



//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CARAND_H
#define CARIG_CARAND_H

#include <stdint.h>
#if __STDC_VERSION__ <= 201710
#   include <stdbool.h>
#endif


// To force inlining on pretty much everything, define CARAND_INLINE before including this header.



// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to pseudo-random number generators.
//
// - struct CARandom
// - func carand_setseed :: CARandom* -> integral -> void
// - func carandi :: CARandom* -> integral
// - func carandi_bound :: CARandom* -> integral -> integral
// - func carandlli :: CARandom* -> integral
// - func carandlli_bound :: CARandom* -> integral -> integral
// - func carandbool :: CARandom* -> bool
// - func carandf :: CARandom* -> float
// - func carandf_bound :: CARandom* -> float -> float
// - func carandd :: CARandom* -> double
// - func carandd_bound :: CARandom* -> double -> double
// - func carandgaussian :: CARandom* -> double -> double -> double
//
// ==================================================









/**
 * Holds the states of a pseudorandom number generator "instance".<br>
 * <b>Do not access its fields manually.</b><br>
 * Use dedicated functions in this file instead.
 */
typedef struct CARandom {
    uint64_t seed;
    double gaussian_deviate;
} CARandom;





/**
 * Sets the seed of a pseudorandom number generator "instance".<br>
 * @param rand the pseudorandom number generator "instance"
 * @param seed the new seed to work with
 */
void carand_setseed(CARandom *rand, uint64_t seed);


/**
 * Randomly generates an integer within <code>[0, 2<sup>32</sup> - 1[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @return an integer within <code>[0, 2<sup>32</sup> - 1[</code>
 */
uint32_t carandi(CARandom *rand);


/**
 * Randomly generates an integer within <code>[0, ceiling[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param ceiling the upper bound of the generated integer
 * @return an integer within <code>[0, ceiling[</code>
 */
uint32_t carandi_bound(CARandom *rand, uint32_t ceiling);


/**
 * Randomly generates an integer within <code>[0, 2<sup>64</sup> - 1[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @return an integer within <code>[0, 2<sup>64</sup> - 1[</code>
 */
uint64_t carandlli(CARandom *rand);


/**
 * Randomly generates an integer within <code>[0, ceiling[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param ceiling the upper bound of the generated integer
 * @return an integer within <code>[0, ceiling[</code>
 */
uint64_t carandlli_bound(CARandom *rand, uint64_t ceiling);


/**
 * Randomly generates a boolean with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @return a boolean
 */
bool carandbool(CARandom *rand);


/**
 * Randomly generates a float within <code>[0.0, 1.0[</code>
 * @param rand the pseudorandom number generator "instance"
 * @return a float within <code>[0.0, 1.0[</code>
 */
float carandf(CARandom *rand);


/**
 * Randomly generates a float within <code>[0.0, ceiling[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param ceiling the upper bound of the generated integer
 * @return a float in <code>[0.0, ceiling[</code>
 */
float carandf_bound(CARandom *rand, float ceiling);


/**
 * Randomly generates a double within <code>[0.0, 1.0[</code>
 * @param rand the pseudorandom number generator "instance"
 * @return a double within <code>[0.0, 1.0[</code>
 */
double carandd(CARandom *rand);


/**
 * Randomly generates a double within <code>[0.0, ceiling[</code> with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param ceiling the upper bound of the generated integer
 * @return a double in <code>[0.0, ceiling[</code>
 */
double carandd_bound(CARandom *rand, double ceiling);


/**
 * Randomly generates a gaussian-distributed double with a pseudorandom number generator "instance".
 * @param rand the pseudorandom number generator "instance"
 * @param mean the mean of the gaussian distribution
 * @param deviation the standard deviation of the gaussian distribution
 * @return
 */
double carandgaussian(CARandom *rand, double mean, double deviation);







// impl

#ifdef CARAND_IMPLEMENTATION



#include "math.h"


//
// The methods in this file are, for the most part, heavily inspired by OpenJDK 20 java.util.Random class.
//


#define INLINE extern inline __attribute((always_inline))

#ifdef CARAND_INLINE
#define MAYBE_INLINE INLINE
#else
#define MAYBE_INLINE
#endif




#define NEXT_SEED(seed) ((seed * 0x5deece66dULL + 0xbULL) & 0x0000ffffffffffffULL)

INLINE void carand_setseed(CARandom *rand, uint64_t seed) {
    rand->seed = (seed ^ 0x5deece66dULL) & 0x0000ffffffffffffULL;
}


MAYBE_INLINE uint32_t carandi_bound(CARandom *rand, uint32_t ceiling) {

#define NEXT_INT_SHIFT_RIGHT_ONE (( rand->seed = NEXT_SEED(rand->seed) ) >> 17)

    uint64_t res = NEXT_INT_SHIFT_RIGHT_ONE;
    const uint64_t ceiling_minus_one = ceiling - 1;

    if (!( ceiling & ceiling_minus_one ))
        res = (ceiling * res) >> 31;
    else
        for (uint64_t i = res;
             i - (res = i % ceiling) + ceiling_minus_one < 0;
             i = NEXT_INT_SHIFT_RIGHT_ONE
                );

#undef NEXT_INT_SHIFT_RIGHT_ONE

    return res;
}


INLINE uint32_t carandi(CARandom *rand) {
    return ( rand->seed = NEXT_SEED(rand->seed) ) >> 16;
}


MAYBE_INLINE uint64_t carandlli_bound(CARandom *rand, uint64_t ceiling) {

#define NEXT_LONG_SHIFT_RIGHT_ONE_BIS(SEED) ({                                        \
    uint64_t CA_UNIQUE_NAME(msb, SEED) = NEXT_SEED(rand->seed);                          \
    uint64_t CA_UNIQUE_NAME(lsb, SEED) = rand->seed = NEXT_SEED(CA_UNIQUE_NAME(msb, SEED)); \
                                                                                      \
    ( (CA_UNIQUE_NAME(msb, SEED) & 0x0000ffffffff0000ULL) << 15 ) |                      \
    ( (CA_UNIQUE_NAME(lsb, SEED) & 0x0000ffffffff0000ULL) >> 17 );                       \
})

#define NEXT_LONG_SHIFT_RIGHT_ONE NEXT_LONG_SHIFT_RIGHT_ONE_BIS(__COUNTER__)

    uint64_t res = NEXT_LONG_SHIFT_RIGHT_ONE;

    const uint64_t ceiling_minus_one = ceiling - 1;

    if (!( ceiling & ceiling_minus_one ))
        res &= ceiling_minus_one;
    else
        for (uint64_t i = res;
             i - (res = i % ceiling) + ceiling_minus_one < 0;
             i = NEXT_LONG_SHIFT_RIGHT_ONE
                );

#undef NEXT_LONG_SHIFT_RIGHT_ONE
#undef NEXT_LONG_SHIFT_RIGHT_ONE_BIS

    return res;
}


INLINE uint64_t carandlli(CARandom *rand) {
    uint64_t msb = NEXT_SEED(rand->seed);
    uint64_t lsb = rand->seed = NEXT_SEED(msb);

    return ( (msb & 0x0000ffffffff0000ULL) << 16 ) | ( (lsb & 0x0000ffffffff0000ULL) >> 16 );
}


INLINE bool carandbool(CARandom *rand) {
    return ( rand->seed = NEXT_SEED(rand->seed) ) >> 47;
}


INLINE float carandf(CARandom *rand) {
    return 0.000000059604644775390625f * (float)(uint32_t)( ( rand->seed = NEXT_SEED(rand->seed) ) >> 24 );
}


MAYBE_INLINE float carandf_bound(CARandom *rand, float ceiling) {
    float res = carandf(rand) * ceiling;
    if (res >= ceiling)
        res = nextafterf(ceiling, -INFINITY);

    return res;
}


INLINE double carandd(CARandom *rand) {
    uint64_t msb = NEXT_SEED(rand->seed);
    uint64_t lsb = rand->seed = NEXT_SEED(msb);

    return  0.00000000000000011102230246251565404236316680908203125 * (double)(
            ( (msb & 0xffffffffffc00000ULL) << 5 ) +
            ( (lsb >> 21) & 0x00000000ffffffffULL )
    );
}


MAYBE_INLINE double carandd_bound(CARandom *rand, double ceiling) {
    double res = carandd(rand) * ceiling;
    if (res >= ceiling)
        res = nextafter(ceiling, -INFINITY);

    return res;
}


double carandgaussian(CARandom *rand, double mean, double deviation) {
    double res;
    const double deviate = rand->gaussian_deviate;
    if (isnan(deviate)) {

        double v1, v2, distance_squared;

        do {
            v1 = 2 * carandd(rand) - 1;
            v2 = 2 * carandd(rand) - 1;
            distance_squared = v1 * v1 + v2 * v2;
        } while( !distance_squared || distance_squared >= 1);

        double polar = sqrt(-2 * log(distance_squared) / distance_squared);
        rand->gaussian_deviate = v2 * polar;

        res = mean + v1 * polar * deviation;

    } else {
        res = mean + deviation * deviate;
        rand->gaussian_deviate = nan("");
    }
    return res;
}




#endif //CARAND_IMPLEMENTATION

#endif //CARIG_CARAND_H



//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CASTRING_H
#define CARIG_CASTRING_H


#include <stdint.h>
#include <string.h>


#ifndef CASTRING_MALLOC
#   include <stdlib.h>
#   define CASTRING_MALLOC malloc
#endif

#ifndef CASTRING_REALLOC
#   include <stdlib.h>
#   define CASTRING_REALLOC realloc
#endif

#ifndef CASTRING_FREE
#   include <stdlib.h>
#   define CASTRING_FREE free
#endif


// To force inlining on pretty much everything, define CASTRING_INLINE before including this header.



// ==================== ABSTRACT ====================
//
// Here we define a few abstractions related to strings, which break down into two parts:
//
//
// # String
//
// A string abstraction which wraps a simple pointer to character with its length cached.
//
// Related to strings in this file are:
//
// - struct string
// - macro ca_str_of :: char* -> string
// - macro ca_str_constant_of :: char* -> string
// - func castrhash :: size_t -> string* -> uint32_t
// - func castrhash_unsafe :: size_t -> char* -> uint32_t
// - func castrconcat :: string* -> string* -> string* -> void
// - func castrcmp :: string* -> string* -> int
// - func castrsubstr :: string* -> string* -> char*
//
//
// # String Buffer
//
// A string buffer abstraction, which behaves like a growable array to build strings.
//
// Related to string buffers in this file are:
//
// - struct StringBuffer
// - macro ca_string_buffer_of :: integral -> StringBuffer
// - macro ca_string_buffer_free :: StringBuffer -> void
// - macro castrbufpop :: StringBuffer* -> char
// - func castrbufpush :: StringBuffer* -> char -> bool
// - func castrbufpush_all :: StringBuffer* -> string* -> bool
// - func castrbufpush_int :: StringBuffer* -> uint32_t -> bool
// - func castrbufpush_bool :: StringBuffer* -> bool -> bool
// - func castrbufpush_float :: StringBuffer* -> float -> int -> bool
//
// ==================================================






/**
 * Represents a string, by holding both the actual character array and its cached size, i.e. the number of characters
 * excluding the final '\0'.
 */
typedef struct string {
    /**
     * The underlying pointer to character
     */
    char *chars;
    /**
     * The size of the string, i.e. the cached result of <code>strlen(chars)</code>
     */
    size_t len;
} string;





/**
 * Creates a <code>string</code> structure on the stack from a pointer to character.
 * @param char_pointer the original string (must stay valid, no deep copy is done)
 */
#define ca_str_of(char_pointer) (string) {    \
    .chars = char_pointer,                    \
    .len = strnlen(char_pointer, SIZE_MAX)    \
}

/**
 * Creates a <code>string</code> structure on the stack from a compilation-constant string.
 * @param char_pointer the constant string
 */
#define ca_str_constant_of(constant_string) (string) { \
    .chars = constant_string,                          \
    .len = sizeof(constant_string) / sizeof(char)      \
}




/**
 * Computes a string's hash value, based on the characters at index <code>[begin, s-&gt;len[</code>
 * in <code>s-&gt;chars</code>.
 *
 * @pre <code>0 &lt;= begin &lt; s-&gt;len</code>
 * @param begin the index in <code>s-&gt;chars</code> to start hashing from
 * @param s the string to hash
 * @return the hash value of <code>s</code>
 */
uint32_t castrhash(size_t begin, string *s);


/**
 * Computes a string's hash value, based on the characters at index <code>[begin, s-&gt;len[</code>
 * in <code>s-&gt;chars</code>.
 * Unsafe version of castrhash
 *
 * @pre <code>0 &lt;= begin &lt; strlen(s)</code>
 * @param begin the index in <code>s</code> to start hashing from
 * @param s the string to hash
 * @return the hash value of <code>s</code>
 */
uint32_t castrhash_unsafe(size_t begin, const char *s);

/**
 * Concatenates <code>s1</code> and <code>s2</code> together in <code>result</code>,
 * that is
 * <pre><code>
 * forall i, j; (0 &lt;= i &lt; s1->len ∧ 0 &lt;= j &lt; s2->len)
 *      → (result->chars[i] = s1->chars[i] ∧ result->chars[s1->len + j] = s2->chars[j])
 * </code></pre>
 * @param result[out] the resulting string. Must be already allocated to hold <code>s1->len + s2->len</code> characters.
 * @param s1[in] the first string to concatenate, as in <code>s1 . s2</code>.
 * @param s2[in] the second string to concatenate, as in <code>s1 . s2</code>.
 */
void castrconcat(string *restrict result, string *restrict s1, string *restrict s2);


/**
 * Lexicographically compares <code>s1</code> and <code>s2</code>.
 * Underlying implementation is directly <code>strncmp</code> from <code>"string.h"</code>.
 * @param s1[in] the first string to compare
 * @param s2[in] the second string to compare
 * @return <code>i</code>, as in
 * <pre><code>
 * (s1 = s2) ↔ (i = 0) ∧ (s1 &gt; s2) ↔ (i &gt; 0) ∧ (s1 &lt; s2) ↔ (i &lt; 0)
 * </code></pre>
 */
int castrcmp(string *restrict s1, string *restrict s2);


/**
 * Determines if <code>sub</code> is a substring of <code>s</code>, that is if
 * @code
 * exists i; 0 &lt;= i ∧ i + sub-&gt;len &lt;= s-&gt;len ∧
 *      (forall j; 0 &lt;= j &lt; sub-&gt;len → s-&gt;chars[i + j] = sub-&gt;chars[j])
 * @endcode
 *
 * @param s[in]
 * @param sub[in]
 * @return if <code>sub</code> is indeed a substring of <code>s</code>,
 * a pointer to the character in <code>s</code> that start the <code>sub</code> sequence, <code>NULL</code> otherwise.
 */
const char* castrsubstr(string *restrict s, string *restrict sub);


//UnboundArray* castrsplit(string *restrict s, string *restrict sub); TODO


/**
 * Represents a growable array of characters. Ideal for building strings.
 * Only basic needs are implemented specifically for string buffers in this file.<br>
 */
typedef struct StringBuffer {
    /**
     * The size of one element in <code>elements</code>.
     */
    size_t element_size;
    /**
     * The underlying array of elements.
     */
    char *elements;
    /**
     * A pointer to a function for reallocating memory. It must returns <code>NULL</code> when the reallocation failed,
     * and a non-NULL value when it succeeded.
     * @return[NULLABLE] a pointer to reallocated memory or <code>NULL</code> if it failed
     */
    void* (*reallocFuncPtr)(void*, size_t);
    /**
     * The capacity, i.e. the total space currently allocated for <code>elements</code>, of this array.
     */
    size_t capacity;
    /**
     * The count, i.e. the number of actually used (valid) elements in <code>elements</code>, of this array.
     * @invariant @code 0 &lt;= count &lt;= capacity@endcode
     */
    uint32_t count;
} StringBuffer;





/**
 * Creates a string buffer with an initial capacity. Malloc and realloc functions used are the ones configurable with macro
 * (See top of file).
 * The user should NULL-check this buffer's elements afterwards in case the allocation failed.<br>
 * The user should also use #ca_string_buffer_free(buffer) when they are done with the buffer.<br>
 * Typical usage:
 * @code
 *  StringBuffer my_string_buffer = ca_string_buffer_of(n);
 *  if ( !my_string_buffer.elements ) {
 *      error("StringBuffer allocation failed");
 *      return MEM_ERROR;
 *  }
 * @endcode
 *
 * @param initial_capacity[in] [integral type] the initial number of characters the buffer will be able to hold
 * @return [StringBuffer] the resulting string buffer
 */
#define ca_string_buffer_of(initial_capacity) (StringBuffer){ \
    .element_size = sizeof(char),                             \
    .elements = CASTRING_MALLOC(sizeof(char) * initial_capacity), \
    .reallocFuncPtr = CASTRING_REALLOC,                       \
    .capacity = initial_capacity\
}


/**
 * Frees a string buffer. The free function used is the one configurable with macro (See top of file).
 * @param buffer[in,out] [StringBuffer] the buffer to free the elements
 */
#define ca_string_buffer_free(buffer) CASTRING_FREE(buffer.elements)





/**
 * Pops the last character in <code>buffer</code> and returns it.<br>
 * Typical usage:
 * @code
 *  if (DEBUG && my_string_buffer.count == 0) {
 *      error("Trying to pop from an empty string buffer. That should never happen");
 *      exit(CODE_NEED_FIX);
 *  }
 *  char c = castrbufpop(&my_string_buffer);
 *  ...
 * @endcode
 *
 * @pre <code>buffer-&gt;count &gt; 0</code>
 * @param array[in,out] [StringBuffer*]
 *          the string buffer to pop a character from. Must be non-empty, i.e. <code>buffer-&gt;count &gt; 0</code>.
 * @return [char] the popped character
 */
#define castrbufpop(buffer) caarrpop(char, (UnboundArray*)(buffer))


/**
 * Appends a character <code>c</code> at the end of a string buffer <code>buffer</code>.
 * @param buffer[in,out] the buffer to append a character to
 * @param c[in] the character to append at the end of <code>buffer</code>
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush(StringBuffer *buffer, char c);


/**
 * Appends a string <code>s</code> at the end of a string buffer <code>buffer</code>.
 * @param buffer[in,out] the buffer to append several characters to
 * @param s[in] the string to append at the end of <code>buffer</code>
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush_all(StringBuffer *buffer, string *restrict s);


/**
 * Appends a 32-bit integer at the end of a StringBuffer.
 * @param buffer[in,out] the buffer to append to
 * @param i[in] the integer to append
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush_int(StringBuffer *buffer, uint32_t i);


/**
 * Appends a boolean value at the end of a StringBuffer.
 * @param buffer[in,out] the buffer to append to
 * @param b[in] the boolean value to append
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush_bool(StringBuffer *buffer, bool b);


/**
 * Appends a float at the end of a StringBuffer.
 * @param buffer[in,out] the buffer to append to
 * @param f[in] the float to append
 * @param frac_digits[in] the number of digits of the fractional part of <code>f</code> to show
 * @return whether the operation succeeded or not.
 *      Upon failure, it is possible that <code>buffer->elements</code> became a dangling pointer,
 *      so it is advised to drop completely <code>buffer</code> in that case.
 */
bool castrbufpush_float(StringBuffer *buffer, float f, int frac_digits);







// impl

#ifdef CASTRING_IMPLEMENTATION


#include <math.h>

#define INLINE extern inline __attribute((always_inline))

#ifdef CASTRING_INLINE
#define MAYBE_INLINE INLINE
#else
#define MAYBE_INLINE
#endif





INLINE uint32_t castrhash(size_t begin, string *s) {
    uint32_t hash = 0;
    for (size_t i = begin; i < s->len; i++)
        hash = 31 * hash + s->chars[i];

    return hash;
}

uint32_t castrhash_unsafe(size_t begin, const char *s) {
    uint32_t hash = 0;
    char c;
    s += begin;
    while ((c = *s++))
        hash = 31 * hash + c;
    return hash;
}


MAYBE_INLINE void castrconcat(string *restrict result, string *restrict s1, string *restrict s2) {
    size_t len1 = s1->len;
    size_t len2 = s2->len;
    char *c = result->chars;
    char *c1 = s1->chars;
    char *c2 = s2->chars;

    for (size_t _ = 0; _ < len1; _++)
        *c++ = *c1++;

    for (size_t _ = 0; _ < len2; _++)
        *c++ = *c2++;
}


INLINE int castrcmp(string *restrict s1, string *restrict s2) {
    return strncmp(s1->chars, s2->chars, MAX(s1->len, s2->len));
}


MAYBE_INLINE const char* castrsubstr(string *restrict s, string *restrict sub) {
    size_t len1 = s->len;
    char *c = s->chars;
    size_t j = 0;

    for (size_t _ = 0; _ < len1; _++) {
        if (*c++ == sub->chars[j])
            j++;
        if (j == sub->len)
            return c;
    }
    return NULL;
}


INLINE bool castrbufpush(StringBuffer *buffer, char c) {
    void* mem = caarrpush(char, (UnboundArray*)buffer, c);
    bool failed = mem;
    if (failed)
        CASTRING_FREE(mem);
    return !failed;
}


bool castrbufpush_all(StringBuffer *buffer, string *restrict s) {
    void *mem = caarrpush_all(char, (UnboundArray*)buffer, s->chars, s->len);
    bool failed = mem;
    if (failed)
        CASTRING_FREE(mem);
    return !failed;
}


MAYBE_INLINE bool castrbufpush_int(StringBuffer *buffer, uint32_t i) {
    char c[10];

    int j = 0;
    for (; i && j < 10; j++) {
        c[9 - j] = '0' + (char)(i % 10);
        i /= 10;
    }
    return castrbufpush_all(buffer, &(string){.chars = c + (10 - j), .len = j});
}


INLINE bool castrbufpush_bool(StringBuffer *buffer, bool b) {
    string s = {
            .chars = b ? "true" : "false",
            .len = 5 - b
    };
    return castrbufpush_all(buffer, &s);
}


#define FLOAT_MAX_DIGIT 32
bool castrbufpush_float(StringBuffer *buffer, float f, int frac_digits) {

    if (isnan(f))
        return castrbufpush_all(buffer, &(string){
                .chars = "NaN",
                .len = 3
        });
    else if (isinf(f)) {
        bool neg = signbit(f);
        return castrbufpush_all(buffer, &(string) {
                .chars = neg ? "-INF" : "INF",
                .len = 3 + neg
        });
    } else {
        frac_digits = MIN(frac_digits, FLOAT_MAX_DIGIT);

        if (signbit(f) && !castrbufpush(buffer, '-'))
            return false;


        float int_partf = 0;
        long frac_part =  abs((long)lrintf( modff(f, &int_partf) * powf(10, (float)frac_digits) ));
        long int_part = abs((long)int_partf);

        char c[FLOAT_MAX_DIGIT];


        // integral part of  f

        int i = 0;
        for (; int_part && i < FLOAT_MAX_DIGIT; i++) {
            c[FLOAT_MAX_DIGIT - 1 - i] = '0' + (char)(int_part % 10);
            int_part /= 10;
        }


        if (
                !castrbufpush_all(buffer, &(string){.chars = c + (FLOAT_MAX_DIGIT - i), .len = i}) ||
                !castrbufpush(buffer, '.')
                )
            return false;


        // fractional part of f

        i = 0;
        for (; frac_part && i < frac_digits; i++) {
            c[FLOAT_MAX_DIGIT - 1 - i] = '0' + (char)(frac_part % 10);
            frac_part /= 10;
        }

        for (; i < frac_digits; i++)
            c[FLOAT_MAX_DIGIT - 1 - i] = '0';


        return castrbufpush_all(buffer, &(string){.chars = c + (FLOAT_MAX_DIGIT - i), .len = i});
    }
}

#endif // CASTRING_IMPLEMENTATION


#endif //CARIG_CASTRING_H


//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CARIG_CALOGGING_H
#define CARIG_CALOGGING_H


#if __STDC_VERSION__ <= 201710
#   include <stdbool.h>
#endif



// To not abort upon allocation failure but instead return silently (not log the message),
// define CALOGGING_NO_ABORT_ERROR before including this header.


// ==================== ABSTRACT ====================
//
// Here we define an abstraction for logging messages into file sequentially
// – as opposed to deferred parallel logging or "asynchronous logging" for short.
//
// Related to logging in this file are:
//
// - func caloginit :: char* -> bool
// - func calogclose :: bool
// - macro CA_INFO :: string -> ... -> void
// - macro CA_WARN :: string -> ... -> void
// - macro CA_ERROR :: string -> ... -> void
// - func calogwriteraw :: string -> ... -> void
//
// ==================================================








/**
 * Initializes the logging service, by specifying the log file path.
 * @param filename[in] NULL or the path to the log file which future calls to CA_INFO, CA_WARN, CA_ERROR and
 *              calogwriteraw will write into.
 *              If it is NULL, those will write into stdout, stderr, stderr and stdout respectively instead of a log file.
 *
 * @return whether the operation succeeded or not
 */
bool caloginit(const char *filename);


/**
 * Closes the logging service, discarding the log file's handle.
 * CA_INFO, CA_WARN, CA_ERROR must <b>not</b> be called afterwards.
 * @return whether the operation succeeded or not
 */
bool calogclose();


#define CALOG_HEADER STRINGIFY(__FILE__) " line " STRINGIFY(__LINE__) ": "


/**
 * Logs a info message.
 *
 * @param MSG[in] [char*] the info message to log. <b>Must be preprocessor-constant</b>,
 * otherwise concatenation won't work.
 * @param ...[in] the formatting arguments
 */
#define CA_INFO(MSG, ...) private_info(" [INFO] " CALOG_HEADER MSG, ##__VA_ARGS__)


/**
 * Logs a warn message.
 *
 * @param MSG[in] [char*] the warn message to log. <b>Must be preprocessor-constant</b>,
 * otherwise concatenation won't work.
 * @param ...[in] the formatting arguments
 */
#define CA_WARN(MSG, ...) private_error(" [WARN] " CALOG_HEADER MSG, ##__VA_ARGS__)


/**
 * Logs a error message.
 *
 * @param MSG[in] [char*] the error message to log. <b>Must be preprocessor-constant</b>,
 * otherwise concatenation won't work.
 * @param ...[in] the formatting arguments
 */
#define CA_ERROR(MSG, ...) private_error(" [ERROR] " CALOG_HEADER MSG, ##__VA_ARGS__)




/**
 * Logs a message. Internal use only!
 * @param msg the message to log
 * @param ... the formatting arguments
 */
void private_info(const char *msg, ...);


/**
 * Logs a message. Internal use only!
 * @param msg the message to log
 * @param ... the formatting arguments
 */
void private_error(const char *msg, ...);


/**
 * Writes a formatted message to the log file, without adding metadata (such as the time).
 * @param msg[in] the formatting string
 * @param ... the format arguments
 */
void calogwriteraw(const char *msg, ...);








// impl

#ifdef CALOGGING_IMPLEMENTATION


#include <stdio.h>
#include <time.h>
#include <stdarg.h>

#define INLINE extern inline __attribute((always_inline))

#ifdef CAMAP_INLINE
#define MAYBE_INLINE INLINE
#else
#define MAYBE_INLINE
#endif






#ifdef CALOGGING_NO_ABORT_ERROR
#define CHECK_TRUE(EXPR)                                        \
    if (!(EXPR)) {                                              \
        fprintf(stderr, "failed to log a message. continuing"); \
        return;                                                 \
    }
#else
#define CHECK_TRUE(EXPR)                                   \
    if (!(EXPR)) {                                         \
        fprintf(stderr, "failed to log a message. fatal"); \
        exit(1);                                           \
    }
#endif



static FILE *info_stream;
static FILE *warn_error_stream;






bool caloginit(const char *filename) {
    if (!filename) {
        info_stream = stdout;
        warn_error_stream = stderr;
        return true;
    }
    return (info_stream = warn_error_stream = fopen(filename, "a")) != NULL;
}


bool calogclose() {
    if (info_stream == stdout)
        return true;
    return !fclose(info_stream);
}


void calogwriteraw(const char *msg, ...) {
    va_list args;
    va_start(args, msg);
    vfprintf(info_stream, msg, args);
    va_end(args);
}


void private_info(const char *msg, ...) {
    time_t now;
    time(&now);
    va_list args;
    va_start(args, msg);
    CHECK_TRUE(
            fputs(ctime(&now), info_stream) >= 0 &&
            vfprintf(info_stream, msg, args) >= 0 &&
            fputc('\n', info_stream) >= 0
    )
    va_end(args);
}

void private_error(const char *msg, ...) {
    time_t now;
    time(&now);
    va_list args;
    va_start(args, msg);
    CHECK_TRUE(
            fputs(ctime(&now), warn_error_stream) >= 0 &&
            vfprintf(warn_error_stream, msg, args) >= 0 &&
            fputc('\n', warn_error_stream) >= 0
    )
    va_end(args);
}



#endif //CALOGGING_IMPLEMENTATION


#endif //CARIG_CALOGGING_H




#endif //CARIG_CARIG_H
