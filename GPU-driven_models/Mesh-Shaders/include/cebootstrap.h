//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CENOTE_CEBOOTSTRAP_H
#define CENOTE_CEBOOTSTRAP_H

#include "cetypes.h"
#include "cefunctions.h"



/**
 * Notes:
 * To make shader printf work, you need to:
 * - Enable instance layer VK_LAYER_KHRONOS_validation, if not already done for debug callback
 * - Extends VkInstanceCreateInfo in pNext with a VkValidationFeaturesEXT structure, followed by the usual
 *   VkDebugUtilsMessengerCreateInfoEXT structure. VkDebugUtilsMessengerCreateInfoEXT pNext must be NULL, so
 *   VkValidationFeaturesEXT must be put before in the pNext chain.
 * - In that very VkValidationFeaturesEXT structure, set pEnabledValidationFeatures to
 *   VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT, and increment enabledValidationFeatureCount.
 * - Make sure the validation debug callback shows messages with severity VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT.
 */

void createSwapchainAndImages(
        VkPhysicalDevice physicalDevice,
        VkDevice device,
        VkSwapchainCreateInfoKHR *swapchainCreateInfo,
        const VkAllocationCallbacks *allocator,
        VkImage *swapchainImages);


void initVk(VulkanSetupInput *input, VulkanSetupOutput *output);


/**
 * Destroys main Vulkan bootstrap objects in order, that is:
 * <ol>
 *      <li>VkDevice</li>
 *      <li>VkSurfaceKHR</li>
 *      <li>VkDebugUtilsMessengerEXT</li>
 *      <li>VkInstance</li>
 * </ol>
 * @param vkInstance
 * @param allocator NULLABLE
 * @param debugMessenger
 * @param surface
 * @param device
 */
void destroy_Device_Surface_DebugMessenger_Instance(
        VkInstance vkInstance,
        const VkAllocationCallbacks *allocator,
        VkDebugUtilsMessengerEXT debugMessenger,
        VkSurfaceKHR surface,
        VkDevice device);


#endif //CENOTE_CEBOOTSTRAP_H