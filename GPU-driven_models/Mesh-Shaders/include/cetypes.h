//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#ifndef CENOTE_CETYPES_H
#define CENOTE_CETYPES_H

#include "ceextern.h"



typedef struct CeBuffer {
    VkBuffer vkBuffer;
    VmaAllocation vmaAllocation;
} CeBuffer;

typedef struct CeImage {
    VkImage vkImage;
    VmaAllocation vmaAllocation;
} CeImage;

typedef struct CeTexture {
    CeImage ceImage;
    VkImageView vkImageView;
} CeTexture;

typedef struct CeSampledTexture {
    CeTexture ceTexture;
    VkSampler vkSampler;
} CeSampledTexture;


typedef struct SwapchainSetupInfo {
    VkSurfaceFormatKHR surfaceFormat;
    VkPresentModeKHR presentMode;
    VkImageUsageFlags imageUsage;
    uint32_t imageCount;
} SwapchainSetupInfo;

typedef struct QueueFeatures {
    VkQueue *queues;
    uint32_t *queueFamilyIndices;
    uint32_t capacity;
} QueueFeatures;



typedef struct QueueFamilySelectInfo {
    bool (**queueFamiliesSelectPredicate)(VkQueueFamilyProperties, VkBool32);
    array queueFamiliesFallback;
} QueueFamilySelectInfo;

typedef struct VulkanSetupInput {
    VkApplicationInfo *appInfo;
    VkAllocationCallbacks *allocCallbacks;
    array requestedLayers;
    array requestedInstanceExtensions;
    array requestedDeviceExtensions;
    void *pNextDeviceCreateInfo;
    char *GPUName;
    VkResult (*getInstanceExtensions_ptr)(uint32_t*, const char***);
    void (*freeInstanceExtensions_ptr)(const char**);
    VkResult (*createVkSurfaceKHR_ptr)(VkInstance, const VkAllocationCallbacks *allocator, VkSurfaceKHR *surface);
    VkPhysicalDeviceFeatures *requestedDeviceFeatures;
    QueueFamilySelectInfo *requestedQueuesInfo;
    SwapchainSetupInfo *requestedSwapchainInfo;
    VmaAllocatorCreateInfo *requestedAllocatorInfo;
} VulkanSetupInput;

typedef struct VulkanSetupOutput {
    VkInstance *instance;
    VkDebugUtilsMessengerEXT *debugMessenger;
    VkPhysicalDevice *physicalDevice;
    VkDevice *device;
    QueueFeatures *queueFeatures;
    VkSwapchainCreateInfoKHR *swapchainInfo;
    VmaAllocator *allocator;
} VulkanSetupOutput;
#endif //CENOTE_CETYPES_H
