#version 450
#pragma shader_stage(fragment)
#extension GL_EXT_debug_printf : enable
#extension GL_EXT_nonuniform_qualifier : require

layout (set = 0, binding = 1) uniform sampler2D myTexture[];

layout (location = 0) in PerTriangle {
    flat uint texID;
    flat float scale;
} perTriangle;

layout (location = 5) in PerVertex {
    vec2 uvs;
    vec4 tint;
    float brightness;
} perVertex;

layout (location = 0) out vec4 color;


void main() {
    //color = IN.color * IN.scale;

    color = vec4(vec3(perVertex.brightness), 1)
                * perVertex.tint * texture(myTexture[nonuniformEXT(perTriangle.texID)], perVertex.uvs);
}
