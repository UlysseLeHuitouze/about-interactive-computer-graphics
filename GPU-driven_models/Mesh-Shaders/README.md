# Mesh Shader Demo

A toy sample covering Minecraft-like models rendered in mesh shaders.

## Points of interest

- `assets/shaders` contains the three shaders used. Their working, compiled SPIR-V counterparts is also offered. These compiled counterparts were compiled with `glslc` and with `--target-env=vulkan1.3 -DWARP_SIZE=32` as arguments. Ultimately, if you own a GPU with a warp size different than 32, you can recompile the shaders with the correct `WARP_SIZE` value, though it won't affect performance much since very few models are rendered.
- `./main.c` contains the entry point of the sample. This sample was originally a mere test for my vulkan boostrapping library, cenote, thus `src` and `include` contain everything related to cenote while `main.c` is in the root folder.
- `Mesh-Shaders.md` explains the as yet unfinished approach to rendering Minecraft models in mesh shaders. It goes into details as much as explaining the bit-packing strategy and a future extension of task shaders to support Minecraft's chunks-like system on the GPU. It assumes prior knowledge in Vulkan, GLSL and voxel rendering.
