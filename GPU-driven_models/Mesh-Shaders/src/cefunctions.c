//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#include "cefunctions.h"


#define VKRESULTTOA_HELPER(dst, vkResult_enum) case vkResult_enum:\
    return #vkResult_enum;

const char* cevkresulttoa(VkResult r) {
    switch (r) {
        VKRESULTTOA_HELPER(_ascii, VK_SUCCESS)
        VKRESULTTOA_HELPER(_ascii, VK_NOT_READY)
        VKRESULTTOA_HELPER(_ascii, VK_TIMEOUT)
        VKRESULTTOA_HELPER(_ascii, VK_EVENT_SET)
        VKRESULTTOA_HELPER(_ascii, VK_EVENT_RESET)
        VKRESULTTOA_HELPER(_ascii, VK_INCOMPLETE)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_OUT_OF_HOST_MEMORY)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_OUT_OF_DEVICE_MEMORY)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_INITIALIZATION_FAILED)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_DEVICE_LOST)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_MEMORY_MAP_FAILED)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_LAYER_NOT_PRESENT)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_EXTENSION_NOT_PRESENT)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_FEATURE_NOT_PRESENT)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_INCOMPATIBLE_DRIVER)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_TOO_MANY_OBJECTS)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_FORMAT_NOT_SUPPORTED)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_FRAGMENTED_POOL)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_UNKNOWN)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_OUT_OF_POOL_MEMORY)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_INVALID_EXTERNAL_HANDLE)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_FRAGMENTATION)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS)
        VKRESULTTOA_HELPER(_ascii, VK_PIPELINE_COMPILE_REQUIRED)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_SURFACE_LOST_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_NATIVE_WINDOW_IN_USE_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_SUBOPTIMAL_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_OUT_OF_DATE_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_INCOMPATIBLE_DISPLAY_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_VALIDATION_FAILED_EXT)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_INVALID_SHADER_NV)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_NOT_PERMITTED_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT)
        VKRESULTTOA_HELPER(_ascii, VK_THREAD_IDLE_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_THREAD_DONE_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_OPERATION_DEFERRED_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_OPERATION_NOT_DEFERRED_KHR)
        VKRESULTTOA_HELPER(_ascii, VK_ERROR_COMPRESSION_EXHAUSTED_EXT)
        VKRESULTTOA_HELPER(_ascii, VK_RESULT_MAX_ENUM)
        default:
            return "Unknown result";
    }
}

void cereadbytes(array *byteBuffer, const char *filename) {
    FILE *file = fopen(filename, "rb");
    BEXIT(file == NULL, "failed to open %s. File may not exist", filename);

    if (fseek(file, 0, SEEK_END) != 0) {
        fclose(file);
        CA_ERROR("failed to read %s", filename);
        exit(1);
    }

    long len = ftell(file);
    rewind(file);

    byteBuffer->elements = smalloc(len);
    byteBuffer->len = len;

    fread(byteBuffer->elements, sizeof(char), byteBuffer->len, file);

    fclose(file);
}