//
// Created by Ulysse Le Huitouze on 04/07/2023.
//

#include <string.h>
#include <math.h>
#include "cetransfer_services.h"

// TODO refactor to batch queue submits and to allow passing arbitrary staging buffers.


static VkDevice device;
static VkQueue queue;
static VkSharingMode sharing_mod;
static uint32_t queueFamilyCount;
static uint32_t *queueFamilyIndices;

static VmaAllocator vmaAllocator;
static VkCommandPool cmdPool;
static VkCommandBuffer *cmdBuffers;
static VkFence *fences;
static CeBuffer *stagingBuffers;
static uint32_t amount;

void siTSPrimeService(VkDevice vkDevice,
                      VkQueue transferQueue,
                      VkSharingMode sharingMode,
                      uint32_t queueFamilyIndexCount,
                      const uint32_t *pQueueFamilyIndices,
                      VmaAllocator allocator,
                      VkCommandPool transferPool,
                      uint32_t uploadBufferingCount)
{
    device = vkDevice;
    queue = transferQueue;
    sharing_mod = sharingMode;
    queueFamilyCount = queueFamilyIndexCount;

    queueFamilyIndices = nmalloc(sizeof *queueFamilyIndices * queueFamilyIndexCount);
    memcpy(queueFamilyIndices, pQueueFamilyIndices, sizeof *queueFamilyIndices * queueFamilyIndexCount);


    vmaAllocator = allocator;
    cmdPool = transferPool;
    amount = uploadBufferingCount;

    cmdBuffers = nmalloc(sizeof *cmdBuffers * uploadBufferingCount);
    fences = nmalloc(sizeof *fences * uploadBufferingCount);
    // calloc to prevent siTSKillService from segfault
    stagingBuffers = ncalloc(uploadBufferingCount, sizeof *stagingBuffers);



    VkCommandBufferAllocateInfo cmdBufferAllocInfo = {
            .sTypeDefault(VkCommandBufferAllocateInfo),
            .commandPool = transferPool,
            .commandBufferCount = uploadBufferingCount,
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY
    };
    CHECK_VK(vkAllocateCommandBuffers(device, &cmdBufferAllocInfo, cmdBuffers));

    VkFenceCreateInfo fenceCreateInfo = {
            .sTypeDefault(VkFenceCreateInfo),
            .flags = VK_FENCE_CREATE_SIGNALED_BIT
    };

    for (uint32_t i = 0; i < uploadBufferingCount; i++)

        CHECK_VK(vkCreateFence(device, &fenceCreateInfo, NULL, fences + i));
}



void siTSKillService()
{
    vkWaitForFences(device, amount, fences, VK_TRUE, UINT64_MAX);

    for (uint32_t i = 0; i < amount; i++)
    {
        vmaDestroyBuffer(vmaAllocator, stagingBuffers[i].vkBuffer, stagingBuffers[i].vmaAllocation);
        vkDestroyFence(device, fences[i], NULL);
    }
    vkFreeCommandBuffers(device, cmdPool, amount, cmdBuffers);
    free(stagingBuffers);
    free(fences);
    free(cmdBuffers);
    free(queueFamilyIndices);
}


static uint32_t current_frame;

/*
 * preUpload and postUpload edits as much global variables as possible as to reduce repetition over
 * siTSStageToBuffer and siTSStageToImage. (no lambdas)
 */

static void preUpload(VkDeviceSize size)
{
    const VkBufferCreateInfo bufferCreateInfo = {
            .sTypeDefault(VkBufferCreateInfo),
            .size = size,
            .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT
    };
    const VmaAllocationCreateInfo allocationCreateInfo = {
            .usage = VMA_MEMORY_USAGE_AUTO,
            .flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
            .requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
    };

    vkWaitForFences(device, 1, fences + current_frame, VK_TRUE, UINT64_MAX);
    vkResetFences(device, 1, fences + current_frame);

    vmaDestroyBuffer(vmaAllocator, stagingBuffers[current_frame].vkBuffer, stagingBuffers[current_frame].vmaAllocation);

    vmaCreateBuffer(vmaAllocator, &bufferCreateInfo, &allocationCreateInfo,
                    &stagingBuffers[current_frame].vkBuffer, &stagingBuffers[current_frame].vmaAllocation, NULL);

    vkResetCommandBuffer(cmdBuffers[current_frame], 0);

    VkCommandBufferBeginInfo beginInfo = {
            .sTypeDefault(VkCommandBufferBeginInfo),
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
    };
    CHECK_VK(vkBeginCommandBuffer(cmdBuffers[current_frame], &beginInfo));
}

static void postUpload()
{

    CHECK_VK(vkEndCommandBuffer(cmdBuffers[current_frame]));

    VkSubmitInfo submitInfo = {
            .sTypeDefault(VkSubmitInfo),
            .commandBufferCount = 1,
            .pCommandBuffers = cmdBuffers + current_frame
    };

    CHECK_VK(vkQueueSubmit(queue, 1, &submitInfo, fences[current_frame]));

    current_frame = (current_frame + 1) % amount;
}

void siTSStageToBuffer(void *pData, VkBuffer dst, VkDeviceSize dstOffset, VkDeviceSize size)
{
    preUpload(size);

    void *ptr;
    CHECK_VK(vmaMapMemory(vmaAllocator, stagingBuffers[current_frame].vmaAllocation, &ptr));
    memcpy(ptr, pData, size);
    vmaUnmapMemory(vmaAllocator, stagingBuffers[current_frame].vmaAllocation);

    vkCmdCopyBuffer(cmdBuffers[current_frame], stagingBuffers[current_frame].vkBuffer, dst, 1, &(VkBufferCopy) {
        .size = size,
        .dstOffset = dstOffset
    });

    postUpload();
}

void siTSStageToImage(const void *pData,
                      VkImage dst,
                      const VkBufferImageCopy *bufferImageCopy,
                      const VkImageSubresourceRange *subresourceRange,
                      const VkImageLayout finalLayout,
                      const VkAccessFlags dstAccessMask,
                      const VkPipelineStageFlags dstStageMask,
                      const VkDeviceSize size)
{
    preUpload(size);

    void *ptr;
    CHECK_VK(vmaMapMemory(vmaAllocator, stagingBuffers[current_frame].vmaAllocation, &ptr));
    memcpy(ptr, pData, size);
    vmaUnmapMemory(vmaAllocator, stagingBuffers[current_frame].vmaAllocation);

    VkImageMemoryBarrier transferBarrier = {
            .sTypeDefault(VkImageMemoryBarrier),
            .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            .image = dst,
            .subresourceRange = *subresourceRange,
            //.srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT
    };
    vkCmdPipelineBarrier(cmdBuffers[current_frame], VK_PIPELINE_STAGE_HOST_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
                         0, NULL, 0, NULL,
                         1, &transferBarrier);
    vkCmdCopyBufferToImage(cmdBuffers[current_frame], stagingBuffers[current_frame].vkBuffer, dst,
                           VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, bufferImageCopy);

    transferBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    transferBarrier.newLayout = finalLayout;
    transferBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    transferBarrier.dstAccessMask = dstAccessMask;

    vkCmdPipelineBarrier(cmdBuffers[current_frame], VK_PIPELINE_STAGE_TRANSFER_BIT, dstStageMask, 0,
                         0, NULL, 0, NULL,
                         1, &transferBarrier);

    postUpload();
}

void siTSStageTexture(const char *filename, CeTexture *texture, const VkPipelineStageFlags dstStageMask, _Bool mipmap_enabled)
{
    uint32_t width, height;
    void *bytes;
    {
        int w = 0, h = 0, c = 4;
        bytes = stbi_load(filename, &w, &h, &c, STBI_rgb_alpha);
        BEXIT(bytes == NULL, "Failed to load %s image", filename);
        width = w;
        height = h;
    }

    uint32_t mipmap_levels = mipmap_enabled ? (uint32_t) log2(MAX(width, height)) + 1 : 1;

    VkDeviceSize imgSize = width * height * 4;
    const VkExtent3D imageExtent = {
            .width = width,
            .height = height,
            .depth = 1
    };

    {
        VkImageCreateInfo imageCreateInfo = {
                .sTypeDefault(VkImageCreateInfo),
                .imageType = VK_IMAGE_TYPE_2D,
                .format = VK_FORMAT_R8G8B8A8_SRGB,
                .extent = imageExtent,
                .mipLevels = mipmap_levels,
                .arrayLayers = 1,
                .samples = VK_SAMPLE_COUNT_1_BIT,
                .tiling = VK_IMAGE_TILING_OPTIMAL,
                .usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
                .sharingMode = sharing_mod,
                .queueFamilyIndexCount = queueFamilyCount,
                .pQueueFamilyIndices = queueFamilyIndices
        };
        VmaAllocationCreateInfo allocationCreateInfo = {
                .usage = VMA_MEMORY_USAGE_AUTO,
                .flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT,
                .priority = 1.f
        };
        vmaCreateImage(vmaAllocator, &imageCreateInfo, &allocationCreateInfo,
                      &texture->ceImage.vkImage, &texture->ceImage.vmaAllocation, NULL);
    }

    VkImageMemoryBarrier transferBarrier = {
            .sTypeDefault(VkImageMemoryBarrier),
            .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            .image = texture->ceImage.vkImage,
            .subresourceRange = {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    //.baseMipLevel = 0,
                    .levelCount = mipmap_levels,
                    //.baseArrayLayer = 0,
                    .layerCount = 1
            },
            .srcQueueFamilyIndex = 0,
            //.srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT
    };

    preUpload(imgSize);

    void *ptr;
    CHECK_VK(vmaMapMemory(vmaAllocator, stagingBuffers[current_frame].vmaAllocation, &ptr));
    memcpy(ptr, bytes, imgSize);
    vmaUnmapMemory(vmaAllocator, stagingBuffers[current_frame].vmaAllocation);

    // base mipmap level
    {
        // vkCmdCopyBufferToImage doesn't allow VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL as dstImageLayout,
        // so we must first transition from UNDEFINED to TRANSFER_DST and to later transition from TRANSFER_DST to TRANSFER_SRC
        // TRANSFER_SRC layout is needed for vkCmdBlitImage to work

        // all mipmap levels: from UNDEFINED to TRANSFER_DST
        vkCmdPipelineBarrier(cmdBuffers[current_frame], VK_PIPELINE_STAGE_HOST_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             0,
                             0, NULL, 0, NULL,
                             1, &transferBarrier);

        VkBufferImageCopy bufferImageCopy = {
                .imageExtent = imageExtent,
                .imageSubresource = {
                        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                        .layerCount = 1
                }
        };
        vkCmdCopyBufferToImage(cmdBuffers[current_frame], stagingBuffers[current_frame].vkBuffer, texture->ceImage.vkImage,
                               VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &bufferImageCopy);

        transferBarrier.subresourceRange.levelCount = 1;
        transferBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        transferBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        transferBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        transferBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;

        // only base mipmap level: from TRANSFER_DST to TRANSFER_SRC
        vkCmdPipelineBarrier(cmdBuffers[current_frame], VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                             0,
                             0, NULL, 0, NULL,
                             1, &transferBarrier);
    }

    // mipmap gen
    VkImageBlit imageBlit = {
            .srcSubresource = {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    //.mipLevel = 0,
                    .layerCount = 1
            },
            .dstSubresource = {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    .layerCount = 1
            },
            .srcOffsets[1] = {.x = (int32_t) width, .y = (int32_t) height, .z = 1},
            .dstOffsets[1] = {.z = 1}
    };

    VkImageSubresourceRange mipmapSubresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .levelCount = 1,
            //.baseArrayLayer = 0,
            .layerCount = 1
    };

    for (uint32_t i = 1; i < mipmap_levels; i++)
    {
        imageBlit.dstSubresource.mipLevel = i;
        imageBlit.dstOffsets[1].x = (int32_t) (width >> i);
        imageBlit.dstOffsets[1].y = (int32_t) (height >> i);
        mipmapSubresourceRange.baseMipLevel = i;
        vkCmdBlitImage(cmdBuffers[current_frame],
                       texture->ceImage.vkImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                       texture->ceImage.vkImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                       1, &imageBlit, VK_FILTER_LINEAR);
    }

    // transition first mipmap level from TRANSFER_SRC to TRANSFER_DST for all the mipmap levels to have the same layout
    transferBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    transferBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    transferBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    transferBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    // only base mipmap level: from TRANSFER_SRC to TRANSFER_DST
    vkCmdPipelineBarrier(cmdBuffers[current_frame], VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
                         0, NULL, 0, NULL,
                         1, &transferBarrier);

    // final transition of all mipmap levels from TRANSFER_DST to SHADER_READ_ONLY
    transferBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT | VK_ACCESS_TRANSFER_READ_BIT;
    transferBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    transferBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    transferBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    transferBarrier.subresourceRange.levelCount = mipmap_levels;
    // all mipmap levels: from TRANSFER_DST to SHADER_READ_ONLY
    vkCmdPipelineBarrier(cmdBuffers[current_frame], VK_PIPELINE_STAGE_TRANSFER_BIT, dstStageMask, 0,
                         0, NULL, 0, NULL,
                         1, &transferBarrier);

    postUpload();

    stbi_image_free(bytes);


    VkImageViewCreateInfo viewCreateInfo = {
            .sTypeDefault(VkImageViewCreateInfo),
            .image = texture->ceImage.vkImage,
            .format = VK_FORMAT_R8G8B8A8_SRGB,
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .subresourceRange = {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    //.baseMipLevel = 0,
                    .levelCount = mipmap_levels,
                    //.baseArrayLayer = 0,
                    .layerCount = 1
            }
    };
    CHECK_VK(vkCreateImageView(device, &viewCreateInfo, NULL, &texture->vkImageView));
}


void siTSStageTexture$Default(const char *filepath, CeTexture *texture)
{
    siTSStageTexture(filepath, texture, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 1);
}