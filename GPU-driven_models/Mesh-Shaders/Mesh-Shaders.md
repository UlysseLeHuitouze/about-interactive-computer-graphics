# Mesh Shaders

Here we present the approach taken to rendering Minecraft-like models that are stored on the GPU.

## About [Minecraft models](https://minecraft.fandom.com/wiki/Tutorials/Models#Block_models)

### Terminology

First off, let us split existing Minecraft models into 2 categories, **Plain** and **Custom** models. The reason for that is that in a voxel engine such as Minecraft, most of the voxel models are in fact a simple voxel without any details, requiring only 6 faces. For these models, there is no need to store vertex information at all. From now on, **Plain models** will refer to models which are comprised of a single cube taking the entirety of the voxel, thus bypassing the need to store vertex information. **Custom models** will refer to non-plain models, that is models which require individual vertex information.

<table style="border-collapse: collapse;" align="center">
<tr>
<td align="center"><img src="https://static.wikia.nocookie.net/minecraft_gamepedia/images/9/99/Furnace_(S)_JE4.png" alt="Plain model"></img></td>
<td align="center"><img src="https://static.wikia.nocookie.net/minecraft_gamepedia/images/a/a0/Oak_Stairs_(N)_JE7_BE6.png" alt="Custom model"></img></td>
</tr>
<tr>
<td align="center"><b>Fig. 1.</b> Plain model<br><a href="https://minecraft.fandom.com/wiki/Furnace" target="_blank">Image source</a></td>
<td align="center"><b>Fig. 2.</b> Custom model<br><a href="https://minecraft.fandom.com/wiki/Stairs" target="_blank">Image source</a></td>
</tr>
</table>

Then, we will also split the **Plain models** group into two groups, **Single** and **Plain** models. The reason for that is that in a voxel engine such as Minecraft, most of the **Plain models** are actually comprised of one and only one texture applied on all 6 faces. For these models, there is no need to store neither vertex information nor all 6 face's texture information at all (only 1 texture information suffices). From here on, **Plain models** will refer to the same models as before. However, some **Plain models** will be further categorized and called **Single models**. These **Single models** will only need to store one face's texture information, improving memory usage by a factor of 6 over **Plain models**.

<table style="border-collapse: collapse;" align="center">
<tr>
<td align="center"><img src="https://static.wikia.nocookie.net/minecraft_gamepedia/images/9/99/Furnace_(S)_JE4.png" alt="Plain model"></img></td>
<td align="center"><img src="https://static.wikia.nocookie.net/minecraft_gamepedia/images/0/08/Diorite.png" alt="Single model"></img></td>
</tr>
<tr>
<td align="center"><b>Fig. 3.</b> Plain model<br><a href="https://minecraft.fandom.com/wiki/Furnace" target="_blank">Image source</a></td>
<td align="center"><b>Fig. 4.</b> Single model<br><a href="https://minecraft.fandom.com/wiki/Diorite" target="_blank">Image source</a></td>
</tr>
</table>

As for **Custom models**, we will split their group into three parts: **Custom Complete models**, **Custom Final models** and **Cutom Abstract models**. The reason for that is that in a voxel engine such as Minecraft, many **Custom models** inherit vertex information from another **Custom model**, overriding only textures (e.g. wooden stairs and stone stairs models both inherit the vertices of a common stairs model and then define their own face's texture). This mechanism allows significant memory savings and is necessary to realistically store Minecraft-like models in the GPU-local memory.

To conclude, we will use the following terminology:

- **Single models** are models comprised of the normalized cube ((0, 0, 0) to (1, 1, 1) in voxel space) and nothing else. Furthermore, all their six faces feature the same texture.
- **Plain models** are models comprised of the normalized cube ((0, 0, 0) to (1, 1, 1) in voxel space) and nothing else. Their six faces may display textures different from each other.
- **Custom Complete models** are models comprised of an arbitrary number of independant faces of arbitray texture, size and position.
- **Custom Abstract models** are models comprised of an arbitrary number of independant faces of arbitray size and position. They don't define their faces' texture on their own, therefore cannot be rendered directly.
- **Custom Final models** are models whose faces' size and position are inherited from their "parent" which must be  **custom abstract**. They define their own texture against their "parent"'s faces definition.

### Storage strategy

- #### Per-face information

##### Vertex positions

According to [Minecraft specification](https://minecraft.fandom.com/wiki/Tutorials/Models#Block_models), face's vertices coordinates can range from -16.0 to 32.0, where 0 to 16 is the voxel space (the normalized cube is divided in 16<sup>3</sup> cubes in Minecraft). For simplicity's sake, we will work with the range [0.0; 48.0]. Additionally, we will only allow for 0.2 steps in precision. Indeed, 0.1 steps in precision would yield 49 × 10 - 9 = 481 possible unique values, whereas 0.2 steps yield 49 × 5 - 4 = 241 possible unique values which fit in one byte.

With this 0.2-step precision in mind, let us define <code>encode<sub>vertex</sub></code> and <code>decode<sub>vertex</sub></code> which translate any component of a vertex's 3D position respectively from the range [-16.0; 32.0] to the range ⟦0; 255⟧ and from the range ⟦0; 255⟧ to the range [-16.0; 32.0].

<pre>encode<sub>vertex</sub>: [-16.0; 32.0] →  ⟦0; 255⟧
                   x      ↦ ⌊5(x + 16)⌋</pre>

<pre>decode<sub>vertex</sub>: ⟦0; 255⟧ → {-16.0, -15.8, -15.6, ..., 34.8, 35.0}
                x    ↦               0.2x - 16</pre>


The storage of vertex positions only concerns **custom complete** and **custom abstract** models and will require 6 bytes per face (since faces' edges are considered axis-aligned).

##### Texture coordinates

According to [Minecraft specification](https://minecraft.fandom.com/wiki/Tutorials/Models#Block_models), texture coordinates (uvs) can range from 0 to 16. The reason uvs uses 17 different values despite the texture space (as well as the voxel space) being paritioned 16 times for each dimension is because they don't refer to the texels resulting from the ×16 partitioning but rather to the planes interleaving these texels.

For that reason, we will work instead with the range ⟦0; 15⟧ as to have it fit in 0.5 byte. The only downside of doing that is that we can no longer express nothingness with 0 or 16: With Minecraft specification, "*(0, 0) to (0, 0)*" can be seen as "*from the top-left corner to the top-left corner of the texel (0, 0)*"  whereas for us "*(0, 0) to (0, 0)*" will be "*fetch texel (0, 0)*".

With this reducation in mind, let us define <code>reduct<sub>face uvs</sub></code> and <code>normalize<sub>face uvs</sub></code> which map any pair of uv vectors (descibing a face's uv coordinates) respectively from the space ⟦0; 16⟧<sup>4</sup> into the space ⟦0; 15⟧<sup>4</sup> and from the space ⟦0; 15⟧<sup>4</sup> into the space [0; 1]<sup>4</sup>. In layman's terms, <code>reduct<sub>face uvs</sub></code> turns Minecraft compliant models into our reducted compact uvs and <code>normalize<sub>face uvs</sub></code> turns our reducted compact uvs into normalized coordinates, i.e. something that can be directly fed to glsl `texture` function.

<pre>reduct<sub>face uvs</sub>: ⟦0; 16⟧<sup>4</sup> →  ⟦0; 15⟧<sup>4</sup>
            (uv<sub>1</sub>, uv<sub>2</sub>) ↦ (min(uv<sub>1</sub>, (15, 15)), max(uv<sub>2</sub> - (1, 1), (0, 0)))</pre>

<pre>normalize<sub>face uvs</sub>: ⟦0; 15⟧<sup>4</sup> →  [0; 1]<sup>4</sup>
               (uv<sub>1</sub>, uv<sub>2</sub>) ↦ (uv<sub>1</sub> / 16, (uv<sub>2</sub> + (1, 1)) / 16)</pre>


where `min` and `max` apply respectively <code>min<sub><b>R</b></sub></code> and <code>max<sub><b>R</b></sub></code> component-wise to two vectors.

The storage of texture coordinates only concerns **custom complete** and **custom abstract** models and will require 2 bytes per face.

##### Texture id

Models of any type except **custom abstract** need to store a "pointer" of some sort to their faces' texture. One unsigned 32-bit integer should amply suffice.

##### Face direction

**Custom complete**, **custom abstract** and **custom final** models should store for each of their faces its direction (north, south, west, east, up, down), for lightning as well as culling purposes.

The storage of a face's direction will require 3 bits, and can be packed into the 32-bit integer texture id (having more than 2<sup>29</sup> different textures will never happen).

##### Cull enabled

**Custom complete**, **custom abstract** and **custom final** models should store for each of their faces whether culling is enabled. Indeed, if a custom model has a face of direction D that doesn't touch the normalized cube's face of corresponding direction D, then that face should never be culled to prevent visual bugs (e.g. slabs in Minecraft have either their upper face or lower face, depending on the slab placement, which shouldn't be culled).

The storage of whether a face should be cullable or not will require 1 bit, and can be packed into the 32-bit integer texture id (having more than 2<sup>28</sup> different textures will never happen).

##### Material

In a voxel engine such as Minecraft, there isn't quite a concept of "material" for the GPU since models of a same material can have different textures. However, material information of a model is still useful for us for one thing, and that is culling. Indeed, let v<sub>1</sub>, v<sub>2</sub> be two adjacent voxels, i.e. such that their normalized cubes have one face in common, where v<sub>2</sub> is at D of v<sub>1</sub>, D a direction. Let F<sub>1</sub> be a face of v<sub>1</sub> facing D and F<sub>2</sub> be a face of v<sub>2</sub> facing the opposite of D. Then, if culling is enabled for F<sub>2</sub> and F<sub>1</sub> has "the power of cull", i.e. F<sub>1</sub> is a square of edge 1 matching a face of the normalized cube and F<sub>1</sub> is opaque, F<sub>2</sub> can be culled.

The storage of whether a face can hide/cull the faces of opposite direction of a neighbouring voxel will require 1 bit and can be packed into the 32-bit integer texture id (having more than 2<sup>27</sup> different textures will never happen).

- #### Per-model information

##### Parent id

**Custom final** models need to store a "pointer" of some sort to their **custom abstract** parent. One unsigned 32-bit integer should amply suffice.

##### Particle texture id

**Single**, **plain**, **custom complete** and **custom final** models should store a "pointer" of some sort to the texture used for particle effects, e.g. when the voxel is destroyed. One unsigned 32-bit integer should amply suffice.

##### Face count

Since **custom complete**, **custom abstract** and **custom final** models all allow for an arbitrary number of faces, their face count should be stored somewhere. We could also use the sentinel-value approach to delimit the end of a sequence; However, it is important to understand that such an approach is incompatible with parallelism.

As for the space this face count will require per model, allocating one 32-bit integer is too big of a waste, and we will instead settle for a maximum of 256 faces possible per model. Furthermore, we won't pack this byte into, say particle texture id integer, but instead will have a seperate array for custom models' face count. The complete justification of this choice will be detailed in the later section **Code intricacies**.

Finally, since **custom final models** feature the same number of face than their **custom abstract** parent, we will store face count for **custom complete** and **custom final models** only. Again, the complete justification of this choice will be detailed in the later section **Code intricacies**.

##### Custom model pointers

For the exact same reason mentioned in **Face count** section, it is not possible to directly access custom models data with just an identifier, i.e. it is not possible to know where custom model#i's data starts unless we iterate on the **face counts** of all the custom models before i. We will need to add one level of indirection of some sort.

In the end, we will have a seperate array of integers to store pointers to the "main" model array, i.e. the array where the actual model data is stored, which could be directly indexed.

- #### Summary

Putting everything together, we get: 

##### Single models

```
1st_uint[31:0]  --> particle texture id
2nd_uint[29:0]  --> face's texture id (for all 6 faces)
2nd_uint[30:30] --> cull enabled (for all 6 faces)
2nd_uint[31:31] --> culling-capable (for all 6 faces)
```

##### Plain models

```
1st_uint[31:0]        --> particle texture id
For all n ∈ ⟦0; 5⟧,
(2 + n)th_uint[29:0]  --> face #n texture id
(2 + n)th_uint[30:30] --> cull enabled for face #n
(2 + n)th_uint[31:31] --> culling-capable for face #n
```

##### Custom complete models

```
1st_uint[31:0]         --> particle texture id
Let N be the number of faces of the model.
For all n ∈ ⟦0; N - 1⟧,
(2 + 3n)th_uint[26:0]  --> face #n texture id
(2 + 3n)th_uint[29:27] --> face #n's direction
(2 + 3n)th_uint[30:30] --> cull enabled for face #n
(2 + 3n)th_uint[31:31] --> culling-capable for face #n
(3 + 3n)th_uint[7:0]   --> face #n's x1
(3 + 3n)th_uint[15:8]  --> face #n's y1
(3 + 3n)th_uint[23:16] --> face #n's z1
(3 + 3n)th_uint[31:24] --> face #n's x2
(4 + 3n)th_uint[7:0]   --> face #n's y2
(4 + 3n)th_uint[15:8]  --> face #n's z2
(4 + 3n)th_uint[19:16] --> face #n's u1
(4 + 3n)th_uint[23:20] --> face #n's v1
(4 + 3n)th_uint[27:24] --> face #n's u2
(4 + 3n)th_uint[31:28] --> face #n's v2
```

##### Custom final models

```
1st_uint[31:0]        --> particle texture id
2nd_uint[31:0]        --> parent id
Let N be the number of faces of the model.
For all n ∈ ⟦0; N - 1⟧,
(3 + n)th_uint[26:0]  --> face #n texture id
(3 + n)th_uint[29:27] --> face #n's direction
(3 + n)th_uint[30:30] --> cull enabled for face #n
(3 + n)th_uint[31:31] --> culling-capable for face #n
```

##### Custom abstract models

```
Let N be the number of faces of the model.
For all n ∈ ⟦0; N - 1⟧,
(2n)th_uint[7:0]       --> face #n's x1
(2n)th_uint[15:8]      --> face #n's y1
(2n)th_uint[23:16]     --> face #n's z1
(2n)th_uint[31:24]     --> face #n's x2
(1 + 2n)th_uint[7:0]   --> face #n's y2
(1 + 2n)th_uint[15:8]  --> face #n's z2
(1 + 2n)th_uint[19:16] --> face #n's u1
(1 + 2n)th_uint[23:20] --> face #n's v1
(1 + 2n)th_uint[27:24] --> face #n's u2
(1 + 2n)th_uint[31:28] --> face #n's v2
```

## About the [Mesh Shader](https://www.khronos.org/blog/mesh-shading-for-vulkan) pipeline

The pipeline we will be using is the new mesh shader pipeline. Despite the name "mesh shader pipeline", the CPU will not be launching mesh shaders directly but will rather launch task shaders first. What makes this task shader interesting is that it can do advanced parallel culling on the GPU then can launch more or less mesh shader workgroups depending on its needs. If one were to search for an equivalent to the task shader with the traditional pipeline, then it would be the act of launching a compute shader prior to the main traditional pipeline. You can think of the task + mesh shader duo just like the trio compute shader + traditional pipeline + indirect draw commands.

The trio compute shader + traditional pipeline + indirect draw commands would result in a GPU command timeline like this:

```
1. issue pipeline barrier: compute stage waits for fragment stage
2. launch compute shader for fast culling
3. issue pipeline barrier: vertex stage waits for compute stage
4. launch traditional pipeline, starts with vertex stage
```

whereas the mesh shader pipeline would result in a GPU command timeline like this:

```
1. launch task shader
```

In our case, the task shader will run at voxel rate while the mesh shader will run at vertex rate. In other words, each task shader workgroup will handle one voxel world's chunk, and each invocation within a workgroup will handle the corresponding voxel in the chunk. An invocation of the task shader will be tasked to compute the number of vertices to be rendered for its corresponding voxel and to launch an appropriate number of mesh shader workgroups. As for the mesh shader, each of its invocation will be tasked to compute the actual position of the vertex on the screen, like a vertex shader would do.

### About mesh shader dispatch

As said before, the purpose of the task shader is to launch more or less mesh shader invocations depending on the number of vertices to be rendered for a voxel model. However, both task and mesh shaders are designed to take advantage of the hardware and require to be launched workgroup by workgroup. The obvious approach would be to set mesh shader's workgroup size to 1 so that the task shader can launch exactly `number_of_vertices_to_render` workgroups, that is `number_of_vertices_to_render` invocations. However, the whole "workgroup" concept isn't just for show and is intended to be mapped on actual hardware. Say your hardware features warps of size N; Then launching 1 mesh shader workgroup of size 1 will result in launching N threads where only 1 would work.

Let `ws` be the size of the mesh shader's workgroups, `N` the actual hardware warp size and `m` the number of mesh shader's workgroups being launched. The actual number of threads that will be claimed for that launch can be computed with `⌈ws / N⌉ × N × m` while only `ws × m` threads will work.

In the end, in order to maximize occupency in any scenario, we will be using mesh shaders with workgroups' size equals to the hardware workgroup/subgroup/warp's size. With this realistic approach, the task shader would not launch `number_of_vertices_to_render` mesh shader workgroups, but rather `⌈number_of_vertices_to_render / N⌉ = ⌊(number_of_vertices_to_render + N - 1) / N⌋` mesh shader workgroups, where `N` is the hardware warp size.

### About task shader dispatch

Earlier, we decided that the task shader will run at a voxel rate, i.e. one invocation of  the task shader is responsible for one voxel, different than the other invocation's. What about the task shader workgroup size?

The task shader workgroup size must be a multiple of `N`, the hardware warp size, out of occupency concerns, just like before with mesh shaders. Other than this constraint, we should focus our attention on a trade-off between memory usage and early culling on the CPU.

Indeed, partitioning the voxel world into small pieces – let's call these "chunks" from now on, and making the task shader workgroups match those chunks in terms of size, means sending more position information to the GPU (one position per chunk) but allows to save entire task shader workgroups in case all voxels in a chunk appear to be void (which happens quite often in a voxel world, c.f. sky) (in which case, such piece need not to be sent to the GPU at all).

On the other hand, partitioning the voxel world into bigger chunks means having to send less position information to the GPU, but entails huge waste of GPU power (since having all voxels void in a big chunk will happen less often), where hundreds of task shader workgroups might work on void voxels for nothing even with not so big chunks (e.g. with chunks comprised of 16<sup>3</sup> voxels, worst case scenario is having only one voxel non-void, in which case 127.96875 warps are wasted, for a GPU featuring a warp size of 32).

This problem is analogous to combating fragmentation with segregated lists in a memory-constrained environment, so there is no right answer.

In the end, for this small-scale Vulkan example, we will settle with chunks of size 4<sup>3</sup> which is the least common multiple of both NVIDIA and AMD GPU warp sizes. The CPU will have to send a 96-bit position vector for each chunk to the GPU, for a task shader workgroup to know its relative position in the voxel world. This partitioning will allow for the CPU to save one task shader workgroup per chunk full of void.

## About the chunk system

Now that we decided on a chunk size for our voxel world, let us define a couple other things regarding the chunk system view by the task shader.

First, we will need to have an array containing all the loaded chunks, called the "main array", paired with a list listing the chunks to actually render (the non-empty ones that passed frustrum check done by the CPU), called the "metadata array".

We will also need an array for storing each chunk relative position in the voxel world.

In the end, the task shader will have access to the following:

- `uint chunk_data[]`, the main array that contains a model identifier for each voxel.

- `uint chunk_ptr[]`, the metadata array that contains for each chunk a 32-bit pointer into the `chunk_data` array followed by 3 integers for the chunk's position.

Example usage:

```glsl
// CPU launched (chunk_count, 1, 1) task shader workgroups
// where chunk_count is the number of loaded non-empty chunks
// that passed frustrum culling (also the length of chunk_ptr / 4)


// A certain task shader invocation

/** it defines among other things:
 * - gl_WorkGroupSize (equal to (4, 4, 4))
 * - gl_WorkGroupID
 * - gl_LocalInvocationID
 * - gl_LocalInvocationIndex
 */

 // the chunk id in chunk_ptr
uint chunkId = gl_WorkGroupID.x;

// the chunk id in chunk_data this time
// (one level of indirection)
uint chunkLoc = chunk_ptr[chunkID];

ivec3 chunkPos = (chunk_ptr[chunkID + 1], chunk_ptr[chunkID + 2], chunk_ptr[chunkID + 3]);
ivec3 voxelPos = chunkPos + gl_LocalInvocationID;

// actual model for this invocation's voxel.
uint modelId = chunk_data[chunkLoc + gl_LocalInvocationIndex];
...
```

<div align="center"><b>Snippet 1.</b> Retrieving basic information in task shader</div>



Additionally, we will trim each voxel model id in `chunk_data` by 6 bits to put a "face mask", a 6-bit mask that indicates for each direction whether the neighbour voxel in that direction is culling-capable (see **Material** section). This allows us to directly know if cullable faces of the model facing that direction should be rendered, without having to first load all its 6 neighbouring voxels.

Example usage:

```glsl
// the chunk id in chunk_ptr
uint chunkId = gl_WorkGroupID.x;

// the chunk id in chunk_data this time
// (one level of indirection)
uint chunkLoc = chunk_ptr[chunkID];

ivec3 chunkPos = (chunk_ptr[chunkID + 1], chunk_ptr[chunkID + 2], chunk_ptr[chunkID + 3]);
ivec3 voxelPos = chunkPos + gl_LocalInvocationID;

uint modelId_and_cullMask = chunk_data[chunkLoc + gl_LocalInvocationIndex];

// actual model for this invocation's voxel.
uint modelId = bitfieldExtract(modelId_and_cullMask, 0, 26);
// bit #i at One means that faces of direction #i that can be culled should be
uint faceMask = bitfieldExtract(modelId_and_cullMask, 26, 6);
...
```

<div align="center"><b>Snippet 2.</b> Retrieving basic information in task shader Pt. 2</div>



## Code Intricacies

Let us now look at some parts of the pipeline code that require our attention.

From now on:

- `mat4 matrices.proj` will refer to the projection matrix of the current frame.

- `mat4 matrices.view` will refer to the view matrix of the current frame.

- `uint chunks.data[]` will refer to the chunk system main array (called `chunk_data` in the previous section).

- `uint chunks.ptr[]` will refer to the chunk system metadata array (called `chunk_ptr` in the previous section).

- `SINGLE_MODEL_COUNT` will refer to the number of available (loaded by the CPU) **single models** for rendering.

- `PLAIN_MODEL_COUNT` will refer to the number of available (loaded by the CPU) **plain models** for rendering.

- `CUSTOM_COMPLETE_MODEL_COUNT` will refer to the number of available (loaded by the CPU) **custom complete models** for rendering.

- `CUSTOM_FINAL_MODEL_COUNT` will refer to the number of available (loaded by the CPU) **custom final models** for rendering.

- `CUSTOM_ABSTRACT_MODEL_COUNT` will refer to the number of available (loaded by the CPU) **custom abstract models** for rendering.

- `CUSTOM_MODEL_SIZE` will refer the number of 32-bit integers claimed to store **custom models**.

- `SINGLE_MODEL_STRIDE` will refer to the number of 32-bit integers required to store 1 **single model**: equal to 2.

- `PLAIN_MODEL_STRIDE` will refer to the number of 32-bit integers required to store 1 **plain model**: equal to 7.

- `PLAIN_MODEL_FACE_STRIDE` will refer to the number of 32-bit integers required to store 1 **plain** or **single model**'s per-face data, as opposed to the per-model data: equal to 1.

- `CUSTOM_COMPLETE_MODEL_FACE_STRIDE` will refer to the number of 32-bit integers required to store 1 **custom complete model**'s per-face data, as opposed to the per-model data: equal to 3.

- `CUSTOM_FINAL_MODEL_FACE_STRIDE` will refer to the number of 32-bit integers required to store 1 **custom final model**'s per-face data, as opposed to the per-model data: equal to 1.

- `CUSTOM_ABSTRACT_MODEL_FACE_STRIDE` will refer to the number of 32-bit integers required to store 1 **custom abstract model**'s per-face data, as opposed to the per-model data: equal to 2.

- `PLAIN_MODEL_HEADER_SIZE` will refer to the number of 32-bit integers required to store 1 **plain** or **single model**'s per-model data, as opposed to the per-face data: equal to 1.

- `CUSTOM_COMPLETE_MODEL_HEADER_SIZE` will refer to the number of 32-bit integers required to store 1 **custom complete model**'s per-model data, as opposed to the per-face data: equal to 1.

- `CUSTOM_FINAL_MODEL_HEADER_SIZE` will refer to the number of 32-bit integers required to store 1 **custom final model**'s per-model data, as opposed to the per-face data: equal to 1.

- `uint models.data[]` will refer to the array containing the models actual data. Its length will always exactly be `SINGLE_MODEL_COUNT * 2 + PLAIN_MODEL_COUNT * 7 + CUSTOM_MODEL_SIZE`. The data it contains will actually be sorted so that **single models** come first, followed by **plain models**, then **custom complete**, **custom final** and finally **custom abstract models**.

- For every model in `models.data`, we shall assume that their faces are ordered such that faces which are eligible for culling (see **Cull enabled** section) come first. Furthermore, within the set of faces eligible for culling, the faces will be ordered such that the ones facing north come first, then south, west, east, upward and finally downward.

- `uint8_t models.face_counts[]` will refer to the array containing **custom models**' face counts, described in **Face count** section. Its length will always exactly be `CUSTOM_COMPLETE_MODEL_COUNT + CUSTOM_FINAL_MODEL_COUNT`. Note that storing the face count for **custom abstract models** rather than **custom final models** will save some memory but will force one level of indirection for accessing those face counts (i.e. a **custom final model** would first need to load its parent's pointer, then lookup into the face count array with it).

- `uint models.ptr[]` will refer to the array containing **custom model** pointers into `models.data`. Its length will always exactly be `CUSTOM_COMPLETE_MODEL_COUNT + CUSTOM_FINAL_MODEL_COUNT`.

- `payload` will refer to the information passed down from the task stage to the mesh stage.

- To each direction will be associated an integral, like so: `North=0, South=1, West=2, East=3, Up=4, Down=5`.

- `WARP_SIZE` will refer to the size of a warp for the compilation target.

### Task Shader

```glsl
// output
taskPayloadSharedEXT struct {
    ivec4 voxelPos_and_modelId_and_cullMask;
    uint vertexCount;
} payload;

void main()
{
    // basic information
    uint chunkId = gl_WorkGroupID.x;
    uint chunkLoc = chunk_ptr[chunkID];

    ivec3 chunkPos = (chunk_ptr[chunkID + 1], chunk_ptr[chunkID + 2], chunk_ptr[chunkID + 3]);
    ivec3 voxelPos = chunkPos + gl_LocalInvocationID;

    uint modelId_and_cullMask = chunk_data[chunkLoc + gl_LocalInvocationIndex];

    uint modelId = bitfieldExtract(modelId_and_cullMask, 0, 26);
    uint faceMask = bitfieldExtract(modelId_and_cullMask, 26, 6);

    payload.voxelPos_and_modelId_and_cullMask = (voxelPos, modelId_and_cullMask);

    // number of faces to actually be rendered
    uint unculledFaceCount = ΛcomputeUnculledFaceCount(modelId, faceMask);
    uint unculledVertexCount = unculledFaceCount << 2;
    payload.vertexCount = unculledVertexCount;

    EmitMeshTasksEXT( (unculledVertexCount + WARP_SIZE - 1) / WARP_SIZE, 1, 1 );
}
```

<div align="center"><b>Snippet 3.</b> Proposed task shader code</div>

where:

- `ΛcomputeUnculledFaceCount/2` is defined as

```glsl
uint ΛcomputeUnculledFaceCount(uint modelId, uint faceMask)
{
    return modelId < SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT ?
        bitCount(faceMask) : ΛcomputeUnculledFaceCountCCModel(modelId, faceMask);
}
```

`ΛcomputeUnculledFaceCountCCModel` will be defined later since it is the main source of intricacies, both in the task and mesh shader. (See the **Unculled Face ID computation and resolving it against the model** section)

### Mesh Shader

```glsl
const ivec3 CUBE_VERTICES[] = {

    // north
    ivec3(1, 1, 0),
    ivec3(1, 0, 0),
    ivec3(0),
    ivec3(0, 1, 0),

    // south
    ivec3(0, 1, 1),
    ivec3(0, 0, 1),
    ivec3(1, 0, 1),
    ivec3(1),

    // west
    ivec3(0, 1, 0),
    ivec3(0),
    ivec3(0, 0, 1),
    ivec3(0, 1, 1),

    // east
    ivec3(1),
    ivec3(1, 0, 1),
    ivec3(1, 0, 0),
    ivec3(1, 1, 0),

    // up
    ivec3(0, 1, 0),
    ivec3(0, 1, 1),
    ivec3(1),
    ivec3(1, 1, 0),

    // down
    ivec3(0, 0, 1),
    ivec3(0),
    ivec3(1, 0, 0),
    ivec3(1, 0, 1)
};


// input
taskPayloadSharedEXT struct {
    ivec4 voxelPos_and_modelId_and_cullMask;
    uint vertexCount;
} payload;

//output
// length equal to second argument passed to SetMeshOutputsEXT
perprimitiveEXT out PerTriangle {
    flat uint textureId;
    ...
} perTriangles[];

// length equal to first argument passed to SetMeshOutputsEXT
out PerVertex {
    vec2 uvs;
    ...
} perVertices[];

void main()
{
    /* the total amount of vertices this workgroup is to produce.
       only 2 cases possible:
       either the task shader launched more than gl_WorkGroupID.x + 1 mesh shader workgroups,
       in which case, every invocation of this workgroup should work.
       or this workgroup is the last of the batch launched by the task shader,
       i.e. the task shader launched gl_WorkGroupID.x + 1 workgroups,
       indicating that some invocations will possibly have to stay idle (from the software pov).
    */
    uint workgroupMaxVertices = ΛcomputeWorkgroupMaxVertices();
    if ( gl_LocalInvocationIndex >= workgroupMaxVertices )
        return;

    SetMeshOutputsEXT(workgroupMaxVertices, workgroupMaxVertices >> 1);

    // current face Id to handle among the unculledFaceCount faces computed in the task stage
    uint faceIDAmongWorkgroups = ΛretrieveFaceIDAmongWorkgroups();
    uint vertexIDAmongFace = ΛretrieveVertexIDAmongFace();
    uint triangleIDAmongWorkgroup = ΛretrieveTriangleIDAmongWorkgroup();

    /* normalized uv coordinates for the vertexIDAmongFace-th vertex of a face
       e.g. top-left vertex will have uv (0, 0), bottom-left (0, 1),
       bottom-right (1, 1) and top-right (1, 0).
    */
    vec2 normalizedUVs = ΛcomputeNormalizedUV(vertexIDAmongFace);

    uint modelId_and_cullMask = payload.voxelPos_and_modelId_and_cullMask.w;
    uint modelId = bitfieldExtract(modelId_and_cullMask, 0, 26);
    uint faceMask = bitfieldExtract(modelId_and_cullMask, 26, 6);

    vec3 vertexPos;
    vec2 actualUVs;
    uint faceDir;
    uint textureId;
    if (modelId < SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT)
    {
        /* "real" face id, i.e. can be used to index directly into models.data.
           faceIDAmongWorkgroups can be seen as the face id relative to unculled faces,
           while faceIDAmongModel would be the face id relative to the whole model.
           In a sense, we are "resolving" faceIDAmongModel against faceIDAmongWorkgroups.
        */
        uint faceIDAmongModel = ΛresolveFaceIDPlainModel(faceIDAmongWorkgroups, faceMask);

        // actual pointer used to access to the model data inside models.data
        uint modelPtr = SINGLE_MODEL_STRIDE * min(modelId, SINGLE_MODEL_COUNT) +
                         max(0, PLAIN_MODEL_STRIDE * ( int(modelId) - int(SINGLE_MODEL_COUNT) ));

        // pointer to the current face inside models.data
        uint facePtr = modelPtr + PLAIN_MODEL_HEADER_SIZE +
            uint(modelId >= SINGLE_MODEL_COUNT) * PLAIN_MODEL_FACE_STRIDE * faceIDAmongModel;

        vertexPos = CUBE_VERTICES[faceIDAmongModel * 4 + vertexIDAmongFace];
        // single and plain models aren't allowed to customize their uv coordinates. keep it normalized
        actualUVs = normalizedUVs;
        // direct correspondence for cuboid voxels
        faceDir = faceIDAmongModel;

        texture_and_cull_info = models.data[facePtr];

        textureId = bitfieldExtract(texture_and_cull_info, 0, 30);

    } else if (modelId < SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT + CUSTOM_COMPLETE_MODEL_COUNT)
    {

    	// models.face_counts and models.ptr only concern custom models
    	// so we need to translate modelId.
    	uint modelIdAmongCustomModels = modelId - (SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT);

        uint faceIDAmongModel = ΛresolveFaceIDCustomModel(
            true /*true if custom complete, false if custom final*/,
            modelIdAmongCustomModels, faceIDAmongWorkgroups, faceMask
        );

        // actual pointer used to access to the model data inside models.data
        uint modelPtr = models.ptr[modelIdAmongCustomModels];

        // pointer to the current face inside models.data
        uint facePtr = modelPtr + CUSTOM_COMPLETE_MODEL_HEADER_SIZE + faceIDAmongModel * CUSTOM_COMPLETE_MODEL_FACE_STRIDE;

        texture_and_dir_and_cull_info = models.data[facePtr];
        textureId = bitfieldExtract(texture_and_dir_and_cull_info, 0, 27);
        faceDir = bitfieldExtract(texture_and_dir_and_cull_info, 27, 3);

        uint vertexPos_and_UVs_part1 = models.data[facePtr + 1];
        uint vertexPos_and_UVs_part2 = models.data[facePtr + 2];

        vertexPos = ΛcomputeVertexPos(faceDir, vertexIDAmongFace, vertexPos_and_UVs_part1, vertexPos_and_UVs_part2);
        actualUVs = ΛcomputeUVs(normalizedUVs, vertexPos_and_UVs_part2);

    } else // assuming custom final model. custom abstract model must never be used directly
    {

        // models.face_counts and models.ptr only concern custom models
    	// so we need to translate modelId.
    	uint modelIdAmongCustomModels = modelId - (SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT);

        uint faceIDAmongModel = ΛresolveFaceIDCustomModel(
            false /*true if custom complete, false if custom final*/,
            modelIdAmongCustomModels, faceIDAmongWorkgroups, faceMask
        );

        // actual pointer used to access to the model data inside models.data
        uint modelPtr = models.ptr[modelIdAmongCustomModels];

        // pointer to the current face inside models.data
        uint facePtr = modelPtr + CUSTOM_FINAL_MODEL_HEADER_SIZE + faceIDAmongModel * CUSTOM_FINAL_MODEL_FACE_STRIDE;

        texture_and_dir_and_cull_info = models.data[facePtr];
        textureId = bitfieldExtract(texture_and_dir_and_cull_info, 0, 27);
        faceDir = bitfieldExtract(texture_and_dir_and_cull_info, 27, 3);

        // vertex positions and uv coordinates are stored in the parent model
        // (one extra level of indirection)
        uint parentPtr = models.data[modelPtr + 1];
        uint parentFacePtr = parentPtr + faceIDAmongModel * CUSTOM_ABSTRACT_MODEL_FACE_STRIDE;

        uint vertexPos_and_UVs_part1 = models.data[parentFacePtr + 1];
        uint vertexPos_and_UVs_part2 = models.data[parentFacePtr + 2];

        vertexPos = ΛcomputeVertexPos(faceDir, vertexIDAmongFace, vertexPos_and_UVs_part1, vertexPos_and_UVs_part2);
        actualUVs = ΛcomputeUVs(normalizedUVs, vertexPos_and_UVs_part2);
    }

    // length of gl_MeshVerticesEXT equal to first argument passed to SetMeshOutputsEXT
    /*
       Parentheses are put to hint clunky compiler:
        A(BX) roughly yields 6 MAD and 2 multiplications while
        (AB)X roughly yields 15 MAD and 5 multiplications
    */
    gl_MeshVerticesEXT[gl_LocalInvocationIndex].gl_Position = matrices.proj * (
                    matrices.view * (vertexPos + payload.voxelPos_and_modelId_and_cullMask.xyz, 1.0)
    );

    perTriangles[triangleIDAmongWorkgroup].textureId = textureId;

    perVertices[gl_LocalInvocationIndex].uvs = actualUVs;

    gl_PrimitiveTriangleIndicesEXT[triangleIDAmongWorkgroup] = ΛcomputeCurrentTriangleAmongWorkgroupIndices(
        triangleIDAmongWorkgroup
    );
}
```

<div align="center"><b>Snippet 4.</b> Proposed mesh shader code</div>

where:

- `ΛcomputeWorkgroupMaxVertices/0` is defined as

```glsl
uint ΛcomputeWorkgroupMaxVertices()
{
    return min(gl_WorkGroupSize.x, payload.vertexCount - gl_WorkGroupSize.x * gl_WorkGroupID.x);
}
```

- `ΛretrieveFaceIDAmongWorkgroups/0` is defined as

```glsl
uint ΛretrieveFaceIDAmongWorkgroups()
{
#if INTELLIGIBLE
    return (gl_LocalInvocationIndex + gl_WorkGroupID.x * gl_WorkGroupSize.x) / 4;
#else
    return bitfieldExtract(gl_GlobalInvocationID.x, 2, 30);
#endif
}
```

- `ΛretrieveVertexIDAmongFace/0` is defined as

```glsl
uint ΛretrieveVertexIDAmongFace()
{
    // only works because gl_WorkGroupSize.x is a multiple of 4
#if INTELLIGIBLE
    return gl_LocalInvocationIndex % 4;
#else
    return bitfieldExtract(gl_LocalInvocationIndex, 0, 2);
#endif
}
```

- `ΛretrieveTriangleIDAmongWorkgroup/0` is defined as

```glsl
uint ΛretrieveTriangleIDAmongWorkgroup()
{
    /*
       for simplicity sake, we shall assume that the first 2 vertices of
       a face belong to the first triangle of that face and the last 2 vertices to the
       second triangle; even though the first and third vertices actually
       belong to both triangles.
    */

    // only works because gl_WorkGroupSize.x is a multiple of 2
#if INTELLIGIBLE
    return gl_LocalInvocationIndex / 2;
#else
    return bitfieldExtract(gl_LocalInvocationIndex, 1, 31);
#endif
}
```

- `ΛcomputeNormalizedUV/1` is defined as

```glsl
vec2 ΛcomputeNormalizedUV(uint vertexIDAmongFace)
{
#if INTELLIGIBLE
    bool a = vertexIDAmongFace / 2;
    bool b = vertexIDAmongFace % 2;
    return (a, a ? !b : b);
#else
    /* we do not use vertexIDAmongFace directly but rather gl_LocalInvocationIndex
       to reduce instruction dependencies
       (useful when manually inlining the functions defined in this file)
    */
    bool a = bitfieldExtract(gl_LocalInvocationIndex, 1, 1);
    bool b = bitfieldExtract(gl_LocalInvocationIndex, 0, 1);
    return (a, a ^^ b);
#endif
}
```

- `ΛcomputeVertexPos/4` is defined as

```glsl
vec3 ΛcomputeVertexPos(uint faceDir, uint vertexIDAmongFace, uint vertexPos_and_UVs_part1, uint vertexPos_and_UVs_part2)
{
#if INTELLIGIBLE
    // raw, encoded vertex positions
    uint x1 = bitfieldExtract(vertexPos_and_UVs_part1, 0, 8);
    uint y1 = bitfieldExtract(vertexPos_and_UVs_part1, 8, 8);
    uint z1 = bitfieldExtract(vertexPos_and_UVs_part1, 16, 8);
    uint x2 = bitfieldExtract(vertexPos_and_UVs_part1, 24, 8);
    uint y2 = bitfieldExtract(vertexPos_and_UVs_part2, 0, 8);
    uint z2 = bitfieldExtract(vertexPos_and_UVs_part2, 8, 8);
    // decoded then normalized vertex positions
    // (decode_vertex is the function defined in the Vertex positions section)
    // (reminder: decode_vertex x = 0.2x - 16.0)
    float fx1 = decode_vertex(x1) / 16.0;
    float fy1 = decode_vertex(y1) / 16.0;
    float fz1 = decode_vertex(z1) / 16.0;
    float fx2 = decode_vertex(x2) / 16.0;
    float fy2 = decode_vertex(y2) / 16.0;
    float fz2 = decode_vertex(z2) / 16.0;
    // side-note: this only work for axis-aligned faces.
    // an entire different approach has to be taken in order to render non-axis-aligned faces
    return mix( (fx1, fy1, fz1), (fx2, fy2, fz2), CUBE_VERTICES[faceDir * 4 + vertexIDAmongFace] );

#else

    return mix(
        (
            bitfieldExtract(vertexPos_and_UVs_part1, 0, 8),
            bitfieldExtract(vertexPos_and_UVs_part1, 8, 8),
            bitfieldExtract(vertexPos_and_UVs_part1, 16, 8)
        ),
        (
            bitfieldExtract(vertexPos_and_UVs_part1, 24, 8),
            bitfieldExtract(vertexPos_and_UVs_part2, 0, 8),
            bitfieldExtract(vertexPos_and_UVs_part2, 8, 8)
        ),
        CUBE_VERTICES[faceDir * 4 + vertexIDAmongFace]
    ) * 0.0125 - 1.0;

#endif
}
```

- `ΛcomputeUVs/2` is defined as

```glsl
vec2 ΛcomputeUVs(normalizedUVs, vertexPos_and_UVs_part2)
{
#if INTELLIGIBLE
    // raw, encoded uv coordinates
    uint u1 = bitfieldExtract(vertexPos_and_UVs_part2, 16, 4);
    uint v1 = bitfieldExtract(vertexPos_and_UVs_part2, 20, 4);
    uint u2 = bitfieldExtract(vertexPos_and_UVs_part2, 24, 4);
    uint v2 = bitfieldExtract(vertexPos_and_UVs_part2, 28, 4);
    // normalized (and translated for uv2) uv coordinates
    // (normalize_face_uvs is the function defined in the Texture coordinates section)
    // (reminder: normalize_face_uvs uv1 uv2 = (uv1 / 16, ( uv2 + (1, 1) ) / 16))
    vec4 all_uvs = normalize_face_uvs( (u1, v1), (u2, v2) );

    return mix(
        all_uvs.xy,
        all_uvs.zw,
        normalizedUVs
    );

#else

    /*
       Note that we don't gain anything from extracting the common '*0.0625' part from the two vectors, i.e.
        vec4 t = (a, b, c, d) * 0.0625;
        return mix(
            t.xy, t.zw + 0.0625, normalizedUVs
        );

       instead of
        return mix((a, b) * 0.0625, (c, d) * 0.0625 + 0.0625, normalizedUVs);

       since (c, d) * 0.0625 + 0.0625 can be done in 1 vector FMA instruction.
       The latter also saves 128 bits worth of registers when working with clunky compilers
    */

    return mix(
        (
            bitfieldExtract(vertexPos_and_UVs_part2, 16, 4),
            bitfieldExtract(vertexPos_and_UVs_part2, 20, 4)
        ) * 0.0625,
        (
            bitfieldExtract(vertexPos_and_UVs_part2, 24, 4),
            bitfieldExtract(vertexPos_and_UVs_part2, 28, 4)
        ) * 0.0625 + 0.0625,
        normalizedUVs
    );

#endif
}
```

- `ΛcomputeCurrentTriangleAmongWorkgroupIndices/1` is defined as

```glsl
uvec3 ΛcomputeCurrentTriangleAmongWorkgroupIndices(uint triangleIDAmongWorkgroup)
{
    /*
       The goal of this function is to produce the following pattern:
        triangleIDAmongWorkgroup = 0 ==> (0, 1, 2)		# first quad
        triangleIDAmongWorkgroup = 1 ==> (0, 2, 3)
        triangleIDAmongWorkgroup = 2 ==> (4, 5, 6)		# second quad
        triangleIDAmongWorkgroup = 3 ==> (4, 6, 7)
        triangleIDAmongWorkgroup = 4 ==> (8, 9, 10)		# third quad
        triangleIDAmongWorkgroup = 5 ==> (8, 10, 11)
        ...
    */
#if INTELLIGIBLE
    uint offset = 4 * (triangleIDAmongWorkgroup / 2);
    uvec3 basePattern = triangleIDAmongWorkgroup % 2 == 0 ? (0, 1, 2) : (0, 2, 3);
    return basePattern + offset;
#else
    // reminder: triangleIDAmongWorkgroup = bitfieldExtract(gl_LocalInvocationIndex, 1, 31);
    // as usual, inline triangleIDAmongWorkgroup to reduce instruction dependency and
    // hoping that the compiler will somehow optimize uses of gl_LocalInvocationIndex.

    const bool t = bitfieldExtract(gl_LocalInvocationIndex, 1, 1);
    return bitfieldInsert(gl_LocalInvocationIndex, 0, 0, 2) + ( 0, (1, 2) + (t, t) );
#endif
}
```



### Unculled Face ID computation and resolving it against the model

At this point, three functions are yet to be defined: `ΛcomputeUnculledFaceCountCCModel/2`, `ΛresolveFaceIDPlainModel/2` and `ΛresolveFaceIDCustomModel/4`. Earlier, we defined `ΛcomputeUnculledFaceCount/2` hastily for plain and single models and hid the most complicated code behind `ΛcomputeUnculledFaceCountCCModel/2`. Now is the time to properly grasp the bigger picture.

Let `f #i` be the (i + 1)th face of a model. Let `FS := {f #_}` be the set of all a model's faces and <code>N := <i>the faces of FS facing north eligible for culling</i></code>, <code>S := <i>the faces of FS facing south eligible for culling</i></code>, <code>W := <i>the faces of FS facing west eligible for culling</i></code>, <code>E := <i>the faces of FS facing east eligible for culling</i></code>, <code>U := <i>the faces of FS facing upward eligible for culling</i></code>, <code>D := <i>the faces of FS facing downward eligible for culling</i></code>, <code>Γ := <i>the faces of FS ineligible for culling</i> = FS / (N ∪ S ∪ W ∪ E ∪ U ∪ D)</code> such that:

`∀ f #n ∈ N, f #s ∈ S, f #w ∈ W, f #e ∈ E, f #u ∈ U, f #d ∈ D, f #γ ∈ Γ, 0 ≤ n < s < w < e < u < d < γ`.

Additionally, let <code>FM ∈ {0, 1}<sup>6</sup></code> be a face mask, i.e. `FM` i-th component dictates whether faces facing direction #i - 1 (directions start at 0) should be rendered (0 ≡ False, 1 ≡ True).

Finally, let `el` be a function that takes an integer and a set of faces, defined as follow:

<pre>el: {0, 1, 2, ...} × {{f #_} | } → {f #_}
                (i, FS)          ↦ f #k such that:
    (∃FS<sub>sub</sub> a strict-subset of FS, FS<sub>sub</sub> = {f #j | j < k, f #j ∈ FS} ∧ |FS<sub>sub</sub>| = i) ∧
   ¬(∃FS<sub>sub</sub> a strict-subset of FS, FS<sub>sub</sub> = {f #j | j < k, f #j ∈ FS} ∧ |FS<sub>sub</sub>| > i)</pre>


It is obvious that the first argument of `el` must be less than the cardinal of the second argument of `el`, otherwise the predicate <code>|FS<sub>sub</sub>| = i</code> will never be satisfied.

- The goal of `ΛcomputeUnculledFaceCount/2` is essentially to prune faces whose facing's direction is represented by a 0 in the face mask `FM` from `FS`. Let us call this pruned-against-`FM` version of `FS` `FSM`. Then,

    <pre>FSM := {f #i | FM<sub>1</sub> = 1, f #i ∈ N} ∪ {f #i | FM<sub>2</sub> = 1, f #i ∈ S} ∪ {f #i | FM<sub>3</sub> = 1, f #i ∈ W}
         ∪ {f #i | FM<sub>4</sub> = 1, f #i ∈ E} ∪ {f #i | FM<sub>5</sub> = 1, f #i ∈ U} ∪ {f #i | FM<sub>6</sub> = 1, f #i ∈ D} ∪ {f #i | f #i ∈ Γ}</pre>
    
    
    
    With that in mind, we now know that the goal of `EmitMeshTasksEXT` in the task shader is to launch `4 × |FSM|` mesh shader invocations (rounded up to the nearest multiple of `WARP_SIZE`). Therefore, one mesh shader invocation ID out of 4 will correspond to a face in `FSM`, noted `invID ↦ el(⌊invID / 4⌋, FSM)`.


- The goal of `ΛresolveFaceID*` is to, from a mesh shader invocation ID, retrieve a concrete integer to index `FS`. We define `resolve` (conceptual version of `ΛresolveFaceID*`) as follow:

    <pre>resolve: {0, 1, 2, ...} × {{f #_} | } × {{f #_} | } → {0, 1, 2, ...}
                         (invID, FS, FSM)               ↦ k such that:
        el(⌊invID / 4⌋, FSM) = el(k, FS)</pre>
    
    
    
    
    where `FSM` is subset of `FS`. It is obvious that k always exists and is unique (so long as `el(⌊invID / 4⌋, FSM)` is defined of course).

Let us now study these pruning and resolving phases in more details for **plain models** and **custom models**.

#### Plain models

 For any **plain model**, `FS := {f #0, f #1, f #2, f #3, f #4, f #5}` and `N := {f #0}`, `S := {f #1}`, `W := {f #2}`, `E := {f #3}`, `U := {f #4}`, `D := {f #5}`, `Γ := ∅`. Indeed, it  doesn't make sense for the face of a **plain model** to be ineligible for culling since it is literally stuck to the neighbor voxel. Therefore, if a voxel is capable of culling its neighbor's face and that neighbor happens to be a **plain model**, then that neighbor needs not to render that face at all.

- Knowing the definition of `FSM`, that `|N| = |S| = |W| = |E| = |U| = |D| = 1` and that `|Γ| = 0`, it is obvious that <code>|FSM| = FM<sub>1</sub> + FM<sub>2</sub> + FM<sub>3</sub> + FM<sub>4</sub> + FM<sub>5</sub> + FM<sub>6</sub></code>. Thus, `ΛcomputeUnculledFaceCount(uint modelId, uint faceMask)` can be implemented for **single** and **plain models** with the single instruction `bitCount(faceMask)`.

- As for the resolving phase, let us describe it in three different ways for clarity sake:

    ##### 1st version - Algorithm
    
    *Let f #faceID ∈ `FSM` be the face we are trying to resolve.*
    
    *Let dir #0 = `FSM ∩ N`, dir #1 = `FSM ∩ S`, dir #2 = `FSM ∩ W`, dir #3 = `FSM ∩ E`, dir #4 = `FSM ∩ U`, dir #5 = `FSM ∩ D`.*
    
    *Ensures that the new faceID is resolved and points to the correct face in the whole model.*
    
    **Step 0**. [Initialization] I <- 0, faceID_resolved <- faceID and go to **Step 1**.
    
    **Step 1**. [Check termination condition] If f #faceID ∈ dir #I then terminate and yield faceID_resolved. Otherwise, go to **Step 2**.
    
    **Step 2**. [Shift face ID if necessary]If dir #I = ∅ (implying <code>FM<sub>I + 1</sub></code> = 0) then faceID_resolved <- faceID_resolved + 1. Finally, I <- I + 1 and go to **Step 1**.
    
    It is interesting to note that the given algorithm requires f #faceID to actually exists (meaning `FSM` isn't empty, i.e. there is something to render), otherwise it would be stuck in an infinite loop.
    
    ##### 2nd version - Unrolled style
    
    Alternatively to the algorithm provided, we can write a loop-unrolled version of the resolving phase, like so:
    
    <pre>ΛresolveFaceIDPlainModel(faceIDAmongWorkgroups, faceMask) =
      faceIDAmongWorkgroups +
      | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub>
      | otherwise (1 if faceMask<sub>1</sub> = 0 otherwise 0) +
          | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> + faceMask<sub>2</sub>
          | otherwise (1 if faceMask<sub>2</sub> = 0 otherwise 0) +
              | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> + faceMask<sub>2</sub> + faceMask<sub>3</sub>
              | otherwise (1 if faceMask<sub>3</sub> = 0 otherwise 0) +
                  | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> + faceMask<sub>2</sub> + faceMask<sub>3</sub> + faceMask<sub>4</sub>
                  | otherwise (1 if faceMask<sub>4</sub> = 0 otherwise 0) +
                      | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> + faceMask<sub>2</sub> + faceMask<sub>3</sub> + faceMask<sub>4</sub> + faceMask<sub>5</sub>
                      | otherwise (1 if faceMask<sub>5</sub> = 0 otherwise 0) +
                          | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> + faceMask<sub>2</sub> + faceMask<sub>3</sub> + faceMask<sub>4</sub> + faceMask<sub>5</sub> + faceMask<sub>6</sub>
                          | otherwise (1 if faceMask<sub>6</sub> = 0 otherwise 0)</pre>
    
    ##### 3rd version - Actual GLSL code
    
    Finally, let us implement the both the algorithm and the unrolled variant in GLSL (as well as two other, micro-optimized, vectorized variants; Necessary in case you dare venture through the obfuscated shader source code).
    
    ```glsl
    uint ΛresolveFaceIDPlainModel(uint faceIDAmongWorkgroups, uint faceMask)
    {
    #if ALGORITHM
    
        uint faceID_resolved = faceIDAmongWorkgroups;
        // current progress through the six possible faces of a plain/single model
        uint faceCount = 0;
        for (uint i = 0; i < 6; i++)
        {
            uint fm_i = bitfieldExtract(faceMask, i, 1);
            faceCount += fm_i;
            if (faceIDAmongWorkgroups < faceCount)
                break;
    
            if (fm_i == 0)
                faceID_resolved++;
        }
        return faceID_resolved;
    
    #elif UNROLLED_FLATTENED
    
        uint fm_0 = bitfieldExtract(faceMask, 0, 1);
        uint fm_1 = bitfieldExtract(faceMask, 1, 1);
        uint fm_2 = bitfieldExtract(faceMask, 2, 1);
        uint fm_3 = bitfieldExtract(faceMask, 3, 1);
        uint fm_4 = bitfieldExtract(faceMask, 4, 1);
        uint fm_5 = bitfieldExtract(faceMask, 5, 1);
        return faceIDAmongWorkgroups +
            (faceIDAmongWorkgroups >= fm_0 && !bool(fm_0)) +
            (faceIDAmongWorkgroups >= fm_0 + fm_1 && !bool(fm_1)) +
            (faceIDAmongWorkgroups >= fm_0 + fm_1 + fm_2 && !bool(fm_2)) +
            (faceIDAmongWorkgroups >= fm_0 + fm_1 + fm_2 + fm_3 && !bool(fm_3)) +
            (faceIDAmongWorkgroups >= fm_0 + fm_1 + fm_2 + fm_3 + fm_4 && !bool(fm_4)) +
            (faceIDAmongWorkgroups >= fm_0 + fm_1 + fm_2 + fm_3 + fm_4 + fm_5 && !bool(fm_5));
    
    #elif MATRIX
        // note that the code in this section isn't valid GLSL:
        // vec6, mat6, integer matrices, vectorial AND as well as
        // the displayed constructors do not exist.
        bvec6 fm = (
        	bitfieldExtract(faceMask, 0, 1),
            bitfieldExtract(faceMask, 1, 1),
            bitfieldExtract(faceMask, 2, 1),
            bitfieldExtract(faceMask, 3, 1),
            bitfieldExtract(faceMask, 4, 1),
            bitfieldExtract(faceMask, 5, 1)
        );
    
        umat6 A = (
        	1, 0, 0, 0, 0, 0,
            1, 1, 0, 0, 0, 0,
            1, 1, 1, 0, 0, 0,
            1, 1, 1, 1, 0, 0,
            1, 1, 1, 1, 1, 0
            1, 1, 1, 1, 1, 1
        );
    
        uvec6 comparisonLeftHandSide = (faceIDAmongWorkgroups);
        uvec6 comparisonRightHandSide = A * uvec6(fm);
    
        uvec6 res = uvec6(greaterThanEqual(comparisonLeftHandSide, comparisonRightHandSide) && not(fm));
        // faceID_resolved is simply the sum of faceIDAmongWorkgroups and all res components
        return faceIDAmongWorkgroups + uint(dot(res, uvec6(1)));
    
    #else
        // this section is valid GLSL.
        // Since there is no vec6 in GLSL, we need to split it
        // into 1 vec4 and 1 vec2.
        // Thus, throughout this section, fm_0_3 joined with fm_4_5
        // will be equivalent to fm in the MATRIX section.
        uvec4 fm_0_3 = uvec4(
        	bitfieldExtract(faceMask, 0, 1),
            bitfieldExtract(faceMask, 1, 1),
            bitfieldExtract(faceMask, 2, 1),
            bitfieldExtract(faceMask, 3, 1)
        );
        uvec2 fm_4_5 = uvec2(
            bitfieldExtract(faceMask, 4, 1),
            bitfieldExtract(faceMask, 5, 1)
        );
        // only used for swizzling
        uvec4 s;
    
        // Computing the sum of face mask, i.e.
        // sfm_0_3 joined with sfm_4_5 is the equivalent of
        // comparisonRightHandSide in the MATRIX section.
        uvec4 sfm_0_3;
        uvec2 sfm_4_5;
        {
            sfm_0_3.x = fm_0_3.x;
    
            s = fm_0_3.xzxx + fm_0_3.ywyy;
            sfm_0_3.y = s.x;
    
            s = uvec4(s.xx, fm_4_5.xx) + uvec4(s.y, fm_0_3.z, fm_4_5.y, 0);
            sfm_0_3.z = s.y;
            sfm_0_3.w = s.x;
    
            s = s.xxxx + s.zwww;
            sfm_4_5.x = s.y;
            sfm_4_5.y = s.x;
        }
    
        // Again, res_0_3 joined with res_4_5 is the equivalent of
        // res in the MATRIX section.
        // There is no vectorial '&&' so we need to use '&' between 2 uvecs.
        uvec4 res_0_3 = uvec4(greaterThanEqual(uvec4(faceIDAmongWorkgroups), sfm_0_3)) & uvec4(not(bvec4(fm_0_3)));
        uvec2 res_4_5 = uvec2(greaterThanEqual(uvec2(faceIDAmongWorkgroups), sfm_4_5)) & uvec2(not(bvec2(fm_4_5)));
    
        // Finally, summing faceIDAmongWorkgroups and all res_0_3 and res_4_5 components.
        s = uvec4(res_0_3.xy, res_4_5.xy) + uvec4(res_0_3.zw, faceIDAmongWorkgroups, 0);
        s += s.yxwz;
        return s.x + s.z;
    
    #endif
    }
    ```
    
    <div align="center"><b>Snippet 5.</b> Proposed code for <b>plain models</b> face Id resolving</div>
    
    The attentive reader may have observed inconsistencies in the two translations of the algorithm (unrolled style and GLSL code). As a matter of fact, that blunder is still in the mesh shader source code. While it does not impede the functionality of the code, it is important to understand it and correct it.
    
    In the algorithm, **Step 1** is executed 6 times at most while **Step 2** is executed 5 times at most. In the translation process that yielded the unrolled style and the GLSL code, what we did wrong was make **Step 2** be executed at most 6 times instead of 5. Indeed, the two last guards in the unrolled style version can be replaced with a 0, since <code>faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> + faceMask<sub>2</sub> + faceMask<sub>3</sub> + faceMask<sub>4</sub> + faceMask<sub>5</sub> + faceMask<sub>6</sub></code> is always true (because <code>|FSM| = FM<sub>1</sub> + FM<sub>2</sub> + FM<sub>3</sub> + FM<sub>4</sub> + FM<sub>5</sub> + FM<sub>6</sub></code>). The same correction can be applied to yield the following corrected GLSL code:
    
    ```glsl
    uint ΛresolveFaceIDPlainModel(uint faceIDAmongWorkgroups, uint faceMask)
    {
    #if ALGORITHM
    
        uint faceID_resolved = faceIDAmongWorkgroups;
        // current progress through the six possible faces of a plain/single model
        uint faceCount = 0;
        for (uint i = 0; i < 5; i++)
        {
            uint fm_i = bitfieldExtract(faceMask, i, 1);
            faceCount += fm_i;
            if (faceIDAmongWorkgroups < faceCount)
                break;
    
            if (fm_i == 0)
                faceID_resolved++;
        }
        return faceID_resolved;
    
    #elif UNROLLED_FLATTENED
    
        uint fm_0 = bitfieldExtract(faceMask, 0, 1);
        uint fm_1 = bitfieldExtract(faceMask, 1, 1);
        uint fm_2 = bitfieldExtract(faceMask, 2, 1);
        uint fm_3 = bitfieldExtract(faceMask, 3, 1);
        uint fm_4 = bitfieldExtract(faceMask, 4, 1);
        return faceIDAmongWorkgroups +
            (faceIDAmongWorkgroups >= fm_0 && !bool(fm_0)) +
            (faceIDAmongWorkgroups >= fm_0 + fm_1 && !bool(fm_1)) +
            (faceIDAmongWorkgroups >= fm_0 + fm_1 + fm_2 && !bool(fm_2)) +
            (faceIDAmongWorkgroups >= fm_0 + fm_1 + fm_2 + fm_3 && !bool(fm_3)) +
            (faceIDAmongWorkgroups >= fm_0 + fm_1 + fm_2 + fm_3 + fm_4 && !bool(fm_4));
    
    #elif MATRIX
    
        bvec5 fm = (
        	bitfieldExtract(faceMask, 0, 1),
            bitfieldExtract(faceMask, 1, 1),
            bitfieldExtract(faceMask, 2, 1),
            bitfieldExtract(faceMask, 3, 1),
            bitfieldExtract(faceMask, 4, 1)
        );
    
        umat5x6 A = (
        	1, 0, 0, 0, 0, 0,
            1, 1, 0, 0, 0, 0,
            1, 1, 1, 0, 0, 0,
            1, 1, 1, 1, 0, 0,
            1, 1, 1, 1, 1, 0
        );
    
        uvec5 comparisonLeftHandSide = (faceIDAmongWorkgroups);
        uvec5 comparisonRightHandSide = A * uvec5(fm);
    
        uvec5 res = uvec5(greaterThanEqual(comparisonLeftHandSide, comparisonRightHandSide) && not(fm));
    
        return faceIDAmongWorkgroups + uint(dot(res, uvec5(1)));
    
    #else
    
        uvec4 fm_0_3 = uvec4(
        	bitfieldExtract(faceMask, 0, 1),
            bitfieldExtract(faceMask, 1, 1),
            bitfieldExtract(faceMask, 2, 1),
            bitfieldExtract(faceMask, 3, 1)
        );
        uint fm_4 = bitfieldExtract(faceMask, 4, 1);
    
        uvec4 s;
    
        uvec4 sfm_0_3;
        uint sfm_4;
        {
            sfm_0_3.x = fm_0_3.x;
    
            s = fm_0_3.xzxx + fm_0_3.ywyy;
            sfm_0_3.y = s.x;
    
            s = uvec4(s.xx, 0, 0) + uvec4(s.y, fm_0_3.z, 0, 0);
            sfm_0_3.z = s.y;
            sfm_0_3.w = s.x;
    
            sfm_4 = s.x + fm_4;
        }
    
        uvec4 res_0_3 = uvec4(greaterThanEqual(uvec4(faceIDAmongWorkgroups), sfm_0_3)) & uvec4(not(bvec4(fm_0_3)));
        uint res_4 = uint(faceIDAmongWorkgroups >= sfm_4 && !bool(fm_4));
    
        s = uvec4(res_0_3.xy, res_4, 0) + uvec4(res_0_3.zw, faceIDAmongWorkgroups, 0);
        s += s.yxwz;
        return s.x + s.z;
    
    #endif
    }
    ```
    
    <div align="center"><b>Snippet 6.</b> Proposed code for <b>plain models</b> face Id resolving [Corrected]</div>
    


#### Custom models

For custom models, there is no additional constraint on `FS`, `N`, `S`, `W`, `E`, `U`, `D` or `Γ` aside that `|FS| ≤ 255`.

- Knowing the definition of `FSM`, implementing `ΛcomputeUnculledFaceCountCCModel/2` is straight-forward, and translates into the following GLSL code:

    ```glsl
    uint ΛcomputeUnculledFaceCountCCModel(uint modelId, uint faceMask)
    {
        // true if the model is custom complete.
        // false if it is custom final. (remember, custom abstract cannot be rendered directly)
        bool isComplete = modelId < SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT + CUSTOM_COMPLETE_MODEL_COUNT;
        uint offset = isComplete ? CUSTOM_COMPLETE_MODEL_HEADER_SIZE : CUSTOM_FINAL_MODEL_HEADER_SIZE;
        uint stride = isComplete ? CUSTOM_COMPLETE_MODEL_FACE_STRIDE : CUSTOM_FINAL_MODEL_FACE_STRIDE;
    
        // models.face_counts and models.ptr only concern custom models
        // so we need to translate modelId.
        uint modelIdAmongCustomModels = modelId - (SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT);
        // |FS|
        uint modelFaceCount = models.face_counts[modelIdAmongCustomModels];
        // actual pointer to start retrieving faces from models.data
        // we don't care about the model's header
        uint modelPtr = offset + models.ptr[modelIdAmongCustomModels];
    
        uint faceToRenderCount = 0;
    
        for (uint i = 0; i < modelFaceCount; i++)
        {
            // pointer to the 1st uint describing face #i of the model
            uint facePtr = modelPtr + stride * i;
    
            uint faceTexture_dir_and_cullInfo = models.data[facePtr];
            bool cullEnabled = bool(bitfieldExtract(faceTexture_dir_and_cullInfo, 30, 1));
            // 0 for north, 1 south, 2 west, 3 east, 4 upward, 5 downward
            uint faceDir = bitfieldExtract(faceTexture_dir_and_cullInfo, 27, 3);
    
            // check if face should survive culling
            if (!cullEnabled || bool( faceMask & (1 << faceDir) ))
                faceToRenderCount++;
        }
    
        return faceToRenderCount;
    }
    ```
    
    <div align="center"><b>Snippet 7.</b> Proposed code for computing <b>custom models</b> unculled face count</div>

- For the resolving phase, we will describe it in two different ways:

    ##### 1st version - Algorithm
    
    *Let f #faceID ∈ `FSM` be the face we are trying to resolve.*
    
    *Let dir #0 = `FSM ∩ N`, dir #1 = `FSM ∩ S`, dir #2 = `FSM ∩ W`, dir #3 = `FSM ∩ E`, dir #4 = `FSM ∩ U`, dir #5 = `FSM ∩ D`, dir #6 = `FSM ∩ Γ`.*
    
    *Let Fdir #0 = `N`, Fdir #1 = `S`, Fdir #2 = `W`, Fdir #3 = `E`, Fdir #4 = `U`,  Fdir #5 = `D`, Fdir #6 = `Γ`.*
    
    *Ensures that the new faceID is resolved and points to the correct face in the whole model.*
    
    **Step 0**. [Initialization] I <- 0, faceID_resolved <- faceID and go to **Step 1**.
    
    **Step 1**. [Check termination condition] If f #faceID ∈ dir #I then terminate and yield faceID_resolved. Otherwise, go to **Step 2**.
    
    **Step 2**. [Shift face ID if necessary]If dir #I = ∅ (implying <code>FM<sub>I + 1</sub></code> = 0) then faceID_resolved <- faceID_resolved + |Fdir #I|. Finally, I <- I + 1 and go to **Step 1**.
    
    This algorithm is a generalization of the one for **single** and **plain models**, where `|N|`, `|S|`, `|W|`, `|E|`, `|U|` and `|D|` aren't necessarily all equal nor equal to 1, and where `Γ` can be non-empty.
    
    ##### 2nd version - Actual GLSL code
    
    Let us now implement this algorithm in GLSL.
    
    ```glsl
    uint ΛresolveFaceIDCustomModel(
        bool isComplete,
        uint modelIdAmongCustomModels,
        uint faceIDAmongWorkgroups,
        uint faceMask)
    {
        uint offset = isComplete ? CUSTOM_COMPLETE_MODEL_HEADER_SIZE : CUSTOM_FINAL_MODEL_HEADER_SIZE;
        uint stride = isComplete ? CUSTOM_COMPLETE_MODEL_FACE_STRIDE : CUSTOM_FINAL_MODEL_FACE_STRIDE;
    
        // |FS|
        uint modelFaceCount = models.face_counts[modelIdAmongCustomModels];
        // actual pointer to start retrieving faces from models.data
        // we don't care about the model's header
        uint modelPtr = offset + models.ptr[modelIdAmongCustomModels];
    
        // keeps track of how many faces that survived culling we encountered.
        // used to early-break the loop when it equals faceIDAmongWorkgroups.
        uint encounteredUnculledFaceCount = 0;
    
        uint faceID_resolved = faceIDAmongWorkgroups;
    
        for (uint i = 0;
             encounteredUnculledFaceCount < faceIDAmongWorkgroups && i < modelFaceCount;
             i++)
        {
            // pointer to the 1st uint describing face #i of the model
            uint facePtr = modelPtr + stride * i;
    
            uint faceTexture_dir_and_cullInfo = models.data[facePtr];
            bool cullEnabled = bool(bitfieldExtract(faceTexture_dir_and_cullInfo, 30, 1));
            // 0 for north, 1 south, 2 west, 3 east, 4 upward, 5 downward
            uint faceDir = bitfieldExtract(faceTexture_dir_and_cullInfo, 27, 3);
    
            // face #i was culled in task shader
            if ( cullEnabled && !bool(faceMask & (1 << faceDir)) )
                faceID_resolved++;
            else
                encounteredUnculledFaceCount++;
        }
    
        return faceID_resolved;
    }
    ```
    
    <div align="center"><b>Snippet 8.</b> Proposed code for <b>custom models</b> face Id resolving</div>
    

Contrary to their **plain model** counterparts, the two algorithms for computing `|FSM|` and resolving the face Id for **custom models** are the antithesis of parallelization, which is a shame since we are writing GPU code. Nevertheless, we already know the answer for turning these into "good" GLSL code thanks to the definition of `FSM` and to the algorithm version of the face Id resolving phase for **custom models**. It was so trivial for **plain models** that we ended up writing perfectly parallelizable code for both problems (unculled face count and face Id resolving) right off the bat. However, to completely grasp the difficulties we are facing with **custom models**, we should state the obvious about culling to see the bigger picture:

*Culling can occur as a combination of the six axis-aligned directions. Therefore, we must iterate as many times as there are different directions (i.e. we cannot guess what directions are involved, and we need to check them all). In other words, culling occurs direction-wide, there is no need to check each face individually.*

Now, it is clear that the problem with our current setup is that **custom models** don't store any per-direction information, forcing us to iterate over all the model faces (which can come in any number from the range ⟦0; 255⟧), when we ineluctably avoided that expensive operation for **plain models** (because, since **plain models** always feature 1 face per direction, iterating over their faces is equivalent to iterating over the six directions).

Also, it is clear that keeping the current solutions for **custom models** and unrolling them – which would entails unrolling 256 times with extra conditions to make sure we don't go past the actual model's number of faces – is unreasonable.

On the other hand, simply storing per-direction information, that is `|N|`, `|S|`, `|W|`, `|E|`, `|U|`, `|D|` and `|Γ|`, for each **custom model** would allow us to write code as clean and parallelizable as for **plain models** earlier. To do that, we will slightly change the properties of `models.face_counts` so that it no longer stores `|FS|` but rather `|N|`, `|S|`, `|W|`, `|E|`, `|U|`, `|D|` and `|Γ|` for each **custom model**. As to not lower the hard-coded upper bound on the number of faces in a **custom model**, we will have to allocate 7 bytes instead of only 1 previously. The first byte would be for `|N|`, the second for `|S|`, etc. Thus, from now on, the length of `models.face_counts` will be `7 × (CUSTOM_COMPLETE_MODEL_COUNT + CUSTOM_FINAL_MODEL_COUNT)`.

With this overhaul in mind, let us once again define the unculled face count computation and the face Id resolving phase for **custom models**.

#### Custom models Pt. 2

- We implement `ΛcomputeUnculledFaceCountCCModel/2` in GLSL as follow:

    ```glsl
    uint ΛcomputeUnculledFaceCountCCModel(uint modelId, uint faceMask)
    {
    #if INTELLIGIBLE
    
        uint modelIdAmongCustomModels = modelId - (SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT);
        // used to index models.face_counts
        uint directionCountPtr = 7 * modelIdAmongCustomModels;
    
        uint unculledFaceCount = 0;
    
        for (uint i = 0; i < 6; i++)
            if (bool( bitfieldExtract(faceMask, i, 1) ))
                unculledFaceCount += models.face_counts[directionCountPtr + i];
    
        // always take faces in Γ into account
        unculledFaceCount += models.face_counts[directionCountPtr + 6];
        return unculledFaceCount;
    
    #elif UNROLLED_FLATTENED
    
        // used to index models.face_counts
        // MAD-friendly version of directionCountPtr in the INTELLIGIBLE version
        uint directionCountPtr = 7 * modelId - (SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT) * 7;
    
        return
            bitfieldExtract(faceMask, 0, 1) * models.face_counts[directionCountPtr] +
            bitfieldExtract(faceMask, 1, 1) * models.face_counts[directionCountPtr + 1] +
            bitfieldExtract(faceMask, 2, 1) * models.face_counts[directionCountPtr + 2] +
            bitfieldExtract(faceMask, 3, 1) * models.face_counts[directionCountPtr + 3] +
            bitfieldExtract(faceMask, 4, 1) * models.face_counts[directionCountPtr + 4] +
            bitfieldExtract(faceMask, 5, 1) * models.face_counts[directionCountPtr + 5] +
                                              models.face_counts[directionCountPtr + 6];
    
    #else
    
        // used to index models.face_counts
        // MAD-friendly version of directionCountPtr in the INTELLIGIBLE version
        uint directionCountPtr = 7 * modelId - (SINGLE_MODEL_COUNT + PLAIN_MODEL_COUNT) * 7;
    
        // direction counts for each 6 directions
        uvec4 dc_0_3 = uvec4(
        	models.face_counts[directionCountPtr],
            models.face_counts[directionCountPtr + 1],
            models.face_counts[directionCountPtr + 2],
            models.face_counts[directionCountPtr + 3]
        );
        uvec2 dc_4_5 = uvec3(
        	models.face_counts[directionCountPtr + 4],
            models.face_counts[directionCountPtr + 5]
        );
    
        // contrary to the samples for plain models,
        // here agressive variable reusing is possible
        // do dc_0_3 will be used for almost everything
    
        dc_0_3 *= uvec4(
            bitfieldExtract(faceMask, 0, 1),
            bitfieldExtract(faceMask, 1, 1),
            bitfieldExtract(faceMask, 2, 1),
            bitfieldExtract(faceMask, 3, 1)
        );
        dc_4_5 *= uvec2(
            bitfieldExtract(faceMask, 4, 1),
            bitfieldExtract(faceMask, 5, 1)
        );
    
        // Finally, summing all components of dc and models.data[directionCountPtr + 6]
        dc_0_3 = uvec4(dc_0_3.xy, dc_4_5.xy) + uvec4(dc_0_3.zw, models.face_counts[directionCountPtr + 6], 0);
        dc_0_3 += dc_0_3.yxwz;
        return dc_0_3.x + dc_0_3.z;
    
    #endif
    }
    ```
    
    <div align="center"><b>Snippet 9.</b> Proposed code for computing <b>custom models</b> unculled face count [Pt. 2]</div>
    
- As for the resolving phase, the algorithm doesn't change. However, now we can properly define an unrolled-style version just like for **plain models** in addition to the actual GLSL code.

    ##### 1st version - Algorithm
    
    [...]
    
    ##### 2nd version - Unrolled style
    
    <pre>ΛresolveFaceIDCustomModel(faceIDAmongWorkgroups, directionCountPtr, faceMask) =
      faceIDAmongWorkgroups +
      | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> × models.face_counts[directionCountPtr]
      | otherwise (models.face_counts[directionCountPtr] if faceMask<sub>1</sub> = 0 otherwise 0) +
          | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> × models.face_counts[directionCountPtr] +
                                         faceMask<sub>2</sub> × models.face_counts[directionCountPtr + 1]
          | otherwise (models.face_counts[directionCountPtr + 1] if faceMask<sub>2</sub> = 0 otherwise 0) +
              | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> × models.face_counts[directionCountPtr] +
                                             faceMask<sub>2</sub> × models.face_counts[directionCountPtr + 1] +
                                             faceMask<sub>3</sub> × models.face_counts[directionCountPtr + 2]
              | otherwise (models.face_counts[directionCountPtr + 2] if faceMask<sub>3</sub> = 0 otherwise 0) +
                  | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> × models.face_counts[directionCountPtr] +
                                                 faceMask<sub>2</sub> × models.face_counts[directionCountPtr + 1] +
                                                 faceMask<sub>3</sub> × models.face_counts[directionCountPtr + 2] +
                                                 faceMask<sub>4</sub> × models.face_counts[directionCountPtr + 3]
                  | otherwise (models.face_counts[directionCountPtr + 3] if faceMask<sub>4</sub> = 0 otherwise 0) +
                      | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> × models.face_counts[directionCountPtr] +
                                                     faceMask<sub>2</sub> × models.face_counts[directionCountPtr + 1] +
                                                     faceMask<sub>3</sub> × models.face_counts[directionCountPtr + 2] +
                                                     faceMask<sub>4</sub> × models.face_counts[directionCountPtr + 3] +
                                                     faceMask<sub>5</sub> × models.face_counts[directionCountPtr + 4]
                      | otherwise (models.face_counts[directionCountPtr + 4] if faceMask<sub>5</sub> = 0 otherwise 0) +
                          | 0 if faceIDAmongWorkgroups &lt; faceMask<sub>1</sub> × models.face_counts[directionCountPtr] +
                                                         faceMask<sub>2</sub> × models.face_counts[directionCountPtr + 1] +
                                                         faceMask<sub>3</sub> × models.face_counts[directionCountPtr + 2] +
                                                         faceMask<sub>4</sub> × models.face_counts[directionCountPtr + 3] +
                                                         faceMask<sub>5</sub> × models.face_counts[directionCountPtr + 4] +
                                                         faceMask<sub>6</sub> × models.face_counts[directionCountPtr + 5]
                          | otherwise (models.face_counts[directionCountPtr + 5] if faceMask<sub>6</sub> = 0 otherwise 0)</pre>
    
    
    ##### 3rd version - Actual GLSL code
    
    ```glsl
    uint ΛresolveFaceIDCustomModel(
        bool isComplete /*unused*/,
        uint modelIdAmongCustomModels,
        uint faceIDAmongWorkgroups,
        uint faceMask)
    {
        // used to index models.face_counts
        uint directionCountPtr = 7 * modelIdAmongCustomModels;
    
    #if ALGORITHM
    
        uint faceID_resolved = faceIDAmongWorkgroups;
        // current progress through the six possible faces of a plain/single model
        uint faceCount = 0;
        for (uint i = 0; i < 6; i++)
        {
            uint fm_i = bitfieldExtract(faceMask, i, 1);
            faceCount += fm_i * models.face_counts[directionCountPtr + i];
            if (faceIDAmongWorkgroups < faceCount)
                break;
    
            if (fm_i == 0)
                faceID_resolved += models.face_counts[directionCountPtr + i];
        }
        return faceID_resolved;
    
    #elif UNROLLED_FLATTENED
    
        uint fm_0 = bitfieldExtract(faceMask, 0, 1);
        uint fm_1 = bitfieldExtract(faceMask, 1, 1);
        uint fm_2 = bitfieldExtract(faceMask, 2, 1);
        uint fm_3 = bitfieldExtract(faceMask, 3, 1);
        uint fm_4 = bitfieldExtract(faceMask, 4, 1);
        uint fm_5 = bitfieldExtract(faceMask, 5, 1);
    
        uint dc_0 = models.face_counts[directionCountPtr];
        uint dc_1 = models.face_counts[directionCountPtr + 1];
        uint dc_2 = models.face_counts[directionCountPtr + 2];
        uint dc_3 = models.face_counts[directionCountPtr + 3];
        uint dc_4 = models.face_counts[directionCountPtr + 4];
        uint dc_5 = models.face_counts[directionCountPtr + 5];
        return faceIDAmongWorkgroups +
            (faceIDAmongWorkgroups >= fm_0 * dc_0 && !bool(fm_0)) * dc_0 +
            (faceIDAmongWorkgroups >= fm_0 * dc_0 + fm_1 * dc_1 && !bool(fm_1)) * dc_1 +
            (faceIDAmongWorkgroups >= fm_0 * dc_0 + fm_1 * dc_1 + fm_2 * dc_2 && !bool(fm_2)) * dc_2 +
            (faceIDAmongWorkgroups >= fm_0 * dc_0 + fm_1 * dc_1 + fm_2 * dc_2 + fm_3 * dc_3 && !bool(fm_3)) * dc_3 +
            (faceIDAmongWorkgroups >= fm_0 * dc_0 + fm_1 * dc_1 + fm_2 * dc_2 + fm_3 * dc_3 + fm_4 * dc_4 && !bool(fm_4)) * dc_4 +
            (faceIDAmongWorkgroups >= fm_0 * dc_0 + fm_1 * dc_1 + fm_2 * dc_2 + fm_3 * dc_3 + fm_4 * dc_4 + fm_5 * dc_5 && !bool(fm_5)) * dc_5;
    
    #elif MATRIX
        // note that the code in this section isn't valid GLSL:
        // vec6, mat6, integer matrices, vectorial AND as well as
        // the displayed constructors do not exist.
        bvec6 fm = (
        	bitfieldExtract(faceMask, 0, 1),
            bitfieldExtract(faceMask, 1, 1),
            bitfieldExtract(faceMask, 2, 1),
            bitfieldExtract(faceMask, 3, 1),
            bitfieldExtract(faceMask, 4, 1),
            bitfieldExtract(faceMask, 5, 1)
        );
    
        uvec6 dc = (
        	models.face_counts[directionCountPtr],
            models.face_counts[directionCountPtr + 1],
            models.face_counts[directionCountPtr + 2],
            models.face_counts[directionCountPtr + 3],
            models.face_counts[directionCountPtr + 4],
            models.face_counts[directionCountPtr + 5]
        );
    
        umat6 A = (
        	1, 0, 0, 0, 0, 0,
            1, 1, 0, 0, 0, 0,
            1, 1, 1, 0, 0, 0,
            1, 1, 1, 1, 0, 0,
            1, 1, 1, 1, 1, 0
            1, 1, 1, 1, 1, 1
        );
    
        uvec6 comparisonLeftHandSide = (faceIDAmongWorkgroups);
        uvec6 comparisonRightHandSide = A * uvec6(fm) * dc;
    
        uvec6 res = uvec6(greaterThanEqual(comparisonLeftHandSide, comparisonRightHandSide) && not(fm)) * dc;
        // faceID_resolved is simply the sum of faceIDAmongWorkgroups and all res components
        return faceIDAmongWorkgroups + uint(dot(res, uvec6(1)));
    
    #else
        // this section is valid GLSL.
        // Since there is no vec6 in GLSL, we need to split it
        // into 1 vec4 and 1 vec2.
        // Thus, throughout this section, fm_0_3 joined with fm_4_5
        // will be equivalent to fm in the MATRIX section.
        uvec4 fm_0_3 = uvec4(
        	bitfieldExtract(faceMask, 0, 1),
            bitfieldExtract(faceMask, 1, 1),
            bitfieldExtract(faceMask, 2, 1),
            bitfieldExtract(faceMask, 3, 1)
        );
        uvec2 fm_4_5 = uvec2(
            bitfieldExtract(faceMask, 4, 1),
            bitfieldExtract(faceMask, 5, 1)
        );
    
        // dc_0_3 joined with dc_4_5 is the equivalent
        // of dc in the MATRIX section.
        uvec4 dc_0_3 = uvec4(
        	models.face_counts[directionCountPtr],
            models.face_counts[directionCountPtr + 1],
            models.face_counts[directionCountPtr + 2],
            models.face_counts[directionCountPtr + 3]
        );
        uvec2 dc_4_5 = uvec4(
        	models.face_counts[directionCountPtr + 4],
            models.face_counts[directionCountPtr + 5]
        );
    
        uvec4 dcm_0_3 = dc_0_3 * fm_0_3;
        uvec2 dcm_4_5 = dc_4_5 * fm_4_5;
    
        // only used for swizzling
        uvec4 s;
    
        // Computing the sum of face mask * direction count, i.e.
        // sdcm_0_3 joined with sdcm_4_5 is the equivalent of
        // comparisonRightHandSide in the MATRIX section.
        uvec4 sdcm_0_3;
        uvec2 sdcm_4_5;
        {
            sdcm_0_3.x = dcm_0_3.x;
    
            s = dcm_0_3.xzxx + dcm_0_3.ywyy;
            sdcm_0_3.y = s.x;
    
            s = uvec4(s.xx, dcm_4_5.xx) + uvec4(s.y, dcm_0_3.z, dcm_4_5.y, 0);
            sdcm_0_3.z = s.y;
            sdcm_0_3.w = s.x;
    
            s = s.xxxx + s.zwww;
            sdcm_4_5.x = s.y;
            sdcm_4_5.y = s.x;
        }
    
        // Again, res_0_3 joined with res_4_5 is the equivalent of
        // res in the MATRIX section.
        // There is no vectorial '&&' so we need to use '&' between 2 uvecs.
        uvec4 res_0_3 = uvec4(greaterThanEqual(uvec4(faceIDAmongWorkgroups), sdcm_0_3)) & uvec4(not(bvec4(fm_0_3)));
        uvec2 res_4_5 = uvec2(greaterThanEqual(uvec2(faceIDAmongWorkgroups), sdcm_4_5)) & uvec2(not(bvec2(fm_4_5)));
        res_0_3 *= dc_0_3;
        res_4_5 *= dc_4_5;
    
        // Finally, summing faceIDAmongWorkgroups, models.face_counts[directionCountPtr + 6]
        // and all res_0_3 and res_4_5 components.
        s = uvec4(res_0_3.xy, res_4_5.xy) + uvec4(res_0_3.zw, faceIDAmongWorkgroups, models.face_counts[directionCountPtr + 6]);
        s += s.yxwz;
        return s.x + s.z;
    
    #endif
    }
    ```
    
    <div align="center"><b>Snippet 10.</b> Proposed code for <b>custom models</b> face Id resolving [Pt. 2]</div>



## About the render passes

After the shader code, we will now discuss actual interactive computer graphic techniques that might just help us out.

By nature, the GPU-driven design is about making the GPU take the rendering decisions instead of the CPU. This design generally comes with spectacular performance benefits because having the CPU take decisions for the GPU implies using pipeline barriers and very heavy memory transactions (take for example doing manually the work a renderpass can do: See [MSAA best practices](https://github.com/KhronosGroup/Vulkan-Samples/blob/main/samples/performance/msaa/README.adoc)). By making use of a built-in pipeline perfectly fit for us, we completely avoid pipeline barriers and let the hard-coded policies handle the dependencies.

However, GPU-driven or not, our engine is still missing an important feature, that is translucency. The traditional way to handle translucency in a generic render engine is as follow:

```
1. Start render pass
2. Start opaque subpass
3. Draw opaque objects (order doesn't matter)
---Optional--- (also, 1. & 2. can be merged with 4. & 5.)
4. Start alplha-test subpass (allows to render transparent objects without the expensive blending operation)
5. Draw "cut-out" objects (order doesn't matter)
--------------
6. Start alpha-blending subpass
7. Sort translucent triangles from translucent objects by depth
8. Draw translucent triangles in that order
9. End render pass
```

The only way to achieve this rendering flow is to have the opaque, "cut-out" (i.e. which contains transparent parts) and translucent objects separated from each other before starting rendering. With the traditional graphic pipeline and a computer shader, ignoring the "cut-out" object pass which can be merged with the opaque one, it would result in the following compute shader pseudo-code

```glsl
layout(local_size_x = CHUNK_SIZE) in;

readonly buffer allObjects {...} allObjects[];
writeonly buffer opaqueObjects {...} opaqueObjects[];
writeonly buffer translucentTriangles {...} translucentTriangles[];

uniform uint opaqueCounter;
uniform uint translucentCount;

void main()
{
    Obj o = allObjects[gl_GlobalInvocationID];

    if (o.isOpaque)
        opaqueObjects[atomicAdd(opaqueCounter, 1)] = o;
    else {
        uint ptr = atomicAdd(translucentCounter, o.triangleCount);
        for (uint i = 0; i < o.triangleCount; i++)
            translucentTriangles[ptr + i] = o.triangles[i];
    }
}
```

and the following CPU timeline

```
1. Start render pass
2. Launch compute shader
2. Issue pipeline barrier: vertex waits for compute
3. Start opaque subpass
4. Draw objects in opaqueObjects buffer
5. Start translucent subpass
6. Sort translucentTriangles by depth
7. Draw triangles in translucentTriangles
8. Issue pipeline barrier: compute waits for fragment
9. End render pass
```

It is clear that such behavior is incompatible with the GPU-driven model. To make it compatible, we need to look around order-independent transparency techniques (OIT) (the usage of "transparency" is misleading since it's actually all about translucency), as to not break our rendering flow that is to render every face in the voxel world in one go, opaque and translucent ones together. Pretty much all order-independent transparency techniques are primarily designed (as their names indicate) to remove the need for step 6 in the previous CPU timeline, but are not designed to merge step 2 to 8 together. Indeed, the memory and computation cost incurred by those techniques are so high that nobody would ever think of using them for both translucent **and** opaque objects. Let us enumerate the most common OIT techniques and determine whether or not they suit our needs:

1. **Linked-list-based OIT**: Unbounded memory usage (in theory), heavy use of atomic operations. Too expensive to use for both translucent and opaque objects.
2. **Multi-pass OIT** (depth peeling, adaptive transparency): Simply too expensive in every aspects to use for both translucent and opaque objects.
3. **Weighted blended OIT**: As the name suggests, it is a customization of the classic alpha blending computation. Since it is common knowledge that using alpha blending for opaque objects on top of translucent ones is a no-go, there is no reason for the weighted-blended OIT technique to be any more viable.
4. **Screen-door-based OIT**: No additional memory usage than normal pass. Minimal extra computations.

For obvious reasons, we will settle for screen-door-based OIT. I call it "screen-door-based" because there is a plethora of different names for similar approaches: Alpha-to-Coverage and stochastic transparency are both applications of the generic technique called alpha dithering, vulgarized as "screen-door transparency"/"stippling". Truth to be told, there is no reason for Alpha-to-Coverage to yield alpha dithering at the pixel level at first glance, but for some reason, in practice it does so (see **Appendix**). Furthermore, Alpha-to-Coverage works over multi-sample anti-aliasing (MSAA) and as such allows to tackle both the transparency problem and the aliasing issues in one fell swoop. Regarding hardware support, since mesh shaders are a newer technology than MSAA, it is reasonable to assume that a GPU which doesn't support MSAA wouldn't support mesh shaders either. Additionally, for a voxel engine such as Minecraft's, MSAA should be enabled regardless of using Alpha-to-Coverage or not: Real-time voxel engines cannot afford gorgeous models and use simpler, cuboid ones instead, so aliased edges stand out even more. For those reasons, we decide to handle transparency by enabling MSAA with Alpha-to-Coverage throughout our render pass. In practice, using Alpha-to-Coverage with two or more layers of translucency results in blatantly wrong images, as if only the first layer of translucency had been taken into account.

In the end, we decide to not dive any further in translucency improvements, since a player won't have more than one layer in translucency 90% of the time in game plays that require absolute performance (e.g. competitive game plays).

Still, as a concrete example, **Fig 5**'s quality could never be reproduced with our engine.

<div align="center"><img src="./assets/images/fog.png" alt="Fog effect"></img></div>

<div align="center"><b>Fig. 5.</b> Fog effect in Minecraft<br><a href="https://www.reddit.com/r/Minecraft/comments/fa77rr/i_want_to_make_a_purple_fog_do_you_like_the_final/" target="_blank">Image source</a></div>

On a final note, for further information on translucency, the reader is more than encouraged to take a peek at these [slides](https://research.nvidia.com/sites/default/files/pubs/2011-08_Stochastic-Transparency/StochTransp-slides.pdf) and this [article](https://patents.google.com/patent/US20070070082).

## Appendix

<div align="center">
<table style="border-collapse: collapse;">
<tr>
<td align="center"><img src="./assets/images/blending.jpg" alt="Alpha blending"></img></td>
<td align="center"><img src="./assets/images/a2c.jpg" alt="Alpha-to-Coverage"></img></td>
</tr>
<td align="center"><b>Fig. 6.</b>&nbsp;Alpha blending without sorting</td>
<td align="center"><b>Fig. 7.</b>&nbsp;Alpha-to-Coverage enabled</td>
</table>
<img src="./assets/images/a2c_zoomed.jpg" alt="Alpha-to-Coverage up close"></img>
<p><b>Fig. 8.</b>&nbsp;Alpha-to-Coverage up close</p>
</div>
