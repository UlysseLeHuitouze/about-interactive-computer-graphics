# How to run

## Dependencies

The following dependencies are required to run the sample:

- [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/) with [VMA](https://gpuopen.com/vulkan-memory-allocator/) and  [Volk](https://github.com/zeux/volk) add-ons installed.
- [GLFW](https://www.glfw.org/)

## Hardware Requirements

### GPU

You should own a GPU supporting the Vulkan 1.3 standard (See [here](https://vulkan.gpuinfo.org/)) and an up-to-date GPU driver. The GPU should also support [mesh shaders](https://github.com/KhronosGroup/Vulkan-Docs/blob/main/proposals/VK_EXT_mesh_shader.adoc). Don't expect the program to run otherwise.

Side note: *While all the functionalities used in this sample have an equivalent for Vulkan <1.3, it is reasonable to assume that a GPU not supporting the Vulkan 1.3 standard wouldn't support VK_EXT_mesh_shader either.*

**Important note:** *If you still try to downgrade the sample to Vulkan <1.3, please note that the offered spir-v shaders were compiled with a Vulkan 1.3 environment in mind, thus requiring you to recompile from their source.*

### OS

You should have a working C and C++ (because of [VMA](https://gpuopen.com/vulkan-memory-allocator/)) compiler, as well as having [CMake](https://cmake.org/) installed, in order to build the sample.

## Source code changes

You may want to change the hardcoded value of the field `GPUName`, line 1256 in `./main.c` to whatever you see fit for your GPU (GPU is selected using a substring check with that `GPUName` field).

## Build

You may want to build with the provided `CMakeLists.txt` file, using cmake, after editing it a bit:

- You may want to remove the line `set(CMAKE_EXE_LINKER_FLAGS "-static")`. Added in order to force C++ stdlib to be linked statically, after encountering missing C++ stdlib errors.
- You most likely want to change `"C:\\GLFW\\glfw-3.3.8.bin.WIN64\\lib-mingw-w64"` and `"C:\\GLFW\\glfw-3.3.8.bin.WIN64\\include"` to GLFW's binary path and GLFW's include path respectively. GLFW does not modify any environment variable when installing, voiding `find_package`.
- You may want to modify the compile definition of `ASSET_PATH` to wherever this sample's `assets` directory will be, relatively to your executable. Default is `".."`.
- You might want to try disabling almost all log message and runtime checks by setting the compile definition of `DEBUG` to `0` (not recommended).
