#include <stdio.h>
#include <string.h>
#include <math.h>
#include "cenote.h"
#include "cemaths.h"
#include "GLFW/glfw3.h"

#define FRAME_COUNT CE_FRAME_COUNT
#define nroundf(afloat) ((uint32_t)roundf(afloat))

/**
 * #createRenderTargets
 * #fillSwapchain
 * #createRenderPass
 * #createFrameBuffers
 * #uploadTexture
 * #createDescriptorLayout
 * #createPipeline
 * #setupUniformAndDescriptors
 * #init
 * #loop
 * #cleanup
 */

/**
 * Notes:
 * To actually use Mesh shaders, you need to:
 * - Enable instance extension VK_KHR_get_physical_device_properties2.
 * - Enable device extension VK_EXT_mesh_shader, VK_KHR_spirv_1_4, VK_KHR_shader_float_controls.
 * - Extend VkDeviceCreateInfo in pNext with a VkPhysicalDeviceMeshShaderFeaturesEXT structure.
 * - Extend that very VkPhysicalDeviceMeshShaderFeaturesEXT structure in pNext with
 *   a VkPhysicalDeviceMaintenance4Features structure (necessary when spir-v got compiled with --target-env=vulkan1.3)
 */


VkPhysicalDeviceFeatures deviceFeatures = {};
VkPipeline pipeline;
VkPipelineLayout pipelineLayout;
VkCommandPool commandPool;
GLFWwindow* window;
VkFence frameFences[FRAME_COUNT];
VkSemaphore imageSemaphores[FRAME_COUNT];
VkSemaphore presentSemaphores[FRAME_COUNT];
VkCommandBuffer commandBuffers[FRAME_COUNT];

VkDescriptorSetLayout descriptorSetLayout;
VkDescriptorPool descriptorPool;
VkDescriptorSet descriptorSets[FRAME_COUNT];

CeBuffer uniformBuffers[FRAME_COUNT];

mat4_t projMat;
mat4_t viewMat;

CeImage colorImage;
CeImage depthImage;

VkSampleCountFlags msaaSamples;

CeSampledTexture glowstoneTexture;
CeSampledTexture stickyTexture;
CeSampledTexture glassLimeTexture;
CeSampledTexture glassOrangeTexture;

CeBuffer voxelSSBO;
typedef struct MSConstants {
    uint32_t SINGLE_MODEL_COUNT;
    uint32_t PLAIN_MODEL_COUNT;
    uint32_t CUSTOM_COMPLETE_MODEL_COUNT;
    uint32_t CUSTOM_FINAL_MODEL_COUNT;
    uint32_t CUSTOM_ABSTRACT_MODEL_COUNT;
    uint32_t CUSTOM_MODEL_SIZE;
} MSConstants;

MSConstants msConstants;
// uint32_t array
UnboundArray singleModelArray, plainModelArray, ccModelArray, cfModelArray, caModelArray;
// uint32_t array
UnboundArray voxelStateArray;
uint32_t voxelStateArraySize;
uint32_t modelArraySize;
char *modelArray;

// uint32_t array
UnboundArray customModelPointers;
uint32_t nextCustomModelPointer;
// uint8_t array
UnboundArray customModelFaceCount;

static const vec3_t CUBE_FACES[] = {

        // north
        vec3(1, 1, 0),
        vec3(0, 0, 0),

        // south
        vec3(0, 1, 1),
        vec3(1, 0, 1),

        // west
        vec3(0, 1, 0),
        vec3(0, 0, 1),

        // east
        vec3(1, 1, 1),
        vec3(1, 0, 0),

        // up
        vec3(0, 1, 0),
        vec3(1, 1, 1),

        // down
        vec3(0, 0, 1),
        vec3(1, 0, 0)
};


typedef struct Cuboid {
    uint32_t texture_and_cos[6];
    // u1 and v1 range in [0, 15]; u2 and v2 range in [1, 16]
    uint32_t u1[6], v1[6], u2[6], v2[6];
    // x, y, z range in [-16.0, 32.0]
    float x1, y1, z1, x2, y2, z2;
} Cuboid;

typedef struct Face {
    uint32_t texture_and_co;
    // u1 and v1 range in [0, 15]; u2 and v2 range in [1, 16]
    uint32_t u1, v1, u2, v2;
    // x, y, z range in [-16.0, 32.0]
    float x1, y1, z1, x2, y2, z2;
} Face;


static VkShaderModule siShaderModuleFromByteBuffer(VkDevice device, const VkAllocationCallbacks *allocator,
                                                   array *buffer, const void *pNext)
{
    VkShaderModuleCreateInfo createInfo = {
            .sTypeDefault(VkShaderModuleCreateInfo),
            .pNext = pNext,
            .codeSize = buffer->len,
            .pCode = (const uint32_t *) buffer->elements
    };
    VkShaderModule shaderModule = VK_NULL_HANDLE;
    CHECK_VK(vkCreateShaderModule(device, &createInfo, allocator, &shaderModule));
    return shaderModule;
}



static void registerSingleModel(uint32_t particle_ao, uint32_t texture_and_co)
{
    caarrpush(uint32_t, &singleModelArray, particle_ao);
    caarrpush(uint32_t, &singleModelArray, texture_and_co);
}

static void registerPlainModel(uint32_t particle_ao,
                               uint32_t texture_and_co1,
                               uint32_t texture_and_co2,
                               uint32_t texture_and_co3,
                               uint32_t texture_and_co4,
                               uint32_t texture_and_co5,
                               uint32_t texture_and_co6)
{
    caarrpush(uint32_t, &plainModelArray, particle_ao);
    caarrpush(uint32_t, &plainModelArray, texture_and_co1);
    caarrpush(uint32_t, &plainModelArray, texture_and_co2);
    caarrpush(uint32_t, &plainModelArray, texture_and_co3);
    caarrpush(uint32_t, &plainModelArray, texture_and_co4);
    caarrpush(uint32_t, &plainModelArray, texture_and_co5);
    caarrpush(uint32_t, &plainModelArray, texture_and_co6);
}
/*
static void registerCustomCompleteModel(uint32_t particle_ao, uint32_t cuboidCount, Cuboid *cuboids)
{
    msConstants.CUSTOM_COMPLETE_MODEL_COUNT++;  // don't remove!


    siAddsArray$uint32_t(&customModelPointers, 1, ARRAY_OF(uint32_t, nextCustomModelPointer));


    siAddsArray$uint8_t(&customModelFaceCount, 7, ARRAY_OF(uint8_t, 1, 1, 1, 1, 1, 1, 0));

    uint32_t *arr = malloc(sizeof *arr * (1 + cuboidCount * 18));
    uint32_t offset = 0;
    arr[offset++] = particle_ao;
    for (uint32_t i = 0; i < cuboidCount; i++)
    {
        Cuboid c = cuboids[i];
        vec3_t A = vec3(c.x1, c.y1, c.z1);
        vec3_t B = vec3(c.x2, c.y2, c.z2);
        for (uint32_t j = 0; j < 6; j++)
        {
            // a & b are the face's boundaries
            vec3_t a1 = mix3(A, B, CUBE_FACES[j * 2]);
            vec3_t b1 = mix3(A, B, CUBE_FACES[j * 2 + 1]);


            // correct boundaries so that a < b, to have correct mix computation in shader
            vec3_t a = vec3(MIN(a1.x, b1.x), MIN(a1.y, b1.y), MIN(a1.z, b1.z));
            vec3_t b = vec3(MAX(a1.x, b1.x), MAX(a1.y, b1.y), MAX(a1.z, b1.z));


            arr[offset++] = c.texture_and_cos[j];
            arr[offset++] = (nroundf(b.x * 5 + 80.f) << 24) | (nroundf(a.z * 5 + 80.f) << 16)
                    | (nroundf(a.y * 5 + 80.f) << 8) | nroundf(a.x * 5 + 80.f);
            arr[offset++] = ((c.v2[j] - 1) << 28) | ((c.u2[j] - 1) << 24) | (c.v1[j] << 20) | (c.u1[j] << 16)
                    | (nroundf(b.z * 5 + 80.f) << 8) | (nroundf(b.y * 5 + 80.f));
        }
    }
    siAddsArray$uint32_t(&ccModelArray, offset, arr);
    nextCustomModelPointer += offset;
    free(arr);
}*/

static void registerCustomCompleteModel(uint32_t particle_ao, uint32_t faceCount, const Face *faces,
                                        const uint8_t faceCountPerDirection[7])
{
    msConstants.CUSTOM_COMPLETE_MODEL_COUNT++;  // don't remove!

    caarrpush(uint32_t, &customModelPointers, nextCustomModelPointer);
    caarrpush(uint8_t, &customModelFaceCount, faceCountPerDirection[0]);
    caarrpush(uint8_t, &customModelFaceCount, faceCountPerDirection[1]);
    caarrpush(uint8_t, &customModelFaceCount, faceCountPerDirection[2]);
    caarrpush(uint8_t, &customModelFaceCount, faceCountPerDirection[3]);
    caarrpush(uint8_t, &customModelFaceCount, faceCountPerDirection[4]);
    caarrpush(uint8_t, &customModelFaceCount, faceCountPerDirection[5]);
    caarrpush(uint8_t, &customModelFaceCount, faceCountPerDirection[6]);

    caarrpush(uint32_t, &ccModelArray, particle_ao);
    for (uint32_t i = 0; i < faceCount; i++)
    {
        const Face f = faces[i];
        caarrpush(uint32_t, &ccModelArray, f.texture_and_co);
        caarrpush(uint32_t, &ccModelArray, (nroundf(f.x2 * 5 + 80.f) << 24) | (nroundf(f.z1 * 5 + 80.f) << 16)
                                           | (nroundf(f.y1 * 5 + 80.f) << 8) | nroundf(f.x1 * 5 + 80.f));

        caarrpush(uint32_t, &ccModelArray, ((f.v2 - 1) << 28) | ((f.u2 - 1) << 24) | (f.v1 << 20) | (f.u1 << 16)
                                           | (nroundf(f.z2 * 5 + 80.f) << 8) | (nroundf(f.y2 * 5 + 80.f)));
    }
    nextCustomModelPointer += 1 + 3 * faceCount;
}

static void registerCCModelCuboid(uint32_t particle_ao, uint32_t cuboidCount, const Cuboid *cuboids)
{
    Face *faces = malloc(sizeof *faces * cuboidCount * 6);
    for (uint32_t i = 0; i < cuboidCount; i++)
    {
        const Cuboid c = cuboids[i];
        vec3_t A = vec3(c.x1, c.y1, c.z1);
        vec3_t B = vec3(c.x2, c.y2, c.z2);
        for (uint32_t j = 0; j < 6; j++)
        {
            uint32_t idx = j * cuboidCount + i;
            faces[idx].texture_and_co = c.texture_and_cos[j];
            faces[idx].u1 = c.u1[j];
            faces[idx].v1 = c.v1[j];
            faces[idx].u2 = c.u2[j];
            faces[idx].v2 = c.v2[j];

            vec3_t a, b;
            {
                // a & b are the face's boundaries
                vec3_t a1 = mix3(A, B, CUBE_FACES[j * 2]);
                vec3_t b1 = mix3(A, B, CUBE_FACES[j * 2 + 1]);
                // correct boundaries so that a < b, to have correct mix computation in shader
                a = vec3(MIN(a1.x, b1.x), MIN(a1.y, b1.y), MIN(a1.z, b1.z));
                b = vec3(MAX(a1.x, b1.x), MAX(a1.y, b1.y), MAX(a1.z, b1.z));
            }
            faces[idx].x1 = a.x;
            faces[idx].y1 = a.y;
            faces[idx].z1 = a.z;
            faces[idx].x2 = b.x;
            faces[idx].y2 = b.y;
            faces[idx].z2 = b.z;
        }
    }
    registerCustomCompleteModel(particle_ao, cuboidCount * 6, faces,
                                ARRAY_OF(uint8_t, cuboidCount, cuboidCount, cuboidCount, cuboidCount, cuboidCount, cuboidCount, 0));
    free(faces);
}


static char faceID(uint32_t i)
{
    switch(i)
    {
        case 0:
            return 'N';
        case 1:
            return 'S';
        case 2:
            return 'W';
        case 3:
            return 'E';
        case 4:
            return 'U';
        case 5:
            return 'D';
        default:
            return '_';
    }
}

static void printParticleAO(uint32_t particle_ao)
{
    StringBuffer buf = ca_string_buffer_of(33);
    castrbufpush_int(&buf, particle_ao);
    castrbufpush(&buf, '\0');
    CA_INFO("\t\t%s particule texture %d ambient occlusion %d", (char*)buf.elements,
            (particle_ao << 1) >> 1, particle_ao >> 31);

    ca_string_buffer_free(buf);
}

static void printTexture$Co(uint32_t texture_and_co)
{
    StringBuffer buf = ca_string_buffer_of(33);
    castrbufpush_int(&buf, texture_and_co);
    castrbufpush(&buf, '\0');
    CA_INFO("\t\t%s texture %d tint idx %d faceID %c shade_enabled %c",
         buf.elements,
         (texture_and_co << 9) >> 9,
         (texture_and_co >> 23) & 0b11111,
         faceID((texture_and_co >> 28) & 0b111),
         texture_and_co >> 31 ? 'T' : 'F');
    ca_string_buffer_free(buf);
}

static void printPos$UVs(uint32_t first_uint, uint32_t second_uint)
{
    StringBuffer buf = ca_string_buffer_of(66);
    castrbufpush_int(&buf, first_uint);
    castrbufpush(&buf, ' ');
    castrbufpush_int(&buf, second_uint);
    castrbufpush(&buf, '\0');
    CA_INFO("\t\t%s x1 %d y1 %d z1 %d x2 %d y2 %d z2 %d u1 %d v1 %d u2 %d v2 %d",
         buf.elements,
         nroundf((float)(first_uint & 0xff) / 5.0f) - 16,
         nroundf((float)((first_uint >> 8) & 0xff) / 5.0f) - 16,
         nroundf((float)((first_uint >> 16) & 0xff) / 5.0f) - 16,
         nroundf((float)((first_uint >> 24) & 0xff) / 5.0f) - 16,
         nroundf((float)(second_uint & 0xff) / 5.0f) - 16,
         nroundf((float)((second_uint >> 8) & 0xff) / 5.0f) - 16,
         second_uint >> 16 & 0xf,
         second_uint >> 20 & 0xf,
         (second_uint >> 24 & 0xf) + 1,
         (second_uint >> 28 & 0xf) + 1);

    ca_string_buffer_free(buf);
}

static void printModels(void *arr, MSConstants constants, uint32_t voxelStateCount)
{
    const uint32_t *arr1 = arr;
    uint32_t offset = 0;
    uint32_t modelCount = 0;
    for (uint32_t i = 0; i < constants.SINGLE_MODEL_COUNT; i++)
    {
        uint32_t particle_ao = arr1[offset++];
        uint32_t texture_and_co = arr1[offset++];
        CA_INFO("Single model #%d", modelCount++);
        printParticleAO(particle_ao);
        printTexture$Co(texture_and_co);
    }

    for (uint32_t i = 0; i < constants.PLAIN_MODEL_COUNT; i++)
    {
        uint32_t particle_ao = arr1[offset++];
        CA_INFO("Plain model #%d", modelCount++);
        printParticleAO(particle_ao);
        for (uint32_t j = 0; j < 6; j++)
        {
            CA_INFO("\tFace #%d", j);
            printTexture$Co(arr1[offset++]);
        }
    }

    const uint32_t *pointers = arr1 + constants.SINGLE_MODEL_COUNT * 2 + constants.PLAIN_MODEL_COUNT * 7 + constants.CUSTOM_MODEL_SIZE;
    const uint8_t *number_of_faces = (uint8_t*)(pointers + constants.CUSTOM_COMPLETE_MODEL_COUNT + constants.CUSTOM_FINAL_MODEL_COUNT);


    for (uint32_t i = 0; i < constants.CUSTOM_COMPLETE_MODEL_COUNT; i++)
    {
        CA_INFO("Custom Complete model #%d; pointer %d", modelCount++, pointers[i]);
        printParticleAO(arr1[offset++]);

        uint32_t face_id = 0;
        for (uint32_t j = 0; j < 7; j++)
        {
            uint32_t face_count = number_of_faces[i * 7 + j];
            CA_INFO("\t%c faces: %d", faceID(j), face_count);
            for (uint32_t k = 0; k < face_count; k++)
            {
                CA_INFO("\tFace #%d", face_id++);
                printTexture$Co(arr1[offset++]);
                uint32_t first = arr1[offset++];
                printPos$UVs(first, arr1[offset++]);
            }
        }
    }
    offset += 0;



    for (uint32_t i = 0; i < voxelStateCount; i++)
    {
        CA_INFO("Voxel State #%d", i);
    }
}



static void registerVoxelState(uint32_t voxel_state)
{
    caarrpush(uint32_t, &voxelStateArray, voxel_state);
}





static void createRenderTargets(uint32_t width, uint32_t height)
{
    VkImageView colorImageView = VK_NULL_HANDLE;
    VkImageView depthImageView = VK_NULL_HANDLE;
    // color buffer
    {
        VkImageCreateInfo imageCreateInfo = {
                .sTypeDefault(VkImageCreateInfo),
                .imageType = VK_IMAGE_TYPE_2D,
                .format = cenote_get_swapchain_info()->imageFormat,
                .usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT,
                .extent = {
                        .width = width,
                        .height = height,
                        .depth = 1
                        },
                .mipLevels = 1,
                .arrayLayers = 1,
                .samples = msaaSamples,
                .tiling = VK_IMAGE_TILING_OPTIMAL
        };

        VmaAllocationCreateInfo allocationCreateInfo = {
                .usage = VMA_MEMORY_USAGE_AUTO,
                .flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT,
                //.requiredFlags = VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT,
                .priority = 1
        };

        ceDestroyCeImage(colorImage);
        ceCreateCeImage(&imageCreateInfo, &allocationCreateInfo, &colorImage, NULL);

        VkImageViewCreateInfo imageViewCreateInfo = {
                .sTypeDefault(VkImageViewCreateInfo),
                .image = colorImage.vkImage,
                .viewType = VK_IMAGE_VIEW_TYPE_2D,
                .format = cenote_get_swapchain_info()->imageFormat,
                .subresourceRange = {
                        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                        .levelCount = 1,
                        .layerCount = 1
                }
        };


        ceCreateImageView(&imageViewCreateInfo, &colorImageView);
    }

    // depth buffer
    {
        VkImageCreateInfo imageCreateInfo = {
                .sTypeDefault(VkImageCreateInfo),
                .imageType = VK_IMAGE_TYPE_2D,
                .format = VK_FORMAT_D32_SFLOAT,
                .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                .extent = {
                        .width = width,
                        .height = height,
                        .depth = 1
                        },
                .mipLevels = 1,
                .arrayLayers = 1,
                .samples = msaaSamples,
                .tiling = VK_IMAGE_TILING_OPTIMAL
        };

        VmaAllocationCreateInfo allocationCreateInfo = {
                .usage = VMA_MEMORY_USAGE_AUTO,
                .flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT,
                //.requiredFlags = VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT,
                .priority = 1
        };

        ceDestroyCeImage(depthImage);
        ceCreateCeImage(&imageCreateInfo, &allocationCreateInfo, &depthImage, NULL);

        VkImageViewCreateInfo imageViewCreateInfo = {
                .sTypeDefault(VkImageViewCreateInfo),
                .image = depthImage.vkImage,
                .viewType = VK_IMAGE_VIEW_TYPE_2D,
                .format = VK_FORMAT_D32_SFLOAT,
                .subresourceRange = {
                        .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
                        .levelCount = 1,
                        .layerCount = 1
                }
        };

        ceCreateImageView(&imageViewCreateInfo, &depthImageView);
    }

    cenote_set_framebuffers_attachments(ARRAY_OF(VkImageView, colorImageView, depthImageView));
}


static void createRenderPass()
{
    VkAttachmentDescription colorAttachment = {
            .format = cenote_get_swapchain_info()->imageFormat,
            .samples = msaaSamples,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    VkAttachmentDescription depthAttachment = {
            .format = VK_FORMAT_D32_SFLOAT,
            .samples = msaaSamples,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkAttachmentDescription colorResolveAttachment = {
            .format = cenote_get_swapchain_info()->imageFormat,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    };

    VkSubpassDependency colorDependency = {
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
    };

    VkSubpassDependency depthDependency = {
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT
    };


    VkRenderPassCreateInfo createInfo = {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
            .subpassCount = 1,
            .pSubpasses = &(VkSubpassDescription){
                .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
                .colorAttachmentCount = 1,
                .pColorAttachments = &(VkAttachmentReference){
                    .attachment = 0,
                    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                },
                .pDepthStencilAttachment = &(VkAttachmentReference){
                    .attachment = 1,
                    .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                },
                .pResolveAttachments = &(VkAttachmentReference){
                    .attachment = 2,
                    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                }
            },
            .attachmentCount = 3,
            .pAttachments = ARRAY_OF(VkAttachmentDescription, colorAttachment, depthAttachment, colorResolveAttachment),
            .dependencyCount = 2,
            .pDependencies = ARRAY_OF(VkSubpassDependency, colorDependency, depthDependency)
    };
    VkRenderPass renderPass = VK_NULL_HANDLE;
    ceCreateRenderPass(&createInfo, &renderPass);
    cenote_set_render_pass(renderPass);
}





static void uploadTexture(const char *filename, CeSampledTexture *texture)
{
    siTSStageTexture$Default(filename, &texture->ceTexture);


    VkSamplerCreateInfo samplerCreateInfo = {
            .sTypeDefault(VkSamplerCreateInfo),
            .magFilter = VK_FILTER_NEAREST,
            .minFilter = VK_FILTER_NEAREST,
            .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
            .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER,
            .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER
    };
    ceCreateSampler(&samplerCreateInfo, &texture->vkSampler);

}


static void createDescriptorLayout()
{
    VkDescriptorSetLayoutCreateInfo layoutCreateInfo = {
            .sTypeDefault(VkDescriptorSetLayoutCreateInfo),
            .bindingCount = 4,
            .pBindings = ARRAY_OF(VkDescriptorSetLayoutBinding, {
                .binding = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .stageFlags = VK_SHADER_STAGE_MESH_BIT_EXT
            }, {
                .binding = 1,
                .descriptorCount = 4,
                .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT
            }, {
                .binding = 2,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .stageFlags = VK_SHADER_STAGE_MESH_BIT_EXT | VK_SHADER_STAGE_TASK_BIT_EXT
            }, {
                .binding = 3,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .stageFlags = VK_SHADER_STAGE_MESH_BIT_EXT | VK_SHADER_STAGE_TASK_BIT_EXT
            })
    };

    ceCreateDescriptorSetLayout(&layoutCreateInfo, &descriptorSetLayout);
}

static void createPipeline()
{
    array taskBytes;
    cereadbytes(&taskBytes, ASSET_PATH "/assets/shaders/testmeshing.task.spv");
    VkShaderModule taskShader = siShaderModuleFromByteBuffer(cenote_get_device(), NULL, &taskBytes, NULL);

    array meshBytes;
    cereadbytes(&meshBytes, ASSET_PATH "/assets/shaders/testmeshing.mesh.spv");
    VkShaderModule meshShader = siShaderModuleFromByteBuffer(cenote_get_device(), NULL, &meshBytes, NULL);

    array fragBytes;
    cereadbytes(&fragBytes, ASSET_PATH "/assets/shaders/testmeshing.fs.glsl.spv");
    VkShaderModule fragmentShader = siShaderModuleFromByteBuffer(cenote_get_device(), NULL, &fragBytes, NULL);

    VkPipelineLayoutCreateInfo layoutCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
            .setLayoutCount = 1,
            .pSetLayouts = &descriptorSetLayout
    };
    ceCreatePipelineLayout(&layoutCreateInfo, &pipelineLayout);

    VkSpecializationInfo specilization = {
            .dataSize = sizeof(MSConstants),
            .mapEntryCount = 6,
            .pMapEntries = ARRAY_OF(VkSpecializationMapEntry, {
                .constantID = 0,
                .offset = offsetof(MSConstants, SINGLE_MODEL_COUNT),
                .size = sizeof(uint32_t)
            }, {
                .constantID = 1,
                .offset = offsetof(MSConstants, PLAIN_MODEL_COUNT),
                .size = sizeof(uint32_t)
            }, {
                .constantID = 2,
                .offset = offsetof(MSConstants, CUSTOM_COMPLETE_MODEL_COUNT),
                .size = sizeof(uint32_t)
            }, {
                .constantID = 3,
                .offset = offsetof(MSConstants, CUSTOM_FINAL_MODEL_COUNT),
                .size = sizeof(uint32_t)
            }, {
                .constantID = 4,
                .offset = offsetof(MSConstants, CUSTOM_ABSTRACT_MODEL_COUNT),
                .size = sizeof(uint32_t)
            }, {
                .constantID = 5,
                .offset = offsetof(MSConstants, CUSTOM_MODEL_SIZE),
                .size = sizeof(uint32_t)
            }),
            .pData = &msConstants
    };

    VkGraphicsPipelineCreateInfo createInfo = {
            .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            .stageCount = 3,
            .pStages = ARRAY_OF(VkPipelineShaderStageCreateInfo,
                                {
                                    .sTypeDefault(VkPipelineShaderStageCreateInfo),
                                    .stage = VK_SHADER_STAGE_TASK_BIT_EXT,
                                    .module = taskShader,
                                    .pName = "main",
                                    .pSpecializationInfo = &specilization
                                }, {
                                    .sTypeDefault(VkPipelineShaderStageCreateInfo),
                                    .stage = VK_SHADER_STAGE_MESH_BIT_EXT,
                                    .module = meshShader,
                                    .pName = "main",
                                    .pSpecializationInfo = &specilization
                                }, {
                                    .sTypeDefault(VkPipelineShaderStageCreateInfo),
                                    .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
                                    .module = fragmentShader,
                                    .pName = "main",
                                    .pSpecializationInfo = NULL
                                }
                                ),
            .pVertexInputState = &(VkPipelineVertexInputStateCreateInfo){
                    .sTypeDefault(VkPipelineVertexInputStateCreateInfo)
            },
            .pInputAssemblyState = &(VkPipelineInputAssemblyStateCreateInfo){
                    .sTypeDefault(VkPipelineInputAssemblyStateCreateInfo),
                    .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST
            },
            .pTessellationState = &(VkPipelineTessellationStateCreateInfo){
                    .sTypeDefault(VkPipelineTessellationStateCreateInfo)
            },
            .pViewportState = &(VkPipelineViewportStateCreateInfo){
                    .sTypeDefault(VkPipelineViewportStateCreateInfo),
                    .viewportCount = 1,
                    .scissorCount = 1
            },
            .pRasterizationState = &(VkPipelineRasterizationStateCreateInfo){
                    .sTypeDefault(VkPipelineRasterizationStateCreateInfo),
                    .depthClampEnable = VK_FALSE,
                    .polygonMode = VK_POLYGON_MODE_FILL,
                    .cullMode = VK_CULL_MODE_BACK_BIT,
                    .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
                    .lineWidth = 1.0f
            },
            .pMultisampleState = &(VkPipelineMultisampleStateCreateInfo){
                    .sTypeDefault(VkPipelineMultisampleStateCreateInfo),
                    .rasterizationSamples = msaaSamples,
                    .alphaToCoverageEnable = VK_TRUE
            },
            .pDepthStencilState = &(VkPipelineDepthStencilStateCreateInfo){
                    .sTypeDefault(VkPipelineDepthStencilStateCreateInfo),
                    .depthTestEnable = 1,
                    .depthWriteEnable = 1,
                    .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
                    .minDepthBounds = 0.0f,
                    .maxDepthBounds = 1.0f
            },
            .pColorBlendState = &(VkPipelineColorBlendStateCreateInfo){
                    .sTypeDefault(VkPipelineColorBlendStateCreateInfo),
                    .attachmentCount = 1,
                    .pAttachments = &(VkPipelineColorBlendAttachmentState){
                            .blendEnable = 0,
                            .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
                            .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
                            .colorBlendOp = VK_BLEND_OP_ADD,
                            .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
                            .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
                            .alphaBlendOp = VK_BLEND_OP_ADD,
                            .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT
                                              | VK_COLOR_COMPONENT_A_BIT
                    },
                    .blendConstants = {0, 0, 0, 0}
            },
            .pDynamicState = &(VkPipelineDynamicStateCreateInfo){
                    .sTypeDefault(VkPipelineDynamicStateCreateInfo),
                    .dynamicStateCount = 2,
                    .pDynamicStates = ARRAY_OF(VkDynamicState, VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR)
            },
            .layout = pipelineLayout,
            .renderPass = cenote_get_render_pass()
    };

    ceCreateGraphicsPipelines(VK_NULL_HANDLE, 1, &createInfo, &pipeline);

    ceDestroyShaderModule(taskShader);
    ceDestroyShaderModule(meshShader);
    ceDestroyShaderModule(fragmentShader);

    free(fragBytes.elements);
    free(meshBytes.elements);
    free(taskBytes.elements);
}


static void setupUniformAndDescriptors()
{
    // uniform buffers

    {
        VkBufferCreateInfo bufferCreateInfo = {
                .sTypeDefault(VkBufferCreateInfo),
                .size = 2 * 16 * sizeof(float),
                .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT
        };
        VmaAllocationCreateInfo allocationCreateInfo = {
                .usage = VMA_MEMORY_USAGE_AUTO,
                .flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT,
                .requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
        };
        for (uint32_t i = 0; i < FRAME_COUNT; i++) {
            ceCreateCeBuffer(&bufferCreateInfo, &allocationCreateInfo,
                           uniformBuffers + i, NULL);
        }
    }

    // voxel ssbo
    {
        VkBufferCreateInfo bufferCreateInfo = {
                .sTypeDefault(VkBufferCreateInfo),
                .size = modelArraySize + voxelStateArraySize,
                .usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT
        };
        VmaAllocationCreateInfo  allocationCreateInfo = {
                .usage = VMA_MEMORY_USAGE_AUTO,
                .flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT,
                .priority = 1
        };
        ceCreateCeBuffer(&bufferCreateInfo, &allocationCreateInfo, &voxelSSBO, NULL);
    }


    // descriptor pool

    VkDescriptorPoolCreateInfo createInfo = {
            .sTypeDefault(VkDescriptorPoolCreateInfo),
            .maxSets = FRAME_COUNT,
            .poolSizeCount = 3,
            .pPoolSizes = ARRAY_OF(VkDescriptorPoolSize, {
                    .descriptorCount = FRAME_COUNT,
                    .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER
            }, {
                .descriptorCount = 1,
                .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
            }, {
                .descriptorCount = 2,
                .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER
            })
    };
    ceCreateDescriptorPool(&createInfo, &descriptorPool);

    // set allocation

    VkDescriptorSetAllocateInfo allocateInfo = {
            .sTypeDefault(VkDescriptorSetAllocateInfo),
            .descriptorPool = descriptorPool,
            .descriptorSetCount = FRAME_COUNT,
            .pSetLayouts = ARRAY_OF(VkDescriptorSetLayout, descriptorSetLayout, descriptorSetLayout, descriptorSetLayout)
    };
    ceAllocateDescriptorSets(&allocateInfo, descriptorSets);

    // uniform buffer descriptor write

    VkDescriptorBufferInfo bufferInfo = {
            .range = 2 * 16 * sizeof(float)
    };
    VkWriteDescriptorSet writeBuffers = {
            .sTypeDefault(VkWriteDescriptorSet),
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .dstBinding = 0,
            .descriptorCount = 1,
            .pBufferInfo = &bufferInfo
    };

    // voxel model ssbo descriptor write

    VkDescriptorBufferInfo modelSsboInfo = {
            .range = modelArraySize,
            .buffer = voxelSSBO.vkBuffer
    };
    VkWriteDescriptorSet writeModelSSBO = {
            .sTypeDefault(VkWriteDescriptorSet),
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .dstBinding = 2,
            .descriptorCount = 1,
            .pBufferInfo = &modelSsboInfo
    };

    // voxel state ssbo descriptor write

    VkDescriptorBufferInfo stateSsboInfo = {
            .buffer = voxelSSBO.vkBuffer,
            .offset = modelArraySize,
            .range = voxelStateArraySize
    };
    VkWriteDescriptorSet writeStateSSBO = {
            .sTypeDefault(VkWriteDescriptorSet),
            .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .dstBinding = 3,
            .descriptorCount = 1,
            .pBufferInfo = &stateSsboInfo
    };

    // texture descriptor write

    VkDescriptorImageInfo imageInfo[] = {
            {
                .sampler = glowstoneTexture.vkSampler,
                .imageView = glowstoneTexture.ceTexture.vkImageView,
                .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
            },
            {
                .sampler = stickyTexture.vkSampler,
                .imageView = stickyTexture.ceTexture.vkImageView,
                .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
            },
            {
                .sampler = glassLimeTexture.vkSampler,
                .imageView = glassLimeTexture.ceTexture.vkImageView,
                .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
            },
            {
                .sampler = glassOrangeTexture.vkSampler,
                .imageView = glassOrangeTexture.ceTexture.vkImageView,
                .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
            }
    };
    VkWriteDescriptorSet writeTextures = {
            .sTypeDefault(VkWriteDescriptorSet),
            .dstBinding = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 4,
            .pImageInfo = imageInfo
    };

    for (uint32_t i = 0; i < FRAME_COUNT; i++)
    {
        bufferInfo.buffer = uniformBuffers[i].vkBuffer;
        writeBuffers.dstSet = writeModelSSBO.dstSet = writeStateSSBO.dstSet = writeTextures.dstSet = descriptorSets[i];
        ceUpdateDescriptorSets(4, ARRAY_OF(VkWriteDescriptorSet, writeBuffers, writeModelSSBO, writeStateSSBO, writeTextures), 0, NULL);
    }
}

static void onResize(__attribute__((unused)) GLFWwindow *wind, int width, int height)
{
    ceDeviceWaitIdle();
    createRenderTargets(width, height);
    cenote_on_resize(width, height);
}



static VkResult getRequiredInstanceExtensions(uint32_t *count, const char ***ext) {
    const char **glfwExt = glfwGetRequiredInstanceExtensions(count);
    if (glfwExt == NULL)
        return VK_ERROR_INITIALIZATION_FAILED;

    *ext = glfwExt;

    return VK_SUCCESS;
}


static VkResult createVkSurfaceKHR(VkInstance instance, const VkAllocationCallbacks *allocator, VkSurfaceKHR *surface) {
    return glfwCreateWindowSurface(instance, window, allocator, surface);
}


/*#define compile(shadername) BEXIT(system("C:/VulkanSDK/1.3.250.1/Bin/glslc.exe \"../assets/shaders/" shadername\
"\" -o \"../assets/shaders/" shadername ".spv\" --target-env=vulkan1.3 -DWARP_SIZE=32") < 0, \
"compiling" shadername "failed")*/

static void init()
{
    /*{
        CA_INFO("Compiling shaders...");
        compile("testmeshing.mesh");
        compile("testmeshing.task");
        compile("testmeshing.fs.glsl");
    }*/


    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    window = glfwCreateWindow(500, 500, "Mesh Shader demo", VK_NULL_HANDLE, VK_NULL_HANDLE);

    BEXIT(window == VK_NULL_HANDLE, "Failed to create a GLFW window");

    const GLFWvidmode *vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    glfwSetWindowPos(window, (vidMode->width - 500) / 2, (vidMode->height - 500) / 2);

    uint32_t width, height;
    {
        int w = 0, h = 0;
        glfwGetFramebufferSize(window, &w, &h);
        width = w; height = h;
    }

    {
        singleModelArray = ca_unbound_array_of(uint32_t, 10, smalloc, realloc);
        plainModelArray = ca_unbound_array_of(uint32_t, 27, smalloc, realloc);
        ccModelArray = ca_unbound_array_of(uint32_t, 10, smalloc, realloc);
        cfModelArray = ca_unbound_array_of(uint32_t, 10, smalloc, realloc);
        caModelArray = ca_unbound_array_of(uint32_t, 10, smalloc, realloc);

        customModelFaceCount = ca_unbound_array_of(uint8_t, 7 * 2, smalloc, realloc);
        customModelPointers = ca_unbound_array_of(uint32_t, 10, smalloc, realloc);
        voxelStateArray = ca_unbound_array_of(uint32_t, 10, smalloc, realloc);

        registerSingleModel(0,
                            0 | (0 << 28) | (1 << 31));
        registerSingleModel(0,
                            3 | (0 << 28) | (1 << 31));

        registerPlainModel(0,
                           0 | (0 << 28) | (1 << 31),
                           0 | (1 << 28) | (1 << 31),
                           0 | (2 << 28) | (1 << 31),
                           0 | (3 << 28) | (1 << 31),
                           1 | (4 << 28) | (1 << 31),
                           0 | (5 << 28) | (1 << 31));
        registerPlainModel(0,
                           1 | (0 << 28) | (1 << 31),
                           1 | (1 << 28) | (1 << 31),
                           1 | (2 << 28) | (1 << 31),
                           1 | (3 << 28) | (1 << 31),
                           2 | (4 << 28) | (1 << 31),
                           1 | (5 << 28) | (0 << 31));
        registerPlainModel(0,
                           2 | (0 << 28) | (1 << 31),
                           2 | (1 << 28) | (1 << 31),
                           2 | (2 << 28) | (1 << 31),
                           2 | (3 << 28) | (1 << 31),
                           3 | (4 << 28) | (1 << 31),
                           2 | (5 << 28) | (1 << 31));
        registerPlainModel(0,
                           3 | (0 << 28) | (1 << 31),
                           3 | (1 << 28) | (1 << 31),
                           3 | (2 << 28) | (1 << 31),
                           3 | (3 << 28) | (1 << 31),
                           1 | (4 << 28) | (1 << 31),
                           3 | (5 << 28) | (1 << 31));

        // #7
        registerCCModelCuboid(0, 1, &(Cuboid){
            .texture_and_cos = {
                    2 | (0 << 28) | (1 << 31),
                    2 | (1 << 28) | (1 << 31),
                    2 | (2 << 28) | (1 << 31),
                    2 | (3 << 28) | (1 << 31),
                    2 | (4 << 28) | (1 << 31),
                    2 | (5 << 28) | (1 << 31)
            },
            .x1 = 4,
            .y1 = 4,
            .z1 = 4,
            .x2 = 12,
            .y2 = 12,
            .z2 = 12,
            .u2 = {16, 16, 16, 16, 16, 16},
            .v2 = {16, 16, 16, 16, 16, 16}
        });

        // #8
        /*registerCCModelCuboid(0, 2, ARRAY_OF(Cuboid, {
            .texture_and_cos = {
                    0 | (0 << 28) | (1 << 31),
                    0 | (1 << 28) | (1 << 31),
                    0 | (2 << 28) | (1 << 31),
                    0 | (3 << 28) | (1 << 31),
                    0 | (4 << 28) | (1 << 31),
                    0 | (5 << 28) | (1 << 31)
            },
            .x2 = 16,
            .y2 = 8,
            .z2 = 16,
            .u2 = {16, 16, 16, 16, 16, 16},
            .v2 = {8, 8, 8, 8, 16, 16}
        }, {
            .texture_and_cos = {
                    0 | (0 << 28) | (1 << 31),
                    0 | (1 << 28) | (1 << 31),
                    0 | (2 << 28) | (1 << 31),
                    0 | (3 << 28) | (1 << 31),
                    0 | (4 << 28) | (1 << 31),
                    0 | (5 << 28) | (1 << 31)
            },
            .x1 = 8,
            .y1 = 8,
            .z1 = 0,
            .x2 = 16,
            .y2 = 16,
            .z2 = 16,
            .u2 = {8, 8, 16, 16, 16, 16},
            .v2 = {8, 8, 8, 8, 8, 8}
        }));*/
        registerCustomCompleteModel(0, 10, ARRAY_OF(Face, {
            .texture_and_co = 0 | (0 << 28) | (1 << 31),
            .x2 = 16,
            .y2 = 8,
            .v1 = 8,
            .u2 = 16,
            .v2 = 16
        }, {
            .texture_and_co = 0 | (0 << 28) | (1 << 31),
            .x1 = 8,
            .y1 = 8,
            .x2 = 16,
            .y2 = 16,
            .u2 = 8,
            .v2 = 8
        }, {
            .texture_and_co = 0 | (1 << 28) | (1 << 31),
            .z1 = 16,
            .x2 = 16,
            .y2 = 8,
            .z2 = 16,
            .v1 = 8,
            .u2 = 16,
            .v2 = 16
        }, {
            .texture_and_co = 0 | (1 << 28) | (1 << 31),
            .x1 = 8,
            .y1 = 8,
            .z1 = 16,
            .x2 = 16,
            .y2 = 16,
            .z2 = 16,
            .u1 = 8,
            .u2 = 16,
            .v2 = 8
        }, {
            .texture_and_co = 0 | (2 << 28) | (1 << 31),
            .y2 = 8,
            .z2 = 16,
            .v1 = 8,
            .u2 = 16,
            .v2 = 16,
        }, {
            .texture_and_co = 0 | (2 << 28) | (1 << 31),
            .x1 = 8,
            .y1 = 8,
            .x2 = 8,
            .y2 = 16,
            .z2 = 16,
            .u2 = 16,
            .v2 = 8
        }, {
            .texture_and_co = 0 | (3 << 28) | (1 << 31),
            .x1 = 16,
            .x2 = 16,
            .y2 = 16,
            .z2 = 16,
            .u2 = 16,
            .v2 = 16
        }, {
            .texture_and_co = 0 | (4 << 28) | (1 << 31),
            .y1 = 8,
            .x2 = 8,
            .y2 = 8,
            .z2 = 16,
            .u2 = 8,
            .v2 = 16
        }, {
            .texture_and_co = 0 | (4 << 28) | (1 << 31),
            .x1 = 8,
            .y1 = 16,
            .x2 = 16,
            .y2 = 16,
            .z2 = 16,
            .u1 = 8,
            .u2 = 16,
            .v2 = 16
        }, {
            .texture_and_co = 0 | (5 << 28) | (1 << 31),
            .x2 = 16,
            .z2 = 16,
            .u2 = 16,
            .v2 = 16
        }), ARRAY_OF(uint8_t, 2, 2, 2, 1, 2, 1, 0));

        registerVoxelState(7);
        registerVoxelState(0);
        registerVoxelState(1);
        registerVoxelState(2);
        registerVoxelState(3);
        registerVoxelState(4);
        registerVoxelState(5);
        registerVoxelState(6);
        registerVoxelState(7);


        for (uint32_t i = 0; i < customModelPointers.count; i++) {
            uint32_t old = caarrget(uint32_t, &customModelPointers, i);
            caarrset(uint32_t, &customModelPointers, i, old + singleModelArray.count + plainModelArray.count);
        }

        msConstants.SINGLE_MODEL_COUNT = singleModelArray.count >> 1;
        msConstants.PLAIN_MODEL_COUNT = plainModelArray.count / 7;
        msConstants.CUSTOM_MODEL_SIZE = ccModelArray.count + cfModelArray.count + caModelArray.count;
        modelArraySize = (singleModelArray.count + plainModelArray.count + msConstants.CUSTOM_MODEL_SIZE
                + (msConstants.CUSTOM_COMPLETE_MODEL_COUNT + msConstants.CUSTOM_FINAL_MODEL_COUNT) * 3
                - ((msConstants.CUSTOM_COMPLETE_MODEL_COUNT + msConstants.CUSTOM_FINAL_MODEL_COUNT) >> 2)) * sizeof(uint32_t);
        voxelStateArraySize = voxelStateArray.count * sizeof(uint32_t);
        modelArray = nmalloc(modelArraySize + voxelStateArraySize);

        memcpy(modelArray, singleModelArray.elements, sizeof(uint32_t) * singleModelArray.count);
        uint32_t offset = singleModelArray.count * sizeof(uint32_t);

        memcpy(modelArray + offset, plainModelArray.elements, sizeof(uint32_t) * plainModelArray.count);
        offset += plainModelArray.count * sizeof(uint32_t);

        memcpy(modelArray + offset, ccModelArray.elements, sizeof(uint32_t) * ccModelArray.count);
        offset += ccModelArray.count * sizeof(uint32_t);

        memcpy(modelArray + offset, cfModelArray.elements, sizeof(uint32_t) * cfModelArray.count);
        offset += cfModelArray.count * sizeof(uint32_t);

        memcpy(modelArray + offset, caModelArray.elements, sizeof(uint32_t) * caModelArray.count);
        offset += caModelArray.count * sizeof(uint32_t);

        memcpy(modelArray + offset, customModelPointers.elements, sizeof(uint32_t) * customModelPointers.count);
        offset += customModelPointers.count * sizeof(uint32_t);

        memcpy(modelArray + offset, customModelFaceCount.elements, sizeof(uint8_t) * customModelFaceCount.count);
        offset += customModelFaceCount.count * sizeof(uint8_t);
        // (face count at the end for alignment)
        memcpy(modelArray + offset, voxelStateArray.elements, sizeof(uint32_t) * voxelStateArray.count);

        printModels(modelArray, msConstants, voxelStateArray.count);

        ca_unbound_array_free(singleModelArray, free);
        ca_unbound_array_free(plainModelArray, free);
        ca_unbound_array_free(ccModelArray, free);
        ca_unbound_array_free(cfModelArray, free);
        ca_unbound_array_free(caModelArray, free);
        ca_unbound_array_free(customModelPointers, free);
        ca_unbound_array_free(customModelFaceCount, free);
        ca_unbound_array_free(voxelStateArray, free);
    }

    CenoteCreateInfo cenoteCreateInfo = {
            .appName = "nothing",
            .appVersion = VK_MAKE_VERSION(1, 0, 0),
            .apiVersion = VK_API_VERSION_1_3,
            .windowWidth = width,
            .windowHeight = height,
            .GPUName = "GTX 1650",
            .getInstanceExtensions_ptr = getRequiredInstanceExtensions,
            .createVkSurfaceKHR_ptr = createVkSurfaceKHR,
            .requestedLayers = ca_array_of(char*),
            .requestedInstanceExtensions = ca_array_of(char*, VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME),
            .requestedDeviceExtensions = ca_array_of(char*,
                                                 VK_KHR_SWAPCHAIN_EXTENSION_NAME,
                                                 VK_EXT_MESH_SHADER_EXTENSION_NAME,
                                                 VK_KHR_SHADER_NON_SEMANTIC_INFO_EXTENSION_NAME,
                                                 VK_KHR_SPIRV_1_4_EXTENSION_NAME,
                                                 VK_KHR_MAINTENANCE_4_EXTENSION_NAME,
                                                 VK_KHR_SHADER_FLOAT_CONTROLS_EXTENSION_NAME,
                                                 VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME),
            .pNextDeviceCreateInfo = &(VkPhysicalDeviceMeshShaderFeaturesEXT){
                    .sTypeDefault(VkPhysicalDeviceMeshShaderFeaturesEXT),
                    .pNext = &(VkPhysicalDeviceMaintenance4Features){
                            .sTypeDefault(VkPhysicalDeviceMaintenance4Features),
                            .pNext = &(VkPhysicalDeviceDescriptorIndexingFeaturesEXT){
                                    .sTypeDefault(VkPhysicalDeviceDescriptorIndexingFeaturesEXT),
                                    .pNext = &(VkPhysicalDevice8BitStorageFeatures){
                                            .sTypeDefault(VkPhysicalDevice8BitStorageFeatures),
                                            .pNext = &(VkPhysicalDeviceShaderFloat16Int8Features){
                                                    .sTypeDefault(VkPhysicalDeviceShaderFloat16Int8Features),
                                                    .shaderInt8 = VK_TRUE
                                            },
                                            .storageBuffer8BitAccess = VK_TRUE
                                    },
                                    .descriptorBindingVariableDescriptorCount = VK_TRUE,
                                    .runtimeDescriptorArray = VK_TRUE,
                                    .shaderSampledImageArrayNonUniformIndexing = VK_TRUE
                            },
                            .maintenance4 = VK_TRUE
                    },
                    .meshShader = VK_TRUE,
                    .taskShader = VK_TRUE
            },
            .requestedDeviceFeatures = &deviceFeatures,
            .requestedSwapchainInfo = &(SwapchainSetupInfo){
                    {
                            .format = VK_FORMAT_B8G8R8A8_SRGB,
                            .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
                    },
                    .presentMode = VK_PRESENT_MODE_MAILBOX_KHR,
                    .imageCount = FRAME_COUNT
            },
    };

    cenote_use(&cenoteCreateInfo, NULL);

    glfwSetFramebufferSizeCallback(window, onResize);

    // "warp" subgroup size
    {
        VkPhysicalDeviceSubgroupProperties subgroupProps = {
                .sTypeDefault(VkPhysicalDeviceSubgroupProperties)
        };
        VkPhysicalDeviceProperties2 props2 = {
                .sTypeDefault(VkPhysicalDeviceProperties2),
                .pNext = &subgroupProps
        };
        ceGetPhysicalDeviceProperties2(&props2);

        VkPhysicalDeviceProperties props = {};

        ceGetPhysicalDeviceProperties(&props);

        CA_INFO("subgroupSize %d max ubo size %d", subgroupProps.subgroupSize, props.limits.maxUniformBufferRange);
    }

    // msaa samples
    {
        VkPhysicalDeviceProperties physicalDeviceProperties;
        ceGetPhysicalDeviceProperties(&physicalDeviceProperties);
        VkSampleCountFlags counts = physicalDeviceProperties.limits.framebufferColorSampleCounts &
                physicalDeviceProperties.limits.framebufferDepthSampleCounts;
        if (counts & VK_SAMPLE_COUNT_8_BIT)     msaaSamples = VK_SAMPLE_COUNT_8_BIT;
        else if (counts & VK_SAMPLE_COUNT_4_BIT)    msaaSamples = VK_SAMPLE_COUNT_4_BIT;
        else    msaaSamples = VK_SAMPLE_COUNT_2_BIT;
    }

    cenote_set_framebuffers_attachment_count(2);

    createRenderPass();
    onResize(NULL, (int)cenote_get_swapchain_width(), (int)cenote_get_swapchain_height());

    createDescriptorLayout();
    createPipeline();

    {
        VkCommandPoolCreateInfo createInfo = {
                .sTypeDefault(VkCommandPoolCreateInfo),
                .queueFamilyIndex = cenote_get_queue_family_index(),
                .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
        };
        ceCreateCommandPool(&createInfo, &commandPool);
    }

    {
        VkFenceCreateInfo fenceCreateInfo = {
                .sTypeDefault(VkFenceCreateInfo),
                .flags = VK_FENCE_CREATE_SIGNALED_BIT
        };
        VkSemaphoreCreateInfo semaphoreCreateInfo = {
                .sTypeDefault(VkSemaphoreCreateInfo)
        };
        VkCommandBufferAllocateInfo allocInfo = {
                .sTypeDefault(VkCommandBufferAllocateInfo),
                .commandPool = commandPool,
                .commandBufferCount = 1,
                .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY
        };




        for (uint32_t i = 0; i < FRAME_COUNT; i++)
        {
            ceCreateFence(&fenceCreateInfo, &frameFences[i]);
            ceCreateSemaphore(&semaphoreCreateInfo, &imageSemaphores[i]);
            ceCreateSemaphore(&semaphoreCreateInfo, &presentSemaphores[i]);
            ceAllocateCommandBuffers(&allocInfo, &commandBuffers[i]);
        }
    }

    siTSPrimeService(cenote_get_device(), cenote_get_main_queue(),
                     VK_SHARING_MODE_EXCLUSIVE,
                     2,
                     ARRAY_OF(uint32_t, cenote_get_queue_family_index(), cenote_get_queue_family_index()),
                     cenote_get_allocator(), commandPool, 1);


    uploadTexture(ASSET_PATH "/assets/textures/glowstone.png", &glowstoneTexture);
    uploadTexture(ASSET_PATH "/assets/textures/sticky.png", &stickyTexture);
    uploadTexture(ASSET_PATH "/assets/textures/glass_lime.png", &glassLimeTexture);
    uploadTexture(ASSET_PATH "/assets/textures/glass_orange.png", &glassOrangeTexture);

    setupUniformAndDescriptors();

    siTSStageToBuffer(modelArray, voxelSSBO.vkBuffer, 0, modelArraySize);
    free(modelArray);

    {
        perspective_lh(projMat, 70, width, height, 0.1f, 100);

        for (uint32_t i = 0; i < FRAME_COUNT; i++)
        {
            void *ptr;
            vmaMapMemory(cenote_get_allocator(), uniformBuffers[i].vmaAllocation, &ptr);
            memcpy(ptr, projMat, sizeof(projMat));
            vmaUnmapMemory(cenote_get_allocator(), uniformBuffers[i].vmaAllocation);
        }

    }
    siTSKillService();
}

double last_x, last_y;
// initially 90
double yaw = 90;
// initially 0
double pitch;

// initially (0, 0, 0)
vec3_t camPos = vec3(0, 0, 0);
// always (0, 1, 0)
vec3_t camUp = vec3(0, 1, 0);
// initially (0, 0, 1)
vec3_t camEye = vec3(0, 0, 1);

_Bool escape = 0;
_Bool pressing_escape = 0;

static void updateMotion()
{
    int res = glfwGetKey(window, GLFW_KEY_ESCAPE);
    if (res == GLFW_PRESS && !pressing_escape)
    {
        pressing_escape = 1;
        escape = !escape;
        glfwSetInputMode(window, GLFW_CURSOR, escape ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
    } else if (res == GLFW_RELEASE)
        pressing_escape = 0;

    vec3_t eyeHorizontal = (vec3_t)normalize(vec4(camEye.x, 0, camEye.z, 0)).gen;
    vec3_t left = cross(camUp, eyeHorizontal);

    float forward = (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS ? 0.02f : 0) +
            (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS ? -0.02f : 0);
    float leftward= (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS ? 0.02f : 0) +
            (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS ? -0.02f : 0);
    float upward  = (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS ? 0.02f : 0) +
            (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS ? -0.02f : 0);

    camPos.gen += forward * eyeHorizontal.gen + leftward * left.gen + camUp.gen * upward;
}


static void mouse_input(__attribute__((unused)) GLFWwindow* win, double xpos, double ypos)
{
    yaw += (xpos - last_x) * 0.3;
    pitch = CLAMP(-89, 89, pitch - (ypos - last_y) * 0.3);
    last_x = xpos;
    last_y = ypos;
}

static void setViewMatrix(uint32_t frameID)
{


    look_at_lh(viewMat, camPos, (vec3_t)(camPos.gen + camEye.gen), camUp);


    void *ptr;
    vmaMapMemory(cenote_get_allocator(), uniformBuffers[frameID].vmaAllocation, &ptr);
    memcpy(ptr + sizeof(mat4_t), viewMat, sizeof(viewMat));
    vmaUnmapMemory(cenote_get_allocator(), uniformBuffers[frameID].vmaAllocation);
}


static void recordCommandBuffer(VkCommandBuffer cmdBuffer, uint32_t frame_id, VkFramebuffer framebuffer)
{
    {
        VkCommandBufferBeginInfo beginInfo = {
                .sTypeDefault(VkCommandBufferBeginInfo)
        };
        CHECK_VK(vkBeginCommandBuffer(cmdBuffer, &beginInfo));
    }

    VkRect2D renderArea = {.offset = {}, .extent = cenote_get_swapchain_info()->imageExtent};
    {
        VkRenderPassBeginInfo beginInfo = {
                .sTypeDefault(VkRenderPassBeginInfo),
                .renderPass = cenote_get_render_pass(),
                .framebuffer = framebuffer,
                .renderArea = renderArea,
                .clearValueCount = 2,
                .pClearValues = ARRAY_OF(VkClearValue, {
                    .color = {.float32 = {1, 1, 1, 1}}
                }, {
                    .depthStencil = 1.0f
                })
        };
        vkCmdBeginRenderPass(cmdBuffer, &beginInfo, VK_SUBPASS_CONTENTS_INLINE);
    }

    vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    vkCmdSetViewport(cmdBuffer, 0, 1, &(VkViewport){
            .minDepth = 0.0f,
            .maxDepth = 1.0f,
            .width = ((float) cenote_get_swapchain_width()),
            .height = -((float) cenote_get_swapchain_height()),
            .y = (float) cenote_get_swapchain_height()
    });

    vkCmdSetScissor(cmdBuffer, 0, 1, &renderArea);
    vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1,
                            &descriptorSets[frame_id], 0, NULL);


    vkCmdDrawMeshTasksEXT(cmdBuffer, 8, 1, 1);

    vkCmdEndRenderPass(cmdBuffer);
    CHECK_VK(vkEndCommandBuffer(cmdBuffer));
}

static void loop()
{
    if (glfwRawMouseMotionSupported())
        glfwSetInputMode(window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwGetCursorPos(window, &last_x, &last_y);
    glfwSetCursorPosCallback(window, mouse_input);
    uint32_t frameIndex = 0;
    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        while (cenote_get_swapchain_width() == 0 || cenote_get_swapchain_height() == 0)
            glfwWaitEvents();

        camEye = vec3(cos(radians(yaw)) * cos(radians(pitch)),
                      sin(radians(pitch)),
                      sin(radians(yaw)) * cos(radians(pitch)));

        updateMotion();
        setViewMatrix(frameIndex);



        VkCommandBuffer cmdBuffer = commandBuffers[frameIndex];

        ceWaitForFences(1, &frameFences[frameIndex], VK_TRUE, 1e9);
        uint32_t acquiredImageIndex = 0;
        CHECK_VK(vkAcquireNextImageKHR(cenote_get_device(), cenote_get_swapchain(), 1e9, imageSemaphores[frameIndex],
                                          VK_NULL_HANDLE, &acquiredImageIndex));

        ceResetFences(1, &frameFences[frameIndex]);
        ceResetCommandBuffer(cmdBuffer, 0);

        recordCommandBuffer(cmdBuffer, frameIndex, cenote_get_framebuffer(acquiredImageIndex));

        VkSubmitInfo submitInfo = {
                .sTypeDefault(VkSubmitInfo),
                .waitSemaphoreCount = 1,
                .pWaitSemaphores = &imageSemaphores[frameIndex],
                .pWaitDstStageMask = ARRAY_OF(VkPipelineStageFlags, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT),
                .commandBufferCount = 1,
                .pCommandBuffers = &cmdBuffer,
                .signalSemaphoreCount = 1,
                .pSignalSemaphores = &presentSemaphores[frameIndex]
        };
        ceQueueSubmit(cenote_get_main_queue(), 1, &submitInfo,
                                  frameFences[frameIndex]);

        VkSwapchainKHR swapchainKhr = cenote_get_swapchain();

        VkPresentInfoKHR presentInfo = {
                .sTypeDefault(VkPresentInfoKHR),
                .swapchainCount = 1,
                .pSwapchains = &swapchainKhr,
                .pImageIndices = &acquiredImageIndex,
                .waitSemaphoreCount = 1,
                .pWaitSemaphores = &presentSemaphores[frameIndex]
        };
        CHECK_VK(vkQueuePresentKHR(cenote_get_main_queue(), &presentInfo));

        frameIndex = (frameIndex + 1U) % FRAME_COUNT;
    }
    ceDeviceWaitIdle();
}

static void cleanup()
{
    printf("Ending");

    ceDestroyDescriptorPool(descriptorPool);
    ceDestroyDescriptorSetLayout(descriptorSetLayout);

    for (uint32_t i = 0; i < FRAME_COUNT; i++)
    {
        ceDestroyCeBuffer(uniformBuffers[i]);
        ceDestroyFence(frameFences[i]);
        ceDestroySemaphore(imageSemaphores[i]);
        ceDestroySemaphore(presentSemaphores[i]);
    }



    ceDestroyCommandPool(commandPool);


    ceDestroyCeBuffer(voxelSSBO);

    ceDestroyCeSampledTexture(glowstoneTexture);
    ceDestroyCeSampledTexture(stickyTexture);
    ceDestroyCeSampledTexture(glassLimeTexture);
    ceDestroyCeSampledTexture(glassOrangeTexture);

    ceDestroyCeImage(depthImage);
    ceDestroyCeImage(colorImage);


    ceDestroyPipeline(pipeline);
    ceDestroyPipelineLayout(pipelineLayout);

    cenote_close();
    glfwDestroyWindow(window);
    glfwTerminate();
}

int main(void) {
    BEXIT(!caloginit(NULL), "caloginit failed");
    init();
    loop();
    cleanup();
    BEXIT(!calogclose(), "calogclose failed");
    return 0;
}