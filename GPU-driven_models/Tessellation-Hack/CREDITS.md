## Credits

Thanks to the authors of these libraries:

- [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/)
- [Vulkan Memory Allocator (VMA)](https://gpuopen.com/vulkan-memory-allocator/)
- [GLFW](https://www.glfw.org/)
- [Lightweight Java Game Library (LWJGL)](https://www.lwjgl.org/)
- [STB Image](https://github.com/nothings/stb)
