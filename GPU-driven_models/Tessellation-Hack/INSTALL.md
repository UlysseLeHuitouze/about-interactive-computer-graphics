# How to run

## Dependencies

The following dependencies are required to run the sample:

- [LWJGL core](https://www.lwjgl.org/customize)
- [LWJGL GLFW](https://www.lwjgl.org/customize)
- [LWJGL Vulkan](https://www.lwjgl.org/customize)
- [LWJGL Vulkan Memory Allocator](https://www.lwjgl.org/customize)
- [LWJGL stb](https://www.lwjgl.org/customize)
- [Vulkan SDK](https://www.lunarg.com/vulkan-sdk/)

## Hardware Requirements

You should have a GPU supporting the Vulkan 1.0 standard (See [here](https://vulkan.gpuinfo.org/)) and an up-to-date GPU driver.
The GPU should also support tessellation and geometry shaders. Don't expect the program to work otherwise.

## Main class

Main class is soberly called `Main.java` and is located in the root package, i.e. `src/`.

**WARNING**: the program won't run if you don't modify manually the requested GPU vendor and GPU name, respectively line 69 and line 70 in `Main.java`. The GPUs found by the API will be listed in the standard output, so you can adjust that way. You may as well want to turn off Vulkan logs by setting `com.xenon.vulkan.boostrap#VkDebug` `displayed_severities` field to 0.


## Extra Instructions

If following the above instructions does not lead to working code, you may want to disable implicit vulkan layers. 

To do so, you will want to set the environment variable `VK_LOADER_LAYERS_DISABLE` to `~implicit~` (See [Vulkan loader documentation](https://github.com/KhronosGroup/Vulkan-Loader/blob/main/docs/LoaderLayerInterface.md#layer-special-case-disable)).

For legacy purposes, here are three well-known problematic implicit layers and their respective environment variable for disabling them one by one:

- VK_MIRILLIS_LAYER: Set the environment variable `ENABLE_MIRILLIS_LAYER` to 1 (yes, you read right. It is "ENABLE") (tested)
- VK_LAYER_AMD_switchable_graphics: Set the environment variable `DISABLE_LAYER_AMD_SWITCHABLE_GRAPHICS_1` to 1 (tested)
- VK_LAYER_NV_optimus: Set the environment variable `DISABLE_LAYER_NV_OPTIMUS_1` to 1 (tested)
