/**
 * This sample is but a remnant from a Vulkan project in Java I had.
 * Eventually, I ended giving on Java entirely for that project
 * so don't expect good readibility (plus Vulkan is the incarnation of boilerplate).
 */
import com.xenon.vulkan.Frame;
import com.xenon.vulkan.SwapchainAndCo;
import com.xenon.vulkan.XeBuffer;
import com.xenon.vulkan.XeShaderModule;
import com.xenon.vulkan.abstraction.App;

import com.xenon.vulkan.abstraction.Debuggable;
import com.xenon.vulkan.boostrap.*;
import com.xenon.vulkan.info.GPUFeaturesCreateInfo;
import com.xenon.vulkan.info.QueueFeaturesCreateInfo;
import com.xenon.vulkan.info.SwapchainCreateInfo;
import com.xenon.vulkan.structs.VkGraphicsPipelineCreateInfos;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.file.Paths;
import java.util.Set;

import static com.xenon.vulkan.XeUtils.arr;
import static com.xenon.vulkan.XeUtils.checkVK;
import static com.xenon.vulkan.structs.VkGraphicsPipelineCreateInfos.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.vulkan.KHRSurface.VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
import static org.lwjgl.vulkan.KHRSwapchain.*;
import static org.lwjgl.vulkan.VK10.*;


/**
 * @author Ulysse Le Huitouze
 */
public class Main implements App, Debuggable {

    public static void main(String[] a) {
        new Main().run();
    }

    Xe10 xenon;
    long commandPool;
    Frame[] frames;
    XeBuffer vb;

    @Override
    public void init() {

        if (!glfwInit())
            throw new RuntimeException();

        VkWindow w = VkWindow.create(800, 500, "");

        try (MemoryStack stack = stackPush()) {
            VkApplicationInfo appInfo = VkApplicationInfo.calloc(stack)
                            .sType$Default()
                            .pApplicationName(stack.UTF8Safe("Hello Triangle"))
                            .applicationVersion(VK_MAKE_VERSION(1, 0, 0))
                            .pEngineName(stack.UTF8Safe("No Engine"))
                            .engineVersion(VK_MAKE_VERSION(1, 0, 0))
                            .apiVersion(VK_API_VERSION_1_0);

            VulkanBundle bundle = VulkanBundle.alloc();
            bundle.window = w;
            bundle.appInfo = appInfo;
            bundle.requestedLayers = Set.of("VK_LAYER_KHRONOS_validation");
            bundle.deviceExtensions = Set.of(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
            bundle.gpuFeatures = GPUFeaturesCreateInfo.calloc(stack).require_tessellationShader().require_geometryShader();
            bundle.queueFeatures = QueueFeaturesCreateInfo.recommended();
            bundle.GPUVendor = "nvidia";
            bundle.GPUName = "GTX 1650";
            bundle.swapchainCreateInfo = SwapchainCreateInfo.usual(
                    VK_FORMAT_B8G8R8A8_SRGB,
                    VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
                    1
            );
            bundle.settings = new VkSettings();
            bundle.allocatorCreateInfo = new VulkanMemoryAllocators.AllocatorCI(0,
                    0, null, 0, null);

            xenon = Xe10.light(bundle);
            VkPipelineLayoutCreateInfo ci = VkPipelineLayoutCreateInfo.calloc(stack)
                    .sType$Default();
            xenon.swapchainAndCo().pipelineLayout(0, xenon, ci);



            XeShaderModule[] modules = {XeShaderModule.createShaderModule(
                    Paths.get("./assets/shaders/vs.glsl.spv"),
                    XeShaderModule.Stage.VERTEX,
                    xenon.device(),
                    null,
                    0, 0,
                    null
            ), XeShaderModule.createShaderModule(
                    Paths.get("./assets/shaders/fs.glsl.spv"),
                    XeShaderModule.Stage.FRAGMENT,
                    xenon.device(),
                    null,
                    0, 0,
                    null
            ), XeShaderModule.createShaderModule(
                    Paths.get("./assets/shaders/tcs.glsl.spv"),
                    XeShaderModule.Stage.TESS_CTRL,
                    xenon.device(),
                    null, 0, 0, null
            ), XeShaderModule.createShaderModule(
                    Paths.get("./assets/shaders/tes.glsl.spv"),
                    XeShaderModule.Stage.TESS_EVAL,
                    xenon.device(),
                    null, 0, 0, null
            ), XeShaderModule.createShaderModule(
                    Paths.get("./assets/shaders/gs.glsl.spv"),
                    XeShaderModule.Stage.GEOMETRY,
                    xenon.device(),
                    null, 0, 0, null
            )};

            var g = VkGraphicsPipelineCreateInfos.fromModules(
                    stack, 0,
                    modules,
                    vis(
                            0,
                            arr(
                                    vibd(0, 2 * Float.BYTES, VK_VERTEX_INPUT_RATE_VERTEX)
                            ),
                            arr(
                                    viad(0, 0, VK_FORMAT_R32G32_SFLOAT, 0)
                            )
                    ),
                    ias(VK_PRIMITIVE_TOPOLOGY_PATCH_LIST, false),
                    ts(0, 1),
                    vps_d(0, 1, 1),
                    rs_u(0, false, false, VK_POLYGON_MODE_FILL,
                            VK_CULL_MODE_NONE, VK_FRONT_FACE_COUNTER_CLOCKWISE),
                    null,
                    null,
                    cbs(0, 0, false, 0,
                            new ColorBlendAttachmentStateCI[]{cbas_u()}, 0, 0, 0, 0),
                    ds(VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR),
                    xenon.swapchainAndCo().pipelineLayouts[0],
                    xenon.swapchainAndCo().renderpass(),
                    0,
                    0,
                    0
            );
            xenon.swapchainAndCo().pipeline(0, xenon, 0, g);

            for (var module : modules)
                module.dispose();

            commandPool = VkCommandPools.create(xenon.queues().familyIndices()[0],
                    VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT, xenon.device(), xenon.allocationCallbacks());

            frames = Frame.genFrames(2, xenon, new Frame.PerFrameData(
                    Frame.fenceData_usual(0, 0),
                            Frame.semaphoreData(0),
                            Frame.semaphoreData(0),
                            commandPool,
                            new int[] {VK_COMMAND_BUFFER_LEVEL_PRIMARY}
            ));



            float[] vertices = {
                    0, 0, 0.5f, 0.3f
            };

            int size = vertices.length * Float.BYTES;

            vb = xenon.createBuffer(Xe10.VERTEX_BUFF, size).bufferStorage(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
                    | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);


            ByteBuffer bb = vb.mapMemory(0, size);
            bb.asFloatBuffer().put(vertices);
            vb.unmapMemory();
        }
    }

    @Override
    public void loop() {
        int frameIndex = 0;
        final VkDevice device = xenon.device();
        while(xenon.window().live()) {
            glfwPollEvents();
            while (xenon.window().width == 0 || xenon.window().height == 0)
                glfwWaitEvents();

            Frame frame = frames[frameIndex];
            VkCommandBuffer buffer = frame.mainThreadCommandBuffers()[0];

            vkWaitForFences(device, frame.frame_fence(), true, -1);

            try (MemoryStack stack = stackPush()) {
                IntBuffer ib = stack.mallocInt(1);
                checkVK(vkAcquireNextImageKHR(device, xenon.swapchainAndCo().swapchain, -1,
                        frame.img_semaphore(), 0, ib), "failed acquire");
                vkResetFences(device, frame.frame_fence());
                vkResetCommandBuffer(buffer, 0);
                recordCommandBuffer(buffer, xenon.swapchainAndCo(), xenon.swapchainAndCo().pipelines[0],
                        xenon.swapchainAndCo().renderpass(), xenon.swapchainAndCo().framebuffers()[ib.get(0)]);
                VkSubmitInfo submitInfo = VkSubmitInfo.calloc(stack)
                        .sType$Default()
                        .waitSemaphoreCount(1)
                        .pWaitSemaphores(stack.longs(frame.img_semaphore()))
                        .pWaitDstStageMask(stack.ints(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT))
                        .pCommandBuffers(stack.pointers(buffer.address()))
                        .pSignalSemaphores(stack.longs(frame.present_semaphore()));

                checkVK(vkQueueSubmit(xenon.queues().queues()[0], submitInfo, frame.frame_fence()), "failed5");

                VkPresentInfoKHR presentInfo = VkPresentInfoKHR.calloc(stack)
                        .sType$Default()
                        .pWaitSemaphores(stack.longs(frame.present_semaphore()))
                        .pSwapchains(stack.longs(xenon.swapchainAndCo().swapchain))
                        .swapchainCount(1)
                        .pImageIndices(ib);

                checkVK(vkQueuePresentKHR(xenon.queues().queues()[0], presentInfo), "failed present");

            }



            frameIndex = (frameIndex + 1) & (frames.length - 1);
        }
        vkDeviceWaitIdle(device);
    }

    private void recordCommandBuffer(VkCommandBuffer buffer, SwapchainAndCo swapchainAndCo,
                                     long pipeline,
                                     long renderpass, long framebuffer) {
        try (MemoryStack stack = stackPush()) {
            VkCommandBufferBeginInfo beginInfo = VkCommandBufferBeginInfo.calloc(stack)
                    .sType$Default();
            checkVK(vkBeginCommandBuffer(buffer, beginInfo), "failed3");

            VkOffset2D offset2D = VkOffset2D.calloc(stack);
            int width = swapchainAndCo.extentWidth, height = swapchainAndCo.extentHeight;

            VkExtent2D extent2D = VkExtent2D.malloc(stack)
                    .set(width, height);

            VkClearValue.Buffer clearBuffer = VkClearValue.malloc(1, stack)
                    .color(VkClearColorValue.calloc(stack).float32(stack.floats(1, 1, 1, 1)));

            VkRenderPassBeginInfo passBeginInfo = VkRenderPassBeginInfo.calloc(stack)
                    .sType$Default()
                    .renderPass(renderpass)
                    .framebuffer(framebuffer)
                    .renderArea(a -> a.offset(offset2D)
                            .extent(extent2D))
                    .pClearValues(clearBuffer);

            vkCmdBeginRenderPass(buffer, passBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

            vkCmdBindPipeline(buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

            VkViewport.Buffer viewports = VkViewport.malloc(1, stack)
                    .x(0).y(0)
                    .width(width).height(height)
                    .minDepth(0).maxDepth(1);

            vkCmdSetViewport(buffer, 0, viewports);

            VkRect2D.Buffer scissors = VkRect2D.malloc(1, stack)
                    .offset(offset2D).extent(extent2D);
            vkCmdSetScissor(buffer, 0, scissors);

            vkCmdBindVertexBuffers(buffer, 0, stack.longs(vb.handle()), stack.longs(0));

            vkCmdDraw(buffer, 2, 1, 0, 0);

            vkCmdEndRenderPass(buffer);
            checkVK(vkEndCommandBuffer(buffer), "failed4");
        }
    }

    @Override
    public void dispose() {
        vb.dispose();
        for (Frame frame : frames)
            frame.dispose();
        VkCommandPools.dispose(commandPool, xenon);
        xenon.douse();
        glfwTerminate();
    }
}
