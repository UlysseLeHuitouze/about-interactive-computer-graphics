package com.xenon.vulkan;

import com.xenon.vulkan.abstraction.Disposable;
import com.xenon.vulkan.boostrap.VkError;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.nio.ByteBuffer;

import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.vulkan.VK10.*;
import static org.lwjgl.vulkan.VK10.vkGetPhysicalDeviceMemoryProperties;

/**
 * Abstraction for VkBuffers. The buffer can either be memory-mutable so that its memory can be reallocated anytime,
 * or be memory-immutable so that its memory can be reallocated.
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 * @see XeBufferMutable
 * @see XeBufferImmutable
 */
public interface XeBuffer extends Disposable {
    /**
     * @return the device this buffer was created with
     */
    VkDevice device();

    /**
     * @return the GPU this buffer was created with
     */
    VkPhysicalDevice physicalDevice();

    /**
     * @return the allocation callbacks this buffer was created with
     */
    VkAllocationCallbacks callbacks();

    /**
     * @return this buffer's handle
     */
    long handle();

    /**
     * @return this buffer allocated memory's handle
     */
    long memHandle();

    /**
     * Creates a mutable buffer with an allocated memory suitable for <code>requiredMemProps</code>.
     * @param requiredMemProps the required memory properties. a combination of <code>VkMemoryPropertyFlagBits</code>.
     * @return a mutable buffer with the memory allocated with the supplied parameter
     * @apiNote Offers similar mechanisms to OpenGL <code>glBufferData</code>
     */
    default XeBuffer bufferData(int requiredMemProps) {
        try (MemoryStack stack = stackPush()) {
            VkMemoryRequirements requirements = VkMemoryRequirements.malloc(stack);
            vkGetBufferMemoryRequirements(device(), handle(), requirements);

            VkPhysicalDeviceMemoryProperties props = VkPhysicalDeviceMemoryProperties.malloc(stack);
            vkGetPhysicalDeviceMemoryProperties(physicalDevice(), props);

            final int index;
            label:
            {
                int typeMask = requirements.memoryTypeBits();

                for (int i = 0; i < props.memoryTypeCount(); i++)
                    if ((typeMask & (1 << i)) != 0 &&
                            (props.memoryTypes().get(i).propertyFlags() & requiredMemProps) == requiredMemProps) {
                        index = i;
                        break label;
                    }
                throw VkError.format("Failed to find suitable GPU memory type for buffer " + handle());
            }

            return bufferData(0, requirements.size(), index);
        }
    }

    /**
     * Allocates GPU memory using the args for building a {@link VkMemoryAllocateInfo} struct,
     * and binds the memory to this buffer with 0 offset.
     * @param pNext pNext
     * @param size the malloc size. Do not directly use the size provided for buffer creation. <br>
     *             Must use
     *             {@link VK10#vkGetBufferMemoryRequirements(VkDevice, long, VkMemoryRequirements)} instead.
     * @param memTypeIndex the memory type index listed in {@link VkPhysicalDeviceMemoryProperties#memoryTypes()}
     * @return a mutable buffer with the allocated memory
     * @apiNote Offers similar mechanisms to OpenGL <code>glBufferData</code>
     */
    XeBuffer bufferData(long pNext, long size, int memTypeIndex);

    /**
     * Creates an immutable buffer with an allocated memory suitable for <code>requiredMemProps</code>.
     * @param requiredMemProps the required memory properties. a combination of <code>VkMemoryPropertyFlagBits</code>.
     * @return an immutable buffer with the memory allocated with the supplied parameter
     * @apiNote Offers similar mechanisms to OpenGL <code>glBufferStorage</code>
     */
    default XeBuffer bufferStorage(int requiredMemProps) {
        try (MemoryStack stack = stackPush()) {
            VkMemoryRequirements requirements = VkMemoryRequirements.malloc(stack);
            vkGetBufferMemoryRequirements(device(), handle(), requirements);

            VkPhysicalDeviceMemoryProperties props = VkPhysicalDeviceMemoryProperties.malloc(stack);
            vkGetPhysicalDeviceMemoryProperties(physicalDevice(), props);

            final int index;
            label:
            {
                int typeMask = requirements.memoryTypeBits();

                for (int i = 0; i < props.memoryTypeCount(); i++)
                    if ((typeMask & (1 << i)) != 0 &&
                            (props.memoryTypes().get(i).propertyFlags() & requiredMemProps) == requiredMemProps) {
                        index = i;
                        break label;
                    }
                throw VkError.format("Failed to find suitable GPU memory type for buffer " + handle());
            }

            return bufferData(0, requirements.size(), index);
        }
    }

    /**
     * Creates a buffer object with the memory allocated with the supplied parameters.<br>
     * Neither {@link #bufferData(long, long, int)} nor <code>bufferStorage()</code> can be called on the created buffer
     * since it's immutable.
     * GPU memory is allocated using the args for building a {@link VkMemoryAllocateInfo} struct,
     * and is thereafter bound to the buffer with 0 offset.
     * @param pNext pNext
     * @param size the malloc size. Do not directly use the size provided for buffer creation. <br>
     *             Must use
     *             {@link VK10#vkGetBufferMemoryRequirements(VkDevice, long, VkMemoryRequirements)} instead.
     * @param memTypeIndex the memory type index listed in {@link VkPhysicalDeviceMemoryProperties#memoryTypes()}
     * @return an immutable buffer with the allocated memory
     * @apiNote Offers similar mechanisms to OpenGL <code>glBufferStorage</code>
     * @see VkMemoryAllocateInfo
     */
    XeBuffer bufferStorage(long pNext, long size, int memTypeIndex);

    /**
     * Maps this buffer's GPU memory, returning a ByteBuffer
     * @param offset the offset in memory to start mapping
     * @param size the size of the mapping
     * @return a ByteBuffer representing the mapped chunk of memory. The ByteBuffer MUST NOT be manually freed.
     * {@link VK10#vkUnmapMemory(VkDevice, long)} will do it.
     */
    default ByteBuffer mapMemory(long offset, long size) {
        try (MemoryStack stack = stackPush()) {
            PointerBuffer mem_ptr = stack.mallocPointer(1); // for some reason, they can't return ByteBuffer directly
            vkMapMemory(device(), memHandle(), offset, size, 0, mem_ptr);
            return mem_ptr.getByteBuffer(0, (int) size);
        }
    }

    /**
     * Unmap all the chunks of this buffer's GPU memory
     */
    default void unmapMemory() {
        vkUnmapMemory(device(), memHandle());
    }

    /**
     * Frees this buffer's allocated memory.<br><br>
     * <h3>IMPORTANT</h3>
     * Implementations MUST automatically call this method in {@link #bufferData(long, long, int)} and
     * {@link #bufferStorage(long, long, int)} in order to free previously allocated memory.
     */
    default void freeMemory() {
        vkFreeMemory(device(), memHandle(), callbacks());
    }

    @Override
    default void dispose() {
        freeMemory();
        vkDestroyBuffer(device(), handle(), callbacks());
    }
}
