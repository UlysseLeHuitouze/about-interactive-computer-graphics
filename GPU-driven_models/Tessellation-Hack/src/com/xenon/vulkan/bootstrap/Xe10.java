package com.xenon.vulkan.boostrap;

import com.xenon.vulkan.*;
import com.xenon.vulkan.abstraction.BiIntConsumer;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.nio.LongBuffer;
import java.util.HashSet;

import static com.xenon.vulkan.XeUtils.checkVK;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.vulkan.KHRSurface.vkDestroySurfaceKHR;
import static org.lwjgl.vulkan.VK10.*;

/**
 * Supervise the process of Vulkan bootstrap-ing
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 * @see #light(VulkanBundle)
 */
public record Xe10(
        VkWindow window,
        VkInstance vkInstance,
        XeGPU gpu,
        VkDevice device,
        VkAllocationCallbacks allocationCallbacks,
        XeQueues queues,
        SwapchainAndCo swapchainAndCo,
        long debug_handle,
        long surface_handle,
        long allocator
) {

    public static final int TRANSFER_SRC_BUFF = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            TRANSFER_DST_BUFF = VK_BUFFER_USAGE_TRANSFER_DST_BIT,
            UNIFORM_TEXEL_BUFF = VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT,
            STORAGE_TEXEL_BUFF = VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT,
            UNIFORM_BUFF = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            STORAGE_BUFF = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
            INDEX_BUFF = VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            VERTEX_BUFF = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            INDIRECT_BUFF = VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;


    /**
     * Initializes Vulkan
     * @param bundle the container
     * @return a VulkanManager for this application
     */
    @Once
    public static Xe10 light(VulkanBundle bundle) {
        bundle.sanity1();
        MemoryStack stack = stackPush();

        bundle.requestedLayers = new HashSet<>(bundle.requestedLayers);
        VkPhysicalDevices.configureLayers(bundle);
        bundle.__debugCreateInfo = VkDebug.genDebugCreateInfo(stack);

        bundle.__instance = VkInstances.create(bundle);
        bundle.__debug = VkDebug.setupDebug(bundle);
        bundle.__gpu = VkPhysicalDevices.pick(bundle).validate(bundle);
        bundle.__surface = VkWindow.createSurface(bundle);
        bundle.__device = VkLogicalDevices.create(bundle);
        bundle.__swapchainAndCo = VkSwapchains.setup(bundle);
        bundle.__allocator = 1;//VulkanMemoryAllocators.create(bundle);

        stack.close();  // free bundle.debugCreateInfo
        bundle.sanity2();

        var xe = new Xe10(
                bundle.window,
                bundle.__instance,
                bundle.__gpu,
                bundle.__device,
                bundle.allocationCallbacks,
                bundle.queueFeatures.immutablecopy(),
                bundle.__swapchainAndCo,
                bundle.__debug,
                bundle.__surface,
                bundle.__allocator
        );
        bundle.window.setResizeCallback(xe::refreshPipeline);

        return xe;
    }

    /**
     * Refreshes the SwapchainAndCo object upon window resize.
     * Typically, make this function the resize callback of your GLFW window.
     * Rest assured, this is done automatically in {@link #light(VulkanBundle)} and can be reversed by simply
     * calling manually {@link VkWindow#setResizeCallback(BiIntConsumer)} thereafter with your custom function.
     * @param width the window's width
     * @param height the window's height
     */
    public void refreshPipeline(int width, int height) {
        if (width == 0 || height == 0)  return; // minimizing case

        vkDeviceWaitIdle(device);
        swapchainAndCo.cleanOutOfDate();
        swapchainAndCo.create(
                gpu.gpu(),
                queues,
                surface_handle,
                width,
                height
        );
    }







    // buffers

    /**
     * Macro for {@link #createBuffer(long, int, long, int, int, int...)}
     * with <code>pNext = 0, flags = 0, sharingMode = VK_SHARING_MODE_EXCLUSIVE</code>.
     * @param usage the buffer usage
     * @param size the size of the buffer
     * @return the newly created buffer
     */
    public XeBuffer createBuffer(int usage, long size) {
        return createBuffer(0, 0, size, usage, VK_SHARING_MODE_EXCLUSIVE);
    }

    /**
     * Creates a new memory-mutable XeBuffer using the args for building a {@link VkBufferCreateInfo} struct.
     * @param pNext pNext
     * @param flags flags
     * @param size the size of the buffer
     * @param usage the buffer usage
     * @param sharingMode sharingMode
     * @param familyIndices familyIndices, only useful if sharingMode is
     * {@link org.lwjgl.vulkan.VK10#VK_SHARING_MODE_CONCURRENT}
     * @return the newly created buffer
     * @see VkBufferCreateInfo
     */
    public XeBuffer createBuffer(long pNext, int flags, long size, int usage, int sharingMode,
                                      int... familyIndices) {
        try (MemoryStack stack = stackPush()) {
            VkBufferCreateInfo ci = VkBufferCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .flags(flags)
                    .size(size)
                    .usage(usage)
                    .sharingMode(sharingMode)
                    .pQueueFamilyIndices(familyIndices.length == 0 ? null : stack.ints(familyIndices));
            LongBuffer ptr = stack.mallocLong(1);
            checkVK(vkCreateBuffer(device, ci, allocationCallbacks, ptr), "Failed to create a buffer");
            return new XeBufferMutable(ptr.get(0), device, gpu.gpu(), allocationCallbacks);
        }
    }


    /**
     * Vulkan cleanup
     */
    @Once
    public void douse() {
        //vmaDestroyAllocator(allocator); TODO
        swapchainAndCo.dispose();
        vkDestroyDevice(device, allocationCallbacks);
        vkDestroySurfaceKHR(vkInstance, surface_handle, allocationCallbacks);
        VkDebug.dispose(this);
        vkDestroyInstance(vkInstance, allocationCallbacks);
        window.dispose();
    }

}
