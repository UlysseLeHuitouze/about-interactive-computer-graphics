package com.xenon.vulkan.boostrap;

import com.xenon.vulkan.XeGPU;
import com.xenon.vulkan.info.GPUFeaturesCreateInfo;
import com.xenon.vulkan.SwapchainAndCo;
import com.xenon.vulkan.info.QueueFeaturesCreateInfo;
import com.xenon.vulkan.info.SwapchainCreateInfo;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.nio.LongBuffer;
import java.util.Collection;
import java.util.function.IntToLongFunction;

import static com.xenon.vulkan.XeUtils.checkVK;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.vulkan.KHRSwapchain.VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
import static org.lwjgl.vulkan.VK10.*;

/**
 * Bundle containing all necessary fields to Vulkan initialization.
 * Essentially here to avoid methods with dozens of parameters.
 * {@link Xe10} tries to makes it as clear as possible regarding the internal state of the bundle,
 * during bootstrap, i.e. bootstrap functions will take a bundle as its one and only parameter
 * but shall not edit its content. Instead, they return a Vulkan object/handle like any factory and only then
 * {@link Xe10} will updated clearly the bundle with the returned value.
 * Regardless, should not be touch lightly, designed to be used internally.
 * <br>Part of the Vulkan bootstrap process supervised by {@link Xe10}.
 * @author Ulysse Le Huitouze
 * @apiNote Don't mess with this class, especially {@link #sanity1()} and {@link #sanity2()}. These methods
 * are only supposed to be called internally and will crash if not used correctly (or if the bundle isn't sane).
 * All public fields should be set! Otherwise, {@link #sanity1()} will not lose the occasion of reminding you.
 * An empty constructor was still provided to allow step-by-step feeding, but in order to be sure not to forget anything,
 * use {@link #alloc(VkWindow, VkApplicationInfo, VkAllocationCallbacks, Collection,
 * Collection, GPUFeaturesCreateInfo, QueueFeaturesCreateInfo, SwapchainCreateInfo,
 * IntToLongFunction, String, String, VkSettings, com.xenon.vulkan.boostrap.VulkanMemoryAllocators.AllocatorCI)}.
 * @since Xe 1.0
 */
public final class VulkanBundle {

    /**
     * @return an empty VulkanBundle for step-by-step initialization
     */
    public static VulkanBundle alloc() {
        return new VulkanBundle();
    }

    /**
     * Recommended constructors in order not to forget any required field.
     * @param window the glfw window
     * @param appInfo the appInfo
     * @param callbacks the allocation callbacks
     * @param requestedLayers the requested layers
     * @param deviceExtensions the requested device extensions
     * @param gpuFeaturesCreateInfo the requested gpu features
     * @param queueFeaturesCreateInfo the requested queue features
     * @param swapchainCreateInfo the info for swapchain creation
     * @param renderPassCreation a function for delayed render pass creation
     * @param GPUName the requested GPU's name
     * @param GPUVendor the requested GPU's vendor
     * @param settings the Vulkan global settings
     * @return a complete VulkanBundle so that you don't forget anything
     */
    public static VulkanBundle alloc(
            VkWindow window,
            VkApplicationInfo appInfo,
            @Nullable VkAllocationCallbacks callbacks,
            Collection<String> requestedLayers,
            Collection<String> deviceExtensions,
            GPUFeaturesCreateInfo gpuFeaturesCreateInfo,
            QueueFeaturesCreateInfo queueFeaturesCreateInfo,
            SwapchainCreateInfo swapchainCreateInfo,
            @Nullable IntToLongFunction renderPassCreation,
            String GPUName,
            String GPUVendor,
            VkSettings settings,
            VulkanMemoryAllocators.AllocatorCI allocatorCreateInfo
    ) {
        return new VulkanBundle(
                window,
                appInfo,
                callbacks,
                requestedLayers,
                deviceExtensions,
                gpuFeaturesCreateInfo,
                queueFeaturesCreateInfo,
                swapchainCreateInfo,
                renderPassCreation,
                GPUName,
                GPUVendor,
                settings,
                allocatorCreateInfo
        );
    }

    // API external fields
    public VkWindow window;
    public VkApplicationInfo appInfo;
    @Nullable
    public VkAllocationCallbacks allocationCallbacks;
    public Collection<String> requestedLayers;
    public Collection<String> deviceExtensions;
    public GPUFeaturesCreateInfo gpuFeatures;
    public QueueFeaturesCreateInfo queueFeatures;
    public SwapchainCreateInfo swapchainCreateInfo;
    @Nullable
    public IntToLongFunction renderPassCreation;
    public String GPUName;
    public String GPUVendor;
    public VkSettings settings;
    public VulkanMemoryAllocators.AllocatorCI allocatorCreateInfo;

    // API internal fields
    VkDebugUtilsMessengerCreateInfoEXT __debugCreateInfo;
    VkInstance __instance;
    long __debug;
    XeGPU __gpu;
    long __surface;
    VkDevice __device;
    SwapchainAndCo __swapchainAndCo;
    long __allocator;

    private VulkanBundle() {}
    private VulkanBundle(
            VkWindow window,
            VkApplicationInfo appInfo,
            @Nullable VkAllocationCallbacks callbacks,
            Collection<String> requestedLayers,
            Collection<String> deviceExtensions,
            GPUFeaturesCreateInfo gpuFeaturesCreateInfo,
            QueueFeaturesCreateInfo queueFeaturesCreateInfo,
            SwapchainCreateInfo swapchainCreateInfo,
            @Nullable IntToLongFunction renderPassCreation,
            String GPUName,
            String GPUVendor,
            VkSettings settings,
            VulkanMemoryAllocators.AllocatorCI allocatorCreateInfo
    ) {
        this.window = window;
        this.appInfo = appInfo;
        allocationCallbacks = callbacks;
        this.requestedLayers = requestedLayers;
        this.deviceExtensions = deviceExtensions;
        gpuFeatures = gpuFeaturesCreateInfo;
        queueFeatures = queueFeaturesCreateInfo;
        this.swapchainCreateInfo = swapchainCreateInfo;
        this.renderPassCreation = renderPassCreation;
        this.GPUName = GPUName;
        this.GPUVendor = GPUVendor;
        this.settings = settings;
        this.allocatorCreateInfo = allocatorCreateInfo;
    }

    // API public fields checking
    /**
     * this method is designed to be used internally, do not call it randomly.
     * If any of its fields is null, throw a RuntimeException
     */
    public void sanity1() {
        if (
                window == null ||
                appInfo == null ||
                requestedLayers == null ||
                deviceExtensions == null ||
                gpuFeatures == null ||
                queueFeatures == null ||
                swapchainCreateInfo == null ||
                GPUName == null ||
                GPUVendor == null ||
                settings == null ||
                allocatorCreateInfo == null
        )
            throw VkError.log("VulkanedBundle has got some missing external fields: " + toString1());

        if (renderPassCreation == null && !settings.useDynamicRendering)    // default render pass creation
            renderPassCreation = imgFormat -> {
            try (MemoryStack stack = stackPush()) {
                VkAttachmentDescription.Buffer colorAttachment = VkAttachmentDescription.malloc(1, stack)
                        .flags(0)
                        .format(imgFormat)
                        .samples(VK_SAMPLE_COUNT_1_BIT)
                        .loadOp(VK_ATTACHMENT_LOAD_OP_CLEAR)
                        .storeOp(VK_ATTACHMENT_STORE_OP_STORE)
                        .stencilLoadOp(VK_ATTACHMENT_LOAD_OP_DONT_CARE)
                        .stencilStoreOp(VK_ATTACHMENT_STORE_OP_DONT_CARE)
                        .initialLayout(VK_IMAGE_LAYOUT_UNDEFINED)
                        .finalLayout(VK_IMAGE_LAYOUT_PRESENT_SRC_KHR);

                VkAttachmentReference.Buffer colorAttachmentRef = VkAttachmentReference.malloc(1, stack)
                        .attachment(0)
                        .layout(VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL);

                VkSubpassDescription.Buffer subpass = VkSubpassDescription.calloc(1, stack)
                        .pipelineBindPoint(VK_PIPELINE_BIND_POINT_GRAPHICS)
                        .colorAttachmentCount(1)
                        .pColorAttachments(colorAttachmentRef);

                VkSubpassDependency.Buffer dependency = VkSubpassDependency.calloc(1, stack)
                        .srcSubpass(VK_SUBPASS_EXTERNAL)
                        .dstSubpass(0)
                        .srcStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
                        .srcAccessMask(0)
                        .dstStageMask(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT)
                        .dstAccessMask(VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT);

                VkRenderPassCreateInfo renderPassInfo = VkRenderPassCreateInfo.calloc(stack)
                        .sType$Default()
                        .pAttachments(colorAttachment)
                        .pSubpasses(subpass)
                        .pDependencies(dependency);

                LongBuffer pRenderPass = stack.mallocLong(1);

                checkVK(vkCreateRenderPass(__device, renderPassInfo, null, pRenderPass),
                        "Failed to create render pass");

                return pRenderPass.get(0);
            }
        };
    }

    // API internal fields checking

    /**
     * this method is designed to be used internally, do not call it randomly.
     * if {@link Xe10} is used well, this method shouldn't do anything.
     */
    public void sanity2() {
        if (
                __debugCreateInfo == null ||
                __instance == null ||
                __debug == VK_NULL_HANDLE ||
                __gpu == null ||
                __surface == VK_NULL_HANDLE ||
                __device == null ||
                __swapchainAndCo == null ||
                __allocator == VK_NULL_HANDLE

        )
            throw VkError.log("[API Internal error]VulkanedBundle has got some missing internal fields" + this);
    }

    public String toString1() {
        return "VulkanBundle{" +
                "window=" + window +
                ", appInfo=" + appInfo +
                ", allocationCallbacks=" + allocationCallbacks +
                ", requestedLayers=" + requestedLayers +
                ", deviceExtensions=" + deviceExtensions +
                ", gpuFeatures=" + gpuFeatures +
                ", queueFeatures=" + queueFeatures +
                ", swapchainCreateInfo=" + swapchainCreateInfo +
                ", GPUName='" + GPUName + '\'' +
                ", GPUVendor='" + GPUVendor + '\'' +
                ", settings='" + settings + '\'' +
                '}';
    }

    @Override
    public String toString() {
        return "VulkanBundle{" +
                "window=" + window +
                ", appInfo=" + appInfo +
                ", allocationCallbacks=" + allocationCallbacks +
                ", requestedLayers=" + requestedLayers +
                ", deviceExtensions=" + deviceExtensions +
                ", gpuFeatures=" + gpuFeatures +
                ", queueFeatures=" + queueFeatures +
                ", swapchainCreateInfo=" + swapchainCreateInfo +
                ", GPUName='" + GPUName + '\'' +
                ", GPUVendor='" + GPUVendor + '\'' +
                ", settings='" + settings + '\'' +
                ", debugCreateInfo=" + __debugCreateInfo +
                ", instance=" + __instance +
                ", debug=" + __debug +
                ", gpu=" + __gpu +
                ", surface=" + __surface +
                ", device=" + __device +
                ", pipelineInfo=" + __swapchainAndCo +
                '}';
    }
}
