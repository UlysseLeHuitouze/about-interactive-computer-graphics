package com.xenon.vulkan.boostrap;

import com.xenon.vulkan.abstraction.Builder;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.util.vma.*;

import static com.xenon.vulkan.XeUtils.checkVK;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.util.vma.Vma.vmaCreateAllocator;

/**
 * Factory for VulkanMemoryAllocator.
 * <br>Part of the Vulkan bootstrap process supervised by {@link Xe10}.
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 */
public class VulkanMemoryAllocators {

    /**
     * Creates a new memory allocator
     * @param bundle the bundle
     * @return a handle to the freshly created allocator
     */
    public static long create(VulkanBundle bundle) {
        try (MemoryStack stack = stackPush()) {
            VmaAllocatorCreateInfo ci = bundle.allocatorCreateInfo.build(stack)
                    .physicalDevice(bundle.__gpu.gpu())
                    .device(bundle.__device)
                    .pAllocationCallbacks(bundle.allocationCallbacks)
                    .pVulkanFunctions(VmaVulkanFunctions.calloc(stack).set(bundle.__instance, bundle.__device))
                    .instance(bundle.__instance)
                    .vulkanApiVersion(bundle.appInfo.apiVersion());
            PointerBuffer ptr = stack.mallocPointer(1);
            checkVK(vmaCreateAllocator(ci, ptr), "Failed to create Vulkan Memory Allocator");
            return ptr.get(0);
        }
    }

    public record AllocatorCI(
            int flags,
            long preferredLargeHeapBlockSize,
            VmaDeviceMemoryCallbacks pDeviceMemoryCallbacks,
            long heapSizeLimit,
            VmaVulkanFunctions pVulkanFunctions,
            int... typeExternalMemoryHandleTypes) implements Builder<VmaAllocatorCreateInfo> {

        @Override
        public VmaAllocatorCreateInfo build(MemoryStack stack) {
            return VmaAllocatorCreateInfo.malloc(stack)
                    .flags(flags)
                    .preferredLargeHeapBlockSize(preferredLargeHeapBlockSize)
                    .pDeviceMemoryCallbacks(pDeviceMemoryCallbacks)
                    .pHeapSizeLimit(stack.longs(heapSizeLimit))
                    .pVulkanFunctions(pVulkanFunctions)
                    .pTypeExternalMemoryHandleTypes(stack.ints(typeExternalMemoryHandleTypes));
        }
    }

}
