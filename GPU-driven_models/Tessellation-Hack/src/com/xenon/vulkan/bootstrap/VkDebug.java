package com.xenon.vulkan.boostrap;

import org.lwjgl.system.Configuration;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.io.PrintStream;
import java.nio.LongBuffer;

import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;
import static org.lwjgl.vulkan.EXTDebugUtils.*;
import static org.lwjgl.vulkan.VK10.*;

/**
 * Factory for Vulkan's debug messenger.
 * <br>Part of the Vulkan bootstrap process supervised by {@link Xe10}.
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 * @see #setupDebug(VulkanBundle)
 */
public final class VkDebug {


    /**
     * All severities are enabled in order to see crashes, however you can prevent some severities
     * from showing in the console using this field.
     */
    public static int displayed_severities = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;

    /**
     * whether we are in development. Essentially
     * <code>org.lwjgl.system.Configuration.DEBUG.get(true)</code>.
     */
    public static final boolean ENABLED = Configuration.DEBUG.get(true);


    /**
     * Creates a new debugCreateInfo structure on the stack
     * @param stack the stack
     * @return the new debugCreateInfo structure
     */
    public static VkDebugUtilsMessengerCreateInfoEXT genDebugCreateInfo(MemoryStack stack) {
        return VkDebugUtilsMessengerCreateInfoEXT.calloc(stack)
                .sType$Default()
                .messageSeverity(VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                        VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
                        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
                .messageType(VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                        VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                        VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT)
                .pfnUserCallback((severity, type, pCallback, pUser) -> {
                    if ((VkDebug.displayed_severities & severity) != 0) {
                        @SuppressWarnings("resource")   // EXTREMELY IMPORTANT, DO NOT FREE msg!!!! heap corruption risk
                        VkDebugUtilsMessengerCallbackDataEXT msg = VkDebugUtilsMessengerCallbackDataEXT.create(pCallback);
                        String s = switch (severity) {
                            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT -> "[VERBOSE]";
                            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT -> "[INFO]";
                            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT -> "[WARNING]";
                            case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT -> "[ERROR]";
                            default -> throw VkError.impossible();
                        } +
                                " msg type: " + switch (type) {
                            case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT -> "GENERAL";
                            case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT -> "VALIDATION";
                            case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT -> "PERFORMANCE";
                            default -> throw VkError.impossible();
                        } +
                                " details: ";
                        PrintStream ps = severity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT ? System.err : System.out;
                        ps.println(s + msg.pMessageString());
                    }
                    return VK_FALSE;
                });
    }

    /**
     *
     * @param bundle the bundle
     * @return a handle for the newly created DebugUtilsMessengerEXT
     */
    @Once
    public static long setupDebug(VulkanBundle bundle) {
        VkInstance instance = bundle.__instance;
        if (ENABLED) try (MemoryStack stack = stackPush()) {
            LongBuffer pDebugMessenger = stack.longs(VK_NULL_HANDLE);

            if (vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT") == NULL ||
                    vkCreateDebugUtilsMessengerEXT(
                            instance,
                            bundle.__debugCreateInfo,
                            bundle.allocationCallbacks,
                            pDebugMessenger
                    ) != VK_SUCCESS)
                throw VkError.log("Failed to set up debug messenger");

            return pDebugMessenger.get(0);
        }
        return VK_NULL_HANDLE;
    }

    /**
     * Dispose of VkDebugMessenger
     */
    @Once
    public static void dispose(Xe10 xenon) {
        if (ENABLED && vkGetInstanceProcAddr(xenon.vkInstance(), "vkDestroyDebugUtilsMessengerEXT") != NULL)
            vkDestroyDebugUtilsMessengerEXT(xenon.vkInstance(), xenon.debug_handle(), xenon.allocationCallbacks());
    }

}
