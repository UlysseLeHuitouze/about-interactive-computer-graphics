package com.xenon.vulkan.boostrap;

import com.xenon.vulkan.Vulkan;

/**
 * Global Vulkan settings.
 * Clean class without special constructors, fields or methods for easy json serialization.
 * <br>Part of the Vulkan bootstrap process supervised by {@link Xe10}.
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 */
public class VkSettings {

    @Vulkan("Requires 1.3")
    public boolean useDynamicRendering = false;

}
