package com.xenon.vulkan.boostrap;

import com.xenon.vulkan.XeGPU;
import com.xenon.vulkan.XeUtils;
import com.xenon.vulkan.info.GPUFeaturesCreateInfo;
import com.xenon.vulkan.info.QueueFeaturesCreateInfo;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.nio.IntBuffer;
import java.util.*;

import static com.xenon.vulkan.XeUtils.checkVK;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.vulkan.KHRSurface.vkGetPhysicalDeviceSurfaceSupportKHR;
import static org.lwjgl.vulkan.VK10.*;

/**
 * Factory for {@link VkDevice}.
 * <br>Part of the Vulkan bootstrap process supervised by {@link Xe10}.
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 * @see #create(VulkanBundle)
 */
public final class VkLogicalDevices {


    /**
     * Creates a VkDevice with the supplied parameters
     * @param bundle the bundle
     * @return a new VkDevice
     */
    @SuppressWarnings("resource")
    @Once
    public static VkDevice create(VulkanBundle bundle) {
        XeGPU gpu = bundle.__gpu;
        VkPhysicalDevice physicalDevice = gpu.gpu();
        QueueFeaturesCreateInfo filteringQueueFamilyFeatures = bundle.queueFeatures;
        GPUFeaturesCreateInfo gpuFeatures = bundle.gpuFeatures;
        Collection<String> layers = bundle.requestedLayers;
        Collection<String> deviceExtensions = bundle.deviceExtensions;
        VkAllocationCallbacks callbacks = bundle.allocationCallbacks;
        long surface_handle = bundle.__surface;

        try (MemoryStack stack = stackPush()) {
            // queue families filtering
            IntBuffer countB = stack.mallocInt(1);
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, countB, null);
            VkQueueFamilyProperties.Buffer props = VkQueueFamilyProperties.calloc(countB.get(0), stack);
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, countB, props);

            // extensive queue family searching

            int[] uniqueIndices;
            {
                IntBuffer surface_present = stack.ints(VK_FALSE);
                int i = 0;

                boolean[] callbacksSuccess = new boolean[filteringQueueFamilyFeatures.capacity()];

                Iterator<VkQueueFamilyProperties> it = props.stream().iterator();
                while (it.hasNext() && someFalse(callbacksSuccess)) {
                    // don't ever put it in a try-with-resources!! it's owned by the MemoryStack.
                    VkQueueFamilyProperties prop = it.next();
                    vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface_handle, surface_present);

                    int j = 0;
                    for (var func : filteringQueueFamilyFeatures) {
                        if (!callbacksSuccess[j] &&
                                (callbacksSuccess[j] = func.test(prop, surface_present.get(0) == VK_TRUE))) {
                            filteringQueueFamilyFeatures.family_index(j, i);
                        }
                        j++;
                    }

                    i++;
                }
                if (someFalse(callbacksSuccess))
                    throw VkError.format("Selected GPU %s does not offer required queues", gpu.name());
                uniqueIndices = filteringQueueFamilyFeatures.uniqueFamilyIndices();
            }


            VkDeviceQueueCreateInfo.Buffer queueCreateInfos = VkDeviceQueueCreateInfo.calloc(uniqueIndices.length, stack);

            for (int i = 0; i < uniqueIndices.length; i++) {
                queueCreateInfos.get(i)
                        .sType$Default()
                        .queueFamilyIndex(uniqueIndices[i])
                        .pQueuePriorities(stack.floats(filteringQueueFamilyFeatures.priority(i)));
            }



            VkDeviceCreateInfo createInfo = VkDeviceCreateInfo.calloc(stack)
                    .sType$Default()
                    .pQueueCreateInfos(queueCreateInfos)
                    .pEnabledFeatures(gpuFeatures.underlying())
                    .ppEnabledExtensionNames(XeUtils.pointerbuffer(stack, deviceExtensions))
            // layers again
                    .ppEnabledLayerNames(XeUtils.pointerbuffer(stack, layers));

            PointerBuffer pDevice = stack.pointers(VK_NULL_HANDLE);
            checkVK(vkCreateDevice(physicalDevice, createInfo, callbacks, pDevice), "Failed to create logical device");

            VkDevice device = new VkDevice(pDevice.get(0), physicalDevice, createInfo);

            PointerBuffer pQueue = stack.pointers(VK_NULL_HANDLE);
            for (int i = 0; i < filteringQueueFamilyFeatures.capacity(); i++) {
                vkGetDeviceQueue(device, filteringQueueFamilyFeatures.family_index(i), 0, pQueue);
                filteringQueueFamilyFeatures.result(i, new VkQueue(pQueue.get(0), device));
            }
            return device;
        }
    }

    private static boolean someFalse(boolean[] bs) {
        for (var b : bs)
            if (!b)
                return true;
        return false;
    }

}