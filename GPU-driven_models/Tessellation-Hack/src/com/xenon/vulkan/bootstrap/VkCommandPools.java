package com.xenon.vulkan.boostrap;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkAllocationCallbacks;
import org.lwjgl.vulkan.VkCommandPoolCreateInfo;
import org.lwjgl.vulkan.VkDevice;

import java.nio.LongBuffer;

import static com.xenon.vulkan.XeUtils.checkVK;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.vulkan.VK10.vkCreateCommandPool;
import static org.lwjgl.vulkan.VK10.vkDestroyCommandPool;

/**
 * Factory for Vulkan's command pools.
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 * @see #create(int, int, VkDevice, VkAllocationCallbacks)
 */
public final class VkCommandPools {

    /**
     * Creates a new command pool using the given parameters
     * @param queueFamilyIndex the queue family index (usually the graphic queue family index)
     * @param flags the flags used in {@link VkCommandPoolCreateInfo}
     * @param device the device
     * @param callbacks the callbacks
     * @return the handle
     */
    public static long create(int queueFamilyIndex, int flags, VkDevice device, VkAllocationCallbacks callbacks) {
        try (MemoryStack stack = stackPush()) {
            LongBuffer lb = stack.mallocLong(1);
            VkCommandPoolCreateInfo createInfo = VkCommandPoolCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(0)
                    .flags(flags)
                    .queueFamilyIndex(queueFamilyIndex);
            checkVK(vkCreateCommandPool(device, createInfo, callbacks, lb), "Failed to create a command pool");
            return lb.get(0);
        }
    }

    /**
     * macro for quick disposal of a command pool
     * @param commandPool the handle
     * @param xe the VulkanManager
     */
    public static void dispose(long commandPool, Xe10 xe) {
        vkDestroyCommandPool(xe.device(), commandPool, xe.allocationCallbacks());
    }

}
