package com.xenon.vulkan;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkAllocationCallbacks;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkMemoryAllocateInfo;
import org.lwjgl.vulkan.VkPhysicalDevice;

import java.nio.LongBuffer;

import static com.xenon.vulkan.XeUtils.checkVK;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.vulkan.VK10.vkAllocateMemory;
import static org.lwjgl.vulkan.VK10.vkBindBufferMemory;

/**
 * A Vulkan memory-mutable buffer. Its memory can be reallocated, using either <code>bufferData</code>
 * or <code>bufferStorage</code> for creating respectively another mutable buffer or an immutable one.
 * Offers similar mechanisms to OpenGL <code>glBufferData</code>.
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 */
public class XeBufferMutable implements XeBuffer{

    private final VkDevice device;
    private final VkPhysicalDevice physicalDevice;
    private final VkAllocationCallbacks callbacks;
    private final long handle;
    public long memHandle;

    public XeBufferMutable(long handle, VkDevice device, VkPhysicalDevice physicalDevice,
                           VkAllocationCallbacks callbacks) {
        this.handle = handle;
        this.device = device;
        this.physicalDevice = physicalDevice;
        this.callbacks = callbacks;
    }

    @Override
    public VkDevice device() {
        return device;
    }

    @Override
    public VkPhysicalDevice physicalDevice() {
        return physicalDevice;
    }

    @Override
    public VkAllocationCallbacks callbacks() {
        return callbacks;
    }

    @Override
    public long handle() {
        return handle;
    }

    @Override
    public long memHandle() {
        return memHandle;
    }

    @Override
    public XeBuffer bufferData(long pNext, long size, int memTypeIndex) {
        freeMemory();
        try (MemoryStack stack = stackPush()) {
            VkMemoryAllocateInfo ai = VkMemoryAllocateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .allocationSize(size)
                    .memoryTypeIndex(memTypeIndex);
            LongBuffer ptr = stack.mallocLong(1);
            checkVK(vkAllocateMemory(device, ai, callbacks, ptr), "Failed to allocate GPU memory");
            memHandle = ptr.get(0);
            checkVK(vkBindBufferMemory(device, handle, memHandle, 0),
                    "Failed to bind allocated memory to buffer");

        }
        return this;
    }

    @Override
    public XeBuffer bufferStorage(long pNext, long size, int memTypeIndex) {
        freeMemory();
        try (MemoryStack stack = stackPush()) {
            VkMemoryAllocateInfo ai = VkMemoryAllocateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .allocationSize(size)
                    .memoryTypeIndex(memTypeIndex);
            LongBuffer ptr = stack.mallocLong(1);
            checkVK(vkAllocateMemory(device, ai, callbacks, ptr), "Failed to allocate GPU memory");
            checkVK(vkBindBufferMemory(device, handle, ptr.get(0), 0),
                    "Failed to bind allocated memory to buffer");
            return new XeBufferImmutable(handle, ptr.get(0), device, physicalDevice, callbacks);
        }
    }
}
