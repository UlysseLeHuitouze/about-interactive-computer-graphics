package com.xenon.vulkan.info;

import com.xenon.vulkan.boostrap.Xe10;

import static org.lwjgl.vulkan.KHRSurface.VK_PRESENT_MODE_MAILBOX_KHR;
import static org.lwjgl.vulkan.VK10.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

/**
 * Holds the necessary information to the creation of a swapchain object.
 * <br>Part of the Vulkan bootstrap process supervised by {@link Xe10}.
 * @author Ulysse Le Huitouze
 * @apiNote designed to be api-internal, though there is no harm in using it outside for it is immutable
 * @since Xe 1.0
 */
public record SwapchainCreateInfo(int preferredSurfaceFormat, int preferredSurfaceColorSpace, int preferredPresentMode,
                                  int additionalImageCount, int imageUsage, int pipelineCount) {

    public static SwapchainCreateInfo usual(int preferredSurfaceFormat, int preferredSurfaceColorSpace, int pipelineCount) {
        return new SwapchainCreateInfo(
                preferredSurfaceFormat,
                preferredSurfaceColorSpace,
                VK_PRESENT_MODE_MAILBOX_KHR,
                1,
                VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                pipelineCount
        );
    }

}
