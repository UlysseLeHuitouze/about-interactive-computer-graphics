package com.xenon.vulkan.info;

import com.xenon.vulkan.boostrap.Xe10;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkPhysicalDeviceFeatures;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Java wrapper for {@link VkPhysicalDeviceFeatures}.
 * Hardcoded all available features listed in {@link VkPhysicalDeviceFeatures}.
 * Java reflection could have prevented writing 55 different functions, but I don't know anymore whether it's allowed
 * by the JVM.
 * <br>Part of the Vulkan bootstrap process supervised by {@link Xe10}.
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 */
public class GPUFeaturesCreateInfo {


    /**
     * Calloc a new GPUFeatures with its underlying VkPhysicalDeviceFeatures object on the stack.
     * GPUFeatures created using this method mustn't be freed manually, i.e. don't ever call {@link #free()}.
     * @param stack the stack
     * @return a new GPUFeatures object
     */
    public static GPUFeaturesCreateInfo calloc(MemoryStack stack) {
        return new GPUFeaturesCreateInfo(VkPhysicalDeviceFeatures.calloc(stack));
    }

    /**
     * Calloc a new GPUFeatures with its underlying VkPhysicalDeviceFeatures object.
     * GPUFeatures created using this method must be freed manually, by calling {@link #free()}.
     * @return a new GPUFeatures object
     */
    public static GPUFeaturesCreateInfo calloc() {
        return new GPUFeaturesCreateInfo(VkPhysicalDeviceFeatures.calloc());
    }

    /*All due checks to validate a VkPhysicalDevice*/
    protected final List<Function<VkPhysicalDeviceFeatures, Boolean>> checks = new ArrayList<>();
    /*underlying device features, used when creating a logical device*/
    protected final VkPhysicalDeviceFeatures underlyingFeatures;


    /**
     * Allows custom allocation for the features
     * @param features the GPU features
     * @see #calloc()
     * @see #calloc(MemoryStack)
     */
    protected GPUFeaturesCreateInfo(VkPhysicalDeviceFeatures features) {
        underlyingFeatures = features;
    }

    /**
     * @return the underlying VkPhysicalDeviceFeatures object
     */
    public VkPhysicalDeviceFeatures underlying() {
        return underlyingFeatures;
    }

    /**
     * Validate the selected device features according to the application needs (set with the methods in this class).
     * @param features the selected device features
     */
    public void validateDeviceFeatures(VkPhysicalDeviceFeatures features) {
        for (Function<VkPhysicalDeviceFeatures, Boolean> f : checks)
            if (!f.apply(features))
                throw new AssertionError("Selected GPU didn't fulfill all the requirements.");
    }

    /**
     * Free the underlying VkPhysicalDeviceFeatures object
     */
    public void free() {
        underlyingFeatures.free();
    }


    public GPUFeaturesCreateInfo require_robustBufferAccess() {
        underlyingFeatures.robustBufferAccess(true);
        checks.add(VkPhysicalDeviceFeatures::robustBufferAccess);
        return this;
    }
    public GPUFeaturesCreateInfo require_fullDrawIndexUint32() {
        underlyingFeatures.fullDrawIndexUint32(true);
        checks.add(VkPhysicalDeviceFeatures::fullDrawIndexUint32);
        return this;
    }
    public GPUFeaturesCreateInfo require_imageCubeArray() {
        underlyingFeatures.imageCubeArray(true);
        checks.add(VkPhysicalDeviceFeatures::imageCubeArray);
        return this;
    }
    public GPUFeaturesCreateInfo require_independentBlend() {
        underlyingFeatures.independentBlend(true);
        checks.add(VkPhysicalDeviceFeatures::independentBlend);
        return this;
    }
    public GPUFeaturesCreateInfo require_geometryShader() {
        underlyingFeatures.geometryShader(true);
        checks.add(VkPhysicalDeviceFeatures::geometryShader);
        return this;
    }
    public GPUFeaturesCreateInfo require_tessellationShader() {
        underlyingFeatures.tessellationShader(true);
        checks.add(VkPhysicalDeviceFeatures::tessellationShader);
        return this;
    }
    public GPUFeaturesCreateInfo require_sampleRateShading() {
        underlyingFeatures.sampleRateShading(true);
        checks.add(VkPhysicalDeviceFeatures::sampleRateShading);
        return this;
    }
    public GPUFeaturesCreateInfo require_dualSrcBlend() {
        underlyingFeatures.dualSrcBlend(true);
        checks.add(VkPhysicalDeviceFeatures::dualSrcBlend);
        return this;
    }
    public GPUFeaturesCreateInfo require_logicOp() {
        underlyingFeatures.logicOp(true);
        checks.add(VkPhysicalDeviceFeatures::logicOp);
        return this;
    }
    public GPUFeaturesCreateInfo require_multiDrawIndirect() {
        underlyingFeatures.multiDrawIndirect(true);
        checks.add(VkPhysicalDeviceFeatures::multiDrawIndirect);
        return this;
    }
    public GPUFeaturesCreateInfo require_drawIndirectFirstInstance() {
        underlyingFeatures.drawIndirectFirstInstance(true);
        checks.add(VkPhysicalDeviceFeatures::drawIndirectFirstInstance);
        return this;
    }
    public GPUFeaturesCreateInfo require_depthClamp() {
        underlyingFeatures.depthClamp(true);
        checks.add(VkPhysicalDeviceFeatures::depthClamp);
        return this;
    }
    public GPUFeaturesCreateInfo require_depthBiasClamp() {
        underlyingFeatures.depthBiasClamp(true);
        checks.add(VkPhysicalDeviceFeatures::depthBiasClamp);
        return this;
    }
    public GPUFeaturesCreateInfo require_fillModeNonSolid() {
        underlyingFeatures.fillModeNonSolid(true);
        checks.add(VkPhysicalDeviceFeatures::fillModeNonSolid);
        return this;
    }
    public GPUFeaturesCreateInfo require_depthBounds() {
        underlyingFeatures.depthBounds(true);
        checks.add(VkPhysicalDeviceFeatures::depthBounds);
        return this;
    }
    public GPUFeaturesCreateInfo require_wideLines() {
        underlyingFeatures.wideLines(true);
        checks.add(VkPhysicalDeviceFeatures::wideLines);
        return this;
    }
    public GPUFeaturesCreateInfo require_largePoints() {
        underlyingFeatures.largePoints(true);
        checks.add(VkPhysicalDeviceFeatures::largePoints);
        return this;
    }
    public GPUFeaturesCreateInfo require_alphaToOne() {
        underlyingFeatures.alphaToOne(true);
        checks.add(VkPhysicalDeviceFeatures::alphaToOne);
        return this;
    }
    public GPUFeaturesCreateInfo require_multiViewport() {
        underlyingFeatures.multiViewport(true);
        checks.add(VkPhysicalDeviceFeatures::multiViewport);
        return this;
    }
    public GPUFeaturesCreateInfo require_samplerAnisotropy() {
        underlyingFeatures.samplerAnisotropy(true);
        checks.add(VkPhysicalDeviceFeatures::samplerAnisotropy);
        return this;
    }
    public GPUFeaturesCreateInfo require_textureCompressionETC2() {
        underlyingFeatures.textureCompressionETC2(true);
        checks.add(VkPhysicalDeviceFeatures::textureCompressionETC2);
        return this;
    }
    public GPUFeaturesCreateInfo require_textureCompressionASTC_LDR() {
        underlyingFeatures.textureCompressionASTC_LDR(true);
        checks.add(VkPhysicalDeviceFeatures::textureCompressionASTC_LDR);
        return this;
    }
    public GPUFeaturesCreateInfo require_textureCompressionBC() {
        underlyingFeatures.textureCompressionBC(true);
        checks.add(VkPhysicalDeviceFeatures::textureCompressionBC);
        return this;
    }
    public GPUFeaturesCreateInfo require_occlusionQueryPrecise() {
        underlyingFeatures.occlusionQueryPrecise(true);
        checks.add(VkPhysicalDeviceFeatures::occlusionQueryPrecise);
        return this;
    }
    public GPUFeaturesCreateInfo require_pipelineStatisticsQuery() {
        underlyingFeatures.pipelineStatisticsQuery(true);
        checks.add(VkPhysicalDeviceFeatures::pipelineStatisticsQuery);
        return this;
    }
    public GPUFeaturesCreateInfo require_vertexPipelineStoresAndAtomics() {
        underlyingFeatures.vertexPipelineStoresAndAtomics(true);
        checks.add(VkPhysicalDeviceFeatures::vertexPipelineStoresAndAtomics);
        return this;
    }
    public GPUFeaturesCreateInfo require_fragmentStoresAndAtomics() {
        underlyingFeatures.fragmentStoresAndAtomics(true);
        checks.add(VkPhysicalDeviceFeatures::fragmentStoresAndAtomics);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderTessellationAndGeometryPointSize() {
        underlyingFeatures.shaderTessellationAndGeometryPointSize(true);
        checks.add(VkPhysicalDeviceFeatures::shaderTessellationAndGeometryPointSize);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderImageGatherExtended() {
        underlyingFeatures.shaderImageGatherExtended(true);
        checks.add(VkPhysicalDeviceFeatures::shaderImageGatherExtended);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderStorageImageExtendedFormats() {
        underlyingFeatures.shaderStorageImageExtendedFormats(true);
        checks.add(VkPhysicalDeviceFeatures::shaderStorageImageExtendedFormats);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderStorageImageMultisample() {
        underlyingFeatures.shaderStorageImageMultisample(true);
        checks.add(VkPhysicalDeviceFeatures::shaderStorageImageMultisample);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderStorageImageReadWithoutFormat() {
        underlyingFeatures.shaderStorageImageReadWithoutFormat(true);
        checks.add(VkPhysicalDeviceFeatures::shaderStorageImageReadWithoutFormat);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderStorageImageWriteWithoutFormat() {
        underlyingFeatures.shaderStorageImageWriteWithoutFormat(true);
        checks.add(VkPhysicalDeviceFeatures::shaderStorageImageWriteWithoutFormat);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderUniformBufferArrayDynamicIndexing() {
        underlyingFeatures.shaderUniformBufferArrayDynamicIndexing(true);
        checks.add(VkPhysicalDeviceFeatures::shaderUniformBufferArrayDynamicIndexing);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderSampledImageArrayDynamicIndexing() {
        underlyingFeatures.shaderSampledImageArrayDynamicIndexing(true);
        checks.add(VkPhysicalDeviceFeatures::shaderSampledImageArrayDynamicIndexing);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderStorageBufferArrayDynamicIndexing() {
        underlyingFeatures.shaderStorageBufferArrayDynamicIndexing(true);
        checks.add(VkPhysicalDeviceFeatures::shaderStorageBufferArrayDynamicIndexing);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderStorageImageArrayDynamicIndexing() {
        underlyingFeatures.shaderStorageImageArrayDynamicIndexing(true);
        checks.add(VkPhysicalDeviceFeatures::shaderStorageImageArrayDynamicIndexing);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderClipDistance() {
        underlyingFeatures.shaderClipDistance(true);
        checks.add(VkPhysicalDeviceFeatures::shaderClipDistance);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderCullDistance() {
        underlyingFeatures.shaderCullDistance(true);
        checks.add(VkPhysicalDeviceFeatures::shaderCullDistance);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderFloat64() {
        underlyingFeatures.shaderFloat64(true);
        checks.add(VkPhysicalDeviceFeatures::shaderFloat64);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderInt64() {
        underlyingFeatures.shaderInt64(true);
        checks.add(VkPhysicalDeviceFeatures::shaderInt64);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderInt16() {
        underlyingFeatures.shaderInt16(true);
        checks.add(VkPhysicalDeviceFeatures::shaderInt16);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderResourceResidency() {
        underlyingFeatures.shaderResourceResidency(true);
        checks.add(VkPhysicalDeviceFeatures::shaderResourceResidency);
        return this;
    }
    public GPUFeaturesCreateInfo require_shaderResourceMinLod() {
        underlyingFeatures.shaderResourceMinLod(true);
        checks.add(VkPhysicalDeviceFeatures::shaderResourceMinLod);
        return this;
    }
    public GPUFeaturesCreateInfo require_sparseBinding() {
        underlyingFeatures.sparseBinding(true);
        checks.add(VkPhysicalDeviceFeatures::sparseBinding);
        return this;
    }
    public GPUFeaturesCreateInfo require_sparseResidencyBuffer() {
        underlyingFeatures.sparseResidencyBuffer(true);
        checks.add(VkPhysicalDeviceFeatures::sparseResidencyBuffer);
        return this;
    }
    public GPUFeaturesCreateInfo require_sparseResidencyImage2D() {
        underlyingFeatures.sparseResidencyImage2D(true);
        checks.add(VkPhysicalDeviceFeatures::sparseResidencyImage2D);
        return this;
    }
    public GPUFeaturesCreateInfo require_sparseResidencyImage3D() {
        underlyingFeatures.sparseResidencyImage3D(true);
        checks.add(VkPhysicalDeviceFeatures::sparseResidencyImage3D);
        return this;
    }
    public GPUFeaturesCreateInfo require_sparseResidency2Samples() {
        underlyingFeatures.sparseResidency2Samples(true);
        checks.add(VkPhysicalDeviceFeatures::sparseResidency2Samples);
        return this;
    }
    public GPUFeaturesCreateInfo require_sparseResidency4Samples() {
        underlyingFeatures.sparseResidency4Samples(true);
        checks.add(VkPhysicalDeviceFeatures::sparseResidency4Samples);
        return this;
    }
    public GPUFeaturesCreateInfo require_sparseResidency8Samples() {
        underlyingFeatures.sparseResidency8Samples(true);
        checks.add(VkPhysicalDeviceFeatures::sparseResidency8Samples);
        return this;
    }
    public GPUFeaturesCreateInfo require_sparseResidency16Samples() {
        underlyingFeatures.sparseResidency16Samples(true);
        checks.add(VkPhysicalDeviceFeatures::sparseResidency16Samples);
        return this;
    }
    public GPUFeaturesCreateInfo require_sparseResidencyAliased() {
        underlyingFeatures.sparseResidencyAliased(true);
        checks.add(VkPhysicalDeviceFeatures::sparseResidencyAliased);
        return this;
    }
    public GPUFeaturesCreateInfo require_variableMultisampleRate() {
        underlyingFeatures.variableMultisampleRate(true);
        checks.add(VkPhysicalDeviceFeatures::variableMultisampleRate);
        return this;
    }
    public GPUFeaturesCreateInfo require_inheritedQueries() {
        underlyingFeatures.inheritedQueries(true);
        checks.add(VkPhysicalDeviceFeatures::inheritedQueries);
        return this;
    }
}
