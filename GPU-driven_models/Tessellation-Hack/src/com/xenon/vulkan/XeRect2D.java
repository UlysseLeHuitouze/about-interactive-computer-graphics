package com.xenon.vulkan;

import com.xenon.vulkan.abstraction.Builder;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkExtent2D;
import org.lwjgl.vulkan.VkOffset2D;
import org.lwjgl.vulkan.VkRect2D;

/**
 * Java wrapper for {@link VkRect2D}
 * @author Ulysse Le Huitouze
 * @see VkRect2D
 * @since Xe 1.0
 */
public class XeRect2D implements Builder<VkRect2D> {

    public int x, y, width, height;

    public XeRect2D() {}
    public XeRect2D(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }


    @Override
    public VkRect2D build(MemoryStack stack) {
        return VkRect2D.malloc(stack)
                .set(VkOffset2D.malloc(stack).set(x, y), VkExtent2D.malloc(stack).set(width, height));
    }
}
