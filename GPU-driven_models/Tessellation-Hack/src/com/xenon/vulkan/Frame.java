package com.xenon.vulkan;


import com.xenon.vulkan.abstraction.Builder;
import com.xenon.vulkan.abstraction.Disposable;
import com.xenon.vulkan.boostrap.Xe10;
import org.jetbrains.annotations.NotNull;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.nio.LongBuffer;

import static com.xenon.vulkan.XeUtils.checkVK;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.vulkan.VK10.*;

/**
 * Basic frame wrapping a fence for:
 * <ul>
 *     <li>frame synchronization (as to not overwrite its own buffers). See {@link #frame_fence}</li>
 *     <li>image synchronization (as to be sure the swapchain image is available before doing anything).
 *     See {@link #img_semaphore}</li>
 *     <li>presentation synchronization (as to be sure the command buffer got correctly submitted
 *     before presenting the image to the window). See {@link #present_semaphore}</li>
 *     <li>Per-frame command buffers. See {@link #mainThreadCommandBuffers}</li>
 * </ul>
 *
 * @author Ulysse Le Huitouze
 * @apiNote Use {@link #genFrames(int, Xe10, PerFrameData)} to create as many frames as you'd like,
 * applying to each the PerFrameData.
 * @see #genFrames(int, Xe10, PerFrameData) genFrames(...)
 * @since Xe 1.0
 */
public record Frame(long frame_fence, long img_semaphore, long present_semaphore,
                    VkCommandBuffer[] mainThreadCommandBuffers,
                    VkDevice device, VkAllocationCallbacks callbacks) implements Disposable {

    /**
     * Creates duplicate frames from the per-frame data, and returns them as an array.
     * @param count frame count
     * @param xe the VulkanManager
     * @param perFrameData per-frame data
     * @return an array of Frames
     */
    public static Frame[] genFrames(int count, @NotNull Xe10 xe, PerFrameData perFrameData) {

        Frame[] r = new Frame[count];
        try (MemoryStack stack = stackPush()) {

            LongBuffer frame_ptr = stack.mallocLong(1);
            VkFenceCreateInfo frame_fenceCreateInfo = perFrameData.frame_fence_data.build(stack);

            LongBuffer img_ptr = stack.mallocLong(1);
            VkSemaphoreCreateInfo img_semaphoreCreateInfo = perFrameData.img_semaphore_data.build(stack);

            LongBuffer present_ptr = stack.mallocLong(1);
            VkSemaphoreCreateInfo present_semaphoreCreateInfo = perFrameData.present_semaphore_data.build(stack);

            PointerBuffer commandBuffer_ptr = stack.mallocPointer(1);
            VkCommandBufferAllocateInfo bufferAllocateInfo = VkCommandBufferAllocateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(0)
                    .commandPool(perFrameData.commandPool)
                    .commandBufferCount(1);


            for (int i = 0; i < count; i++) {
                checkVK(vkCreateFence(xe.device(), frame_fenceCreateInfo, xe.allocationCallbacks(), frame_ptr),
                    "Failed to create fence for frame round-tripping");
                checkVK(vkCreateSemaphore(xe.device(), img_semaphoreCreateInfo, xe.allocationCallbacks(), img_ptr),
                        "Failed to create semaphore for retrieving swapchain images");
                checkVK(vkCreateSemaphore(xe.device(), present_semaphoreCreateInfo, xe.allocationCallbacks(), present_ptr),
                        "Failed to create semaphore for presentation");

                VkCommandBuffer[] commandBuffers = new VkCommandBuffer[perFrameData.commandBuffersLevel.length];
                for (int j = 0; j < commandBuffers.length; j++) {
                    bufferAllocateInfo.level(perFrameData.commandBuffersLevel[j]);
                    checkVK(vkAllocateCommandBuffers(xe.device(), bufferAllocateInfo, commandBuffer_ptr),
                            "Failed to create a command buffer");
                    commandBuffers[j] = new VkCommandBuffer(commandBuffer_ptr.get(0), xe.device());
                }

                r[i] = new Frame(frame_ptr.get(0), img_ptr.get(0), present_ptr.get(0), commandBuffers,
                        xe.device(), xe.allocationCallbacks());
            }
        }

        return r;
    }


    /**
     * Disposes of this frame's sync objects. CommandBuffers are not freed, we expect the pool to do it.
     */
    @Override
    public void dispose() {
        vkDestroyFence(device, frame_fence, callbacks);
        vkDestroySemaphore(device, img_semaphore, callbacks);
        vkDestroySemaphore(device, present_semaphore, callbacks);
    }


    // Macros

    /**
     *
     * @param frame_fence_data the data for creating the frame fence
     * @param img_semaphore_data the data for creating the swapchain image semaphore
     * @param present_semaphore_data the data for creating the presentation semaphore
     * @param commandPool the command pool to allocate the command buffers with
     * @param commandBuffersLevel an array containing all command buffers' level. Will be used to create
     *                            the ordered VkCommandBuffer array, so its length must match with the desired
     *                            VkCommandBuffers count.<br><br>
     *                            <h3>Main property:</h3>
     *                            <code>Frame.VkCommandBuffer[i] <--
     *                            VkCommandBufferAllocateInfo.withLevel(PerFrameData.commandBuffersLevel[i]).alloc()</code>
     */
    public record PerFrameData(
            FenceData frame_fence_data,
            SemaphoreData img_semaphore_data,
            SemaphoreData present_semaphore_data,
            long commandPool,
            int[] commandBuffersLevel) {
    }

    /**
     * Builder for {@link VkSemaphoreCreateInfo}.
     * @param pNext pNext
     * @return a new macro for {@link VkSemaphoreCreateInfo}
     * @see SemaphoreData#build(MemoryStack)
     */
    public static SemaphoreData semaphoreData(long pNext) {
        return new SemaphoreData(pNext);
    }

    /**
     * Java wrapper for {@link VkSemaphoreCreateInfo}
     * @param pNext
     * @see VkSemaphoreCreateInfo
     */
    public record SemaphoreData(long pNext) implements Builder<VkSemaphoreCreateInfo> {
        @Override
        public VkSemaphoreCreateInfo build(MemoryStack stack) {
            return VkSemaphoreCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .flags(0);
        }
    }

    /**
     * Builder for {@link VkFenceCreateInfo}.
     * @param pNext pNext
     * @param flags flags
     * @return a new macro for {@link VkFenceCreateInfo}
     * @see FenceData#build(MemoryStack)
     */
    public static FenceData fenceData(long pNext, int flags) {
        return new FenceData(pNext, flags);
    }

    /**
     * Similar to {@link #fenceData(long, int)} but with VK_FENCE_CREATE_SIGNALED_BIT set by default for flags.
     * @param pNext pNext
     * @param additional_flags additional flags to add to the hardcoded VK_FENCE_CREATE_SIGNALED_BIT
     * @return a new macro for {@link VkFenceCreateInfo}
     * @see #fenceData(long, int)
     */
    public static FenceData fenceData_usual(long pNext, int additional_flags) {
        return fenceData(pNext, additional_flags | VK_FENCE_CREATE_SIGNALED_BIT);
    }

    /**
     * Java wrapper for {@link VkFenceCreateInfo}
     * @param pNext
     * @param flags
     * @see VkFenceCreateInfo
     */
    public record FenceData(long pNext, int flags) implements Builder<VkFenceCreateInfo>{
        @Override
        public VkFenceCreateInfo build(MemoryStack stack) {
            return VkFenceCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .flags(flags);
        }
    }
}
