package com.xenon.vulkan;

import com.xenon.vulkan.abstraction.Disposable;
import com.xenon.vulkan.boostrap.*;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.util.function.IntToLongFunction;

import static com.xenon.vulkan.XeUtils.*;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.vulkan.KHRSurface.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
import static org.lwjgl.vulkan.KHRSurface.vkGetPhysicalDeviceSurfaceCapabilitiesKHR;
import static org.lwjgl.vulkan.KHRSwapchain.*;
import static org.lwjgl.vulkan.VK10.*;
import static org.lwjgl.vulkan.VK10.vkCreatePipelineLayout;

/**
 * Class holding information about a swapchain and its associated objects.
 * These are:
 * <ul>
 *     <li>a bunch of information on the settings used to create the swapchain, notably
 *     {@link #imgFormat}, {@link #imgColorSpace}, {@link #imgUsage}, {@link #presentMode}.<br>
 *     See {@link #build(int, int, int, int, int, boolean, int, IntToLongFunction, VkDevice, VkAllocationCallbacks)}
 *     for the full description.</li>
 *     <li>the swapchain itself, represented as an handle.<br>
 *     See {@link #swapchain}.</li>
 *     <li>the current extent. Without platform-specific glitches,
 *     should always be equal to the dimension of the GLFW window.<br>
 *     See {@link #extentWidth} and {@link #extentHeight}.</li>
 *     <li>the swapchain images.<br>
 *     See {@link #images}.</li>
 *     <li>the swapchain image views.</li>
 * </ul><br>
 * The user can either choose to create a SwapchainAndCo suited for dynamic rendering or classic render pass,
 * through {@link VkSettings#useDynamicRendering}.<br>
 * See {@link SwapchainAndCoDynamicRendering} and {@link SwapchainAndCoRenderPass}.
 * If the render pass way is taken, the SwapchainAndCo object will offer 2 more fields:
 * <ul>
 *     <li>an handle to the renderpass, through {@link #renderpass()}.</li>
 *     <li>an array of handles representing the framebuffers associated to the swapchain images,
 *     through {@link #framebuffers()}.</li>
 * </ul>
 * Note that {@link #renderpass()} and {@link #framebuffers()} will both throw {@link UnsupportedOperationException}
 * if called on an object created for dynamic rendering. See {@link SwapchainAndCoDynamicRendering}.
 * <br><br><br>
 *
 * The following properties are always verified by a {@link SwapchainAndCo} object created through {@link Xe10}:
 * <br>
 * <code>images.length = imageViews.length</code><br>
 * <code>pipelines.length = pipelineLayouts.length</code><br><br>
 * If dynamic rendering is NOT used:<br>
 * <code>images.length = framebuffers().length</code><br>
 * @author Ulysse Le Huitouze
 */
public abstract class SwapchainAndCo implements Disposable {

    /**
     * Creates a new PipelineInfo object. Either {@link SwapchainAndCoRenderPass} or {@link SwapchainAndCoDynamicRendering}
     * depending on {@link VkSettings#useDynamicRendering}.
     * @param img_format the format
     * @param img_colorSpace the color space
     * @param img_usage the image usage
     * @param present_mode the present mode
     * @param img_count the image count
     * @param useDynamicRendering whether a swapchainAndCo suited for dynamic rendering should be created
     * @param pipelineCount the maximum number of pipelines
     * @param renderPassCreation Lambda for delayed renderpass creation
     * @param device the device to create the swapchain with
     * @param callbacks the callbacks to create the swapchain with
     * @return a new PipelineInfo object
     */
    public static SwapchainAndCo build(
            int img_format, int img_colorSpace, int img_usage, int present_mode, int img_count,
            boolean useDynamicRendering, int pipelineCount, @Nullable IntToLongFunction renderPassCreation,
            VkDevice device, VkAllocationCallbacks callbacks) {
        if (useDynamicRendering)
            return new SwapchainAndCoDynamicRendering(
                            img_format,
                            img_colorSpace,
                            img_usage,
                            present_mode,
                            img_count,
                            pipelineCount,
                            device,
                            callbacks
                    );

        assert renderPassCreation != null;
        return new SwapchainAndCoRenderPass(
                img_format,
                img_colorSpace,
                img_usage,
                present_mode,
                img_count,
                pipelineCount,
                renderPassCreation,
                device,
                callbacks
        );
    }

    public final VkDevice device;
    public final VkAllocationCallbacks callbacks;
    public final long[] pipelines, pipelineLayouts;
    public final long[] images, imageViews;
    public final int imgFormat, imgColorSpace, imgUsage, presentMode;
    public long swapchain = VK_NULL_HANDLE;
    public int extentWidth, extentHeight;

    /**
     * init final fields
     */
    protected SwapchainAndCo(int img_format, int img_colorSpace, int img_usage, int present_mode,
                             int img_count, int pipelineCount, VkDevice device, VkAllocationCallbacks callbacks) {
        imgFormat = img_format;
        imgColorSpace = img_colorSpace;
        imgUsage = img_usage;
        presentMode = present_mode;
        images = new long[img_count];
        imageViews = new long[img_count];
        pipelines = new long[pipelineCount];
        pipelineLayouts = new long[pipelineCount];
        this.device = device;
        this.callbacks = callbacks;
    }

    /**
     * abstraction to avoid type cast
     * @return the framebuffers handles
     */
    public abstract long[] framebuffers();

    /**
     * abstraction to avoid type cast
     * @return the renderpass handle
     */
    public abstract long renderpass();

    /**
     * Pipeline creation. Call this method upon window resize
     * @param physicalDevice the gpu
     * @param queues the queues
     * @param surface_handle the surface
     * @param width the window's width
     * @param height the window's height
     */
    public void create(VkPhysicalDevice physicalDevice,
                       XeQueues queues,
                       long surface_handle,
                       final int width, final int height
    ) {
        try (MemoryStack stack = stackPush()) {
            final VkSurfaceCapabilitiesKHR caps = VkSurfaceCapabilitiesKHR.malloc(stack);
            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface_handle, caps);

            final VkExtent2D extent2D;
            {
                if (caps.currentExtent().width() != -1)
                    extent2D = caps.currentExtent();
                else {
                    extent2D = VkExtent2D.malloc(stack);

                    VkExtent2D minEx = caps.minImageExtent(),
                            maxEx = caps.maxImageExtent();

                    extent2D.width(clamp(minEx.width(), maxEx.width(), width))
                            .height(clamp(minEx.height(), maxEx.height(), height));
                }
                extentWidth = extent2D.width();
                extentHeight = extent2D.height();
            }

            VkSwapchainCreateInfoKHR swapchainCreateInfo = VkSwapchainCreateInfoKHR.malloc(stack)
                    .sType$Default()
                    .pNext(0)   // calloc stuff
                    .flags(0)   // calloc stuff
                    .surface(surface_handle)
                    .minImageCount(images.length)
                    .imageFormat(imgFormat)
                    .imageColorSpace(imgColorSpace)
                    .imageExtent(extent2D)
                    .imageArrayLayers(1)
                    .imageUsage(imgUsage);

            int[] familyIndices = queues.familyIndices();

            if (familyIndices[0] != familyIndices[1]) {
                swapchainCreateInfo.imageSharingMode(VK_SHARING_MODE_CONCURRENT)
                        .pQueueFamilyIndices(stack.ints(
                            familyIndices[0],
                            familyIndices[1]
                        ));
            } else swapchainCreateInfo.imageSharingMode(VK_SHARING_MODE_EXCLUSIVE);

            swapchainCreateInfo.preTransform(caps.currentTransform())
                    .compositeAlpha(VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
                    .presentMode(presentMode)
                    .clipped(true)
                    .oldSwapchain(swapchain);

            LongBuffer ptr = stack.longs(VK_NULL_HANDLE);
            checkVK(vkCreateSwapchainKHR(device, swapchainCreateInfo, callbacks, ptr), "Failed to create swap chain");

            if (swapchain != VK_NULL_HANDLE)    // dispose of the old swapchain
                vkDestroySwapchainKHR(device, swapchain, callbacks);

            swapchain = ptr.get(0);

            // hierarchy: swapchain -> images -> image views

            // images
            try (MemoryStack stack1 = stackPush()) {
                IntBuffer countB = stack1.callocInt(1);
                vkGetSwapchainImagesKHR(device, swapchain, countB, null);
                int count = countB.get(0);
                LongBuffer pSwapChainImages = stack1.mallocLong(count);
                vkGetSwapchainImagesKHR(device, swapchain, countB, pSwapChainImages);

                if (count != images.length)
                    throw VkError.impossible();

                for (int i = 0; i < count; i++)
                    images[i] = pSwapChainImages.get(i);
            }

            // image views
            try (MemoryStack stack1 = stackPush()) {
                ptr = stack.longs(VK_NULL_HANDLE);
                VkImageViewCreateInfo imageViewCreateInfo = VkImageViewCreateInfo.calloc(stack1)
                        .sType$Default()
                        .viewType(VK_IMAGE_VIEW_TYPE_2D)
                        .format(imgFormat);
                imageViewCreateInfo.subresourceRange()
                        .aspectMask(VK_IMAGE_ASPECT_COLOR_BIT)
                        .baseMipLevel(0)
                        .levelCount(1)
                        .baseArrayLayer(0)
                        .layerCount(1);

                for (int i = 0; i < images.length; i++) {
                    imageViewCreateInfo.image(images[i]);
                    checkVK(vkCreateImageView(device, imageViewCreateInfo, callbacks, ptr),
                            "Failed to create image view n°" + i);

                    imageViews[i] = ptr.get(0);
                }
            }
        }
    }

    /**
     * Creates a new pipeline layout using the provided createInfo struct and puts it in {@link #pipelineLayouts}
     * at the provided index.
     * Effectively<br>
     * <code>
     *     check_err(vkCreatePipelineLayout(xe.device, &createInfo, xe.allocationCallbacks, &pipelineLayout[index]));
     * </code>
     * @param index the index
     * @param xe the VulkanManager
     * @param createInfo the createInfo for pipeline layout creation
     */
    public void pipelineLayout(int index, Xe10 xe, VkPipelineLayoutCreateInfo createInfo) {
        try (MemoryStack stack = stackPush()) {
            LongBuffer lb = stack.mallocLong(1);
            checkVK(vkCreatePipelineLayout(xe.device(), createInfo, xe.allocationCallbacks(), lb),
                    "Failed to create a pipeline layout");
            pipelineLayouts[index] = lb.get(0);
        }
    }

    /**
     * Macro for creating a single pipeline,
     * calling {@link #pipelines(int[], Xe10, long, VkGraphicsPipelineCreateInfo.Buffer)}.
     * Creates a new pipeline using the provided createInfo struct and puts it at the designated index in
     * {@link #pipelines}.
     * @param index the index
     * @param xe the VulkanManager
     * @param pipelineCache the pipeline cache
     * @param createInfo the createInfo
     * @see #pipelines(int[], Xe10, long, VkGraphicsPipelineCreateInfo.Buffer)
     */
    public void pipeline(int index, Xe10 xe, long pipelineCache, VkGraphicsPipelineCreateInfo createInfo) {
        try (MemoryStack stack = stackPush()) {
            VkGraphicsPipelineCreateInfo.Buffer buffer = VkGraphicsPipelineCreateInfo.malloc(1, stack)
                    .put(0, createInfo);
            pipelines(new int[]{index}, xe, pipelineCache, buffer);
        }
    }

    /**
     * Creates multiple pipelines using the provided createInfo buffer and puts them in {@link #pipelines} at the
     * corresponding indices, in order (<code>pipelines[indices[i]] <-- buffer.get(i)</code>)
     * @param indices the indices
     * @param xe the VulkanManager
     * @param pipelineCache the pipeline cache
     * @param buffer the createInfo buffer
     */
    public void pipelines(int[] indices, Xe10 xe, long pipelineCache,
                          VkGraphicsPipelineCreateInfo.Buffer buffer) {
        int len = indices.length;
        if (len != buffer.capacity())
            throw VkError.log("Incoherent indices count and VkGraphicsPipelineCreateInfo structs count");
        try (MemoryStack stack = stackPush()) {
            LongBuffer lb = stack.mallocLong(len);

            checkVK(vkCreateGraphicsPipelines(xe.device(), pipelineCache, buffer.rewind(), xe.allocationCallbacks(), lb),
                    "Failed to create pipeline");
            for (int i = 0; i < len; i++)
                pipelines[indices[i]] = lb.get(i);
        }
    }

    // swapchain handle is not destroyed in cleanOutOfDate to make use of oldSwapchain field in VkSwapchainCreateInfoKHR

    /**
     * Disposes of the pipeline objects depending on window size, for recreation
     */
    public void cleanOutOfDate() {
        for (long view : imageViews)
            vkDestroyImageView(device, view, callbacks);
    }

    /**
     * Disposes of the pipeline objects
     */
    public void dispose() {
        cleanOutOfDate();
        vkDestroySwapchainKHR(device, swapchain, callbacks);

        for (long pipeline : pipelines)
            vkDestroyPipeline(device, pipeline, callbacks);
        for (long pipelineLayout : pipelineLayouts)
            vkDestroyPipelineLayout(device, pipelineLayout, callbacks);
    }



    /**
     * Swapchain&Co for RenderPass implementation
     */
    public static class SwapchainAndCoRenderPass extends SwapchainAndCo {

        private final long[] framebuffers;
        private final long renderpass;  // renderpass do not need to be recreated on resize

        protected SwapchainAndCoRenderPass(int img_format, int img_colorSpace, int img_usage, int present_mode,
                                           int img_count, int pipelineCount,
                                           IntToLongFunction renderpassCreation,
                                           VkDevice device, VkAllocationCallbacks callbacks) {
            super(img_format, img_colorSpace, img_usage, present_mode, img_count, pipelineCount, device, callbacks);
            framebuffers = new long[img_count];
            renderpass = renderpassCreation.applyAsLong(img_format);
        }

        /**
         * Pipeline creation. Call this method upon window resize
         * @param physicalDevice the gpu
         * @param queues the queues
         * @param surface_handle the surface
         * @param width the window's width
         * @param height the window's height
         */
        @Override
        public void create(
                VkPhysicalDevice physicalDevice,
                XeQueues queues,
                long surface_handle,
                int width, int height
        ) {
            super.create(physicalDevice, queues, surface_handle, width, height);
            // framebuffers
            try (MemoryStack stack1 = stackPush()) {
                VkFramebufferCreateInfo fbInfo = VkFramebufferCreateInfo.malloc(stack1)
                        .sType$Default()
                        .pNext(0)   // calloc stuff
                        .flags(0)   // calloc stuff
                        .renderPass(renderpass)
                        .width(width)
                        .height(height)
                        .layers(1);

                LongBuffer ptr = stack1.mallocLong(1);
                LongBuffer attachment = stack1.mallocLong(1);

                for (int i = 0; i < framebuffers.length; i++) {
                    attachment.put(0, imageViews[i]);
                    fbInfo.pAttachments(attachment);
                    checkVK(vkCreateFramebuffer(device, fbInfo, callbacks, ptr),
                            "Failed to create framebuffer n°"+i);
                    framebuffers[i] = ptr.get(0);
                }
            }
        }

        @Override
        public final long[] framebuffers() {
            return framebuffers;
        }

        @Override
        public final long renderpass() {
            return renderpass;
        }

        @Override
        public void cleanOutOfDate() {
            for (long framebuffer : framebuffers)
                vkDestroyFramebuffer(device, framebuffer, callbacks);
            super.cleanOutOfDate();
        }

        @Override
        public void dispose() {
            super.dispose();
            vkDestroyRenderPass(device, renderpass, callbacks); // not destroy renderpass upon resize
        }
    }


    /**
     * Swapchain&Co for DynamicRendering implementation
     */
    public static class SwapchainAndCoDynamicRendering extends SwapchainAndCo {

        protected SwapchainAndCoDynamicRendering(
                int img_format, int img_colorSpace, int img_usage, int present_mode, int img_count, int pipelineCount,
                VkDevice device, VkAllocationCallbacks callbacks) {
            super(img_format, img_colorSpace, img_usage, present_mode, img_count, pipelineCount, device, callbacks);
        }

        @Override
        public final long[] framebuffers() {
            throw new UnsupportedOperationException();
        }

        @Override
        public final long renderpass() {
            throw new UnsupportedOperationException();
        }
    }

}




