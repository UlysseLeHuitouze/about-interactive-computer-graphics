package com.xenon.vulkan;

import org.lwjgl.vulkan.VkQueue;

/**
 * @author Ulysse Le Huitouze
 */
public record XeQueues(VkQueue[] queues, int[] familyIndices) {
}
