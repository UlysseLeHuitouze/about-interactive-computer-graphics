package com.xenon.vulkan.abstraction;

import org.lwjgl.system.MemoryStack;

/**
 * Java abstraction for Vulkan structs
 * @author Ulysse Le Huitouze
 */
public interface Builder<T> {
    /**
     * Builds the object on the given stack
     * @param stack the stack
     * @return the resulting built object
     */
    T build(MemoryStack stack);



}
