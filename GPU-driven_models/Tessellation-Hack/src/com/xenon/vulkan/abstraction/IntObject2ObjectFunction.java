package com.xenon.vulkan.abstraction;

/**
 * A lambda for
 * {@code
 * R function(int x, P p);
 * }
 * @author Ulysse Le Huitouze
 */
@FunctionalInterface
public interface IntObject2ObjectFunction<P, R> {

    R run(int x, P p);

}
