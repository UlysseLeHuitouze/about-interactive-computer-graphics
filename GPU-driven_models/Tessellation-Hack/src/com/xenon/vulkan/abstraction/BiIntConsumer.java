package com.xenon.vulkan.abstraction;

/**
 * Consumer of 2 integers
 * @author Ulysse Le Huitouze
 */
@FunctionalInterface
public interface BiIntConsumer {

    void consume(int x, int y);
}
