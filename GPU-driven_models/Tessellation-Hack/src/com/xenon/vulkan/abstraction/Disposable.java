package com.xenon.vulkan.abstraction;

import com.xenon.vulkan.boostrap.Once;

/**
 * @author Ulysse Le Huitouze
 */
public interface Disposable {

    @Once
    void dispose();
}
