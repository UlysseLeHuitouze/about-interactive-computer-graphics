package com.xenon.vulkan;

import org.lwjgl.vulkan.VkAllocationCallbacks;
import org.lwjgl.vulkan.VkDevice;
import org.lwjgl.vulkan.VkPhysicalDevice;

/**
 * A Vulkan memory-immutable buffer. Its memory cannot be reallocated.
 * Offers similar mechanisms to OpenGL <code>glBufferStorage</code>.
 * @author Ulysse Le Huitouze
 * @since Xe 1.0
 */
public record XeBufferImmutable(long handle, long memHandle, VkDevice device,
                                VkPhysicalDevice physicalDevice, VkAllocationCallbacks callbacks) implements XeBuffer{


    @Override
    public VkDevice device() {
        return device;
    }

    @Override
    public VkPhysicalDevice physicalDevice() {
        return physicalDevice;
    }

    @Override
    public VkAllocationCallbacks callbacks() {
        return callbacks;
    }

    @Override
    public long handle() {
        return handle;
    }

    @Override
    public long memHandle() {
        return memHandle;
    }

    /**
     * Consider using {@link XeBufferMutable}.
     * @throws UnsupportedOperationException always
     */
    @Override
    public XeBuffer bufferData(long pNext, long size, int memTypeIndex) {
        throw new UnsupportedOperationException();
    }

    /**
     * Consider using {@link XeBufferMutable}.
     * @throws UnsupportedOperationException always
     */
    @Override
    public XeBuffer bufferStorage(long pNext, long size, int memTypeIndex) {
        throw new UnsupportedOperationException();
    }
}
