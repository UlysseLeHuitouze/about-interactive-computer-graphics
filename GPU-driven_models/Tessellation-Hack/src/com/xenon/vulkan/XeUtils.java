package com.xenon.vulkan;

import com.xenon.vulkan.abstraction.Builder;
import com.xenon.vulkan.abstraction.IntObject2ObjectFunction;
import com.xenon.vulkan.boostrap.VkError;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.PointerBuffer;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.Struct;
import org.lwjgl.system.StructBuffer;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.function.Function;

import static org.lwjgl.system.MemoryUtil.NULL;
import static org.lwjgl.vulkan.VK10.VK_SUCCESS;

/**
 * Statically-typed utils.
 * <ul>
 *     <li>For Java objects to Buffers, see {@link #pointerbuffer(MemoryStack, Collection, Function)}</li>
 *     <li>For cuda-styled checks, see {@link #checkVK(int, String)}, {@link #checkPtr(long, String)} and
 *     {@link #checkCount(int, String)}.</li>
 * </ul>
 * @author Ulysse Le Huitouze
 */
public class XeUtils {

    /**
     * Creates a PointerBuffer out of a collection of strings, using {@link MemoryStack#UTF8(CharSequence)}.
     * @param stack the stack
     * @param strings the strings
     * @return the resulting PointerBuffer
     */
    public static PointerBuffer pointerbuffer(MemoryStack stack, Collection<String> strings) {
        return pointerbuffer(stack, strings, stack::UTF8);
    }

    /**
     * Generic methods for converting a collection to a PointerBuffer through a mapping function of the generic into
     * ByteBuffer.
     * @param stack the stack
     * @param collection the collection
     * @param map the mapping function
     * @return the resulting PointerBuffer
     * @param <T> generic collection
     */
    public static <T> PointerBuffer pointerbuffer(MemoryStack stack,
                                                  Collection<T> collection,
                                                  Function<? super T, ? extends ByteBuffer> map) {
        PointerBuffer pb = stack.mallocPointer(collection.size());
        collection.stream().map(map).forEach(pb::put);
        return pb.flip();
    }

    // cuda style checks

    /**
     * Checks whether the function succeeded.
     * Literally
     * <code>if (func_result != VK_SUCCESS) throw RuntimeException</code>
     * @param func_result the function exit code
     * @param log the log to print
     */
    public static void checkVK(int func_result, String log) {
        if (func_result != VK_SUCCESS)
            throw VkError.log("Error code[" + func_result + "] ;" + log);
    }

    /**
     * Checks whether the pointer is valid.
     * Literally
     * <code>if (ptr == NULL) throw RuntimeException</code>
     * @param ptr the pointer
     * @param log the log to print
     */
    public static void checkPtr(long ptr, String log) {
        if (ptr == NULL)
            throw VkError.log(log);
    }

    /**
     * Checks whether the count is 0.
     * Literally
     * <code>if (count == 0) throw RuntimeException</code>
     * @param count the count
     * @param log the log to print
     */
    public static void checkCount(int count, String log) {
        if (count == 0)
            throw VkError.log(log);
    }



    // Unsafe/expensive macros to help relieve the pain of java syntax
    // designed to be used internally by the api

    /**
     * Macro for filling a struct buffer.
     * @apiNote Do not use it in main loop if you care about performances
     * @param stack the stack
     * @param createFunc the buffer creation function
     * @param builders the array of builders
     * @return a buffer containing the built builders
     * @param <S> the structs to be built and put in a buffer
     * @param <B> the buffer to append to
     */
    @SafeVarargs
    public static <S extends Struct, B extends StructBuffer<S, B>> B fromArray(
            MemoryStack stack,
            IntObject2ObjectFunction<MemoryStack, B> createFunc,
            @Nullable Builder<S>... builders) {
        if (builders == null)   return null;

        B buffer = createFunc.run(builders.length, stack);

        for (Builder<S> builder : builders)
            if (builder != null)
                buffer.put(builder.build(stack));

        buffer.rewind();
        return buffer;
    }

    /**
     * Macro for creating arrays.
     * Java verbosity and Vulkan naming don't know how to work together.
     * Typical use case:<br><br>
     * <code>new VkPipelineColorBlendAttachmentState[] {VkPipelineColorBlendAttachmentState.malloc(stack), ...}</code>
     * <br>-><br>
     * <code>arr(VkPipelineColorBlendAttachmentState.malloc(stack), ...)</code>
     *
     * @param els the elements
     * @return els
     * @param <T> the type of the object. Use at your own risks
     */
    @SafeVarargs
    public static <T> T[] arr(T... els) {
        return els;
    }

    /**
     * Literally<br>
     * <code>builder == null ? null : builder.build(stack)</code>
     * @param stack the stack
     * @param builder the builder
     * @return a new T built from the builder
     * @param <T> the class to be built
     */
    public static <T> T buildSafe(MemoryStack stack, @Nullable Builder<T> builder) {
        return builder == null ? null : builder.build(stack);
    }


    public static int clamp(int a, int b, int val) {
        return Math.max(a, Math.min(b, val));
    }
}
