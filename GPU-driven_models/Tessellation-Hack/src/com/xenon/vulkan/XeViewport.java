package com.xenon.vulkan;

import com.xenon.vulkan.abstraction.Builder;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkViewport;

/**
 * Java wrapper for {@link VkViewport}
 * @author Ulysse Le Huitouze
 * @see VkViewport
 * @since Xe 1.0
 */
public class XeViewport implements Builder<VkViewport> {


    public float x, y, width, height, minDepth, maxDepth;

    public XeViewport() {}
    public XeViewport(float x, float y, float width, float height) {
        this(x, y, width, height, 0, 1);
    }
    public XeViewport(float x, float y, float width, float height, float minDepth, float maxDepth) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.minDepth = minDepth;
        this.maxDepth = maxDepth;
    }


    @Override
    public VkViewport build(MemoryStack stack) {
        return VkViewport.malloc(stack).set(x, y, width, height ,minDepth, maxDepth);
    }
}
