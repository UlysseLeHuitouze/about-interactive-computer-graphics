package com.xenon.vulkan.structs;

import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.VkApplicationInfo;

import java.nio.ByteBuffer;

/**
 * Factory class for creating {@link VkApplicationInfo} on the stack.
 * @author Ulysse Le Huitouze
 * @apiNote Use {@link #usual(MemoryStack, String, int, String, int, int)} for common use cases.<br>
 *          Use {@link #custom(VkApplicationInfo, ByteBuffer, int, ByteBuffer, int, int)} if for some reason
 *          you want to create the buffers containing the strings yourself.
 */
public final class VkApplicationInfos {

    /**
     * Factory for {@link VkApplicationInfo}
     * @param stack the stack
     * @param applicationName the app name
     * @param applicationVersion the app version. Use {@link org.lwjgl.vulkan.VK10#VK_MAKE_VERSION(int, int, int)}
     * @param engineName the engine name
     * @param engineVersion the engine version. Use {@link org.lwjgl.vulkan.VK10#VK_MAKE_VERSION(int, int, int)}
     * @param apiVersion the api version. Use <code>VK*.VK_API_VERSION_*_*</code>
     * @return a new {@link VkApplicationInfo} allocated on the stack
     */
    public static VkApplicationInfo usual(MemoryStack stack, String applicationName, int applicationVersion,
                                            String engineName, int engineVersion, int apiVersion) {
        return custom(VkApplicationInfo.malloc(stack), stack.UTF8(applicationName),
                applicationVersion, stack.UTF8(engineName), engineVersion, apiVersion);
    }


    /**
     * Fills the supplied VkApplicationInfo with the supplied parameters.
     * @param info the VkApplicationInfo
     * @param pApplicationName the app name
     * @param applicationVersion the app version. Use {@link org.lwjgl.vulkan.VK10#VK_MAKE_VERSION(int, int, int)}
     * @param pEngineName the engine name
     * @param engineVersion the engine version. Use {@link org.lwjgl.vulkan.VK10#VK_MAKE_VERSION(int, int, int)}
     * @param apiVersion the api version. Use <code>VK*.VK_API_VERSION_*_*</code>
     * @return <code>info</code>
     */
    public static VkApplicationInfo custom(VkApplicationInfo info, ByteBuffer pApplicationName,
                                            int applicationVersion, ByteBuffer pEngineName, int engineVersion,
                                            int apiVersion) {
        return info.sType$Default()
                .pNext(0)
                .pApplicationName(pApplicationName)
                .applicationVersion(applicationVersion)
                .pEngineName(pEngineName)
                .engineVersion(engineVersion)
                .apiVersion(apiVersion);
    }

}
