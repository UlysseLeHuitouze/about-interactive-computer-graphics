package com.xenon.vulkan.structs;

import com.xenon.vulkan.XeRect2D;
import com.xenon.vulkan.XeViewport;
import com.xenon.vulkan.abstraction.Builder;
import com.xenon.vulkan.boostrap.VkError;
import com.xenon.vulkan.XeShaderModule;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.vulkan.*;

import java.nio.IntBuffer;

import static com.xenon.vulkan.XeUtils.*;
import static org.lwjgl.vulkan.VK10.*;

/**
 * @author Ulysse Le Huitouze
 */
public class VkGraphicsPipelineCreateInfos {


    public static VkGraphicsPipelineCreateInfo usual(
            MemoryStack stack,
            XeShaderModule[] shaderModules,
            @NotNull VertexInputStateCI vertexInputStateCI,
            @NotNull InputAssemblyStateCI inputAssemblyStateCI,
            @Nullable TesselationStateCI tesselationStateCI,
            @NotNull ViewportStateCI viewportStateCI,
            @Nullable MultisampleStateCI multisampleStateCI,
            long layout,
            long renderpass,
            int subpass,
            long basePipelineHandle,
            int basePipelineIndex) {
        return fromModules(stack, 0, shaderModules,
                vertexInputStateCI,
                inputAssemblyStateCI,
                tesselationStateCI,
                viewportStateCI,
                rs_u(0, false, false, VK_POLYGON_MODE_FILL,
                        VK_CULL_MODE_BACK_BIT, VK_FRONT_FACE_COUNTER_CLOCKWISE),
                multisampleStateCI,
                null,
                cbs(0, 0, false, 0,
                        new ColorBlendAttachmentStateCI[]{cbas_u()}, 0, 0, 0, 0),
                ds(VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR),
                layout,
                renderpass,
                subpass,
                basePipelineHandle,
                basePipelineIndex
        );
    }

    /**
     * Macro for filling {@link VkGraphicsPipelineCreateInfo}.
     * @param stack the stack
     * @param flags VkPipelineCreateFlags
     * @param shaderModules the shader modules
     * @param vertexInputStateCI VkPipelineVertexInputStateCreateInfo
     * @param inputAssemblyStateCI VkPipelineInputAssemblyStateCreateInfo
     * @param tesselationStateCI VkPipelineTessellationStateCreateInfo
     * @param viewportStateCI VkPipelineViewportStateCreateInfo
     * @param rasterizationStateCI VkPipelineRasterizationStateCreateInfo
     * @param multisampleStateCI VkPipelineMultisampleStateCreateInfo
     * @param depthStencilStateCI VkPipelineDepthStencilStateCreateInfo
     * @param colorBlendStateCI VkPipelineColorBlendStateCreateInfo
     * @param dynamicStateCI VkPipelineDynamicStateCreateInfo
     * @param layout the pipeline layout
     * @param renderpass the render pass
     * @param subpass the subpass
     * @param basePipelineHandle the base pipeline handle
     * @param basePipelineIndex the base pipeline index
     * @return a filled VkGraphicsPipelineCreateInfo
     */
    public static VkGraphicsPipelineCreateInfo fromModules(
            MemoryStack stack,
            int flags,
            XeShaderModule[] shaderModules,
            @Nullable VertexInputStateCI vertexInputStateCI,
            @Nullable InputAssemblyStateCI inputAssemblyStateCI,
            @Nullable TesselationStateCI tesselationStateCI,
            @Nullable ViewportStateCI viewportStateCI,
            @Nullable RasterizationStateCI rasterizationStateCI,
            @Nullable MultisampleStateCI multisampleStateCI,
            @Nullable DepthStencilStateCI depthStencilStateCI,
            @Nullable ColorBlendStateCI colorBlendStateCI,
            @Nullable DynamicStateCI dynamicStateCI,
            long layout,
            long renderpass,
            int subpass,
            long basePipelineHandle,
            int basePipelineIndex) {

        VkPipelineShaderStageCreateInfo.Buffer stages = VkPipelineShaderStageCreateInfo.malloc(shaderModules.length, stack);

        for (XeShaderModule module : shaderModules)
            stages.put(module.createInfo(stack));

        VkPipelineVertexInputStateCreateInfo vi = buildSafe(stack, vertexInputStateCI);
        VkPipelineInputAssemblyStateCreateInfo ia = buildSafe(stack, inputAssemblyStateCI);
        VkPipelineTessellationStateCreateInfo t = buildSafe(stack, tesselationStateCI);
        VkPipelineViewportStateCreateInfo v = buildSafe(stack, viewportStateCI);
        VkPipelineRasterizationStateCreateInfo r = buildSafe(stack, rasterizationStateCI);
        VkPipelineMultisampleStateCreateInfo m = buildSafe(stack, multisampleStateCI);
        VkPipelineDepthStencilStateCreateInfo ds = buildSafe(stack, depthStencilStateCI);
        VkPipelineColorBlendStateCreateInfo cb = buildSafe(stack, colorBlendStateCI);
        VkPipelineDynamicStateCreateInfo d = buildSafe(stack, dynamicStateCI);

        return custom(stack, flags, stages.rewind(), vi, ia, t, v, r, m, ds, cb, d,
                layout, renderpass, subpass, basePipelineHandle,basePipelineIndex);
    }


    /**
     * Not so useful macro for creating a VkGraphicsPipelineCreateInfo
     */
    public static VkGraphicsPipelineCreateInfo custom(
            MemoryStack stack,
            int flags,
            VkPipelineShaderStageCreateInfo.Buffer pStages,
            VkPipelineVertexInputStateCreateInfo pVertexInputState,
            VkPipelineInputAssemblyStateCreateInfo pInputAssemblyState,
            VkPipelineTessellationStateCreateInfo pTessellationState,
            VkPipelineViewportStateCreateInfo pViewportState,
            VkPipelineRasterizationStateCreateInfo pRasterizationState,
            VkPipelineMultisampleStateCreateInfo pMultisampleState,
            VkPipelineDepthStencilStateCreateInfo pDepthStencilState,
            VkPipelineColorBlendStateCreateInfo pColorBlendState,
            VkPipelineDynamicStateCreateInfo pDynamicState,
            long layout,
            long renderPass,
            int subpass,
            long basePipelineHandle,
            int basePipelineIndex) {
        return VkGraphicsPipelineCreateInfo.malloc(stack)
                .sType$Default()
                .pNext(0)
                .flags(flags)
                .pStages(pStages.rewind())
                .pVertexInputState(pVertexInputState)
                .pInputAssemblyState(pInputAssemblyState)
                .pTessellationState(pTessellationState)
                .pViewportState(pViewportState)
                .pRasterizationState(pRasterizationState)
                .pMultisampleState(pMultisampleState)
                .pDepthStencilState(pDepthStencilState)
                .pColorBlendState(pColorBlendState)
                .pDynamicState(pDynamicState)
                .layout(layout)
                .renderPass(renderPass)
                .subpass(subpass)
                .basePipelineHandle(basePipelineHandle)
                .basePipelineIndex(basePipelineIndex);
    }


    /*---------------- MACROS -------------------*/

    // vertex input

    /**
     * Vertex Input State
     * @param pNext pNext
     * @param pVertexBindingDescriptions the vertex bindings
     * @param pVertexAttributeDescriptions the vertex attributes
     * @return a new macro for {@link VkPipelineVertexInputStateCreateInfo}
     * @see VertexInputStateCI#build(MemoryStack)
     */
    public static VertexInputStateCI vis(
            long pNext,
            VertexInputBindingDescription[] pVertexBindingDescriptions,
            VertexInputAttributeDescription[] pVertexAttributeDescriptions) {
        return new VertexInputStateCI(pNext, pVertexBindingDescriptions, pVertexAttributeDescriptions);
    }

    /**
     * Java wrapper for {@link VkPipelineVertexInputStateCreateInfo}.
     * @param pNext
     * @param pVertexBindingDescriptions
     * @param pVertexAttributeDescriptions
     * @see VkPipelineVertexInputStateCreateInfo
     */
    public record VertexInputStateCI(
            long pNext,
            @Nullable VertexInputBindingDescription[] pVertexBindingDescriptions,
            @Nullable VertexInputAttributeDescription[] pVertexAttributeDescriptions)
            implements Builder<VkPipelineVertexInputStateCreateInfo> {

        @Override
        public VkPipelineVertexInputStateCreateInfo build(MemoryStack stack) {
            return VkPipelineVertexInputStateCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .flags(0)
                    .pVertexBindingDescriptions(
                            fromArray(stack, VkVertexInputBindingDescription::malloc, pVertexBindingDescriptions))
                    .pVertexAttributeDescriptions(
                            fromArray(stack, VkVertexInputAttributeDescription::malloc, pVertexAttributeDescriptions)
                    );
        }
    }

    /**
     * Vertex Input Binding Description
     * @param binding the vertex input binding
     * @param stride the stride until the next input
     * @param inputRate the input rate, either per vertex or per instance
     * @return a new macro for {@link VkVertexInputBindingDescription}
     */
    public static VertexInputBindingDescription vibd(int binding, int stride, int inputRate) {
        return new VertexInputBindingDescription(binding, stride, inputRate);
    }

    /**
     * Java wrapper for {@link VkVertexInputBindingDescription}
     * @param binding
     * @param stride
     * @param inputRate
     * @see VkVertexInputBindingDescription
     */
    public record VertexInputBindingDescription(int binding, int stride, int inputRate)
            implements Builder<VkVertexInputBindingDescription>{

        @Override
        public VkVertexInputBindingDescription build(MemoryStack stack) {
            return VkVertexInputBindingDescription.malloc(stack)
                    .binding(binding)
                    .stride(stride)
                    .inputRate(inputRate);
        }
    }

    /**
     * Vertex Input Attribute Description
     * @param location the location
     * @param binding the binding
     * @param format the format
     * @param offset the offset
     * @return a new macro for {@link VkVertexInputAttributeDescription}
     */
    public static VertexInputAttributeDescription viad(
            int location, int binding, int format, int offset) {
        return new VertexInputAttributeDescription(location, binding, format, offset);
    }

    /**
     * Java wrapper for {@link VkVertexInputAttributeDescription}
     * @param location
     * @param binding
     * @param format
     * @param offset
     * @see VkVertexInputAttributeDescription
     */
    public record VertexInputAttributeDescription(int location, int binding, int format, int offset)
            implements Builder<VkVertexInputAttributeDescription>{

        @Override
        public VkVertexInputAttributeDescription build(MemoryStack stack) {
            return VkVertexInputAttributeDescription.malloc(stack)
                    .location(location)
                    .binding(binding)
                    .format(format)
                    .offset(offset);
        }
    }



    // input assembly

    /**
     * Input Assembly State
     * @param topology the topology
     * @param primitiveRestart whether primitive restart should be enabled
     * @return a new macro for {@link VkPipelineInputAssemblyStateCreateInfo}
     * @see InputAssemblyStateCI#build(MemoryStack)
     */
    public static InputAssemblyStateCI ias(int topology, boolean primitiveRestart) {
        return new InputAssemblyStateCI(topology, primitiveRestart);
    }

    /**
     * Java wrapper for {@link VkPipelineInputAssemblyStateCreateInfo}
     * @param topology
     * @param primitiveRestart
     * @see VkPipelineInputAssemblyStateCreateInfo
     */
    public record InputAssemblyStateCI(int topology, boolean primitiveRestart)
            implements Builder<VkPipelineInputAssemblyStateCreateInfo>{

        @Override
        public VkPipelineInputAssemblyStateCreateInfo build(MemoryStack stack) {
            return VkPipelineInputAssemblyStateCreateInfo.calloc(stack)
                    .sType$Default()
                    .topology(topology)
                    .primitiveRestartEnable(primitiveRestart);
        }
    }


    // tesselation

    /**
     * Tessellation State
     * @param pNext pNext
     * @param patchControlPoints patchControlPoints
     * @return a new macro for VkPipelineTessellationStateCreateInfo
     * @see TesselationStateCI#build(MemoryStack)
     */
    public static TesselationStateCI ts(long pNext, int patchControlPoints) {
        return new TesselationStateCI(pNext, patchControlPoints);
    }

    /**
     * Java wrapper for {@link VkPipelineTessellationStateCreateInfo}
     * @param pNext
     * @param patchControlPoints
     * @see VkPipelineTessellationStateCreateInfo
     */
    public record TesselationStateCI(long pNext, int patchControlPoints)
            implements Builder<VkPipelineTessellationStateCreateInfo>{

        @Override
        public VkPipelineTessellationStateCreateInfo build(MemoryStack stack) {
            return VkPipelineTessellationStateCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .flags(0)
                    .patchControlPoints(patchControlPoints);
        }
    }



    // viewports & scissors

    /**
     * Viewport State Dynamic
     * Same as {@link #vps(long, XeViewport[], XeRect2D[])} but for dynamic
     * viewport & scissors.
     * @param pNext pNext
     * @param viewportCount viewport count
     * @param scissorCount scissor count
     * @return a new macro for {@link VkPipelineViewportStateCreateInfo}
     * @see #vps(long, XeViewport[], XeRect2D[])
     */
    public static ViewportStateCI vps_d(long pNext, int viewportCount, int scissorCount) {
        return new ViewportStateCI(pNext, null, viewportCount, null, scissorCount);
    }

    /**
     * Viewport State
     * @param pNext pNext
     * @param viewport the static viewport
     * @param scissors the static scissors
     * @return a new macro for {@link VkPipelineViewportStateCreateInfo}
     * @see #vps(long, XeViewport[], XeRect2D[])
     */
    public static ViewportStateCI vps(long pNext, XeViewport viewport, XeRect2D scissors) {
        return vps(pNext, arr(viewport), arr(scissors));
    }

    /**
     * Viewport State
     * @param pNext pNext
     * @param pViewports an array of viewports
     * @param pScissors an array of scissors
     * @return a new macro for {@link VkPipelineViewportStateCreateInfo}
     * @see ViewportStateCI#build(MemoryStack)
     */
    public static ViewportStateCI vps(
            long pNext,
            @Nullable XeViewport[] pViewports,
            @Nullable XeRect2D[] pScissors) {
        return new ViewportStateCI(pNext, pViewports,
                pViewports == null ? 0 : pViewports.length, pScissors,
                pScissors == null ? 0 : pScissors.length);
    }

    /**
     * Java wrapper for {@link VkPipelineViewportStateCreateInfo}
     * @param pNext
     * @param pViewports
     * @param viewportCount
     * @param pScissors
     * @param scissorCount
     * @see VkPipelineViewportStateCreateInfo
     */
    public record ViewportStateCI(long pNext,
                                  @Nullable XeViewport[] pViewports, int viewportCount,
                                  @Nullable XeRect2D[] pScissors, int scissorCount)
            implements Builder<VkPipelineViewportStateCreateInfo>{

        @Override
        public VkPipelineViewportStateCreateInfo build(MemoryStack stack) {
            return VkPipelineViewportStateCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .flags(0)
                    .pViewports(
                            fromArray(stack, VkViewport::malloc, pViewports)
                    )
                    .viewportCount(viewportCount)   // overwrite the pViewports count. Do not move this line!
                    .pScissors(
                            fromArray(stack, VkRect2D::malloc, pScissors)
                    )
                    .scissorCount(scissorCount);    // overwrite the pScissors count. Do not move this line!
        }
    }


    // rasterization

    /**
     * Rasterization State Usual
     * Same as {@link #rs(long, boolean, boolean, int, int, int, boolean, float, float, float, float)}
     * but with depth bias disabled and line width set to 1.0f.
     * @param pNext pNext
     * @param depthClamp whether clamping depth should be enabled
     * @param rasterizerDiscard whether the rasterizer should discard everything. keep it to true
     * @param polygonMode the polygon mode
     * @param cullMode the culling mode
     * @param frontFace define what's a front face
     * @return a new macro for {@link VkPipelineRasterizationStateCreateInfo}
     * @see #rs(long, boolean, boolean, int, int, int, boolean, float, float, float, float)
     */
    public static RasterizationStateCI rs_u(
            long pNext,
            boolean depthClamp,
            boolean rasterizerDiscard,
            int polygonMode,
            int cullMode,
            int frontFace) {
        return rs(pNext, depthClamp, rasterizerDiscard, polygonMode, cullMode, frontFace,
                false, 0, 0, 0, 1f);
    }

    /**
     * Rasterization State
     * @param pNext pNext
     * @param depthClamp whether clamping depth should be enabled
     * @param rasterizerDiscard whether the rasterizer should discard everything. keep it to true
     * @param polygonMode the polygon mode
     * @param cullMode the culling mode
     * @param frontFace define what's a front face
     * @param depthBias whether depth bias should be enabled
     * @param depthBiasConstantFactor depth bias constant factor
     * @param depthBiasClamp depth bias clamp
     * @param depthBiasSlopeFactor depth bias slope factor
     * @param lineWidth line width
     * @return a new macro for {@link VkPipelineRasterizationStateCreateInfo}
     * @see RasterizationStateCI#build(MemoryStack)
     */
    public static RasterizationStateCI rs(
            long pNext,
            boolean depthClamp,
            boolean rasterizerDiscard,
            int polygonMode,
            int cullMode,
            int frontFace,
            boolean depthBias,
            float depthBiasConstantFactor,
            float depthBiasClamp,
            float depthBiasSlopeFactor,
            float lineWidth) {
        return new RasterizationStateCI(pNext, depthClamp, rasterizerDiscard, polygonMode, cullMode,
                frontFace, depthBias, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor, lineWidth);
    }

    /**
     * Java wrapper for {@link VkPipelineRasterizationStateCreateInfo}
     * @param pNext
     * @param depthClamp
     * @param rasterizerDiscard
     * @param polygonMode
     * @param cullMode
     * @param frontFace
     * @param depthBias
     * @param depthBiasConstantFactor
     * @param depthBiasClamp
     * @param depthBiasSlopeFactor
     * @param lineWidth
     * @see VkPipelineRasterizationStateCreateInfo
     */
    public record RasterizationStateCI(
            long pNext,
            boolean depthClamp,
            boolean rasterizerDiscard,
            int polygonMode,
            int cullMode,
            int frontFace,
            boolean depthBias,
            float depthBiasConstantFactor,
            float depthBiasClamp,
            float depthBiasSlopeFactor,
            float lineWidth) implements Builder<VkPipelineRasterizationStateCreateInfo>{

        @Override
        public VkPipelineRasterizationStateCreateInfo build(MemoryStack stack) {
            return VkPipelineRasterizationStateCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .flags(0)
                    .depthClampEnable(depthClamp)
                    .rasterizerDiscardEnable(rasterizerDiscard)
                    .polygonMode(polygonMode)
                    .cullMode(cullMode)
                    .frontFace(frontFace)
                    .depthBiasEnable(depthBias)
                    .depthBiasConstantFactor(depthBiasConstantFactor)
                    .depthBiasClamp(depthBiasClamp)
                    .depthBiasSlopeFactor(depthBiasSlopeFactor)
                    .lineWidth(lineWidth);
        }
    }



    // multisample

    /**
     * Multisample State
     * @param pNext pNext
     * @param rasterizationSamples rasterization samples
     * @param sampleShading whether sample shading should be enabled
     * @param minSampleShading minimum sample shading
     * @param pSampleMask pSampleMask, possibly null
     * @param alphaToCoverage whether alphaToCoverage should be enabled
     * @param alphaToOne whether alphaToOne should be enabled
     * @return a new macro for {@link VkPipelineMultisampleStateCreateInfo}
     * @see MultisampleStateCI#build(MemoryStack)
     */
    public static MultisampleStateCI mss(
            long pNext,
            int rasterizationSamples,
            boolean sampleShading,
            float minSampleShading,
            @Nullable IntBuffer pSampleMask,
            boolean alphaToCoverage,
            boolean alphaToOne
    ) {
        return new MultisampleStateCI(
                pNext, rasterizationSamples, sampleShading, minSampleShading, pSampleMask, alphaToCoverage, alphaToOne);
    }

    /**
     * Java wrapper for {@link VkPipelineMultisampleStateCreateInfo}
     * @param pNext
     * @param rasterizationSamples
     * @param sampleShading
     * @param minSampleShading
     * @param pSampleMask
     * @param alphaToCoverage
     * @param alphaToOne
     * @see VkPipelineMultisampleStateCreateInfo
     */
    public record MultisampleStateCI(
            long pNext,
            int rasterizationSamples,
            boolean sampleShading,
            float minSampleShading,
            @Nullable IntBuffer pSampleMask,
            boolean alphaToCoverage,
            boolean alphaToOne) implements Builder<VkPipelineMultisampleStateCreateInfo>{

        @Override
        public VkPipelineMultisampleStateCreateInfo build(MemoryStack stack) {
            return VkPipelineMultisampleStateCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .flags(0)
                    .rasterizationSamples(rasterizationSamples)
                    .sampleShadingEnable(sampleShading)
                    .minSampleShading(minSampleShading)
                    .pSampleMask(pSampleMask)
                    .alphaToCoverageEnable(alphaToCoverage)
                    .alphaToOneEnable(alphaToOne);
        }
    }



    // depth stencil

    /**
     * Depth Stencil State
     * @param flags the flags
     * @param depthTest whether depth should be tested
     * @param depthWrite whether writes to the depth buffer should be enabled
     * @param depthCompareOp depth comparison operation
     * @param depthBoundsTest whether bounds should be tested during depth test
     * @param stencilTest whether stencil test should be enabled
     * @param front the front stencil operation state
     * @param back the back stencil operation state
     * @param minDepthBounds min depth bounds
     * @param maxDepthBounds max depth bounds
     * @return a new macro for {@link VkPipelineDepthStencilStateCreateInfo}
     * @see DepthStencilStateCI#build(MemoryStack)
     */
    public static DepthStencilStateCI dss(
            int flags,
            boolean depthTest,
            boolean depthWrite,
            int depthCompareOp,
            boolean depthBoundsTest,
            boolean stencilTest,
            VkStencilOpState front,
            VkStencilOpState back,
            float minDepthBounds,
            float maxDepthBounds
    ) {
        return new DepthStencilStateCI(flags, depthTest, depthWrite, depthCompareOp, depthBoundsTest,
                stencilTest, front, back, minDepthBounds, maxDepthBounds);
    }

    /**
     * Java wrapper for {@link VkPipelineDepthStencilStateCreateInfo}
     * @param flags
     * @param depthTest
     * @param depthWrite
     * @param depthCompareOp
     * @param depthBoundsTest
     * @param stencilTest
     * @param front
     * @param back
     * @param minDepthBounds
     * @param maxDepthBounds
     * @see VkPipelineDepthStencilStateCreateInfo
     */
    public record DepthStencilStateCI(
            int flags,
            boolean depthTest,
            boolean depthWrite,
            int depthCompareOp,
            boolean depthBoundsTest,
            boolean stencilTest,
            VkStencilOpState front,
            VkStencilOpState back,
            float minDepthBounds,
            float maxDepthBounds) implements Builder<VkPipelineDepthStencilStateCreateInfo>{

        @Override
        public VkPipelineDepthStencilStateCreateInfo build(MemoryStack stack) {
            return VkPipelineDepthStencilStateCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(0)
                    .flags(flags)
                    .depthTestEnable(depthTest)
                    .depthWriteEnable(depthWrite)
                    .depthCompareOp(depthCompareOp)
                    .depthBoundsTestEnable(depthBoundsTest)
                    .stencilTestEnable(stencilTest)
                    .front(front)
                    .back(back)
                    .minDepthBounds(minDepthBounds)
                    .maxDepthBounds(maxDepthBounds);
        }
    }



    // color blending

    /**
     * Color Blend State
     * @param pNext pNext
     * @param flags flags
     * @param logicOpEnable whether blend operation should be enabled
     * @param logicOp the blending logic operation
     * @param colorBlendAttachmentStates pAttachments
     * @param blendConstants the blend constants. Must be 4 of length
     * @return a new macro for {@link VkPipelineColorBlendStateCreateInfo}
     * @see ColorBlendStateCI#build(MemoryStack)
     */
    public static ColorBlendStateCI cbs(
            long pNext,
            int flags,
            boolean logicOpEnable,
            int logicOp,
            @Nullable ColorBlendAttachmentStateCI[] colorBlendAttachmentStates,
            float... blendConstants
    ) {
        if (blendConstants.length != 4)
            throw VkError.log("Invalid blend constants count");
        return new ColorBlendStateCI(pNext, flags, logicOpEnable, logicOp, colorBlendAttachmentStates, blendConstants);
    }

    /**
     * Java wrapper for {@link VkPipelineColorBlendStateCreateInfo}
     * @param pNext
     * @param flags
     * @param logicOpEnable
     * @param logicOp
     * @param colorBlendAttachmentStates
     * @param blendConstants
     * @see VkPipelineColorBlendStateCreateInfo
     */
    public record ColorBlendStateCI(
            long pNext,
            int flags,
            boolean logicOpEnable,
            int logicOp,
            @Nullable ColorBlendAttachmentStateCI[] colorBlendAttachmentStates,
            float... blendConstants) implements Builder<VkPipelineColorBlendStateCreateInfo>{

        @Override
        public VkPipelineColorBlendStateCreateInfo build(MemoryStack stack) {
            var ci = VkPipelineColorBlendStateCreateInfo.malloc(stack)
                    .sType$Default()
                    .pNext(pNext)
                    .flags(flags)
                    .logicOpEnable(logicOpEnable)
                    .logicOp(logicOp)
                    .pAttachments(
                            fromArray(stack, VkPipelineColorBlendAttachmentState::malloc, colorBlendAttachmentStates)
                    );

            for (int i = 0; i < blendConstants.length; i++)
                ci.blendConstants(i, blendConstants[i]);

            return ci;
        }
    }


    /**
     * Color Blend Attachment State Usual
     * Same as {@link #cbas(boolean, int, int, int, int, int, int, int)} with default parameters.
     * @return a new macro for {@link VkPipelineColorBlendAttachmentState}
     * @see #cbas(boolean, int, int, int, int, int, int, int)
     */
    public static ColorBlendAttachmentStateCI cbas_u() {
        return cbas(true, VK_BLEND_FACTOR_ONE, VK_BLEND_FACTOR_ZERO, VK_BLEND_OP_ADD,
                VK_BLEND_FACTOR_ONE, VK_BLEND_FACTOR_ZERO, VK_BLEND_OP_ADD, VK_COLOR_COMPONENT_R_BIT |
                        VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT);
    }

    /**
     * Color Blend Attachment State
     * @param blend whether blend should be enabled
     * @param srcColorBlendFactor src color blend factor
     * @param dstColorBlendFactor dst color blend factor
     * @param colorBlendOp color blend operation
     * @param srcAlphaBlendFactor src alpha blend factor
     * @param dstAlphaBlendFactor dst alpha blend factor
     * @param alphaBlendOp alpha blend operation
     * @param colorWriteMask color write mask
     * @return a new macro for {@link VkPipelineColorBlendAttachmentState}
     * @see ColorBlendAttachmentStateCI#build(MemoryStack)
     */
    public static ColorBlendAttachmentStateCI cbas(
            boolean blend,
            int srcColorBlendFactor,
            int dstColorBlendFactor,
            int colorBlendOp,
            int srcAlphaBlendFactor,
            int dstAlphaBlendFactor,
            int alphaBlendOp,
            int colorWriteMask
    ) {
        return new ColorBlendAttachmentStateCI(blend, srcColorBlendFactor, dstColorBlendFactor, colorBlendOp,
                srcAlphaBlendFactor, dstAlphaBlendFactor, alphaBlendOp, colorWriteMask);
    }

    /**
     * Java wrapper for {@link VkPipelineColorBlendAttachmentState}
     * @param blend
     * @param srcColorBlendFactor
     * @param dstColorBlendFactor
     * @param colorBlendOp
     * @param srcAlphaBlendFactor
     * @param dstAlphaBlendFactor
     * @param alphaBlendOp
     * @param colorWriteMask
     * @see VkPipelineColorBlendAttachmentState
     */
    public record ColorBlendAttachmentStateCI(
            boolean blend,
            int srcColorBlendFactor,
            int dstColorBlendFactor,
            int colorBlendOp,
            int srcAlphaBlendFactor,
            int dstAlphaBlendFactor,
            int alphaBlendOp,
            int colorWriteMask) implements Builder<VkPipelineColorBlendAttachmentState>{

        @Override
        public VkPipelineColorBlendAttachmentState build(MemoryStack stack) {
            return VkPipelineColorBlendAttachmentState.malloc(stack)
                    .blendEnable(blend)
                    .srcColorBlendFactor(srcColorBlendFactor)
                    .dstColorBlendFactor(dstColorBlendFactor)
                    .colorBlendOp(colorBlendOp)
                    .srcAlphaBlendFactor(srcAlphaBlendFactor)
                    .dstAlphaBlendFactor(dstAlphaBlendFactor)
                    .alphaBlendOp(alphaBlendOp)
                    .colorWriteMask(colorWriteMask);
        }

    }


    // dynamic state

    /**
     * Dynamic State
     * @param dynamicStates the requested dynamic states
     * @return a new macro for {@link VkPipelineDynamicStateCreateInfo}
     * @see DynamicStateCI#build(MemoryStack)
     */
    public static DynamicStateCI ds(int... dynamicStates) {
        return new DynamicStateCI(dynamicStates);
    }

    /**
     * Java wrapper for {@link VkPipelineDynamicStateCreateInfo}
     * @param dynamicStates
     * @see VkPipelineDynamicStateCreateInfo
     */
    public record DynamicStateCI(int... dynamicStates) implements Builder<VkPipelineDynamicStateCreateInfo>{

        @Override
        public VkPipelineDynamicStateCreateInfo build(MemoryStack stack) {
            return VkPipelineDynamicStateCreateInfo.calloc(stack)
                    .sType$Default()
                    .pDynamicStates(stack.ints(dynamicStates));
        }
    }

}
