#version 450
#pragma shader_stage(tesscontrol)

layout (vertices = 4) out;

#define inv gl_InvocationID

const vec2 positions[] = {
    vec2(-0.5, -0.5),
    vec2(-0.5, 0.5),
    vec2(0.5, 0.5),
    vec2(0.5, -0.5)
};

void main() {

    gl_TessLevelOuter[0] = 5.0;
    gl_TessLevelOuter[1] = 5.0;
    gl_TessLevelOuter[2] = 5.0;
    gl_TessLevelOuter[3] = 5.0;

    gl_TessLevelInner[0] = 5.0;
    gl_TessLevelInner[1] = 5.0;

    gl_out[inv].gl_Position = vec4(positions[inv] + gl_in[0].gl_Position.xy, 0.0, 1.0);
}
