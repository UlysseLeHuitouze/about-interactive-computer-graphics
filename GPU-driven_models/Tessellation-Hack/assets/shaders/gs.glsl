#version 450
#pragma shader_stage(geometry)

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

layout (location = 0) in TES_OUT {
    vec3 col;
    uint index;
} gs_in[];

layout (location = 0) out vec3 color;

bool allequal(uint u, uint v, uint w) {
    return u == v && u == w;
}

void main() {
    if (allequal(gs_in[0].index, gs_in[1].index, gs_in[2].index)) {
        for (uint i = 0; i < 3; i++) {
            gl_Position = gl_in[i].gl_Position;
            color = gs_in[i].col;
            EmitVertex();
        }
        //EndPrimitive();
    }
}
