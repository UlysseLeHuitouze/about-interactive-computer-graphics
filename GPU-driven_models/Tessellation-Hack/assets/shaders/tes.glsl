#version 450
#pragma shader_stage(tesseval)

layout (quads, equal_spacing, ccw) in;

layout (location = 0) out TES_OUT {
    vec3 col;
    uint index;
} tes_out;

vec4 interpolate(vec4 v0, vec4 v1, vec4 v2, vec4 v3) {
    vec4 a = mix(v0, v1, gl_TessCoord.x);
    vec4 b = mix(v3, v2, gl_TessCoord.x);
    return mix(a, b, gl_TessCoord.y);
}
 const vec3 colors[] = {
    vec3(0.5, 1.0, 0.0),
    vec3(1.0, 0.2, 0.3),
    vec3(0.9, 0.9, 0.1),
    vec3(0.0, 1.0, 0.0),
    vec3(0.4, 0.6, 0.2),
    vec3(0.0, 0.0, 1.0),
    vec3(0.0, 0.0, 0.0),
    vec3(1.0, 1.0, 0.0),
    vec3(0.0, 1.0, 1.0)
};


void main() {

    uint x = uint(round(gl_TessCoord.x * 5.0));
    uint y = uint(round(gl_TessCoord.y * 5.0));
    uint a = x >> 1;
    uint b = y >> 1;
    uint i = b + a * 3;
    tes_out.col = colors[i];
    tes_out.index = i;

    gl_Position = interpolate(gl_in[0].gl_Position,
                                gl_in[1].gl_Position,
                                gl_in[2].gl_Position,
                                gl_in[3].gl_Position);
}
