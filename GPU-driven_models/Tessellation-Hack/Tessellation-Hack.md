# Tessellation Hack

Here, we present the general idea on how to generate independent quads using GLSL tessellation shaders.

## About [quad tessellation](https://www.khronos.org/opengl/wiki/Tessellation#Quads)

The tessellation we will study typically looks like this:

<div align="center">

![5, 5 inner subdivision](https://www.khronos.org/opengl/wiki_opengl/images/QuadInnerOnlyQuadCorr.png)

</div>

<div align="center">

**Fig. 1.** The normalized square subdivided 5 times in both directions

</div>

<div align="center">

[Image source](https://www.khronos.org/opengl/wiki/Tessellation#Quads)

</div>

We will only use the inner tessellation levels, since playing with outer levels deforms the rectangles into generic quads which makes it harder to visualize what's going on.

That means, in practice, OL-1 = OL-3 = IL-0 and OL-0 = OL-2 = IL-1 will hold.

However, we won't be able to use the above diagram directly since we want independent quads, i.e. quads that don't share any vertex with each other: For instance, a cube model is made of 24 vertices, not 8. Thus, only quads that don't touch each other are eligible for our independent quad generation. The eligible quads for **Fig. 1** are shown **Fig. 2**.

<div align="center">

![Quads to render](./assets/tessellated_square_wanted.png)

</div>

<div align="center">

**Fig. 2.** The normalized square subdivided 5, 5 with eligible quads in red

</div>

Now, let's quickly find and prove the optimal pattern based from the above diagram for our quad tessellation of interest.

### Proof

**Goal:** Find optimal tessellation parameters for generating `N` independent quads.

#### 1st Statement

- "**Alg. A** is optimal for choosing eligible quads among one row of the tessellated normalized square."

```Haskell
Algorithm A(Row)
----------------
if IsNil(Row)
    return nil
else
    q <- ExtractHead(Row)
    if IsNil(Row)
        return {q}
    else
        ExtractHead(Row)
        return {q} ∪ A(Row)
    end if
end if
```

<div align="center">

**Alg. A**

</div>

Let `O'` be the optimal solution for choosing eligible quads among one row `R` of the tessellated normalized square.<br>
Let `O` be the solution by running **Alg. A** on `R`.

If `O'` is the empty set, since a row composed of one single quad makes that single quad eligible, it is obvious that `R` is, in that case, empty. If `R` is empty, **Alg. A** will also blatantly return the empty set. Therefore, when `R` is empty, both `O'` and `O` are empty. Also, if `R` isn't empty, since a row composed of at least one quad has at least one eligible quad, `O'` won't be empty. As for `O`, it is easy to show that it won't be empty either since it only returns `nil` if and only if `R` is empty.

Let `R` be non-empty.

`O'` is then of the form <code>{q'<sub>1</sub>,... q'<sub>n'</sub>}</code> and `O` is then of the form <code>{q<sub>1</sub>,... q<sub>n</sub>}</code>. Let `k` ∈ [1, n'] such that for all `i` > 0 and < `k`, <code>q'<sub>i</sub> = q<sub>i</sub></code>. Can <code>q'<sub>k</sub></code> be substituted with <ocde>q<sub>k</sub></code>?

Since **Alg. A** selects quads in a one-out-of-two fashion, selecting the very first quad, and since <code>q'<sub>k - 1</sub> = q<sub>k - 1</sub></code>, <code>q<sub>k</sub></code> is compatible with <code>q'<sub>k - 1</sub></code>. Because of that one-out-of-two behaviour, it is also impossible to find an eligible quad closer to <code>q'<sub>k - 1</sub></code> than <code>q<sub>k</sub></code> is. Therefore, <code>q<sub>k</sub></code> is at least as best (i.e. the closest possible to the previous quad) as <code>q'<sub>k</sub></code>, and therefore, <code>q<sub>k</sub></code> is also compatible with <code>q'<sub>k + 1</sub></code>.

`O'` can thus be substituted with `O`, and **Alg. A** is optimal.

- Results

What are the results of **Alg. A** in actuality?

It is easy to demonstrate that **Alg. A** returns `⌈N / 2⌉` eligible quads out of a row of `N` quads by proving formally **Alg. A** (not shown here).

#### 2nd Statement

- "Having `OL-0 = 1 and OL-1 = x, x >= m` is optimal"

We now have an optimal algorithm for choosing eligible quads out of one row. Here we won't give a formal proof of the promotion of **Alg. A** from one-dimensional to two-dimensional for simplicity sake, and we will settle for a simple analysis:

*Selecting a quad to become eligible makes all its neighbours, right, left, up, down, right-up, right-down, left-up, left-down, ineligible. Therefore, staying in one dimension instead of two dimensions allows to waste approximately 4 times less quads.*

From now on, we will use `OL-0 = 1` and will run **Alg. A** on the one and only generated row. Since **Alg. A** selects `⌈N / 2⌉` eligible quads out of `N` quads, it is optimal to create a row of `2x - 1` quads in order to have `x` eligible quads.

### How to filter eligible quads

We now know how to choose whether a quad should be eligible or not. But the question is, how do we discard the ineligible ones? If we don't discard them, we will face a problem similar to when trying to draw quads by using triangle strips, that is having wrong extra quads that hinder performance and can be visually wrong if a model isn't exactly cube-like (See Appendix).

To discard those ineligible quads, we have no choice but to use a geometry shader. The geometry shader won't do any heavy computation and will just let pass eligible triangles, discarding the others.

## Concrete GLSL

The following is some simplistic GLSL code about implementing what we discussed above. It depicts some general ideas about building quads on the tessellation stage for custom models, contrary to the JAVA sample provided, which only covers quads hardcoded in the shader.

```glsl
/**
 * Packed data that contains:
 *
 * - 1 uint for the model's ID
 * - 3 uint for the voxel position
 */
in uvec4 voxelData;

/**
 * The number of quads needed to be generated for the current model.
 */
out uint quadCount;

// The models data
readonly buffer Models {
    ...
} models;

void main()
{
    gl_Position = voxelData; // pass voxel data to next stage

    quadCount = EXTRACT_QUAD_COUNT_FROM_MODEL_ID(voxelData.x); // lookup into the buffer models
}
```

<div align="center">

**Snippet 1.** Vertex Shader called once every voxel

</div>

```glsl
// number of vertices of the primitive to tessellate.
// we tessellate the normalized square so 4 vertices.
// This instruction also defines the degree of parallelism of this shader:
// Here, 4 instances of this shader will be called for every vertex shader input (model).
layout (vertices = 4) out;

// an array of vertex shader's outputs.
// here, one vertex shader invocation equals one "patch" (one model generated),
// so patch size is 1 and this array always contains only 1 elements.
int uint quadCounts[];

// voxel data. It was packed in gl_Position in the vertex shader to minimize payload size,
// now it's unpacked to be passed to the tessellation evaluation shader.
// "patch" is to say it is global to the current patch, the current tessellation, that is the current model in our case.
patch out uvec4 voxelData;

void main()
{
    if (gl_InvocationID == 0) {
        voxelData = gl_in[0].gl_Position; // "gl_in[0]" for the same reason why there is only 1 element in quadCounts
        gl_TessLevelOuter[1] = gl_TessLevelOuter[3] = gl_TessLevelInner[0] = quadCounts[0] * 2 - 1;
        gl_TessLevelOuter[0] = gl_TessLevelOuter[2] = gl_TessLevelInner[1] = 1.0;
    }
}
```

<div align="center">

**Snippet 2.** Tessellation Control Shader, defines tessellation levels

</div>

```glsl
// tessellates quads, of equal_spacing (important for denormalizing tessellated vertex coordinates)
// of counter-clock-wise winding order (must match application culling parameters)
layout (quads, equal_spacing, ccw) in;

// voxel data again from tessellation control shader. This is per-patch and not per-vertex.
patch in uvec4 voxelData;

/**
 * The output of the tessellation evaluation shader
 */
out TessOut {
    vec2 uvs;
    uint textureID;
    uint quadID;
    ... // other info to pass to fragment shader
} tessOut;

// The models data
readonly buffer Models {
    ...
} models;

void main()
{
    uint modelID = voxelData.x;

    // denormalized coordinates to deduce quadID
    uint x = uint(round( gl_TessCoord.x * gl_TessLevelInner[0] ));
    // since we don't do any tessellation alongside y, gl_TessCoord.y should be either 0.0 or 1.0; there shouldn't be a need to round up.
    bool y = bool(gl_TessCoord.y);

    uint quadID = x / 2; // one quad out of two is eligible
    // computes vertex id within a rectangle (counter-clock-wise, starting from left-top corner) from a vertex's coordinates
    uint vertexID = (x & uint(1)) * 2 + (bool(x & uint(1)) ^^ y);

    // lookup into models buffer
    vec3 vertexPos = RETRIEVE_VERTEX_POS_FOR_A_MODEL_FROM_QUAD_ID_AND_VERTEX_ID(modelID, quadID, vertexID);
    gl_Position = VIEW_MATRIX * PROJ_MATRIX * vec4(vertexPos + voxelData.yzw, 1.0);

    tessOut.uvs = RETRIEVE_UVS_FOR_A_MODEL_FROM_QUAD_ID_AND_VERTEX_ID(modelID, quadID, vertexID);
    tessOut.textureID = RETRIEVE_TEX_ID_FOR_A_MODEL_FROM_QUAD_ID_AND_VERTEX_ID(modelID, quadID, vertexID);
    tessOut.quadID = quadID;
}
```

<div align="center">

**Snippet 3.** Tessellation Evaluation Shader called once every tessellated vertex

</div>

```glsl
// tessellation evaluation shader outputs triangles (which represent quads)
layout (triangles) in;
// geometry shader outputs triangles
layout (triangle_strip, max_vertices = 3) out;

in TessOut {
    vec2 uvs;
    uint textureID;
    uint quadID;
    ... // other info to pass to fragment shader
} tessOut[];

out GeoOut {
    vec2 uvs;
    uint textureID;
    ... // other info to pass to fragment shader
} geoOut;

bool all_equal(uint a, uint b, uint c)
{
    return a == b && a == c;
}

void main()
{
    if (all_equal(tessOut[0].quadID, tessOut[1].quadID, tessOut[2].quadID)) {
        for (int i = 0; i < 3; i++) {
            gl_Position = gl_in[i].gl_Position;
            geoOut.uvs = tessOut[i].uvs;
            geoOut.textureID = tessOut[i].textureID;
            ...
            EmitVertex();
        }
        EndPrimitive(); // seems to work without this line
    } // discard non-eligible triangles
}
```

<div align="center">

**Snippet 4.** Geometry Shader for filtering eligible triangles out

</div>

No fragment shader is shown since it has nothing to do with generating quads on the GPU. For a full working (modulo driver problems) example that features a simple hardcoded model of 9 quads rendered twice at different positions on the screen, see the JAVA sample in this folder.

## Appendix

<div align="center">

<table style="border-collapse: collapse;">
<tr>
<td align="center"><img src="./assets/gs_off.png" alt="GS Filtering off"></img></td>
<td align="center"><img src="./assets/gs_on.png" alt="GS Filtering on"></img></td>
</tr>
<td align="center"><b>Fig. 3.</b>&nbsp;Geometry Shader Filtering off</td>
<td align="center"><b>Fig. 4.</b>&nbsp;Geometry Shader Filtering on</td>
</table>

From the JAVA sample in this folder (the hardcoded model is drawn only once for this screenshot, whereas in `Main.java` it is drawn twice).

</div>
